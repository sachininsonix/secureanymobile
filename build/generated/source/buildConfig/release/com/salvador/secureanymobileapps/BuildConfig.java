/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.salvador.secureanymobileapps;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.salvador.secureanymobileapps";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 14;
  public static final String VERSION_NAME = "2.3";
}
