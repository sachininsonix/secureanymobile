package com.salvador.secureanymobileapps.sms;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.gms.analytics.HitBuilders;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.ChatMainActivity;
import com.salvador.secureanymobileapps.chat.Getter_Setter2;
import com.salvador.secureanymobileapps.chat.Getter_messg1;
import com.salvador.secureanymobileapps.chat.LoginActivity;
import com.salvador.secureanymobileapps.documents.SimmpleApp;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by KPC on 04/08/2014.
 */
public class SmsSentActivity extends BaseActivity implements View.OnClickListener {
    private ListView listViewSMS;
    private Context context;
    private Button menu;
    private Button back;
    private SharedPreferences isTextNull;

    Cursor cursor;
    static Uri uriInbox,uriInbox1;
    My_Adapter my_adapter;
    static List<Getter_messg1> messages = new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sms_sent);
        ApplicationClass.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("SmsSent Activity")
                .build());
        this.listViewSMS = (ListView) findViewById(R.id.inboxList);
        this.context = this;
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
        this.menu = (Button) findViewById(R.id.menuSentMessage);
        this.menu.setOnClickListener(this);
        this.registerForContextMenu(this.menu);


        uriInbox = Uri.parse("content://sms/sent");

        cursor = getContentResolver().query(uriInbox, null, null, null, "date ASC");


        for (boolean hasData = cursor.moveToLast(); hasData; hasData = cursor.moveToPrevious()) {

            Getter_messg1 getter_messg1 = new Getter_messg1();
            getter_messg1.setAddress(cursor.getString(cursor.getColumnIndexOrThrow("address")).toString());
            getter_messg1.setBody(cursor.getString(cursor.getColumnIndexOrThrow("body")).toString());
            String TIMESTAMP1 =cursor.getString(cursor.getColumnIndexOrThrow("date")).toString();
            Date date = new Date(Long.parseLong(TIMESTAMP1));
            SimpleDateFormat SDF = new SimpleDateFormat("dd-MMM-yyyy");
            getter_messg1.setDate(SDF.format(date));

            messages.add(getter_messg1);
        }


        hasDuplicates(messages);
        cursor.close();
        my_adapter = new My_Adapter(SmsSentActivity.this, messages);
        if (Build.VERSION.SDK_INT <= 22) {
//            Collections.reverse(messages);
        }else{

        }
        listViewSMS.setAdapter(my_adapter);
        listViewSMS.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Getter_messg1 msg = (Getter_messg1) adapterView.getAdapter().getItem(position);
                String address1 = getContactName(SmsSentActivity.this, msg.getAddress());
                String address = msg.getAddress();
                Intent intent = new Intent(SmsSentActivity.this, Msg_data.class);
                Log.d("addd","addd"+address1);
                intent.putExtra("address", address);
                intent.putExtra("address1", address1);
                intent.putExtra("msgsent","sentmessage");
                startActivity(intent);
                finish();
                String Phone_number = address;
                String sms = "address='" + Phone_number + "'";
                Log.e("SMSMSMS", sms);

                Msg_data.messages_Data.clear();

                uriInbox1 = Uri.parse("content://sms/sent");
                Cursor cursor1 = getContentResolver().query(uriInbox1, null, sms, null, null);


                for (boolean hasData1 = cursor1.moveToFirst(); hasData1; hasData1 = cursor1
                        .moveToNext()) {


                    Getter_Setter2 getter_setter2 = new Getter_Setter2();
                    getter_setter2.setBody(cursor1.getString(cursor1.getColumnIndexOrThrow("body")).toString());
//                    String TIMESTAMP1=getter_setter2.setDate(cursor1.getString(cursor1.getColumnIndexOrThrow("date")));
                    String TIMESTAMP1 = cursor1.getString(cursor1.getColumnIndexOrThrow("date"));
                    Date date = new Date(Long.parseLong(TIMESTAMP1));
                    SimpleDateFormat SDF = new SimpleDateFormat("dd-MMM-yyyy");
                    getter_setter2.setDate(SDF.format(date));
                    Msg_data.messages_Data.add(getter_setter2);

                }


                cursor1.close();
            }
        });

    }

    @Override
    public void onResume() {

        super.onResume();


        if(isFinishing()) {
            Intent intent = new Intent(SmsSentActivity.this, ChatMainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent=new Intent(SmsSentActivity.this,MsgList.class);
        startActivity(intent);
//        finish();
    }

    @Override
    public void onClick(View view) {
        String tag = view.getTag() + "";
        if (tag.equals("back")) {
            this.onBackPressed();
        } else if (tag.equals("menu")) {
            this.openContextMenu(view);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        int view_id = v.getId();
        menu.setHeaderTitle("Options");
        menu.add(0, view_id, 0, "Compose New");
        menu.add(1, view_id, 0, "Inbox");
        view_id = 0;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int itemId = item.getGroupId();
        Intent intent;
        if (itemId == 0) {
            intent = new Intent(SmsSentActivity.this, SmsSendActivity.class);
            startActivity(intent);
        } else if (itemId == 1) {
            intent = new Intent(SmsSentActivity.this, MsgList.class);
            startActivity(intent);
        }
        return true;
    }

    public void hasDuplicates(List<Getter_messg1> p_cars) {
        final List<String> usedNames = new ArrayList<String>();
        Iterator<Getter_messg1> it = p_cars.iterator();
        while (it.hasNext()) {
            Getter_messg1 car = it.next();
            final String name = car.getAddress();

            if (usedNames.contains(name)) {
                it.remove();

            } else {
                usedNames.add(name);
            }
        }

    }
    public static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if(cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if(cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        if(contactName == null)
        {
            return phoneNumber;
        }

        return contactName;
    }

}
