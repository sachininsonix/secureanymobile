package com.salvador.secureanymobileapps.sms;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.hardware.display.DisplayManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.salvador.secureanymobileapps.BuildConfig;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.ChatMainActivity;
import com.salvador.secureanymobileapps.chat.Getter_Setter2;
import com.salvador.secureanymobileapps.chat.Getter_messg1;
import com.salvador.secureanymobileapps.chat.LoginActivity;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainMenu;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by insonix on 7/4/16.
 */
public class MsgList extends BaseActivity implements View.OnClickListener {
    ListView list_item;
    Cursor cursor;
    static Uri uriInbox,uriInbox1;
    String num, address1;
    private Button back;
    private Button optionButton;
    My_Adapter my_adapter;

    static List<Getter_messg1> messages = new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_inbox);
        list_item=(ListView)findViewById(R.id.inboxList);
        this.optionButton = (Button) findViewById(R.id.optionButton);
        this.optionButton.setOnClickListener(this);
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MsgList.this, MainMenu.class);
                startActivity(intent);
                finish();
            }
        });
        uriInbox = Uri.parse("content://sms/inbox");

        cursor = getContentResolver().query(uriInbox, null, null, null, "date DESC");


        for (boolean hasData = cursor.moveToFirst(); hasData; hasData = cursor.moveToNext()) {

            Getter_messg1 getter_messg1 = new Getter_messg1();
            getter_messg1.setAddress(cursor.getString(cursor.getColumnIndexOrThrow("address")).toString());
            getter_messg1.setBody(cursor.getString(cursor.getColumnIndexOrThrow("body")).toString());



            String TIMESTAMP1 =cursor.getString(cursor.getColumnIndexOrThrow("date")).toString();

            Date date = new Date(Long.parseLong(TIMESTAMP1));
            SimpleDateFormat SDF = new SimpleDateFormat("dd-MMM-yyyy");
            getter_messg1.setDate(SDF.format(date));


            messages.add(getter_messg1);
        }


        hasDuplicates(messages);
        cursor.close();

         my_adapter = new My_Adapter(MsgList.this, messages);
        if (Build.VERSION.SDK_INT >= 23) {
//            Collections.reverse(messages);
            list_item.setAdapter(my_adapter);
        }
        else{
            list_item.setAdapter(my_adapter);
        }

        //  removeDuplicates(arrayList);


       list_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
               Getter_messg1 msg  = (Getter_messg1) adapterView.getAdapter().getItem(position);
               String address1 = getContactName(MsgList.this, msg.getAddress());
               String address =  msg.getAddress();
               Log.d("addd","addd"+address1);
               Intent intent = new Intent(MsgList.this,Msg_data.class);
               intent.putExtra("address", address);
               intent.putExtra("address1", address1);
               intent.putExtra("msgsent","inboxmessage");
               startActivity(intent);
               finish();
               String Phone_number = address;
               String sms = "address='"+ Phone_number + "'";
               Log.e("SMSMSMS", sms);
               Msg_data.messages_Data.clear();
               uriInbox1 = Uri.parse("content://sms/inbox");
               Cursor cursor1 = getContentResolver().query(uriInbox1, null, sms, null, null);

               for (boolean hasData1 = cursor1.moveToLast(); hasData1 ; hasData1 = cursor1
                       .moveToPrevious()) {
                   Getter_Setter2 getter_setter2 = new Getter_Setter2();
                   getter_setter2.setBody(cursor1.getString(cursor1.getColumnIndexOrThrow("body")).toString());
                   String TIMESTAMP = cursor1.getString(cursor1.getColumnIndexOrThrow("date"));
                   Date date = new Date(Long.parseLong(TIMESTAMP));
                   SimpleDateFormat SDF = new SimpleDateFormat("dd-MMM-yyyy");
                   getter_setter2.setDate(SDF.format(date));
                   Msg_data.messages_Data.add(getter_setter2);
               }
               cursor1.close();
           }
       });
    }
    @Override
    public void onClick(View v) {
        String tag = v.getTag() + "";
        Intent intent = null;
        if (tag.equals("options")) {
            openOptionsMenu();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.inbox_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent ;

        if (item.getItemId() == R.id.compose_new) {
            intent = new Intent(MsgList.this, SmsSendActivity.class);
            intent.putExtra("phone_number","");
            startActivity(intent);
            finish();

        } else if (item.getItemId()== R.id.sent_box) {
            if (BuildConfig.DEBUG)
            {
                System.gc();
            }
            intent = new Intent(MsgList.this, SmsSentActivity.class);
            startActivity(intent);
            finish();
        }

        return (super.onOptionsItemSelected(item));
    }

    public void hasDuplicates(List<Getter_messg1> p_cars) {
        final List<String> usedNames = new ArrayList<String>();
        Iterator<Getter_messg1> it = p_cars.iterator();
        while (it.hasNext()) {
            Getter_messg1 car = it.next();
            final String name = car.getAddress();

            if (usedNames.contains(name)) {
                it.remove();

            } else {
                usedNames.add(name);
            }
        }

    }
    @Override
    public void onResume() {

        super.onResume();


        if(isFinishing()||!isScreenOn(MsgList.this)) {
            Intent intent = new Intent(MsgList.this, ChatMainActivity.class);
            startActivity(intent);
            finish();
        }

    }
    public boolean isScreenOn(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            DisplayManager dm = (DisplayManager) context.getSystemService(Context.DISPLAY_SERVICE);
            boolean screenOn = false;
            for (Display display : dm.getDisplays()) {
                if (display.getState() != Display.STATE_OFF) {
                    screenOn = true;
                }
            }
            return screenOn;
        } else {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            //noinspection deprecation
            return pm.isScreenOn();
        }
    }
    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    if(isScreenOn(MsgList.this)){
                        Log.d("screen is on","screen on");
                    }else{
                        Log.d("screen is off","screen off");
                    }
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(MsgList.this, MainMenu.class);
        startActivity(intent);
        finish();
    }
    public static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if(cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if(cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        if(contactName == null)
        {
            return phoneNumber;
        }

        return contactName;
    }

}
