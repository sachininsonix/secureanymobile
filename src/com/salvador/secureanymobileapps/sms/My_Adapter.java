package com.salvador.secureanymobileapps.sms;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.Getter_messg1;

import java.util.List;

/**
 * Created by karundhawan on 07-04-2016.
 */
public class My_Adapter extends BaseAdapter {

    List<Getter_messg1> messages;
    LayoutInflater layoutInflater;
    Context context;
    String phone_num;

    public My_Adapter (Activity context,List<Getter_messg1> messages){

        this.messages = messages;
        this.context = context;
        layoutInflater = layoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = layoutInflater.inflate(R.layout.message_view_new, null);
        TextView txt_numb = (TextView)convertView.findViewById(R.id.txt_numb);
        TextView txt_msg = (TextView)convertView.findViewById(R.id.txt_msg);
        TextView txt_date = (TextView)convertView.findViewById(R.id.txt_date);


        Getter_messg1 getter_messg1 = messages.get(position);

        phone_num=getter_messg1.getAddress();
//          if(getter_messg1.getAddress().equals("")){
//              txt_numb.setText(getter_messg1.getNumadress());
//          }else{
              txt_numb.setText(getContactName(context,getter_messg1.getAddress()));
//          }
        txt_date.setText(getter_messg1.getDate());
        txt_msg.setText(getter_messg1.getBody());



        return convertView;
    }
    public String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        if(contactName==null){
            return phone_num;
        }
        return contactName;
    }
}
