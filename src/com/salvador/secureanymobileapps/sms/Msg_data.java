package com.salvador.secureanymobileapps.sms;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.ChatMainActivity;
import com.salvador.secureanymobileapps.chat.Getter_Setter2;
import com.salvador.secureanymobileapps.chat.LoginActivity;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.sms.util.Encryption;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainMenu;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by admin on 07-04-2016.
 */
public class Msg_data extends Activity implements Iconstant {
    ListView list;
   // ArrayList hashMap;
   private Button back,btn_response;
    My_Adapter2 my_adapter2;
    String url,key1,address,address1,addresscon,messagesentcheck;
    ProgressDialog progressDialog;
    private SharedPreferences isTextNull;

    static List<Getter_Setter2> messages_Data = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.msg_data);
        list=(ListView)findViewById(R.id.list);

        this.back = (Button) findViewById(R.id.back_button);
        progressDialog=new ProgressDialog(Msg_data.this);
        btn_response = (Button) findViewById(R.id.btn_response);

//
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);

try{
address=getIntent().getExtras().getString("address");
addresscon=getIntent().getExtras().getString("address1");
    messagesentcheck=getIntent().getExtras().getString("msgsent");
    address1=address.replace("+91","");
    Log.d("qqqqq", "qqqq" + address1);

}catch (Exception e){
 e.printStackTrace();
}
        btn_response.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Msg_data.this, SmsSendActivity.class);
                intent.putExtra("MSGBOXTYPE", "TO:");

                    intent.putExtra("phone_number", address1);

                    intent.putExtra("addresscon", addresscon);


                startActivity(intent);
                finish();
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(messagesentcheck.equals("inboxmessage")) {
                    Intent intent = new Intent(Msg_data.this, MsgList.class);

                    startActivity(intent);
                    finish();
                }else{
                    Intent intent = new Intent(Msg_data.this, SmsSentActivity.class);

                    startActivity(intent);
                    finish();
                }
            }
        });
        my_adapter2= new My_Adapter2(Msg_data.this,messages_Data);
        if (messagesentcheck.equals("sentmessage")) {

        }
        else {
            Collections.reverse(messages_Data);
        }
//        list.setSelection(my_adapter2.getCount()-1);
        list.setAdapter(my_adapter2);
        Getkey();

    }

    public class My_Adapter2 extends BaseAdapter {

        List<Getter_Setter2> messages_Data;
        Context context;
        LayoutInflater layoutInflater;

        public My_Adapter2(Activity context, List<Getter_Setter2> messages_Data) {

            this.context = context;
            this.messages_Data = messages_Data;
            layoutInflater = LayoutInflater.from(context);

        }

        @Override
        public int getCount() {
            return messages_Data.size();
        }

        @Override
        public Object getItem(int position) {
            return messages_Data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = layoutInflater.inflate(R.layout.read_sms, null);
            final Getter_Setter2 getter_setter2 = messages_Data.get(position);
            final Button btn_decr = (Button) convertView.findViewById(R.id.btn_decr);
           final TextView textView_sms = (TextView) convertView.findViewById(R.id.txt_sms);
            TextView txt_date = (TextView) convertView.findViewById(R.id.txt_date);
            btn_decr.setTag(position);
            String stringToBeChecked =getter_setter2.getBody() ;

            Log.d("stringToBeChecked","stringToBeChecked"+stringToBeChecked);
//            boolean isBase64 = Base64.isArrayByteBase64(stringToBeChecked.getBytes());
            Log.d("stringToBeChecked","stringToBeChecked"+stringToBeChecked.getBytes());
            btn_decr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (decript_sms(getter_setter2.getBody()) != null) {
                        textView_sms.setText(decript_sms(getter_setter2.getBody()));
                        btn_decr.setVisibility(View.GONE);
                    } else {

                        textView_sms.setText(getter_setter2.getBody());
                    }
                }
            });

            if (stringToBeChecked.endsWith("=\n")||stringToBeChecked.endsWith("==\n")||stringToBeChecked.endsWith("==")||stringToBeChecked.endsWith("=") ) {
                textView_sms.setText(getter_setter2.getBody());
                btn_decr.setVisibility(View.VISIBLE);

            } else {
                btn_decr.setVisibility(View.GONE);
                textView_sms.setText(getter_setter2.getBody());
            }
//            textView_sms.setText(getter_setter2.getBody());
            txt_date.setText(getter_setter2.getDate());


            return convertView;
        }

        private String decript_sms(String smsEncript) {
            String encrypted = smsEncript.toString();
            Encryption encryption = new Encryption();
            String decrypted = encryption.decrypt(key1, encrypted);

            return decrypted;
        }
    }
    public void Getkey() {
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url = BaseUrl+"get_keybyid.php?from_id=" + isTextNull.getString("uid", "")+"&phone="+address1+"&user_number="+isTextNull.getString("phone_number","");
        Log.d("url1111:", "url" + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try {
                    String Status = jsonObject.getString("status");
                    if (Status.equals("true")) {
                        String from_id = jsonObject.getString("from_id");
                        key1 = jsonObject.getString("key");
                        Log.d("key1", "key1" + key1);//
                        String phone = jsonObject.getString("phone");
                    } else if (Status.equals("false")) {

                    }
                } catch (Exception e) {
                    Log.d("eeeee:", "eeeee" + e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
                progressDialog.dismiss();
            }


        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(messagesentcheck.equals("inboxmessage")) {
            Intent intent = new Intent(Msg_data.this, MsgList.class);

            startActivity(intent);
            finish();
        }else{
            Intent intent = new Intent(Msg_data.this, SmsSentActivity.class);

            startActivity(intent);
            finish();
        }
    }
    @Override
    public void onResume() {

        super.onResume();


        if(isFinishing()) {
            Intent intent = new Intent(Msg_data.this, ChatMainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }
}