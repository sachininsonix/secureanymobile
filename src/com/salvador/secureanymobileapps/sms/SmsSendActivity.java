package com.salvador.secureanymobileapps.sms;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.ChatMainActivity;
import com.salvador.secureanymobileapps.chat.ChatUserlist;
import com.salvador.secureanymobileapps.chat.LoginActivity;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.documents.SimmpleApp;
import com.salvador.secureanymobileapps.sms.util.Encryption;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;

import org.json.JSONObject;

/**
 * Created by KPC on 25/07/2014.
 */
public class SmsSendActivity extends BaseActivity implements View.OnClickListener, Iconstant {

    private static final String TAG = SmsSendActivity.class.getSimpleName();
    private static final int REQUEST_CODE_PICK_CONTACTS = 1;
    private Uri uriContact;
    private String contactID, ContName;     // contacts unique ID
    private String fn, msg, url, url1, key1, phoneNo, key2 = "", number;
    private Button sendBtn;
    private ImageView ibSelectContact;
    private Button menu;
    private Button back;
    SharedPreferences.Editor editor;
    Dialog imageDialog;
    static int w = 0, h = 0;
    private EditText txtphoneNo;
    private EditText txtMessage;
    private SharedPreferences isTextNull;
    ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_send);
        try {
            ApplicationClass.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                    .setLabel("SmsSend Activity")
                    .build());
        } catch (Exception e) {

        }
        // btn  of  View  send  message
        this.sendBtn = (Button) findViewById(R.id.btnSendSMS);
        this.sendBtn.setOnClickListener(this);
        this.ibSelectContact = (ImageView) findViewById(R.id.ibContactsSelect);
        this.ibSelectContact.setOnClickListener(this);
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
        this.menu = (Button) findViewById(R.id.menuSendMessage);
        this.menu.setOnClickListener(this);
        this.registerForContextMenu(this.menu);
        /// textInput  view  send  message
        this.txtphoneNo = (EditText) findViewById(R.id.editTextPhoneNo);
        this.txtMessage = (EditText) findViewById(R.id.editTextSMS);
        Bundle extras = getIntent().getExtras();
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        editor = isTextNull.edit();
        progressDialog = new ProgressDialog(SmsSendActivity.this);
        Display display = getWindowManager().getDefaultDisplay();
        w = display.getWidth();
        h = display.getHeight();
        TelephonyManager tMgr = (TelephonyManager) SmsSendActivity.this.getSystemService(Context.TELEPHONY_SERVICE);

        number = tMgr.getLine1Number();
        Log.d("number", "number" + number);
        try {
            phoneNo = getIntent().getExtras().getString("phone_number");
            ContName = getIntent().getExtras().getString("addresscon");
//            address1=ContName.replace("+91", "");
            Log.d("ContName", "ContName" + ContName);
            Log.d("phoneNo", "phoneNo" + phoneNo);
            if (ContName == null) {
                txtphoneNo.setText(phoneNo);
            } else {
                txtphoneNo.setText(ContName);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(SmsSendActivity.this, MsgList.class);

        startActivity(intent);
        finish();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onClick(View view) {

        String tag = view.getTag() + "";
        if (tag.equals("back")) {
            onBackPressed();
        } else if (tag.equals("ibcontacsSelect")) {
            hideSoftKeyboard(SmsSendActivity.this);
            Intent intent = new Intent(SmsSendActivity.this, ChatUserlist.class);
            intent.putExtra("smssen", "smsact");
            startActivity(intent);
            finish();
        } else if (tag.equals("sendmessages")) {
            fn = txtphoneNo.getText().toString().trim();
            msg = txtMessage.getText().toString().trim();
            if (fn.length() == 0) {
                txtphoneNo.setError("can't be Empty");
                return;
            }
            if (msg.length() == 0) {
                txtphoneNo.setError(null);
                txtMessage.setError("can't be Empty");
                return;
            }
            if (msg.length() > 0) {
                txtMessage.setError(null);
            }
            if (key2.equalsIgnoreCase("")) {
                sendDirectSMSMessage();
            } else {
                Getkey();
            }
        } else if (tag.equals("menu")) {
            this.openContextMenu(view);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        int view_id = v.getId();
        menu.setHeaderTitle("Options");
        menu.add(0, view_id, 0, "Inbox");
        menu.add(1, view_id, 0, "Sent Message");
        view_id = 0;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int itemId = item.getGroupId();
        Intent intent;
        if (itemId == 0) {
            intent = new Intent(SmsSendActivity.this, MsgList.class);
            startActivity(intent);
            finish();
        } else if (itemId == 1) {
            intent = new Intent(SmsSendActivity.this, SmsSentActivity.class);
            startActivity(intent);
            finish();
        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case (1001):
                if (data != null) {
                    uriContact = data.getData();
                } else {
                    Toast.makeText(getApplicationContext(), "Not data Validate",
                            Toast.LENGTH_LONG).show();
                    break;
                }
                // cursor  one  for get name  and id contact
                String _ID = ContactsContract.Contacts._ID;
                String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
                String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;


                Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
                String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

//                Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
//                String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
//                String DATA = ContactsContract.CommonDataKinds.Email.DATA;


                String contact_id = null;
                String name = null;
                String phoneNumber = null;


                // this method  get  name and  phone  name
                Cursor cursor = getContentResolver().query(uriContact, null, null, null, null);
                if (cursor.getCount() > 0) {
                    while (cursor.moveToNext()) {
                        contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                        name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                        int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
                        //get  number  Phone
                        if (hasPhoneNumber > 0) {
                            Cursor phoneCursor = getContentResolver().query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);
                            while (phoneCursor.moveToNext()) {
                                phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                            }
                            phoneCursor.close();
                        }
                        // get ID contact
                        if (contact_id.length() > 0) {
                            Cursor cursorID = getContentResolver().query(uriContact, new String[]{ContactsContract.Contacts._ID}, null, null, null);
                            while (cursorID.moveToNext()) {
                                contact_id = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
                            }
                            cursorID.close();
                        }
                    }
                }

                if (phoneNumber != null) {
                    txtphoneNo.setText(phoneNumber);
                }

                Log.d(TAG, "Contact Name Contact : " + name);
                Log.d(TAG, "Contact Phone Number: " + phoneNumber);
                Log.d(TAG, "Contact Id contact: " + contact_id);
        }

    }

    @Override
    public void onResume() {

        super.onResume();


        if (isFinishing()) {
            Intent intent = new Intent(SmsSendActivity.this, ChatMainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }

    public void Getkey() {
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            if (phoneNo.equals("null") || phoneNo.equals("")) {
                url = BaseUrl + "get_keybyid.php?from_id=" + isTextNull.getString("uid", "") + "&phone=" + txtphoneNo.getText().toString() + "&user_number=" + isTextNull.getString("phone_number", "");
                Log.d("url1111:", "url" + url);
            } else {
                url = BaseUrl + "get_keybyid.php?from_id=" + isTextNull.getString("uid", "") + "&phone=" + phoneNo + "&user_number=" + isTextNull.getString("phone_number", "");

                Log.d("urll:", "urlfff" + url);
            }
        } catch (Exception e) {

        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
//                phoneNo = txtphoneNo.getText().toString();
                String message = txtMessage.getText().toString();
                Log.d("messss", "messs" + message);
                try {
                    String Status = jsonObject.getString("status");
                    if (Status.equals("true")) {
                        String from_id = jsonObject.getString("from_id");
                        key1 = jsonObject.getString("key");
                        key2 = key1.substring(0, 16);

                        Log.d("key2", "key2" + key2 + "" + key1);
//
                        String phone = jsonObject.getString("phone");
                        sendSMSMessage();
//
                    } else if (Status.equals("false")) {
                        new com.salvador.secureanymobileapps.chat.ShowMsg().createDialogSignUp(SmsSendActivity.this, "Before start chat you need to first share a key, GoTo-> Settings->Profile-> click on Share icon to share key.");

                    } else {
                        Toast.makeText(SmsSendActivity.this, "Number not registered, Before start chat you need to first share a key, GoTo-> Settings->Profile-> click on Share icon to share key", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.d("eeeee:", "eeeee" + e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
                progressDialog.dismiss();
            }


        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

    protected void sendSMSMessage() {
        Log.i("Send SMS", "");
        String phone = phoneNo;
        String message = txtMessage.getText().toString();
        Encryption encryption = new Encryption();
        String encrypted = encryption.encrypt(key2.getBytes(), message.getBytes());
        Log.d("encrypted", "encrypted" + encrypted);
        try {
            SmsManager smsManager = SmsManager.getDefault();
            if (phoneNo.equals("null") || phoneNo.equals("")) {
                smsManager.sendTextMessage(txtphoneNo.getText().toString(), null, encrypted, null, null);
            } else {
                smsManager.sendTextMessage(phoneNo, null, encrypted, null, null);
            }

            Toast.makeText(getApplicationContext(), "SMS sent.",
                    Toast.LENGTH_LONG).show();
            Intent intent = new Intent(SmsSendActivity.this, MsgList.class);

            startActivity(intent);
            finish();
        } catch (Exception e) {

            Log.e("debug  app datos ", e + "");
            Toast.makeText(getApplicationContext(),
                    "SMS failed, please try again.",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }


    }


    /**
     * Direct message
     */
    protected void sendDirectSMSMessage() {
        Log.i("Send SMS", "");
        if(phoneNo.equalsIgnoreCase("")){
            phoneNo=txtphoneNo.getText().toString();
        }
        String phone = phoneNo;
        String message = txtMessage.getText().toString();
        SmsManager smsManager = SmsManager.getDefault();
        if (phoneNo.equals("null") || phoneNo.equals("")) {
            Toast.makeText(SmsSendActivity.this, "Phone number can't be empty", Toast.LENGTH_LONG).show();
//                smsManager.sendTextMessage(txtphoneNo.getText().toString(), null, message, null, null);
        } else if(phoneNo.length()<10){
            Toast.makeText(SmsSendActivity.this, "Phone number can't be less than 10 digits", Toast.LENGTH_LONG).show();
        }else {
            try {
                smsManager.sendTextMessage(phoneNo, null, message, null, null);
                Toast.makeText(getApplicationContext(), "SMS sent.",
                        Toast.LENGTH_LONG).show();
                Intent intent = new Intent(SmsSendActivity.this, MsgList.class);

                startActivity(intent);
                finish();
            } catch (Exception e) {

                Log.e("debug  app datos ", e + "");
                Toast.makeText(getApplicationContext(),
                        "SMS failed, please try again.",
                        Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }


    }

}
