package com.salvador.secureanymobileapps.sms;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.sms.util.Encryption;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;

import org.json.JSONObject;

/**
 * Created by KPC on 05/08/2014.
 */
public class SmsInboxView extends BaseActivity implements View.OnClickListener,Iconstant {

    private TextView tvSenderType;
    private TextView tvFrom;
    private TextView tvBody;
    ProgressDialog progressDialog;
    private SharedPreferences isTextNull;
    private Button back;
    private Button btn_response,btn_decr;

    private String address,strBody,key,url,key1,key2,address1;

    @Override
    public void onCreate(Bundle savedInstanceState) {

//        setTheme(ThemeSetter.GetTheme(this));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_view);
        progressDialog=new ProgressDialog(SmsInboxView.this);
//
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);

        btn_response = (Button) findViewById(R.id.btn_response);
        btn_decr = (Button) findViewById(R.id.btn_decr);
        btn_response.setOnClickListener(this);
        btn_decr.setOnClickListener(this);
//        key2=isTextNull.getString("key2","");

//        Log.d("key","key"+key);
        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            String strType = extras.getString("MSGBOXTYPE");
            tvSenderType = (TextView)findViewById(R.id.txtSenderType);
            tvSenderType.setText(strType);
            String strFrom = extras.getString("FROM");
            address = strFrom;
            address1=address.substring(3,address.length());
            tvFrom = (TextView)findViewById(R.id.txtSmsViewFrom);
            tvFrom.setText(getContactName(this, strFrom));
             strBody = extras.getString("BODY");
            tvBody = (TextView)findViewById(R.id.txtSmsViewBody);
                tvBody.setText(strBody);
        }
//        if(key2.equals("")){
            Getkey();
//        }else{
//
//        }
    }
//
    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        String tag = v.getTag() + "";
        String PhoneNumber = "";
        if (tag.equals("back")) {
            onBackPressed();
        }else if(tag.equals("response")){
            Intent intent = new Intent(SmsInboxView.this, SmsSendActivity.class);
            intent.putExtra("MSGBOXTYPE", "TO:");
            intent.putExtra("PhoneNumber", address);
            startActivity(intent);
            finish();
        }else if(tag.equals("decr")) {
            if( this.decript_sms(strBody) != null){
                tvBody.setText(this.decript_sms(strBody));
            }else{
                tvBody.setText(strBody);
            }
        }
    }

    public String getPhoneNumber(String name, Context context) {
        String ret = null;
        String selection = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" like'%" + name +"%'";
        String[] projection = new String[] { ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor c = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection, selection, null, null);
        if (c.moveToFirst()) {
            ret = c.getString(0);
        }
        c.close();
        if(ret==null)
            ret = "Unsaved";
        return ret;
    }

    private String decript_sms(String smsEncript){
        //  TextView txtEnc = (TextView)findViewById(R.id.txtSmsViewBody);
        String encrypted = smsEncript.toString();
        Encryption encryption = new Encryption();
//        String key = "insonix@220#gmai&199$com";
        String decrypted = encryption.decrypt(key2, encrypted);
//        if(decrypted != null){
//            txtEnc.setText(decrypted);
//        }
        return decrypted;
    }

    public boolean isInteger(String str) {
        int size = str.length();
        for (int i = 0; i < size; i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return size > 0;
    }

    public static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if(cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if(cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        if(contactName == null)
        {
            return phoneNumber;
        }
        return contactName;
    }

    public void Getkey() {
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url = BaseUrl+"get_keybyid.php?from_id=" + isTextNull.getString("uid", "")+"&phone="+address1+"&user_number="+isTextNull.getString("phone_number","");
        Log.d("url1111:", "url" + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
//


                try {
                    String Status = jsonObject.getString("status");
                    if (Status.equals("true")) {
//                        JSONObject response=jsonObject.getJSONObject("response");
                        String from_id = jsonObject.getString("from_id");
                        key1 = jsonObject.getString("key");
                        key2=key1.substring(0, 16);

                        Log.d("key2", "key2" + key2);
//
                        String phone = jsonObject.getString("phone");

//
//
//


//
                    } else if (Status.equals("false")) {

//

//                        Toast.makeText(SmsSendActivity.this, "Invalid username and password", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.d("eeeee:", "eeeee" + e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
            }


        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

//
//



}
