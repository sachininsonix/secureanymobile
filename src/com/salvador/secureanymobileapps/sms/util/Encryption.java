package com.salvador.secureanymobileapps.sms.util;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Encryption {

    private String charsetName = "UTF8";
    private String algorithm = "AES";
    private int base64Mode = Base64.DEFAULT;

    public String getCharsetName() {
        return charsetName;
    }

    public void setCharsetName(String charsetName) {
        this.charsetName = charsetName;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public int getBase64Mode() {
        return base64Mode;
    }

    public void setBase64Mode(int base64Mode) {
        this.base64Mode = base64Mode;
    }

    public String encrypt(byte[] key, byte[] data) {
        if (key == null || data == null)
            return null;
        try {

//            DESKeySpec desKeySpec = new DESKeySpec(key);
//            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(algorithm);
            SecretKeySpec skeySpec = new SecretKeySpec(key, algorithm);
//            SecretKey secretKey = secretKeyFactory.generateSecret(desKeySpec);
            byte[] dataBytes = data;
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            return Base64.encodeToString(cipher.doFinal(dataBytes), base64Mode);
        } catch (Exception e) {
            return null;
        }
    }

    public String decrypt(String key, String data) {
        if (key == null || data == null)
            return null;
        try {
            byte[] dataBytes = Base64.decode(data, base64Mode);
//
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(charsetName), algorithm);
//
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte[] dataBytesDecrypted = (cipher.doFinal(dataBytes));
            return new String(dataBytesDecrypted);
        } catch (Exception e) {
            return null;
        }
    }
}