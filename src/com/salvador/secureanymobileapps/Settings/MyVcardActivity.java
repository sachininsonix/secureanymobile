package com.salvador.secureanymobileapps.Settings;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.ChatHistory;
import com.salvador.secureanymobileapps.chat.ShowMsg;
import com.salvador.secureanymobileapps.sms.SmsSendActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URLEncoder;


public class MyVcardActivity extends Activity {

    private TextView vLabelName,secr_text;
    private TextView vLabelEmail;
    private TextView vLabelTelNum;
    private String sFname;
    private String sLname,encrypted;
    private ImageView share_button;
    ProgressDialog progressDialog;
    private ImageView imageView;
    private SharedPreferences isTextNull;
    private String key,url;
    private SharedPreferences settings;
    private String AvatarPath,phn,message,smss,chat;
    File picFile;
    private ShareActionProvider mShareActionProvider;

    @SuppressWarnings("static-access")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_my_vcard);
//
        progressDialog=new ProgressDialog(MyVcardActivity.this);
        this.imageView = (ImageView) findViewById(R.id.imageView2);
        this.vLabelName = (TextView) findViewById(R.id.vLabelName);
        this.vLabelEmail = (TextView) findViewById(R.id.vLabelEmail);
        this.vLabelTelNum = (TextView) findViewById(R.id.vLabelTelNum);
        this.secr_text = (TextView) findViewById(R.id.secr_text);
        this.settings = this
                .getSharedPreferences(UserPreferencesKey.keyUser, 0);
        this.key = settings.getString(UserPreferencesKey.keyUserId, "");
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        this.settings = this.getSharedPreferences(
                UserPreferencesKey.keyCreateAcount + "-" + key, 0);
        share_button = (ImageView) findViewById(R.id.share_button);
        this.sFname = settings.getString(UserPreferencesKey.keyFirstName, "");
        this.sLname = settings.getString(UserPreferencesKey.keyLastName, "");

        try {

            phn = getIntent().getExtras().getString("phn");
            message = getIntent().getExtras().getString("message");
            smss = getIntent().getExtras().getString("smss");
            chat = getIntent().getExtras().getString("chat");

            key = isTextNull.getString("phone_number","")+phn+"gmai@insonix@220";

//

        }catch (Exception e){

        }
        if(smss.equals("smssnd")) {
            new ShowMsg().createDialog(MyVcardActivity.this, "Before sending message to this user first You need to share your secret key.If he/she accept then you can communicate");
            secr_text.setVisibility(View.VISIBLE);
        }else{
            secr_text.setVisibility(View.GONE);
        }
//
        vLabelName.setText("Name: "+isTextNull.getString("username",""));
        vLabelEmail.setText("Email: "+isTextNull.getString("email",""));
        vLabelTelNum.setText("Phone no. "+isTextNull.getString("phone_number", ""));
        share_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(smss.equals("smssnd")) {
                    InsertKey();
                }else{
                    shareit();
                }

            }
        });

            }


    public void shareit()
    {

        View view =  findViewById(android.R.id.content);//your layout id
        view.getRootView();
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state))
        {
            File picDir  = new File(Environment.getExternalStorageDirectory()+ "/myPic");
            if (!picDir.exists())
            {
                picDir.mkdir();
            }
            view.setDrawingCacheEnabled(true);
            view.buildDrawingCache(true);
            Bitmap bitmap = view.getDrawingCache();
//          Date date = new Date();
            String fileName = "myvcard" + ".jpg";
            picFile = new File(picDir + "/" + fileName);
            try
            {
                picFile.createNewFile();
                FileOutputStream picOut = new FileOutputStream(picFile);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), (int) (bitmap.getHeight() / 1.2));
                boolean saved = bitmap.compress(Bitmap.CompressFormat.JPEG, 100, picOut);
                if (saved)
                {
                    //Toast.makeText(getApplicationContext(), "Image saved to your device Pictures "+ "directory!", Toast.LENGTH_SHORT).show();
                } else
                {
                    //Error
                }
                picOut.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            view.destroyDrawingCache();

            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("image/jpeg");
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(picFile.getAbsolutePath()));
            startActivity(Intent.createChooser(shareIntent, "Share via"));

        } else {
            //Error

        }


    }
    public void InsertKey(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url="  http://52.35.22.61/web/save_keys.php?from_id="+isTextNull.getString("uid","")+"&key="+ URLEncoder.encode(key)+"&phone="+phn+"&default="+chat;
        Log.d("url1111:", "url" + url);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());

                try {
                    String Status = jsonObject.getString("status");
                    if(Status.equals("true")) {
                        if(chat.equals("smsActivity")){
                            Intent intent = new Intent(MyVcardActivity.this, SmsSendActivity.class);
                            startActivity(intent);
                            finish();
                        }else {
//
                            Intent intent = new Intent(MyVcardActivity.this, ChatHistory.class);
                            startActivity(intent);
                            finish();
                        }
                    }else{
                        Toast.makeText(MyVcardActivity.this, "User deviceid not registered", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Log.d("eeeee:", "eeeee" + e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
                progressDialog.dismiss();
            }


        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }



}
