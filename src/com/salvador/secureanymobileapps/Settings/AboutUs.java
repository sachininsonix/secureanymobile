package com.salvador.secureanymobileapps.Settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.ChatMainActivity;
import com.salvador.secureanymobileapps.documents.SimmpleApp;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;

/**
 * Created by station12 on 9/29/14.
 */
public class AboutUs extends BaseActivity implements View.OnClickListener {

    private Button back;
    private SharedPreferences isTextNull;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ApplicationClass.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("About_us Activity")
                .build());
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);

    }
    public void onResume() {
        super.onResume();
        if(isFinishing()) {
            Intent intent = new Intent(AboutUs.this, ChatMainActivity.class);
            startActivity(intent);
            finish();
        }
        TextView VersionText = (TextView) findViewById(R.id.tvVersionInfo);

        try {
            String versionName = getApplicationContext().getPackageManager()
                .getPackageInfo(getPackageName(), 0).versionName;

            if(versionName != null && !versionName.isEmpty()) {
                VersionText.setText("Version: "+versionName);
            }

        } catch (PackageManager.NameNotFoundException n) {
            n.printStackTrace();
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(AboutUs.this,SettingsActivity.class);

        startActivity(intent);
        overridePendingTransition(0,0);
        finish();


    }

    @Override
    public void onClick(View view) {

        this.onBackPressed();

    }


    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }



}
