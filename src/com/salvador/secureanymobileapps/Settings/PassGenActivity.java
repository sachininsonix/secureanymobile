package com.salvador.secureanymobileapps.Settings;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.tokenlogin.ToolsActivity;
import com.salvador.secureanymobileapps.util.PasswordGenerator;

public class PassGenActivity extends Activity implements View.OnClickListener {

    private ScrollView containerViewPassGen;

    private TextView TitleViewPassGen;
    private TextView passwordField;
    private TextView labelLowerAlpha;
    private TextView labelUpperAlpha;
    private TextView labelNumbers;
    private TextView labelSpecialChars;
    private TextView labelLength;

    private Button back_button;
    private Button btnCopy;
    private Button btnGenerate;

    private View lineDivider1;
    private View lineDivider2;
    private View lineDivider3;
    private View lineDivider4;
    private View lineDivider5;
    private View lineDivider6;
    private View lineDivider7;

    private PasswordGenerator passGenerator;
    private boolean lowerAlpha = true;
    private boolean upperAlpha = true;
    private boolean number = true;
    private boolean specChar = true;
    private boolean removeMisspelling = true;

    private CheckBox checkLowerAlpha;
    private CheckBox checkUpperAlpha;
    private CheckBox checkNumbers;
    private CheckBox checkSpecials;
    String copy="";
    private Spinner spinner;

    private SharedPreferences isTextNull;

    public int passLength=8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_gen);
        this.containerViewPassGen = (ScrollView) findViewById(R.id.containerViewPassGen);

        this.back_button = (Button) findViewById(R.id.back_button);
        this.btnGenerate = (Button) findViewById(R.id.btnGenerate);
        this.btnCopy = (Button) findViewById(R.id.btnCopy);

        this.lineDivider1 = (View) findViewById(R.id.lineDivider1);
        this.lineDivider2 = (View) findViewById(R.id.lineDivider2);
        this.lineDivider3 = (View) findViewById(R.id.lineDivider3);
        this.lineDivider4 = (View) findViewById(R.id.lineDivider4);
        this.lineDivider5 = (View) findViewById(R.id.lineDivider5);
        this.lineDivider6 = (View) findViewById(R.id.lineDivider6);
        this.lineDivider7 = (View) findViewById(R.id.lineDivider7);

        this.TitleViewPassGen = (TextView) findViewById(R.id.TitleViewPassGen);
        this.passwordField = (TextView) findViewById(R.id.passwordField);
        this.labelLowerAlpha = (TextView) findViewById(R.id.labelLowerAlpha);
        this.labelUpperAlpha = (TextView) findViewById(R.id.labelUpperAlpha);
        this.labelNumbers = (TextView) findViewById(R.id.labelNumbers);
        this.labelSpecialChars = (TextView) findViewById(R.id.labelSpecialChars);
        this.labelLength = (TextView) findViewById(R.id.labelLength);

        this.checkLowerAlpha = (CheckBox) findViewById(R.id.checkLowerAlpha);
        this.checkUpperAlpha = (CheckBox) findViewById(R.id.checkUpperAlpha);
        this.checkNumbers = (CheckBox) findViewById(R.id.checkNumbers);
        this.checkSpecials = (CheckBox) findViewById(R.id.checkSpecials);

        this.spinner = (Spinner) findViewById(R.id.spinner);

        this.back_button.setOnClickListener(this);
        this.btnGenerate.setOnClickListener(this);
        this.btnCopy.setOnClickListener(this);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                int i;

                passwordField.setText("");
                for (i = 0; i < position + 8; i++) {
                    passwordField.append("-");
                    passLength = position + 8;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PassGenActivity.this, ToolsActivity.class);
                startActivity(intent);
                finish();
            }
        });


        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pass_gen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(PassGenActivity.this, ToolsActivity.class);
        startActivity(in);
        overridePendingTransition(0,0);
        finish();
    }

    @Override
    public void onClick(View view) {

        String tag = "" + view.getTag();
        if (tag.equals("back")) {
            this.onBackPressed();
        } else if(tag.equals("generate")) {
            copy="1";
            try {

                this.lowerAlpha = this.checkLowerAlpha.isChecked();
                this.upperAlpha = this.checkUpperAlpha.isChecked();
                this.number = this.checkNumbers.isChecked();
                this.specChar = this.checkSpecials.isChecked();

                this.passGenerator = new PasswordGenerator(this.lowerAlpha, this.upperAlpha, this.number, this.specChar, this.removeMisspelling);
                this.passwordField.setText(this.passGenerator.getNewPassword(this.passLength));
            }catch (Exception e){
                Toast toast = Toast.makeText(PassGenActivity.this.getApplicationContext(), "Please select atleast one option", Toast.LENGTH_SHORT);
                toast.show();
                e.printStackTrace();
            }
        } else if(tag.equals("copy")) {
            try {
                if (copy.equals("1")) {

                    ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                    ClipData myClip;
                    myClip = ClipData.newPlainText("text", this.passwordField.getText());
                    clipboard.setPrimaryClip(myClip);

                    Toast toast = Toast.makeText(PassGenActivity.this.getApplicationContext(), "Password Copied to Clipboard!", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    Toast toast = Toast.makeText(PassGenActivity.this.getApplicationContext(), "You must generate password before copy", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

}
