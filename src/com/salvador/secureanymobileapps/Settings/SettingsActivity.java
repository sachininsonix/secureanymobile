package com.salvador.secureanymobileapps.Settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.madx.updatechecker.lib.UpdateRunnable;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.ChatMainActivity;
import com.salvador.secureanymobileapps.chat.IAppConfigurationConstant;
import com.salvador.secureanymobileapps.chat.ShowMsg;
import com.salvador.secureanymobileapps.chat.UserProfile;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.documents.PasswordAttributes;
import com.salvador.secureanymobileapps.documents.Prefrences_Manager;
import com.salvador.secureanymobileapps.documents.SimmpleApp;
import com.salvador.secureanymobileapps.k9.Account;
import com.salvador.secureanymobileapps.lockpattern.LockPatternActivity;
import com.salvador.secureanymobileapps.lockpattern.util.Settings;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainMenu;
import com.salvador.secureanymobileapps.tokenlogin.TokenPairActivity;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

//import org.jivesoftware.smack.XMPPConnection;

/**
 * Created by station12 on 9/9/14.
 */
public class SettingsActivity extends BaseActivity implements OnClickListener, IAppConfigurationConstant,Iconstant {

    private TextView title;
    private TextView labelHeader1;
    private TextView labelHeader2;
    private TextView labelHeader3;
    private TextView labelHeader4;
    private TextView labelHeader5;
    private TextView labelProfile;
    private TextView theme;
    private TextView labelAutoLock;
    private TextView labelEditPassSms;
    private TextView labelEditPassDocuments;
    private TextView labelEditPassVoip;
    private TextView labelEditPassChat;
    private TextView labelEditPassEmail;
    private TextView labelAlgoritmos;
    private TextView labelThemeSelected;
    TextView cur_val;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private String servername = "2.8.4";
    File file;
    private Map<String, Account> accounts = null;
    int downloadedSize = 0;
    int totalSize = 0;
    private TextView labelAboutUs,labellogout;
    private TextView labelUpdate;
    private TextView labelGeneralPassword;
    private TextView labelNotPassword;
    private TextView labelVcard;
    private TextView labelEnableToken;
    private TextView labelEnableProximity;
    private TextView labelAddRemoveToken;
    private TextView labelGeneratePassword;
    private TextView labelPatternUnlock;
   String acc;
    private RelativeLayout containerViewSettings;
    private ApplicationClass appd;
    private SharedPreferences isTextNull;
    private SharedPreferences settings;
    private Button save;
    private Button back;
    ProgressDialog progressDialog;
    private View separatorTitle;
    private View lineDivider3;
    private View lineDivider4;
    private View lineDivider5;
    private View lineDivider6;
    private View lineDivider7;
    private View lineDivider8;
    private View lineDivider9;
    private View lineDivider10;
    private View lineDivider11;
    private View lineDivider12;
    private View lineDivider13;
    private View lineDivider14;
    private View lineDivider15;
    private View lineDivider16;
    private View lineDivider17;

    private View header1;
    private View header2;
    private View header3;
    private View header4;
    private View header5;
    RelativeLayout containerPopupUnlock;
    private ImageView icon;
    private ImageView icon2;

    private ToggleButton switchAutoLock;
    private ToggleButton switchPasswordGeneral;
    private ToggleButton switchNotPassword;
    private ToggleButton switchEnableToken;
    private ToggleButton switchEnableProximity;
    private ToggleButton switchPatternUnlock;

    private Spinner spinner;

    private String typeOption,url,url1,pass1,pass2;
    private int ValidateOptionsEditPass;
    private Prefrences_Manager prefrences_Manager;

    private boolean isChangethemes;
    private int typeAlgorithm = 0;
    boolean isLock;
//    XMPPConnection connection;
    Context ctx = this;
    ProgressBar pb;
    Dialog dialog;
    private static final int REQ_CREATE_PATTERN = 1;
    private static final int REQ_ENTER_PATTERN = 2;

    private String versionName = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {

//        setTheme(ThemeSetter.GetTheme(this));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
//
        try {
            ApplicationClass.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                    .setLabel("Settings Activity")
                    .build());
            acc=getIntent().getExtras().getString("account");
        }catch (Exception e){
            e.printStackTrace();
        }
        preferences=getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
        prefrences_Manager = new Prefrences_Manager(SettingsActivity.this);
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        editor = isTextNull.edit();
        isLock = isTextNull.getBoolean("lock", false);

        //textViewSettings
        this.title = (TextView) findViewById(R.id.TitleView);
        this.labelHeader1 = (TextView) findViewById(R.id.labelHeader1);
        this.labelHeader2 = (TextView) findViewById(R.id.labelHeader2);
        this.labelHeader3 = (TextView) findViewById(R.id.labelHeader3);
        this.labelHeader4 = (TextView) findViewById(R.id.labelHeader4);
        this.labelHeader5 = (TextView) findViewById(R.id.labelHeader5);
        this.labelProfile = (TextView) findViewById(R.id.labelProfile);
//        this.labellogout = (TextView) findViewById(R.id.labellogout);
        this.theme = (TextView) findViewById(R.id.labelTheme);
        this.labelAutoLock = (TextView) findViewById(R.id.labelAutoLock);
        this.labelEditPassSms = (TextView) findViewById(R.id.labelEditPassSms);
        this.labelEditPassDocuments = (TextView) findViewById(R.id.labelEditPassDocuments);
        this.labelEditPassVoip = (TextView) findViewById(R.id.labelEditPassVoip);
        this.labelEditPassChat = (TextView) findViewById(R.id.labelEditPassChat);
        this.labelEditPassEmail = (TextView) findViewById(R.id.labelEditPassEmail);
        this.labelAlgoritmos = (TextView) findViewById(R.id.labelAlgoritmos);
        this.labelThemeSelected = (TextView) findViewById(R.id.labelThemeSelected);

        this.labelAboutUs = (TextView) findViewById(R.id.labelAboutUs);
        this.labelUpdate = (TextView) findViewById(R.id.labelUpdate);
        this.labelGeneralPassword = (TextView) findViewById(R.id.labelPasswordGeneral);
        this.labelNotPassword = (TextView) findViewById(R.id.labelNoPassword);
        this.labelVcard = (TextView) findViewById(R.id.labelVcard);
        this.labelAddRemoveToken = (TextView) findViewById(R.id.labelAddRemoveToken);
        this.labelEnableToken = (TextView) findViewById(R.id.labelEnableToken);
        this.labelEnableProximity = (TextView) findViewById(R.id.labelEnableProximity);

        this.switchEnableToken = (ToggleButton) findViewById(R.id.switchEnableToken);
        this.switchEnableProximity = (ToggleButton) findViewById(R.id.switchEnableProximity);
        this.switchPatternUnlock = (ToggleButton) findViewById(R.id.switchPatternUnlock);

        this.labelGeneratePassword = (TextView) findViewById(R.id.labelGeneratePassword);
        this.labelPatternUnlock = (TextView) findViewById(R.id.labelPatternUnlock);
        dialog = new Dialog(SettingsActivity.this);
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyPrefTokenAddress, 0);
        String tokenAddress = this.settings.getString(UserPreferencesKey.keyTokenAddress, "");
        progressDialog=new ProgressDialog(SettingsActivity.this);

        this.icon = (ImageView) findViewById(R.id.imagesiconMenu);

        this.labelProfile.setOnClickListener(this);
        this.labelEditPassSms.setOnClickListener(this);
        this.labelEditPassDocuments.setOnClickListener(this);
        this.labelEditPassVoip.setOnClickListener(this);
        this.labelEditPassChat.setOnClickListener(this);
        this.labelEditPassEmail.setOnClickListener(this);
        this.labelAlgoritmos.setOnClickListener(this);
        this.theme.setOnClickListener(this);
        this.labelAboutUs.setOnClickListener(this);
        this.labelUpdate.setOnClickListener(this);
        this.labelVcard.setOnClickListener(this);
        this.labelAddRemoveToken.setOnClickListener(this);
        this.labelGeneratePassword.setOnClickListener(this);

        //register context menu
        this.registerForContextMenu(this.labelAlgoritmos);
        this.registerForContextMenu(this.theme);

        //button
        this.save = (Button) findViewById(R.id.btnSave);
        this.save.setOnClickListener(this);
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);


        this.containerViewSettings = (RelativeLayout) findViewById(R.id.containerViewSettings);
        this.separatorTitle = findViewById(R.id.lineDivider2);
        this.lineDivider3 = findViewById(R.id.lineDivider3);
        this.lineDivider4 = findViewById(R.id.lineDivider4);
        this.lineDivider5 = findViewById(R.id.lineDivider5);
        this.lineDivider6 = findViewById(R.id.lineDivider6);
        this.lineDivider7 = findViewById(R.id.lineDivider7);
        this.lineDivider8 = findViewById(R.id.lineDivider8);
        this.lineDivider9 = findViewById(R.id.lineDivider9);
        this.lineDivider10 = findViewById(R.id.lineDivider10);
        this.lineDivider11 = findViewById(R.id.lineDivider11);
        this.lineDivider12 = findViewById(R.id.lineDivider12);
        this.lineDivider13 = findViewById(R.id.lineDivider13);
        this.lineDivider14 = findViewById(R.id.lineDivider14);
        this.lineDivider15 = findViewById(R.id.lineDivider15);
        this.lineDivider16 = findViewById(R.id.lineDivider16);
        this.lineDivider17 = findViewById(R.id.lineDivider17);

        this.header1 = findViewById(R.id.header1);
        this.header2 = findViewById(R.id.header2);
        this.header3 = findViewById(R.id.header3);
        this.header4 = findViewById(R.id.header4);
        this.header5 = findViewById(R.id.header5);

        this.switchAutoLock = (ToggleButton) findViewById(R.id.switchAutoLock);
        this.switchPasswordGeneral = (ToggleButton) findViewById(R.id.switchPasswordGeneral);
        this.switchNotPassword = (ToggleButton) findViewById(R.id.switchNoPassword);
        this.switchPasswordGeneral.setOnClickListener(this);
        this.switchNotPassword.setOnClickListener(this);
        this.switchEnableToken.setOnClickListener(this);
        this.switchEnableProximity.setOnClickListener(this);
        this.switchPatternUnlock.setOnClickListener(this);

        this.appd = (ApplicationClass) getApplicationContext();
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyPrefName, 0);
        int tmp = this.settings.getInt(UserPreferencesKey.keyAlgoritmoCode, 0);

        this.spinner = (Spinner) findViewById(R.id.spinner);
        try {
            new UpdateRunnable(this, new Handler()).start();
        }catch (Exception e){
            e.printStackTrace();
        }

        if (tmp >= 0) {
            typeAlgorithm = tmp;
            spinner.setSelection(tmp);
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {

                typeAlgorithm = pos;
                Log.d("typeAlgorithm", "typeAlgorithm" + typeAlgorithm);
            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing, just another required interface callback
            }
        });


//        this.settings = this.getSharedPreferences(UserPreferencesKey.keyautoLock, 0);

//        this.switchAutoLock.setChecked(isLock);
if(isLock==true){
    this.switchAutoLock.setChecked(true);
}else{

}
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyEnToken, 0);
        boolean isToken = this.settings.getBoolean(UserPreferencesKey.keyEnableToken, false);
        this.switchEnableToken.setChecked(isToken);
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyEnProxy, 0);
        boolean isProxy = this.settings.getBoolean(UserPreferencesKey.keyEnableProximity, false);
        this.switchEnableProximity.setChecked(isProxy);
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyEnPattern, 0);
        boolean isPattern = this.settings.getBoolean(UserPreferencesKey.keyEnablePattern, false);
        this.switchPatternUnlock.setChecked(isPattern);
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyNoPass, 0);
        boolean isPassGeneral = this.settings.getBoolean(UserPreferencesKey.keyNOPassword, false);
        this.switchNotPassword.setChecked(isPassGeneral);
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyGeneralPass, 0);
        isPassGeneral = this.settings.getBoolean(UserPreferencesKey.keyGeneralPassword, false);
        this.switchPasswordGeneral.setChecked(isPassGeneral);

        if (this.switchPatternUnlock.isChecked()) {
            Intent intent = new Intent(LockPatternActivity.ACTION_COMPARE_PATTERN, null,
                    ctx, LockPatternActivity.class);

            startActivityForResult(intent, REQ_ENTER_PATTERN);

        }

        if (this.switchNotPassword.isChecked()) {
            this.hiddenGlobalPasswor();
            this.hiddenOptionsEditPassword();
        } else {
            this.showGlobalPasswor();
            if (this.switchPasswordGeneral.isChecked()) {
                this.hiddenOptionsEditPassword();
            } else {
                this.showOptionsEditPassword();
            }
        }

        applythemes();
//        labellogout.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (isNetworkAvailable() == false) {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
//                    builder.setMessage("Not able to connect to BuddyGap. Please check your network connection and try again.")
//                            .setTitle("Error")
//                            .setCancelable(false)
//                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    //  Action for 'NO' Button
//                                    dialog.cancel();
//                                }
//                            });
//                    AlertDialog alert = builder.create();
//                    alert.show();
//                } else {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
//
//                    builder.setTitle("Confirm");
//                    builder.setMessage("Are you sure you want to logout?");
//
//                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//
//                        public void onClick(DialogInterface dialog, int which) {
//                            // Do nothing but close the dialog
//
//                            Logout();
//                        }
//
//                    });
//
//                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            // Do nothing
////
//                            dialog.dismiss();
//                        }
//                    });
//
//                    AlertDialog alert = builder.create();
//                    alert.show();
//
//                }
//
//            }
//        });

//
    }

//

    @Override
    public void onResume() {
        super.onResume();

        this.settings = this.getSharedPreferences(UserPreferencesKey.keyEnToken, 0);
        boolean isToken = this.settings.getBoolean(UserPreferencesKey.keyEnableToken, false);
        this.switchEnableToken.setChecked(isToken);
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyEnProxy, 0);
        boolean isProxy = this.settings.getBoolean(UserPreferencesKey.keyEnableProximity, false);
        this.switchEnableProximity.setChecked(isProxy);

        applythemes();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        if(containerPopupUnlock.getVisibility()==View.VISIBLE) {
//                switchNotPassword.setChecked(false);
//        }else{
            Intent in = new Intent(SettingsActivity.this, MainMenu.class);
        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        in.putExtra("account",acc);
            startActivity(in);
            overridePendingTransition(0, 0);
            finish();
//        }
    }



    @Override
    public void onClick(View view) {
        this.ValidateOptionsEditPass = 0;
        this.isChangethemes = false;
        String tag = "" + view.getTag();
        Log.d("tag", "tag" + tag);
        if (tag.equals("back")) {
            this.onBackPressed();
        } else if (tag.equals("save")) {

            if (isTextNull.getString("theme", "").equals("")) {
                isTextNull.edit().putString("theme", this.labelThemeSelected.getText().toString()).commit();
            } else {
                if (!isTextNull.getString("theme", "").equals(this.labelThemeSelected.getText().toString())) {
                    isTextNull.edit().putString("theme", this.labelThemeSelected.getText().toString()).commit();
                }
            }
            this.saveSettings();
            this.saveAutoLock();
            this.savePassworGeneral();
            this.saveEnableToken();
            this.saveEnableProximity();
            this.savePatternUnlock();

//            this.onBackPressed();
        } else if (tag.equals("profile")) {
            Intent intent = new Intent(SettingsActivity.this, UserProfile.class);
            intent.putExtra("titleProfile", "User Profile");

            startActivity(intent);
            finish();
        } else if (tag.equals("Vcard")) {
            Intent intent = new Intent(SettingsActivity.this, MyVcardActivity.class);
            intent.putExtra("smss","vcard");
            startActivity(intent);
        } else if (tag.equals("editpasssms")) {
            this.typeOption = "Change Password SMS";
            this.
                    ValidateOptionsEditPass = 1;
            this.editPassword();
        } else if (tag.equals("editpassdocuments")) {
            this.typeOption = "Change Password DOCUMENTS";
            this.ValidateOptionsEditPass = 2;
            this.editPassword();
        } else if (tag.equals("editpassvoip")) {
            this.typeOption = "change Password VOIP";
            this.ValidateOptionsEditPass = 3;
            this.editPassword();
        } else if (tag.equals("editpasschat")) {
            this.typeOption = "Change Password CHAT";
            this.ValidateOptionsEditPass = 4;
            this.editPassword();
        } else if (tag.equals("editpassemail")) {
            this.typeOption = "Change Password EMAIL";
            this.ValidateOptionsEditPass = 5;
            this.editPassword();
        } else if (tag.equals("selectedTheme")) {
            this.isChangethemes = true;
            this.openContextMenu(view);
        } else if (tag.equals("algoritmo")) {
            this.openContextMenu(view);
        } else if (tag.equals("about")) {
            Intent intent = new Intent(SettingsActivity.this, AboutUs.class);
            startActivity(intent);
            finish();
        } else if (tag.equals("update")) {
            new UpdateRunnable(this, new Handler()).force(true).start();
//            Version();
//
        } else if (tag.equals("switchPassGeneral")) {
            if (this.switchPasswordGeneral.isChecked() == true) {
                this.hiddenOptionsEditPassword();
                this.typeOption = "Global Password";
                this.ValidateOptionsEditPass = 6;
                this.editPassword();
                Log.d("UI thread", "Creating Unlock Pattern...");
            } else {
                this.showOptionsEditPassword();
            }
        } else if (tag.equals("PatternUnlock")) {
            if (this.switchPatternUnlock.isChecked() == true) {
                Log.d("UI thread", "Creating Unlock Pattern...");
                Settings.Security.setAutoSavePattern(ctx, true);
                Intent intent = new Intent(LockPatternActivity.ACTION_CREATE_PATTERN, null,
                        ctx, LockPatternActivity.class);
                startActivityForResult(intent, REQ_CREATE_PATTERN);

            }
        } else if (tag.equals("switchNoPass")) {
            if (this.switchNotPassword.isChecked() == true) {
                this.hiddenOptionsEditPassword();
                this.hiddenGlobalPasswor();
                this.confirmChangePassword();
            } else {
                this.showGlobalPasswor();
                this.showOptionsEditPassword();
                this.switchPasswordGeneral.setChecked(false);
                settings = getSharedPreferences(UserPreferencesKey.keyNoPass, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(UserPreferencesKey.keyNOPassword, switchNotPassword.isChecked());
                editor.commit();
            }
        } else if (tag.equals("addRemoveToken")) {
            //Open TokenPairActivity
            Intent myIntent = new Intent(SettingsActivity.this, TokenPairActivity.class);
            SettingsActivity.this.startActivity(myIntent);
        } else if (tag.equals("passGen")) {
            //Open TokenPairActivity
            Intent myIntent = new Intent(SettingsActivity.this, PassGenActivity.class);
            SettingsActivity.this.startActivity(myIntent);
        }
    }
    public void showProgress(String file_path) {

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.myprogressdialog);
        dialog.setTitle("Download Progress");
        TextView text = (TextView) dialog.findViewById(R.id.tv1);
        text.setText("Downloading file from ... " + file_path);
        cur_val = (TextView) dialog.findViewById(R.id.cur_pg_tv);
        cur_val.setText("Starting download...");
        dialog.show();
        pb = (ProgressBar) dialog.findViewById(R.id.progress_bar);
        pb.setProgress(0);
        pb.setProgressDrawable(getResources().getDrawable(R.drawable.green_progress));
    }
    private void saveEnableToken() {
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyEnToken, 0);
        SharedPreferences.Editor editor = this.settings.edit();
        editor.putBoolean(UserPreferencesKey.keyEnableToken, this.switchEnableToken.isChecked());
        // Commit the edits!
        editor.commit();
    }

    private void saveEnableProximity() {
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyEnProxy, 0);
        SharedPreferences.Editor editor = this.settings.edit();
        editor.putBoolean(UserPreferencesKey.keyEnableProximity, this.switchEnableProximity.isChecked());
        // Commit the edits!
        editor.commit();
    }

    private void savePatternUnlock() {
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyEnPattern, 0);
        SharedPreferences.Editor editor = this.settings.edit();
        editor.putBoolean(UserPreferencesKey.keyEnablePattern, this.switchPatternUnlock.isChecked());
        // Commit the edits!
        editor.commit();
    }

    private void saveSettings() {
//        new ShowMsg().createDialog(SettingsActivity.this,"Your settings have been saved");
        Toast.makeText(SettingsActivity.this, "Your settings have been saved", Toast.LENGTH_SHORT).show();
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyPrefName, 0);
        SharedPreferences.Editor editor = this.settings.edit();
        editor.putInt(UserPreferencesKey.keyAlgoritmoCode, typeAlgorithm);
        // Commit the edits!
        editor.commit();
    }

    private void saveAutoLock() {
        if(switchAutoLock.isChecked()){
//            SharedPreferences.Editor editor = this.settings.edit();
            editor.putBoolean("lock", true);
            editor.commit();
        }else{
//            SharedPreferences.Editor editor = this.settings.edit();
            editor.putBoolean("lock", false);
            editor.commit();
        }
//        this.settings = this.getSharedPreferences(UserPreferencesKey.keyautoLock, 0);
//        SharedPreferences.Editor editor = this.settings.edit();
//        editor.putBoolean(UserPreferencesKey.keyisAutoLock, this.switchAutoLock.isChecked());
//        // Commit the edits!
//        editor.commit();
    }

    private void savePassworGeneral() {
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyGeneralPass, 0);
        SharedPreferences.Editor editor = this.settings.edit();
        editor.putBoolean(UserPreferencesKey.keyGeneralPassword, this.switchPasswordGeneral.isChecked());
        // Commit the edits!
        editor.commit();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {

        super.onCreateContextMenu(menu, v, menuInfo);
        int view_id = v.getId();
        //menu.setHeaderTitle("Options");

        if (this.isChangethemes) {
            getMenuInflater().inflate(R.menu.menu_theme_select, menu);

        } else {
            getMenuInflater().inflate(R.menu.menu_algorithm_select, menu);
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int itemId = item.getGroupId();


        if (this.isChangethemes) {
            switch (item.getItemId()) {
                case R.id.SAM_1:
                    this.labelThemeSelected.setText("SAM Theme 1");
                    return true;/*
                case R.id.other:
                    this.labelThemeSelected.setText("Other Theme");
                    return true;
                    */
            }
            //Save Selected Theme...
            if (isTextNull.getString("theme", "").equals("")) {
                isTextNull.edit().putString("theme", this.labelThemeSelected.getText().toString()).commit();
            } else {
                if (!isTextNull.getString("theme", "").equals(this.labelThemeSelected.getText().toString())) {
                    isTextNull.edit().putString("theme", this.labelThemeSelected.getText().toString()).commit();
                }
            }
            this.applythemes();
        } else {
            /*
            switch (item.getItemId()) {

                case R.id.AES256:
                    typeAlgorithm = 0;
                    labelAlgoritmosSelected.setText("AES (256 bit)");
                    return true;

                case R.id.BF256:
                    typeAlgorithm = 3;
                    labelAlgoritmosSelected.setText("Blowfish (256 bit)");
                    return true;

                case R.id.GOST256:
                    typeAlgorithm = 5;
                    labelAlgoritmosSelected.setText("Gost28147 (256 bit)");
                    return true;

                case R.id.BF448:
                    typeAlgorithm = 6;
                    labelAlgoritmosSelected.setText("Blowfish (448 bit)");
                    return true;
            }
            */
        }
        return true;
    }

    private void editPassword() {
        final Dialog dialog = new Dialog(SettingsActivity.this, R.style.Dialog_White);
        dialog.setContentView(R.layout.docs_dialog_layout);
        dialog.setTitle(this.typeOption);
        RelativeLayout containerPopupUnlock = (RelativeLayout) dialog.findViewById(R.id.containerPopupUnlock);
        Button button = (Button) dialog.findViewById(R.id.button_create_passwd);
        final ImageView strengthMeasure = (ImageView) dialog.findViewById(R.id.PD_strengthView);
        final TextView pet = (TextView) dialog.findViewById((R.id.PD_strengthText));
        final EditText editText = (EditText) dialog.findViewById(R.id.editText1);
        TextView text = (TextView) dialog.findViewById(R.id.tv11);
        final EditText editText1 = (EditText) dialog.findViewById(R.id.editText2);
        TextView text2 = (TextView) dialog.findViewById(R.id.tv12);
        text.setText("Old password");
        text2.setText("New password");
        button.setText("Reset");
        //this.applyThemesFirtsLaunch(containerPopupUnlock, pet, text, text2, editText, editText1);
        editText.addTextChangedListener((new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                strengthMeasure.setImageResource(PasswordAttributes.getSMImageID(PasswordAttributes.checkPasswordStrengthWeight(s.toString())));
                pet.setText(PasswordAttributes.getCommentID(PasswordAttributes.checkPasswordStrengthWeight(s.toString())));
            }
        }));
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                pass1 = editText.getText().toString().trim();
                pass2 = editText1.getText().toString().trim();
                if (isNetworkAvailable() == false) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                    builder.setMessage("Not able to connect to SAM. Please check your network connection and try again.")
                            .setTitle("Error")
                            .setCancelable(false)
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                    //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
                } else if (pass1.length() == 0) {
                    editText.setError("can't be Empty!");
                    editText.requestFocus();
                } else if (pass2.length() == 0) {
                    editText1.setError("can't be Empty!");
                    editText1.requestFocus();
                } else {

//                    UpdateGlobalPassword();

                    if (ValidateOptionsEditPass == 1) {
                        if (pass1.equals(prefrences_Manager.getPasswordSms())) {

                            prefrences_Manager.setPasswordSms(pass2);
                            prefrences_Manager.setFirstLaunchSms(false);
                            Toast toast = Toast.makeText(SettingsActivity.this.getApplicationContext(), "Your password has been changed", Toast.LENGTH_SHORT);
                            toast.show();
                        } else {
                            Toast toast = Toast.makeText(SettingsActivity.this.getApplicationContext(), "You entered wrong password", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }
//                    if (pass1.equals(pass2)) {
//                        if (ValidateOptionsEditPass == 1) {
//                            Log.d("2 is","2222");
//                            prefrences_Manager.setPasswordSms(pass1);
//                            prefrences_Manager.setFirstLaunchSms(false);
//                        }
                    else if (ValidateOptionsEditPass == 2) {
                        if (pass1.equals(prefrences_Manager.getPassword())) {
                            prefrences_Manager.setPassword(pass2);
                            prefrences_Manager.setFirstLaunch(false);
                            Toast toast = Toast.makeText(SettingsActivity.this.getApplicationContext(), "Your password has been changed", Toast.LENGTH_SHORT);
                            toast.show();

                        } else {
                            Toast toast = Toast.makeText(SettingsActivity.this.getApplicationContext(), "You entered wrong password", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    } else if (ValidateOptionsEditPass == 3) {
                        if (pass1.equals(prefrences_Manager.getPasswordVoip())) {
                            prefrences_Manager.setPasswordVoip(pass2);
                            prefrences_Manager.setFirstLaunchVoip(false);
                            Toast toast = Toast.makeText(SettingsActivity.this.getApplicationContext(), "Your password has been changed", Toast.LENGTH_SHORT);
                            toast.show();
                        } else {
                            Toast toast = Toast.makeText(SettingsActivity.this.getApplicationContext(), "You entered wrong password", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    } else if (ValidateOptionsEditPass == 4) {
                        if (pass1.equals(prefrences_Manager.getPasswordChat())) {
                            prefrences_Manager.setPasswordChat(pass2);
                            prefrences_Manager.setFirstLaunchChat(false);
                            Toast toast = Toast.makeText(SettingsActivity.this.getApplicationContext(), "Your password has been changed", Toast.LENGTH_SHORT);
                            toast.show();
                        } else {
                            Toast toast = Toast.makeText(SettingsActivity.this.getApplicationContext(), "You entered wrong password", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    } else if (ValidateOptionsEditPass == 5) {
                        if (pass1.equals(prefrences_Manager.getPasswordEmail())) {

                            prefrences_Manager.setPasswordEmail(pass2);
                            prefrences_Manager.setFirstLaunchEmail(false);
                            Toast toast = Toast.makeText(SettingsActivity.this.getApplicationContext(), "Your password has been changed", Toast.LENGTH_SHORT);
                            toast.show();
                        } else {
                            Toast toast = Toast.makeText(SettingsActivity.this.getApplicationContext(), "Error: You entered wrong old password", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    } else if (ValidateOptionsEditPass == 6) {

                        prefrences_Manager.setPasswordSms(pass2);
                        prefrences_Manager.setFirstLaunchSms(false);
                        prefrences_Manager.setPassword(pass2);
                        prefrences_Manager.setFirstLaunch(false);
                        prefrences_Manager.setPasswordEmail(pass2);
                        prefrences_Manager.setFirstLaunchEmail(false);
                        prefrences_Manager.setPasswordVoip(pass2);
                        prefrences_Manager.setFirstLaunchVoip(false);
                        prefrences_Manager.setPasswordChat(pass2);
                        prefrences_Manager.setFirstLaunchChat(false);
                        switchPasswordGeneral.setChecked(true);
                        Toast toast = Toast.makeText(SettingsActivity.this.getApplicationContext(), "Your password has been changed", Toast.LENGTH_SHORT);
                        toast.show();
                    }

//                    }
                    dialog.dismiss();

                }
            }

        });

        dialog.show();
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public void UpdateGlobalPassword(){
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            url1=BaseUrl+"update_pass.php?id="+isTextNull.getString("uid","")+"&oldpass="+pass1+"&newpassword="+pass2;
            Log.d("url1", "url1" + url1);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                    Log.d("res:", "resss" + jsonObject.toString());
                    try {
                        String status=jsonObject.getString("status");
                        String message=jsonObject.getString("message");
                        if(status.equalsIgnoreCase("true")){
                            new ShowMsg().createDialog(SettingsActivity.this,message );
                            switchPasswordGeneral.setChecked(true);
//                            dialog.dismiss();
                        }else{
                        new ShowMsg().createDialog(SettingsActivity.this,message );
                            switchPasswordGeneral.setChecked(false);
                        }
                    }catch(Exception e){
                    }
                    progressDialog.dismiss();
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d("volleyError:", "volleyError" + volleyError);
                    progressDialog.dismiss();
                }


            });
//        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
            MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
        }

    public void confirmChangePassword() {
        final Dialog dialog = new Dialog(SettingsActivity.this, R.style.Dialog_White);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.docs_dialog_layout1);
        //ThemeSetter.OverrideFonts(this, findViewById(android.R.id.content));
         containerPopupUnlock = (RelativeLayout) dialog.findViewById(R.id.containerPopupUnlock);
        TextView text = (TextView) dialog.findViewById(R.id.tv12);
        Button button = (Button) dialog.findViewById(R.id.button_create_passwd);
        final EditText editText = (EditText) dialog.findViewById(R.id.editText1);
        //this.applyThemerUnlock(containerPopupUnlock, text, button, editText);
        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (editText.getText().toString().length() != 0) {
                    settings = getSharedPreferences(UserPreferencesKey.keyCreateAcount, 0);
                    String string2 = settings.getString(UserPreferencesKey.keyPassword, "");
                    Log.d("string2", "string2" + string2);
                    String string = editText.getText().toString().trim();
                    if (!string.equals(string2)) {
                        editText.setError("Password Wrong");
                        switchNotPassword.setChecked(false);

                    } else {
                        setPassword();
                        settings = getSharedPreferences(UserPreferencesKey.keyNoPass, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putBoolean(UserPreferencesKey.keyNOPassword, switchNotPassword.isChecked());
                        // Commit the edits!
                        editor.commit();
                        switchNotPassword.setChecked(true);
                        dialog.dismiss();
                    }
                }
            }
        });

        dialog.show();
    }

    private void setPassword() {
        prefrences_Manager.setPasswordSms("");
        prefrences_Manager.setFirstLaunchSms(true);

        prefrences_Manager.setPassword("");
        prefrences_Manager.setFirstLaunch(true);

        prefrences_Manager.setPasswordVoip("");
        prefrences_Manager.setFirstLaunchVoip(true);

        prefrences_Manager.setPasswordChat("");
        prefrences_Manager.setFirstLaunchChat(true);
    }

    private void hiddenOptionsEditPassword() {
        this.labelEditPassSms.setVisibility(View.GONE);
        this.labelEditPassDocuments.setVisibility(View.GONE);
        this.labelEditPassVoip.setVisibility(View.GONE);
        this.labelEditPassChat.setVisibility(View.GONE);
        this.labelEditPassEmail.setVisibility(View.GONE);
        this.lineDivider11.setVisibility(View.GONE);
        this.lineDivider6.setVisibility(View.GONE);
        this.lineDivider7.setVisibility(View.GONE);
        this.lineDivider8.setVisibility(View.GONE);
    }

    private void showOptionsEditPassword() {
        this.labelEditPassSms.setVisibility(View.VISIBLE);
        this.labelEditPassDocuments.setVisibility(View.VISIBLE);
        this.labelEditPassVoip.setVisibility(View.VISIBLE);
        this.labelEditPassChat.setVisibility(View.VISIBLE);
        this.labelEditPassEmail.setVisibility(View.VISIBLE);
        this.lineDivider11.setVisibility(View.VISIBLE);
        this.lineDivider6.setVisibility(View.VISIBLE);
        this.lineDivider7.setVisibility(View.VISIBLE);
        this.lineDivider8.setVisibility(View.VISIBLE);
    }

    private void hiddenGlobalPasswor() {
        this.labelGeneralPassword.setVisibility(View.GONE);
        this.switchPasswordGeneral.setVisibility(View.GONE);
        this.lineDivider12.setVisibility(View.GONE);
    }

    private void showGlobalPasswor() {
        this.labelGeneralPassword.setVisibility(View.VISIBLE);
        this.switchPasswordGeneral.setVisibility(View.VISIBLE);
        this.lineDivider12.setVisibility(View.VISIBLE);
    }

    public void applythemes() {

        this.settings = this.getSharedPreferences(UserPreferencesKey.keyPrefTokenAddress, 0);
        String tokenAddress = this.settings.getString(UserPreferencesKey.keyTokenAddress, "");
        Resources resources = getResources();

        //Check if app has a Hardware Token paired...
        if (!tokenAddress.equals("")) {
            Log.i(TAG, "Paired Token: " + tokenAddress);
            this.switchEnableToken.setEnabled(true);
            this.switchEnableProximity.setEnabled(true);

            if (!isTextNull.getString("theme", "").equals("")) {
                if (isTextNull.getString("theme", "").equals("Other Theme")) {
                    this.labelAddRemoveToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                    this.labelEnableToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                    this.labelEnableProximity.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                }
                if (isTextNull.getString("theme", "").equals("SAM Theme 1")) {
                    this.labelAddRemoveToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                    this.labelEnableToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                    this.labelEnableProximity.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                }
                if (isTextNull.getString("theme", "").equals("iOS")) {
                    this.labelAddRemoveToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme3));
                    this.labelEnableToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme3));
                    this.labelEnableProximity.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme3));
                }
                if (isTextNull.getString("theme", "").equals("Windows")) {
                    this.labelAddRemoveToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                    this.labelEnableToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                    this.labelEnableProximity.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                }
            }
            this.labelAddRemoveToken.setText("Remove Paired Token");
        } else {
            Log.i(TAG, "There are NO Paired Tokens");
            this.switchEnableToken.setEnabled(false);
            this.switchEnableProximity.setEnabled(false);
            if (!isTextNull.getString("theme", "").equals("")) {
                if (isTextNull.getString("theme", "").equals("Other Theme")) {
                    this.labelAddRemoveToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                    this.labelEnableToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                    this.labelEnableProximity.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                }
                if (isTextNull.getString("theme", "").equals("SAM Theme 1")) {
                    this.labelAddRemoveToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                    this.labelEnableToken.setTextColor(resources.getColor(R.color.colorlabelLightTheme2));
                    this.labelEnableProximity.setTextColor(resources.getColor(R.color.colorlabelLightTheme2));
                }
                if (isTextNull.getString("theme", "").equals("iOS")) {
                    this.labelAddRemoveToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme3));
                    this.labelEnableToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                    this.labelEnableProximity.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                }
                if (isTextNull.getString("theme", "").equals("Windows")) {
                    this.labelAddRemoveToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                    this.labelEnableToken.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                    this.labelEnableProximity.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                }
            }
            this.labelAddRemoveToken.setText("Add Secure Token");
        }

        if (!isTextNull.getString("theme", "").equals("")) {
            if (isTextNull.getString("theme", "").equals("Other Theme")) {
                this.labelThemeSelected.setText("Other Theme");

            } else if (isTextNull.getString("theme", "").equals("SAM Theme 1")) {
                this.labelThemeSelected.setText("SAM Theme 1");

            }
        }

    }


    private void applyThemesFirtsLaunch(RelativeLayout containerPopupUnlock, TextView pet, TextView text, TextView text2, EditText editText, EditText editText1) {
        Resources resources = getResources();
        if (!isTextNull.getString("theme", "").equals("")) {
            if (isTextNull.getString("theme", "").equals("Other Theme")) {
                containerPopupUnlock.setBackgroundColor(resources.getColor(R.color.backgroundTheme1));
                text.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                editText.setBackgroundResource(R.drawable.txt_tirilla_light);
                editText1.setBackgroundResource(R.drawable.txt_tirilla_light);
                pet.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                text2.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                //editText.setTextColor(resources.getColor(R.color.colorTextInputTheme1));

            } else if (isTextNull.getString("theme", "").equals("SAM Theme 1")) {
                containerPopupUnlock.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
                text.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                editText.setBackgroundResource(R.drawable.txt_tirilla_light);
                editText1.setBackgroundResource(R.drawable.txt_tirilla_light);
                pet.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                text2.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                //editText.setTextColor(resources.getColor(R.color.colorTextInputTheme2));
            } else if (isTextNull.getString("theme", "").equals("iOS")) {
                containerPopupUnlock.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
                text.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                editText.setBackgroundResource(R.drawable.txt_tirilla_light);
                editText1.setBackgroundResource(R.drawable.txt_tirilla_light);
                pet.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                text2.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                //editText.setTextColor(resources.getColor(R.color.colorTextInputTheme2));
            } else if (isTextNull.getString("theme", "").equals("Windows")) {
                containerPopupUnlock.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
                text.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                editText.setBackgroundResource(R.drawable.txt_tirilla_light);
                editText1.setBackgroundResource(R.drawable.txt_tirilla_light);
                pet.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                text2.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                //editText.setTextColor(resources.getColor(R.color.colorTextInputTheme2));
            }
        } else {
            containerPopupUnlock.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
            text.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
            editText.setBackgroundResource(R.drawable.txt_tirilla_light);
            editText1.setBackgroundResource(R.drawable.txt_tirilla_light);
            pet.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
            text2.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
            //editText.setTextColor(resources.getColor(R.color.colorTextInputTheme2));
        }

    }

    private void applyThemerUnlock(RelativeLayout containerPopupUnlock, TextView text, Button button, EditText editText) {
        Resources resources = getResources();
        if (!isTextNull.getString("theme", "").equals("")) {
            if (isTextNull.getString("theme", "").equals("Other Theme")) {
                containerPopupUnlock.setBackgroundColor(resources.getColor(R.color.backgroundTheme1));
                text.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                editText.setBackgroundResource(R.drawable.txt_tirilla_light);
                //editText.setTextColor(resources.getColor(R.color.colorTextInputTheme1));

            } else if (isTextNull.getString("theme", "").equals("SAM Theme 1")) {
                containerPopupUnlock.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
                text.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                editText.setBackgroundResource(R.drawable.txt_tirilla_light);
                // editText.setTextColor(resources.getColor(R.color.colorTextInputTheme2));
            } else if (isTextNull.getString("theme", "").equals("iOS")) {
                containerPopupUnlock.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
                text.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                editText.setBackgroundResource(R.drawable.txt_tirilla_light);
                // editText.setTextColor(resources.getColor(R.color.colorTextInputTheme2));
            } else if (isTextNull.getString("theme", "").equals("Windows")) {
                containerPopupUnlock.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
                text.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                editText.setBackgroundResource(R.drawable.txt_tirilla_light);
                // editText.setTextColor(resources.getColor(R.color.colorTextInputTheme2));
            }
        } else {
            containerPopupUnlock.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
            text.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
            editText.setBackgroundResource(R.drawable.txt_tirilla_light);
            // editText.setTextColor(resources.getColor(R.color.colorTextInputTheme2));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQ_CREATE_PATTERN: {
                if (resultCode == RESULT_OK) {
                    char[] pattern = data.getCharArrayExtra(
                            LockPatternActivity.EXTRA_PATTERN);

                } else {
                    this.switchPatternUnlock.setChecked(false);
                }

                break;
            }// REQ_CREATE_PATTERN

            case REQ_ENTER_PATTERN: {
        /*
         * NOTE that there are 4 possible result codes!!!
         */
                switch (resultCode) {
                    case RESULT_OK:
//                        this.finish();
                        // The user passed
                        break;
                    case RESULT_CANCELED:
                        // The user cancelled the task
                        this.finish();
                        break;
                    case LockPatternActivity.RESULT_FAILED:
                        // The user failed to enter the pattern
                        this.finish();
                        break;
                    case LockPatternActivity.RESULT_FORGOT_PATTERN:
                        // The user forgot the pattern and invoked your recovery Activity.
                        this.finish();
                        break;
                }

        /*
         * In any case, there's always a key EXTRA_RETRY_COUNT, which holds
         * the number of tries that the user did.
         */
                int retryCount = data.getIntExtra(
                        LockPatternActivity.EXTRA_RETRY_COUNT, 0);

                break;
            }// REQ_ENTER_PATTERN
        }
    }
//
    public void Logout(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url1=BaseUrl+"logout.php?id="+isTextNull.getString("uid","");
        Log.d("url1111:", "url" + url1);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try {
                    String Status = jsonObject.getString("status");
//                    DocsMainActivity.isFirstLaunch=true;
//                    AccountManager am = AccountManager.get(SettingsActivity.this);
//                    Account[] accounts = Preferences.getPreferences(SettingsActivity.this).getAccounts();
//                 ApplicationClass.isHideSpecialAccounts();
//                        android.accounts.Account accountToRemove = accounts[0];
//                        am.removeAccount(accountToRemove, null, null);

                    prefrences_Manager.Allclear();

//                    DocsMainActivity.isLocked=false;
//                    prefrences_Manager.
                    editor.clear();
                    editor.commit();
                    Intent intent=new Intent(SettingsActivity.this, ChatMainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();


//


                }catch (Exception e){
                    Log.d("eeeee:", "eeeee" + e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
            }


        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    public class UpdateApplication extends AsyncTask<String, Void, Void> {
        public Context context;

        public void setContext(Context contextf) {
            context = contextf;
        }

        @Override
        protected Void doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();
                String PATH = "/mnt/sdcard/Download/";
                file = new File(PATH);
                file.mkdirs();
                File outputFile = new File(file, "update.apk");
                if (outputFile.exists()) {
                    outputFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);
                InputStream is = c.getInputStream();
                totalSize = c.getContentLength();
                runOnUiThread(new Runnable() {
                    public void run() {
                        pb.setMax(totalSize);
                    }
                });
                byte[] buffer = new byte[1024];
                int len1 = 0;
                Log.d("is", "is" + is);
                Log.d("fos", "fos" + fos);
                Log.d("is.read", "isread" + is.read(buffer));
                Log.d("totalSize", "totalSize" + totalSize);
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                    downloadedSize += len1;
                    // update the progressbar //
                    runOnUiThread(new Runnable() {
                        public void run() {
                            pb.setProgress(downloadedSize);
                            float per = ((float) downloadedSize / totalSize) * 100;
                            cur_val.setText("Downloaded " + downloadedSize + "KB / " + totalSize + "KB (" + (int) per + "%)");
                        }
                    });
                }
                fos.close();
                is.close();
                runOnUiThread(new Runnable() {
                    public void run() {
                        dialog.dismiss(); // if you want close it..
                    }
                });
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File("/mnt/sdcard/Download/update.apk")), "application/vnd.android.package-archive");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.setDataAndType(Uri.fromFile(new File("/mnt/sdcard/Download/update.apk")),"application/vnd.android.package-archive");
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
//            context.startActivity(intent);
            } catch (Exception e) {
                Log.e("UpdateAPP", "Update error! " + e.getMessage());
            }
            return null;
        }

    }
//    @Override
//    public void onPause() {
//        super.onPause();
//        if (!this.isFinishing()){
//            //Insert code for HOME  key Event
//            MainActivity.getInstance().clearPreferences();
//        }
//    }
    private void clearPreferences() {
        try {
            // clearing app data
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("pm clear com.salvador.secureanymobileapps");
//            Intent intent=new Intent(SettingsActivity.this, ChatMainActivity.class);
////            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
            Log.e("UpdateAPP", "Update error! ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}




