package com.salvador.secureanymobileapps.Settings;

/**
 * Created by station12 on 10/15/14.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;

/*** Listens for incoming SIP calls, intercepts and hands them off to WalkieTalkieActivity.
 */
public class IncomingCallReceiver extends BroadcastReceiver {
    /**
     * Processes the incoming call, answers it, and hands it over to the
     * WalkieTalkieActivity.
     * @param context The context under which the receiver is running.
     * @param intent The intent being received.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        BaseActivity wtActivity = (BaseActivity) context;
        wtActivity.receiveCall(intent);
        //wtActivity.updateStatus(incomingCall);
        Log.d("Secure Any Mobile", "incomingCall --- ");
    }
}