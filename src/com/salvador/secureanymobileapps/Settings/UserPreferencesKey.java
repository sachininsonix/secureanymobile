package com.salvador.secureanymobileapps.Settings;

/**
 * Created by station6 on 9/23/14.
 */
public class UserPreferencesKey {

    public static final String keyPrefTokenAddress = "tokenPreferencesAddress";
    public static final String keyTokenAddress = "tokenAddress";

    public static final String keyPrefName = "userPreferences";
    public static final String keyAlgoritmoCode = "algoritomo";

    public static final String keyautoLock = "lockPreference";
    public static final String keyisAutoLock = "lock";

    public static final String keyEnToken = "enTokenPreference";
    public static final String keyEnableToken = "enableToken";
    public static final String keyEnProxy = "enProxyPreference";
    public static final String keyEnableProximity= "enableProximity";

    public static final String keyEnPattern = "enPatternPreference";
    public static final String keyEnablePattern= "enablePattern";

    //key  for  password  general
    public static final String keyGeneralPass = "General_pass";
    public static final String keyGeneralPassword = "pass";

    //key for  No Password
    public static final String keyNoPass = "No_pass";
    public static final String keyNOPassword = "Nopass";

    //key  user  profile
    public static final String keyAvatar= "avatar";
    public static final String keyCreateAcount = "createAcount";
    public static final String keyFirstName = "firstname";
    public static final String keyLastName = "lastname";
    public static final String keyUserName = "username";
    public static final String keyPassword = "password";
    public static final String keyEmail = "email";
    public static final String keyPhoneNumber = "phonenumber";
    public static final String keyCountry = "country";

    // username  for  get data user  logger
    public static final String keyUser= "user";
    public static final String keyUserId = "id";

    //inbox active
    public static final String inboxkey= "inbox";
    public static final String isactiveinboxkey = "isactiveinbox";

}
