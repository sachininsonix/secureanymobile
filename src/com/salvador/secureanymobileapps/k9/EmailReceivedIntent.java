package com.salvador.secureanymobileapps.k9;

public class EmailReceivedIntent {

    public static final String ACTION_EMAIL_RECEIVED = "com.salvador.secureanymobileapps.k9.intent.action.EMAIL_RECEIVED";
    public static final String EXTRA_ACCOUNT    = "com.salvador.secureanymobileapps.k9.intent.extra.ACCOUNT";
    public static final String EXTRA_FOLDER     = "com.salvador.secureanymobileapps.k9.intent.extra.FOLDER";
    public static final String EXTRA_SENT_DATE  = "com.salvador.secureanymobileapps.k9.intent.extra.SENT_DATE";
    public static final String EXTRA_FROM       = "com.salvador.secureanymobileapps.k9.intent.extra.FROM";
    public static final String EXTRA_TO         = "com.salvador.secureanymobileapps.k9.intent.extra.TO";
    public static final String EXTRA_CC         = "com.salvador.secureanymobileapps.k9.intent.extra.CC";
    public static final String EXTRA_BCC        = "com.salvador.secureanymobileapps.k9.intent.extra.BCC";
    public static final String EXTRA_SUBJECT    = "com.salvador.secureanymobileapps.k9.intent.extra.SUBJECT";
}
