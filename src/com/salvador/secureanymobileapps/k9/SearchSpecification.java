
package com.salvador.secureanymobileapps.k9;

import com.salvador.secureanymobileapps.k9.mail.Flag;

public interface SearchSpecification {

    public Flag[] getRequiredFlags();

    public Flag[] getForbiddenFlags();

    public boolean isIntegrate();

    public String getQuery();

    public String[] getAccountUuids();

    public String[] getFolderNames();
}