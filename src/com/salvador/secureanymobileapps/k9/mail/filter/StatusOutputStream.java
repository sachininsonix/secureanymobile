package com.salvador.secureanymobileapps.k9.mail.filter;

import android.util.Config;
import android.util.Log;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class StatusOutputStream extends FilterOutputStream {
    private long mCount = 0;

    public StatusOutputStream(OutputStream out) {
        super(out);
    }

    @Override
    public void write(int oneByte) throws IOException {
        super.write(oneByte);
        mCount++;
        if (Config.LOGV) {
            if (mCount % 1024 == 0) {
                Log.v(ApplicationClass.LOG_TAG, "# " + mCount);
            }
        }
    }
}
