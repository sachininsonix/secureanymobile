package com.salvador.secureanymobileapps.k9.helper;

public final class StringUtils {

    public static boolean isNullOrEmpty(String string){
        return string == null || string.length() == 0;
    }

}
