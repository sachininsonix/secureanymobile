package com.salvador.secureanymobileapps.k9.activity;

import android.app.ExpandableListActivity;
import android.os.Bundle;

import com.salvador.secureanymobileapps.util.ThemeSetter;

/**
 * @see ExpandableListActivity
 */
public class K9ExpandableListActivity extends ExpandableListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(ThemeSetter.GetTheme(this));
        super.onCreate(savedInstanceState);
    }
}
