package com.salvador.secureanymobileapps.k9.activity.setup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.widget.Toast;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.k9.Preferences;
import com.salvador.secureanymobileapps.k9.activity.Accounts;
import com.salvador.secureanymobileapps.k9.activity.ColorPickerDialog;
import com.salvador.secureanymobileapps.k9.activity.K9PreferenceActivity;
import com.salvador.secureanymobileapps.k9.helper.DateFormatter;
import com.salvador.secureanymobileapps.k9.helper.FileBrowserHelper;
import com.salvador.secureanymobileapps.k9.helper.FileBrowserHelper.FileBrowserFailOverCallback;
import com.salvador.secureanymobileapps.k9.preferences.CheckBoxListPreference;
import com.salvador.secureanymobileapps.k9.preferences.TimePickerPreference;
import com.salvador.secureanymobileapps.k9.service.MailService;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.util.ThemeSetter;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;


public class Prefs extends K9PreferenceActivity {

    /**
     * Immutable empty {@link CharSequence} array
     */
    private static final CharSequence[] EMPTY_CHAR_SEQUENCE_ARRAY = new CharSequence[0];

    /*
     * Keys of the preferences defined in res/xml/global_preferences.xml
     */
    private static final String PREFERENCE_LANGUAGE = "language";
    private static final String PREFERENCE_THEME = "theme";
    private static final String PREFERENCE_FONT_SIZE = "font_size";
    private static final String PREFERENCE_DATE_FORMAT = "dateFormat";
    private static final String PREFERENCE_ANIMATIONS = "animations";
    private static final String PREFERENCE_GESTURES = "gestures";
    private static final String PREFERENCE_VOLUME_NAVIGATION = "volumeNavigation";
    private static final String PREFERENCE_MANAGE_BACK = "manage_back";
    private static final String PREFERENCE_START_INTEGRATED_INBOX = "start_integrated_inbox";
    private static final String PREFERENCE_CONFIRM_ACTIONS = "confirm_actions";
    private static final String PREFERENCE_PRIVACY_MODE = "privacy_mode";
    private static final String PREFERENCE_MEASURE_ACCOUNTS = "measure_accounts";
    private static final String PREFERENCE_COUNT_SEARCH = "count_search";
    private static final String PREFERENCE_HIDE_SPECIAL_ACCOUNTS = "hide_special_accounts";
    private static final String PREFERENCE_MESSAGELIST_TOUCHABLE = "messagelist_touchable";
    private static final String PREFERENCE_MESSAGELIST_PREVIEW_LINES = "messagelist_preview_lines";
    private static final String PREFERENCE_MESSAGELIST_STARS = "messagelist_stars";
    private static final String PREFERENCE_MESSAGELIST_CHECKBOXES = "messagelist_checkboxes";
    private static final String PREFERENCE_MESSAGELIST_SHOW_CORRESPONDENT_NAMES = "messagelist_show_correspondent_names";
    private static final String PREFERENCE_MESSAGELIST_SHOW_CONTACT_NAME = "messagelist_show_contact_name";
    private static final String PREFERENCE_MESSAGELIST_CONTACT_NAME_COLOR = "messagelist_contact_name_color";
    private static final String PREFERENCE_MESSAGEVIEW_FIXEDWIDTH = "messageview_fixedwidth_font";
    private static final String PREFERENCE_COMPACT_LAYOUTS = "compact_layouts";

    private static final String PREFERENCE_MESSAGEVIEW_RETURN_TO_LIST = "messageview_return_to_list";
    private static final String PREFERENCE_MESSAGEVIEW_SHOW_NEXT = "messageview_show_next";
    private static final String PREFERENCE_MESSAGEVIEW_ZOOM_CONTROLS_ENABLED = "messageview_zoom_controls";
    private static final String PREFERENCE_QUIET_TIME_ENABLED = "quiet_time_enabled";
    private static final String PREFERENCE_QUIET_TIME_STARTS = "quiet_time_starts";
    private static final String PREFERENCE_QUIET_TIME_ENDS = "quiet_time_ends";


    private static final String PREFERENCE_MESSAGEVIEW_MOBILE_LAYOUT = "messageview_mobile_layout";
    private static final String PREFERENCE_BACKGROUND_OPS = "background_ops";
    private static final String PREFERENCE_GALLERY_BUG_WORKAROUND = "use_gallery_bug_workaround";
    private static final String PREFERENCE_DEBUG_LOGGING = "debug_logging";
    private static final String PREFERENCE_SENSITIVE_LOGGING = "sensitive_logging";

    private static final String PREFERENCE_ATTACHMENT_DEF_PATH = "attachment_default_path";

    private static final int ACTIVITY_CHOOSE_FOLDER = 1;
    private ListPreference mLanguage;
    private ListPreference mTheme;
    private ListPreference mDateFormat;
    private CheckBoxPreference mAnimations;
    private CheckBoxPreference mGestures;
    private CheckBoxListPreference mVolumeNavigation;
    private CheckBoxPreference mManageBack;
    private CheckBoxPreference mStartIntegratedInbox;
    private CheckBoxListPreference mConfirmActions;
    private CheckBoxPreference mPrivacyMode;
    private CheckBoxPreference mMeasureAccounts;
    private CheckBoxPreference mCountSearch;
    private CheckBoxPreference mHideSpecialAccounts;
    private CheckBoxPreference mTouchable;
    private ListPreference mPreviewLines;
    private CheckBoxPreference mStars;
    private CheckBoxPreference mCheckboxes;
    private CheckBoxPreference mShowCorrespondentNames;
    private CheckBoxPreference mShowContactName;
    private CheckBoxPreference mChangeContactNameColor;
    private CheckBoxPreference mFixedWidth;
    private CheckBoxPreference mReturnToList;
    private CheckBoxPreference mShowNext;
    private CheckBoxPreference mZoomControlsEnabled;
    private CheckBoxPreference mMobileOptimizedLayout;
    private ListPreference mBackgroundOps;
    private CheckBoxPreference mUseGalleryBugWorkaround;
    private CheckBoxPreference mDebugLogging;
    private CheckBoxPreference mSensitiveLogging;
    private CheckBoxPreference compactLayouts;

    private CheckBoxPreference mQuietTimeEnabled;
    private com.salvador.secureanymobileapps.k9.preferences.TimePickerPreference mQuietTimeStarts;
    private com.salvador.secureanymobileapps.k9.preferences.TimePickerPreference mQuietTimeEnds;
    private Preference mAttachmentPathPreference;


    public static void actionPrefs(Context context) {
        Intent i = new Intent(context, Prefs.class);
        context.startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        setTheme(ThemeSetter.GetTheme(this));
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.global_preferences);

        mLanguage = (ListPreference) findPreference(PREFERENCE_LANGUAGE);
        List<CharSequence> entryVector = new ArrayList<CharSequence>(Arrays.asList(mLanguage.getEntries()));
        List<CharSequence> entryValueVector = new ArrayList<CharSequence>(Arrays.asList(mLanguage.getEntryValues()));
        String supportedLanguages[] = getResources().getStringArray(R.array.supported_languages);
        HashSet<String> supportedLanguageSet = new HashSet<String>(Arrays.asList(supportedLanguages));
        for (int i = entryVector.size() - 1; i > -1; --i) {
            if (!supportedLanguageSet.contains(entryValueVector.get(i))) {
                entryVector.remove(i);
                entryValueVector.remove(i);
            }
        }
        initListPreference(mLanguage, ApplicationClass.getK9Language(),
                           entryVector.toArray(EMPTY_CHAR_SEQUENCE_ARRAY),
                           entryValueVector.toArray(EMPTY_CHAR_SEQUENCE_ARRAY));

        final String theme = (ApplicationClass.getK9Theme() == android.R.style.Theme) ? "dark" : "light";
        mTheme = setupListPreference(PREFERENCE_THEME, theme);

        findPreference(PREFERENCE_FONT_SIZE).setOnPreferenceClickListener(
        new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                onFontSizeSettings();
                return true;
            }
        });

        mDateFormat = (ListPreference) findPreference(PREFERENCE_DATE_FORMAT);
        String[] formats = DateFormatter.getFormats(this);
        CharSequence[] entries = new CharSequence[formats.length];
        CharSequence[] values = new CharSequence[formats.length];
        for (int i = 0 ; i < formats.length; i++) {
            String format = formats[i];
            entries[i] = DateFormatter.getSampleDate(this, format);
            values[i] = format;
        }
        initListPreference(mDateFormat, DateFormatter.getFormat(this), entries, values);

        mAnimations = (CheckBoxPreference)findPreference(PREFERENCE_ANIMATIONS);
        mAnimations.setChecked(ApplicationClass.showAnimations());

        mGestures = (CheckBoxPreference)findPreference(PREFERENCE_GESTURES);
        mGestures.setChecked(ApplicationClass.gesturesEnabled());

        compactLayouts = (CheckBoxPreference)findPreference(PREFERENCE_COMPACT_LAYOUTS);
        compactLayouts.setChecked(ApplicationClass.useCompactLayouts());

        mVolumeNavigation = (CheckBoxListPreference)findPreference(PREFERENCE_VOLUME_NAVIGATION);
        mVolumeNavigation.setItems(new CharSequence[] {getString(R.string.volume_navigation_message), getString(R.string.volume_navigation_list)});
        mVolumeNavigation.setCheckedItems(new boolean[] {ApplicationClass.useVolumeKeysForNavigationEnabled(), ApplicationClass.useVolumeKeysForListNavigationEnabled()});

        mManageBack = (CheckBoxPreference)findPreference(PREFERENCE_MANAGE_BACK);
        mManageBack.setChecked(ApplicationClass.manageBack());

        mStartIntegratedInbox = (CheckBoxPreference)findPreference(PREFERENCE_START_INTEGRATED_INBOX);
        mStartIntegratedInbox.setChecked(ApplicationClass.startIntegratedInbox());

        mConfirmActions = (CheckBoxListPreference) findPreference(PREFERENCE_CONFIRM_ACTIONS);
        mConfirmActions.setItems(new CharSequence[] {
                                     getString(R.string.global_settings_confirm_action_delete),
                                     getString(R.string.global_settings_confirm_action_delete_starred),
                                     getString(R.string.global_settings_confirm_action_spam),
                                     getString(R.string.global_settings_confirm_action_mark_all_as_read)
                                 });
        mConfirmActions.setCheckedItems(new boolean[] {
        		ApplicationClass.confirmDelete(),
        		ApplicationClass.confirmDeleteStarred(),
        		ApplicationClass.confirmSpam(),
        		ApplicationClass.confirmMarkAllAsRead()
                                        });

        mPrivacyMode = (CheckBoxPreference) findPreference(PREFERENCE_PRIVACY_MODE);
        mPrivacyMode.setChecked(ApplicationClass.keyguardPrivacy());

        mMeasureAccounts = (CheckBoxPreference)findPreference(PREFERENCE_MEASURE_ACCOUNTS);
        mMeasureAccounts.setChecked(ApplicationClass.measureAccounts());

        mCountSearch = (CheckBoxPreference)findPreference(PREFERENCE_COUNT_SEARCH);
        mCountSearch.setChecked(ApplicationClass.countSearchMessages());

        mHideSpecialAccounts = (CheckBoxPreference)findPreference(PREFERENCE_HIDE_SPECIAL_ACCOUNTS);
        mHideSpecialAccounts.setChecked(ApplicationClass.isHideSpecialAccounts());

        mTouchable = (CheckBoxPreference)findPreference(PREFERENCE_MESSAGELIST_TOUCHABLE);
        mTouchable.setChecked(ApplicationClass.messageListTouchable());

        mPreviewLines = setupListPreference(PREFERENCE_MESSAGELIST_PREVIEW_LINES,
                                            Integer.toString(ApplicationClass.messageListPreviewLines()));

        mStars = (CheckBoxPreference)findPreference(PREFERENCE_MESSAGELIST_STARS);
        mStars.setChecked(ApplicationClass.messageListStars());

        mCheckboxes = (CheckBoxPreference)findPreference(PREFERENCE_MESSAGELIST_CHECKBOXES);
        mCheckboxes.setChecked(ApplicationClass.messageListCheckboxes());

        mShowCorrespondentNames = (CheckBoxPreference)findPreference(PREFERENCE_MESSAGELIST_SHOW_CORRESPONDENT_NAMES);
        mShowCorrespondentNames.setChecked(ApplicationClass.showCorrespondentNames());

        mShowContactName = (CheckBoxPreference)findPreference(PREFERENCE_MESSAGELIST_SHOW_CONTACT_NAME);
        mShowContactName.setChecked(ApplicationClass.showContactName());

        mChangeContactNameColor = (CheckBoxPreference)findPreference(PREFERENCE_MESSAGELIST_CONTACT_NAME_COLOR);
        mChangeContactNameColor.setChecked(ApplicationClass.changeContactNameColor());
        if (ApplicationClass.changeContactNameColor()) {
            mChangeContactNameColor.setSummary(R.string.global_settings_registered_name_color_changed);
        } else {
            mChangeContactNameColor.setSummary(R.string.global_settings_registered_name_color_default);
        }
        mChangeContactNameColor.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final Boolean checked = (Boolean) newValue;
                if (checked) {
                    onChooseContactNameColor();
                    mChangeContactNameColor.setSummary(R.string.global_settings_registered_name_color_changed);
                } else {
                    mChangeContactNameColor.setSummary(R.string.global_settings_registered_name_color_default);
                }
                mChangeContactNameColor.setChecked(checked);
                return false;
            }
        });

        mFixedWidth = (CheckBoxPreference)findPreference(PREFERENCE_MESSAGEVIEW_FIXEDWIDTH);
        mFixedWidth.setChecked(ApplicationClass.messageViewFixedWidthFont());

        mReturnToList = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGEVIEW_RETURN_TO_LIST);
        mReturnToList.setChecked(ApplicationClass.messageViewReturnToList());

        mShowNext = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGEVIEW_SHOW_NEXT);
        mShowNext.setChecked(ApplicationClass.messageViewShowNext());

        mZoomControlsEnabled = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGEVIEW_ZOOM_CONTROLS_ENABLED);
        mZoomControlsEnabled.setChecked(ApplicationClass.zoomControlsEnabled());

        mMobileOptimizedLayout = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGEVIEW_MOBILE_LAYOUT);
        if (Build.VERSION.SDK_INT <= 7) {
            mMobileOptimizedLayout.setEnabled(false);
        }


        mMobileOptimizedLayout.setChecked(ApplicationClass.mobileOptimizedLayout());

        mQuietTimeEnabled = (CheckBoxPreference) findPreference(PREFERENCE_QUIET_TIME_ENABLED);
        mQuietTimeEnabled.setChecked(ApplicationClass.getQuietTimeEnabled());

        mQuietTimeStarts = (TimePickerPreference) findPreference(PREFERENCE_QUIET_TIME_STARTS);
        mQuietTimeStarts.setDefaultValue(ApplicationClass.getQuietTimeStarts());
        mQuietTimeStarts.setSummary(ApplicationClass.getQuietTimeStarts());
        mQuietTimeStarts.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final String time = (String) newValue;
                mQuietTimeStarts.setSummary(time);
                return false;
            }
        });

        mQuietTimeEnds = (TimePickerPreference) findPreference(PREFERENCE_QUIET_TIME_ENDS);
        mQuietTimeEnds.setSummary(ApplicationClass.getQuietTimeEnds());
        mQuietTimeEnds.setDefaultValue(ApplicationClass.getQuietTimeEnds());
        mQuietTimeEnds.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final String time = (String) newValue;
                mQuietTimeEnds.setSummary(time);
                return false;
            }
        });




        mBackgroundOps = setupListPreference(PREFERENCE_BACKGROUND_OPS, ApplicationClass.getBackgroundOps().toString());

        mUseGalleryBugWorkaround = (CheckBoxPreference)findPreference(PREFERENCE_GALLERY_BUG_WORKAROUND);
        mUseGalleryBugWorkaround.setChecked(ApplicationClass.useGalleryBugWorkaround());

        mDebugLogging = (CheckBoxPreference)findPreference(PREFERENCE_DEBUG_LOGGING);
        mSensitiveLogging = (CheckBoxPreference)findPreference(PREFERENCE_SENSITIVE_LOGGING);

        mDebugLogging.setChecked(ApplicationClass.DEBUG);
        mSensitiveLogging.setChecked(ApplicationClass.DEBUG_SENSITIVE);

        mAttachmentPathPreference = findPreference(PREFERENCE_ATTACHMENT_DEF_PATH);
        mAttachmentPathPreference.setSummary(ApplicationClass.getAttachmentDefaultPath());
        mAttachmentPathPreference
        .setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                FileBrowserHelper
                .getInstance()
                .showFileBrowserActivity(Prefs.this,
                                         new File(ApplicationClass.getAttachmentDefaultPath()),
                                         ACTIVITY_CHOOSE_FOLDER, callback);

                return true;
            }

            FileBrowserFailOverCallback callback = new FileBrowserFailOverCallback() {

                @Override
                public void onPathEntered(String path) {
                    mAttachmentPathPreference.setSummary(path);
                    ApplicationClass.setAttachmentDefaultPath(path);
                }

                @Override
                public void onCancel() {
                    // canceled, do nothing
                }
            };
        });
    }

    private void saveSettings() {
        SharedPreferences preferences = Preferences.getPreferences(this).getPreferences();

        ApplicationClass.setK9Language(mLanguage.getValue());
        ApplicationClass.setK9Theme(mTheme.getValue().equals("dark") ? android.R.style.Theme : android.R.style.Theme_Light);
        ApplicationClass.setAnimations(mAnimations.isChecked());
        ApplicationClass.setGesturesEnabled(mGestures.isChecked());
        ApplicationClass.setCompactLayouts(compactLayouts.isChecked());
        ApplicationClass.setUseVolumeKeysForNavigation(mVolumeNavigation.getCheckedItems()[0]);
        ApplicationClass.setUseVolumeKeysForListNavigation(mVolumeNavigation.getCheckedItems()[1]);
        ApplicationClass.setManageBack(mManageBack.isChecked());
        ApplicationClass.setStartIntegratedInbox(!mHideSpecialAccounts.isChecked() && mStartIntegratedInbox.isChecked());
        ApplicationClass.setConfirmDelete(mConfirmActions.getCheckedItems()[0]);
        ApplicationClass.setConfirmDeleteStarred(mConfirmActions.getCheckedItems()[1]);
        ApplicationClass.setConfirmSpam(mConfirmActions.getCheckedItems()[2]);
        ApplicationClass.setConfirmMarkAllAsRead(mConfirmActions.getCheckedItems()[3]);
        ApplicationClass.setKeyguardPrivacy(mPrivacyMode.isChecked());
        ApplicationClass.setMeasureAccounts(mMeasureAccounts.isChecked());
        ApplicationClass.setCountSearchMessages(mCountSearch.isChecked());
        ApplicationClass.setHideSpecialAccounts(mHideSpecialAccounts.isChecked());
        ApplicationClass.setMessageListTouchable(mTouchable.isChecked());
        ApplicationClass.setMessageListPreviewLines(Integer.parseInt(mPreviewLines.getValue()));
        ApplicationClass.setMessageListStars(mStars.isChecked());
        ApplicationClass.setMessageListCheckboxes(mCheckboxes.isChecked());
        ApplicationClass.setShowCorrespondentNames(mShowCorrespondentNames.isChecked());
        ApplicationClass.setShowContactName(mShowContactName.isChecked());
        ApplicationClass.setChangeContactNameColor(mChangeContactNameColor.isChecked());
        ApplicationClass.setMessageViewFixedWidthFont(mFixedWidth.isChecked());
        ApplicationClass.setMessageViewReturnToList(mReturnToList.isChecked());
        ApplicationClass.setMessageViewShowNext(mShowNext.isChecked());
        ApplicationClass.setMobileOptimizedLayout(mMobileOptimizedLayout.isChecked());
        ApplicationClass.setQuietTimeEnabled(mQuietTimeEnabled.isChecked());

        ApplicationClass.setQuietTimeStarts(mQuietTimeStarts.getTime());
        ApplicationClass.setQuietTimeEnds(mQuietTimeEnds.getTime());


        ApplicationClass.setZoomControlsEnabled(mZoomControlsEnabled.isChecked());
        ApplicationClass.setAttachmentDefaultPath(mAttachmentPathPreference.getSummary().toString());
        boolean needsRefresh = ApplicationClass.setBackgroundOps(mBackgroundOps.getValue());
        ApplicationClass.setUseGalleryBugWorkaround(mUseGalleryBugWorkaround.isChecked());

        if (!ApplicationClass.DEBUG && mDebugLogging.isChecked()) {
            Toast.makeText(this, R.string.debug_logging_enabled, Toast.LENGTH_LONG).show();
        }
        ApplicationClass.DEBUG = mDebugLogging.isChecked();
        ApplicationClass.DEBUG_SENSITIVE = mSensitiveLogging.isChecked();

        Editor editor = preferences.edit();
        ApplicationClass.save(editor);
        DateFormatter.setDateFormat(editor, mDateFormat.getValue());
        editor.commit();

        if (needsRefresh) {
            MailService.actionReset(this, null);
        }
    }

    @Override
    protected void onPause() {
        saveSettings();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (ApplicationClass.manageBack()) {
            Accounts.listAccounts(this);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private void onFontSizeSettings() {
        FontSizeSettings.actionEditSettings(this);
    }

    private void onChooseContactNameColor() {
        new ColorPickerDialog(this, new ColorPickerDialog.OnColorChangedListener() {
            public void colorChanged(int color) {
            	ApplicationClass.setContactNameColor(color);
            }
        },
        ApplicationClass.getContactNameColor()).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case ACTIVITY_CHOOSE_FOLDER:
            if (resultCode == RESULT_OK && data != null) {
                // obtain the filename
                Uri fileUri = data.getData();
                if (fileUri != null) {
                    String filePath = fileUri.getPath();
                    if (filePath != null) {
                        mAttachmentPathPreference.setSummary(filePath.toString());
                        ApplicationClass.setAttachmentDefaultPath(filePath.toString());
                    }
                }
            }
            break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
