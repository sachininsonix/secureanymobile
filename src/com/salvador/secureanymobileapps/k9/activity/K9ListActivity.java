package com.salvador.secureanymobileapps.k9.activity;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.AdapterView;
import android.widget.ListView;

import com.salvador.secureanymobileapps.k9.helper.DateFormatter;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;
import com.salvador.secureanymobileapps.util.ThemeSetter;

public class K9ListActivity extends ListActivity {
    @Override
    public void onCreate(Bundle icicle) {
        K9Activity.setLanguage(this, ApplicationClass.getK9Language());
        setTheme(ThemeSetter.GetTheme(this));
        super.onCreate(icicle);
        setupFormats();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupFormats();
    }

    private java.text.DateFormat mDateFormat;
    private java.text.DateFormat mTimeFormat;

    private void setupFormats() {
        mDateFormat = DateFormatter.getDateFormat(this);
        mTimeFormat = android.text.format.DateFormat.getTimeFormat(this);   // 12/24 date format
    }

    public java.text.DateFormat getTimeFormat() {
        return mTimeFormat;
    }

    public java.text.DateFormat getDateFormat() {
        return mDateFormat;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Shortcuts that work no matter what is selected
        switch (keyCode) {
        case KeyEvent.KEYCODE_VOLUME_UP: {
            final ListView listView = getListView();
            if (ApplicationClass.useVolumeKeysForListNavigationEnabled()) {
                int currentPosition = listView.getSelectedItemPosition();
                if (currentPosition == AdapterView.INVALID_POSITION || listView.isInTouchMode()) {
                    currentPosition = listView.getFirstVisiblePosition();
                }
                if (currentPosition > 0) {
                    listView.setSelection(currentPosition - 1);
                }
                return true;
            }
        }
        case KeyEvent.KEYCODE_VOLUME_DOWN: {
            final ListView listView = getListView();
            if (ApplicationClass.useVolumeKeysForListNavigationEnabled()) {
                int currentPosition = listView.getSelectedItemPosition();
                if (currentPosition == AdapterView.INVALID_POSITION || listView.isInTouchMode()) {
                    currentPosition = listView.getFirstVisiblePosition();
                }

                if (currentPosition < listView.getCount()) {
                    listView.setSelection(currentPosition + 1);
                }
                return true;
            }
        }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        // Swallow these events too to avoid the audible notification of a volume change
        if (ApplicationClass.useVolumeKeysForListNavigationEnabled()) {
            if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP) || (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
                if (ApplicationClass.DEBUG)
                return true;
            }
        }
        return super.onKeyUp(keyCode, event);
    }
}
