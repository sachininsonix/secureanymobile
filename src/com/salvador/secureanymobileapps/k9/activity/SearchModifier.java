package com.salvador.secureanymobileapps.k9.activity;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.k9.mail.Flag;

/**
 * This enum represents filtering parameters used by {@link com.salvador.secureanymobileapps.k9.SearchAccount}.
 */
enum SearchModifier {
    FLAGGED(R.string.flagged_modifier, new Flag[]{Flag.FLAGGED}, null),
    UNREAD(R.string.unread_modifier, null, new Flag[]{Flag.SEEN});

    final int resId;
    final Flag[] requiredFlags;
    final Flag[] forbiddenFlags;

    SearchModifier(int nResId, Flag[] nRequiredFlags, Flag[] nForbiddenFlags) {
        resId = nResId;
        requiredFlags = nRequiredFlags;
        forbiddenFlags = nForbiddenFlags;
    }

}