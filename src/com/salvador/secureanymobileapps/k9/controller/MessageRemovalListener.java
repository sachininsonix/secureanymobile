package com.salvador.secureanymobileapps.k9.controller;

import com.salvador.secureanymobileapps.k9.mail.Message;

public interface MessageRemovalListener {
    public void messageRemoved(Message message);
}
