package com.salvador.secureanymobileapps.documents;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.Settings.SettingsActivity;
import com.salvador.secureanymobileapps.Settings.UserPreferencesKey;
import com.salvador.secureanymobileapps.chat.ChatHistory;
import com.salvador.secureanymobileapps.chat.ChatMainActivity;
import com.salvador.secureanymobileapps.chat.LoginActivity;
import com.salvador.secureanymobileapps.documents.encrypt.decrypt.SimppleFileUtil;
import com.salvador.secureanymobileapps.k9.activity.Accounts;
import com.salvador.secureanymobileapps.sms.MsgList;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;
import com.salvador.secureanymobileapps.tokenlogin.Globals;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainMenu;
import com.salvador.secureanymobileapps.tokenlogin.TokenInformation;
import com.salvador.secureanymobileapps.tokenlogin.ToolsActivity;
import com.salvador.secureanymobileapps.voip.VoipActivity;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by station12 on 9/10/14.
 */
public class FolderDefaultActivity extends BaseActivity implements View.OnClickListener, StrongBoxConstants {


    private SharedPreferences isTextNull;
    private ListView listViewAddFile;
    private List<String> fileList;
    private String pathDocument = "";
    private String name;
    private String trashPath = "";
    private Button back;
    private Button search;
    private Button home;
    private Button add;
    private TextView TitleNamefolder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

//
        super.onCreate(savedInstanceState);
        setContentView(R.layout.docs_folder_default_activity);
//
        ApplicationClass.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("FolderDefault Activity")
                .build());
        Bundle bundle = getIntent().getExtras();
        name = bundle.getString("name");
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);

        this.listViewAddFile = (ListView) findViewById(R.id.listContentDataFolder);
        this.listViewAddFile.setFastScrollEnabled(true);
//        this.listViewAddFile.setOnItemLongClickListener(this);
        this.fileList = new ArrayList<String>();
        this.pathDocument = SimmpleApp.simmpleBoxPath + "/" + name+"/";
        Log.d("pathhhhh","pathhhhh"+pathDocument);
        this.trashPath = SimmpleApp.simmpleBoxPath + "/Trash/";
        Log.d("trashPath","trashPath"+trashPath);
        this.TitleNamefolder = (TextView) findViewById(R.id.nameFolder);
        this.TitleNamefolder.setText(name);
        if(name.equals("Trash")){

        }else{
        listViewAddFile.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
@Override
public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
            final Context context = FolderDefaultActivity.this;
            final TextView textView = (TextView) view.findViewById(R.id.dirName);
            final String fileName = textView.getText().toString();
            System.out.println("file name=" + textView.getText().toString());
            final Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.docs_custom);
            dialog.setTitle("Delete File");
            RelativeLayout containerDelete = (RelativeLayout) dialog.findViewById(R.id.containerDelete);
            View separator = (View) dialog.findViewById(R.id.lineDivider4);
            TextView labelRestore = (TextView) dialog.findViewById(R.id.restoreTextView);
            TextView labelDelete = (TextView) dialog.findViewById(R.id.deleteTextView);
            Resources resources = getResources();
//


            labelDelete.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(FolderDefaultActivity.this);

                    builder.setTitle("Delete File: " +  textView.getText() + "?");
                    builder.setMessage("Are you sure?");
                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing but close the dialog
                            //Yes button clicked
                            // TODO Auto-generated method stub
                            new SimppleFileUtil(context).moveToTrash(pathDocument, trashPath ,
                                    textView.getText().toString()+"_0");
                            Toast.makeText(getApplicationContext(), "File moved to trash",
                                    Toast.LENGTH_SHORT).show();
//                        try {
//                            File file = new File(pathDocument);
//                            if (file.exists()) {
////                                new SimppleFileUtil(context).deleteFile(pathDocument,textView.getText().toString());
//                                File file2[] = file.listFiles();
//                                for (int i = 0; i < file2.length; i++) {
//                                    if (file2[i].isFile()) {
//                                        String tmp = file2[i].getName();
//                                        int sizeNameFile = tmp.length();
//                                        String finallChar = tmp.charAt(sizeNameFile - 1) + "";
//                                        try {
//                                            int num = Integer.parseInt(finallChar);
//                                            String[] partnameFile = tmp.split("_");
//                                            tmp = partnameFile[0];
//
//                                            if (tmp.equals(textView.getText().toString())) {
//                                                file2[i].delete();
//                                                break;
//                                            }
//                                        } catch (NumberFormatException e) {
//                                              e.printStackTrace();
//                                        }
//                                    }
//                                }
//                            }

//                        } catch (Exception e) {
//                            // TODO: handle exception
//                            e.printStackTrace();
//                        }
                            getAllFiles(pathDocument);
                            dialog.dismiss();
                        }

                    });

                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();
                    dialog.dismiss();
                }



            });

            labelRestore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    byte[] decryptData = new SimppleFileUtil(FolderDefaultActivity.this)
                            .decryptAndGet(pathDocument, textView.getText().toString());
//                if (decryptData == null) {
//                    Toast.makeText(getApplicationContext(), "is not possible to decipher the file",
//                            Toast.LENGTH_SHORT).show();
//                    return;
//                }
                    File mydir = new File(Environment.getExternalStorageDirectory() + "/Android/obb/com.salvador.secureanymobileapps/");
                    SimmpleApp.simmpleBoxPath = mydir.getPath();

                    //Try to Restore File to public directory path.....
                    File decryptFile = new File(Environment.getExternalStorageDirectory() + "/" + fileName);

                    if(!decryptFile.exists()) {
                        try {
                            decryptFile.createNewFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }


                    try {
                        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(decryptFile));
                        try {
                            bos.write(decryptData);
                            bos.flush();
                            bos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } catch (FileNotFoundException f) {
                        f.printStackTrace();
                    }

                    if(decryptFile.exists()) {
                        Toast.makeText(getApplicationContext(), "File Restored to Public Directories...",
                                Toast.LENGTH_SHORT).show();
                        File removeFile = new File(SimmpleApp.simmpleBoxPath + "Documents/" + fileName);
                        removeFile.delete();
                    }

                    dialog.dismiss();
                }
            });

            dialog.show();
    return false;
}
        });
        }
        //this.search = (Button) findViewById(R.id.button23);
        this.home = (Button) findViewById(R.id.button_home);
        this.home.setOnClickListener(this);
        this.add = (Button) findViewById(R.id.button_add);
        this.add.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
//        if(isFinishing()) {
//            Intent intent = new Intent(FolderDefaultActivity.this, ChatMainActivity.class);
//            startActivity(intent);
//            finish();
//        }
        getAllFiles(pathDocument);
    }


    @Override
    public void onPause() {
        super.onPause();
//        if(!isFinishing()) {
//            new CountDownTimer(10000, 1000) {
//
//                public void onTick(long millisUntilFinished) {
//
////                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
//                    //here you can have your logic to set text to edittext
//                }
//
//                public void onFinish() {
//                    finish();
////                mTextField.setText("done!");
//                }
//
//            }.start();
//        }
//        myTimer.schedule(myTask, 10000, 1000);
    }
//
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
//        super.onBackPressed();
        Intent intent=new Intent(FolderDefaultActivity.this,MainView.class);
        startActivity(intent);
        finish();
    }

//

    @Override
    public void onClick(View view) {

        String tag = view.getTag() + "";


        if (tag.equals("back")) {
            this.onBackPressed();
        } else if (tag.equals("addfile")) {
            this.addFile(view);
        } else if (tag.equals("gohome")) {
            this.goHome(view);
        }


    }

//    implements  methods  OnItemLongClickListener

//    @Override
//    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//        final Context context = FolderDefaultActivity.this;
//        final TextView textView = (TextView) view.findViewById(R.id.dirName);
//        final String fileName = textView.getText().toString();
//        System.out.println("file name=" + textView.getText().toString());
//        final Dialog dialog = new Dialog(context);
//        dialog.setContentView(R.layout.docs_custom);
//        dialog.setTitle("Delete File");
//        RelativeLayout containerDelete = (RelativeLayout) dialog.findViewById(R.id.containerDelete);
//        View separator = (View) dialog.findViewById(R.id.lineDivider4);
//        TextView labelRestore = (TextView) dialog.findViewById(R.id.restoreTextView);
//        TextView labelDelete = (TextView) dialog.findViewById(R.id.deleteTextView);
//        Resources resources = getResources();
////
//
//
//        labelDelete.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(FolderDefaultActivity.this);
//
//                builder.setTitle("Delete File: " +  textView.getText() + "?");
//                builder.setMessage("Are you sure?");
//                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//
//                    public void onClick(DialogInterface dialog, int which) {
//                        // Do nothing but close the dialog
//                        //Yes button clicked
//                        // TODO Auto-generated method stub
//                        new SimppleFileUtil(context).moveToTrash(pathDocument, trashPath ,
//                                textView.getText().toString()+"_0");
//                        Toast.makeText(getApplicationContext(), "File moved to trash",
//                                Toast.LENGTH_SHORT).show();
////                        try {
////                            File file = new File(pathDocument);
////                            if (file.exists()) {
//////                                new SimppleFileUtil(context).deleteFile(pathDocument,textView.getText().toString());
////                                File file2[] = file.listFiles();
////                                for (int i = 0; i < file2.length; i++) {
////                                    if (file2[i].isFile()) {
////                                        String tmp = file2[i].getName();
////                                        int sizeNameFile = tmp.length();
////                                        String finallChar = tmp.charAt(sizeNameFile - 1) + "";
////                                        try {
////                                            int num = Integer.parseInt(finallChar);
////                                            String[] partnameFile = tmp.split("_");
////                                            tmp = partnameFile[0];
////
////                                            if (tmp.equals(textView.getText().toString())) {
////                                                file2[i].delete();
////                                                break;
////                                            }
////                                        } catch (NumberFormatException e) {
////                                              e.printStackTrace();
////                                        }
////                                    }
////                                }
////                            }
//
////                        } catch (Exception e) {
////                            // TODO: handle exception
////                            e.printStackTrace();
////                        }
//                        getAllFiles(pathDocument);
//                        dialog.dismiss();
//                    }
//
//                });
//
//                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        // Do nothing
//                        dialog.dismiss();
//                    }
//                });
//
//                AlertDialog alert = builder.create();
//                alert.show();
//                dialog.dismiss();
//            }
//
//
//
//        });
//
//        labelRestore.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                byte[] decryptData = new SimppleFileUtil(FolderDefaultActivity.this)
//                        .decryptAndGet(pathDocument, textView.getText().toString());
////                if (decryptData == null) {
////                    Toast.makeText(getApplicationContext(), "is not possible to decipher the file",
////                            Toast.LENGTH_SHORT).show();
////                    return;
////                }
//                File mydir = new File(Environment.getExternalStorageDirectory() + "/Android/obb/com.salvador.secureanymobileapps/");
//                SimmpleApp.simmpleBoxPath = mydir.getPath();
//
//                //Try to Restore File to public directory path.....
//                File decryptFile = new File(Environment.getExternalStorageDirectory() + "/" + fileName);
//
//                if(!decryptFile.exists()) {
//                    try {
//                        decryptFile.createNewFile();
//                    } catch (IOException ex) {
//                        ex.printStackTrace();
//                    }
//                }
//
//
//                try {
//                    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(decryptFile));
//                    try {
//                        bos.write(decryptData);
//                        bos.flush();
//                        bos.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                } catch (FileNotFoundException f) {
//                    f.printStackTrace();
//                }
//
//                if(decryptFile.exists()) {
//                    Toast.makeText(getApplicationContext(), "File Restored to Public Directories...",
//                            Toast.LENGTH_SHORT).show();
//                    File removeFile = new File(SimmpleApp.simmpleBoxPath + "Documents/" + fileName);
//                    removeFile.delete();
//                }
//
//                dialog.dismiss();
//            }
//        });
//
//        dialog.show();
//        return true;
//    }

    public void goHome(View view) {
        startActivity(new Intent(FolderDefaultActivity.this, MainMenu.class));
        finish();
    }

    private void getAllFiles(String paath) {
        fileList.clear();
        fileList = new SimppleFileUtil(FolderDefaultActivity.this).getFileList(paath);
        this.listViewAddFile.setAdapter(new MyAdapter());
    }


    // add  File  method

    public void addFile(View view) {

        final Context context = FolderDefaultActivity.this;

        final Dialog dialog = new Dialog(context);
        dialog.setTitle("Select a type");
        dialog.setContentView(R.layout.docs_dialog_layout2);
        ListView listView = (ListView) dialog.findViewById(R.id.listViewDialog);
        final Resources resources = getResources();
        listView.setAdapter(new ArrayAdapter(context,
                R.layout.menu_docs_adapter, getResources()
                .getStringArray(R.array.options)
        ));

        if (!isTextNull.getString("theme", "").equals("")) {
            if (isTextNull.getString("theme", "").equals("Other Theme")) {
//                listView.setBackgroundColor(resources.getColor(R.color.backgroundTheme1));
            } else {
//                listView.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
            }
        } else {
//            listView.setBackgroundColor(resources.getColor(R.color.backgroundTheme1));
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                TextView textView = (TextView) arg1;
                String type = textView.getText().toString();
                if (type.equals("Password")) {
                    startActivity(new Intent(context, PasswordDoc.class)
                            .putExtra("pathToSave", pathDocument));
                    dialog.dismiss();
                } else if (type.equals("Bank Card")) {
                    Intent intent = new Intent(context,
                            MasterCardDoc.class);
                    intent.putExtra("pathToSave", pathDocument);
                    intent.putExtra("from", "Master_card");
                    startActivity(intent);
                    dialog.dismiss();
                } else if (type.equals("Bank Account")) {
                    startActivity(new Intent(context, BankAccDoc.class)
                            .putExtra("pathToSave", pathDocument));
                    dialog.dismiss();
                } else if (type.equals("Membership Card")) {
                    startActivity(new Intent(context,
                            MembershipDoc.class).putExtra("pathToSave",
                            pathDocument));
                    dialog.dismiss();
                } else if (type.equals("Email Account")) {
                    startActivity(new Intent(context, EmailDoc.class)
                            .putExtra("pathToSave", pathDocument));
                    dialog.dismiss();
                } else if (type.equals("Contact")) {
                    startActivity(new Intent(context, ContactDoc.class)
                            .putExtra("pathToSave", pathDocument));
                    dialog.dismiss();
                } else if (type.equals("Website")) {
                    startActivity(new Intent(context, WebsiteDoc.class)
                            .putExtra("pathToSave", pathDocument));
                    dialog.dismiss();
				}
            }
        });
        dialog.show();

    }


    // adapter  view type  Files

    private static class Holder {
        TextView dirName;
        ImageView iconImage;
        String nameOriginal;
        int codeAlgorithm = 0;
    }


    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return fileList.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return arg0;
        }

        @SuppressLint("NewApi")
        @Override
        public View getView(int arg0, View convertView, ViewGroup arg2) {
            View view = convertView;
            final Holder holder;
            if (view == null) {
                view = LayoutInflater.from(FolderDefaultActivity.this).inflate(
                        R.layout.docs_custom_trashitem, null);
                holder = new Holder();
                holder.dirName = (TextView) view.findViewById(R.id.dirName);

                holder.iconImage = (ImageView) view.findViewById((R.id.iconLabel));
                view.setTag(holder);
            } else {
                holder = (Holder) view.getTag();
            }
            String filename = fileList.get(arg0);
            holder.nameOriginal = filename;
            String extension = filename.substring(filename.lastIndexOf("."));
            String[] partExtension = extension.split("_");
            extension = partExtension[0];
            String[] partnameFile = filename.split("_");
            filename = partnameFile[0];

            if (partExtension.length > 1) holder.codeAlgorithm = Integer.parseInt(partExtension[1]);

            holder.dirName.setText(filename);
            final Resources resources = getResources();
            isTextNull = getApplicationContext().getSharedPreferences(
                    "LoginPreferences", 0);


            if (!isTextNull.getString("theme", "").equals("")) {
                if (isTextNull.getString("theme", "").equals("Black/Grey")) {
//                    holder.dirName.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));

                    if (extension.equals(PASSWORD_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(R.drawable.paswd_blue);
                    } else if (extension.equals(MASTERCARD_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.bank_blue);
                    } else if (extension.equals(VISA_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.visa_light);
                    } else if (extension.equals(MAESTRO_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.maestro_light);
                    } else if (extension.equals(BANKACC_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.saf_light);
                    } else if (extension.equals(HOMEBANK_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.home_bank_light);
                    } else if (extension.equals(MEMCARD_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.mem_light);
                    } else if (extension.equals(EMAIL_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.gmail_light);
                    } else if (extension.equals(CONTACT_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.contact_light);
                    } else if (extension.equals(WEBSITE_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.www_light);
                    }
                } else {
//                    holder.dirName.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));

                    if (extension.equals(PASSWORD_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(R.drawable.paswd_blue);
                    } else if (extension.equals(MASTERCARD_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.bank_blue);
                    } else if (extension.equals(VISA_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.visa_light);
                    } else if (extension.equals(MAESTRO_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.maestro_light);
                    } else if (extension.equals(BANKACC_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.saf_light);
                    } else if (extension.equals(HOMEBANK_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.home_bank_light);
                    } else if (extension.equals(MEMCARD_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.mem_light);
                    } else if (extension.equals(EMAIL_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.gmail_light);
                    } else if (extension.equals(CONTACT_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.contact_light);
                    } else if (extension.equals(WEBSITE_FILE_EXTENSION)) {
                        holder.iconImage.setBackgroundResource(
                                R.drawable.www_light);
                    }
                }
            } else {
//                holder.dirName.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));

                if (extension.equals(PASSWORD_FILE_EXTENSION)) {
                    holder.iconImage.setBackgroundResource(R.drawable.paswd_blue);
                } else if (extension.equals(MASTERCARD_FILE_EXTENSION)) {
                    holder.iconImage.setBackgroundResource(
                            R.drawable.bank_blue);
                } else if (extension.equals(VISA_FILE_EXTENSION)) {
                    holder.iconImage.setBackgroundResource(
                            R.drawable.visa_light);
                } else if (extension.equals(MAESTRO_FILE_EXTENSION)) {
                    holder.iconImage.setBackgroundResource(
                            R.drawable.maestro_light);
                } else if (extension.equals(BANKACC_FILE_EXTENSION)) {
                    holder.iconImage.setBackgroundResource(
                            R.drawable.saf_light);
                } else if (extension.equals(HOMEBANK_FILE_EXTENSION)) {
                    holder.iconImage.setBackgroundResource(
                            R.drawable.home_bank_light);
                } else if (extension.equals(MEMCARD_FILE_EXTENSION)) {
                    holder.iconImage.setBackgroundResource(
                            R.drawable.mem_light);
                } else if (extension.equals(EMAIL_FILE_EXTENSION)) {
                    holder.iconImage.setBackgroundResource(
                            R.drawable.gmail_light);
                } else if (extension.equals(CONTACT_FILE_EXTENSION)) {
                    holder.iconImage.setBackgroundResource(
                            R.drawable.contact_light);
                } else if (extension.equals(WEBSITE_FILE_EXTENSION)) {
                    holder.iconImage.setBackgroundResource(
                            R.drawable.www_light);
                }
            }

            holder.dirName.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    String fileName = holder.nameOriginal;
                   final EditText editText1, editText2, editText3, editText4, editText5, editText6, editText7, editText8, editText9, editText10, editText11;
                    byte[] data = new SimppleFileUtil(FolderDefaultActivity.this)
                            .decryptAndGet(pathDocument, fileName);
                    if (data == null) {
                        Toast.makeText(getApplicationContext(), "is not possible to decipher the file",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    String getData = new String(data);
                    Log.d("getdata", "getdata" + getData);
                    final Dialog dialog = new Dialog(FolderDefaultActivity.this,
                            android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    String extension = fileName.substring(fileName.lastIndexOf("."));
                    String[] partExtension = extension.split("_");
                    extension = partExtension[0];

                    final Button back2,decrptPasswd,encrptPasswd;
                    if (extension.equals(PASSWORD_FILE_EXTENSION)) {
                        dialog.setContentView(R.layout.docs_password_layout);

                        RelativeLayout containerGeneral = (RelativeLayout) dialog.findViewById(R.id.containergeneral);
                        editText1 = (EditText) dialog
                                .findViewById(R.id.editText1);
                        editText2 = (EditText) dialog
                                .findViewById(R.id.editText2);
                        editText3 = (EditText) dialog
                                .findViewById(R.id.editText3);
                        editText4 = (EditText) dialog
                                .findViewById(R.id.editText4);
                        editText5 = (EditText) dialog
                                .findViewById(R.id.editText5);

                        back2 = (Button) dialog.findViewById(R.id.back_button);
                        back2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onBackPressed();
                            }
                        });
                        encrptPasswd = (Button) dialog.findViewById(R.id.button1);
                        decrptPasswd = (Button) dialog.findViewById(R.id.button2);
                        decrptPasswd.setText("Decrypt");
                        encrptPasswd.setText("Encrypt");
                        encrptPasswd.setVisibility(View.GONE);
                        dialog.findViewById(R.id.button2).setVisibility(View.VISIBLE);

                        decrptPasswd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editText3.setInputType(InputType.TYPE_CLASS_TEXT);
                                decrptPasswd.setVisibility(View.GONE);
                                encrptPasswd.setVisibility(View.VISIBLE);
                            }
                        });
                        encrptPasswd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editText3.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                                editText3.setTransformationMethod(PasswordTransformationMethod.getInstance());
                                decrptPasswd.setVisibility(View.VISIBLE);
                                encrptPasswd.setVisibility(View.GONE);
                            }
                        });
                        try {
                            JSONObject jsonObject = new JSONObject(getData);
                            editText1.setText(jsonObject.getString("name"));
                            editText2.setText(jsonObject.getString("username"));
                            editText3.setText(jsonObject.getString("password"));
                            editText4.setText(jsonObject.getString("access"));
                            editText5.setText(jsonObject.getString("other"));
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }
                    } else if (extension.equals(MASTERCARD_FILE_EXTENSION)) {
                        dialog.setContentView(R.layout.docs_master_card_layout);
                        RelativeLayout containerGeneral = (RelativeLayout) dialog.findViewById(R.id.containergeneral);
                        editText1 = (EditText) dialog
                                .findViewById(R.id.editText1);
                        editText2 = (EditText) dialog
                                .findViewById(R.id.editText2);
                        editText3 = (EditText) dialog
                                .findViewById(R.id.editText3);
                        editText4 = (EditText) dialog
                                .findViewById(R.id.editText4);
                        editText5 = (EditText) dialog
                                .findViewById(R.id.editText5);
                        editText6 = (EditText) dialog
                                .findViewById(R.id.editText6);
                        editText7 = (EditText) dialog
                                .findViewById(R.id.editText7);
                        editText8 = (EditText) dialog
                                .findViewById(R.id.editText8);
                        editText9 = (EditText) dialog
                                .findViewById(R.id.editText9);
                        editText10 = (EditText) dialog
                                .findViewById(R.id.editText10);
                        editText11 = (EditText) dialog
                                .findViewById(R.id.editText11);

                        back2 = (Button) dialog.findViewById(R.id.back_button);
                        back2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onBackPressed();
                            }
                        });
                        dialog.findViewById(R.id.button1).setVisibility(
                                View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(getData);
                            editText1.setText(jsonObject.getString("name"));
                            editText2.setText(jsonObject.getString("iban"));
                            editText3.setText(jsonObject.getString("card_num"));
                            editText4.setText(jsonObject.getString("pin"));
                            editText5.setText(jsonObject.getString("cvv"));
                            editText6.setText(jsonObject.getString("bank"));
                            editText7.setText(jsonObject.getString("f_name"));
                            editText8.setText(jsonObject.getString("l_name"));
                            editText9.setText(jsonObject
                                    .getString("start_date"));
                            editText10.setText(jsonObject
                                    .getString("expiry_date"));
                            editText11.setText(jsonObject
                                    .getString("call_if_loast"));
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                    } else if (extension.equals(VISA_FILE_EXTENSION)) {
                        dialog.setContentView(R.layout.docs_master_card_layout);
                        RelativeLayout containerGeneral = (RelativeLayout) dialog.findViewById(R.id.containergeneral);
                        editText1 = (EditText) dialog
                                .findViewById(R.id.editText1);
                        editText2 = (EditText) dialog
                                .findViewById(R.id.editText2);
                        editText3 = (EditText) dialog
                                .findViewById(R.id.editText3);
                        editText4 = (EditText) dialog
                                .findViewById(R.id.editText4);
                        editText5 = (EditText) dialog
                                .findViewById(R.id.editText5);
                        editText6 = (EditText) dialog
                                .findViewById(R.id.editText6);
                        editText7 = (EditText) dialog
                                .findViewById(R.id.editText7);
                        editText8 = (EditText) dialog
                                .findViewById(R.id.editText8);
                        editText9 = (EditText) dialog
                                .findViewById(R.id.editText9);
                        editText10 = (EditText) dialog
                                .findViewById(R.id.editText10);
                        editText11 = (EditText) dialog
                                .findViewById(R.id.editText11);

                        back2 = (Button) dialog.findViewById(R.id.back_button);
                        back2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onBackPressed();
                            }
                        });
                        dialog.findViewById(R.id.button1).setVisibility(
                                View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(getData);
                            editText1.setText(jsonObject.getString("name"));
                            editText2.setText(jsonObject.getString("iban"));
                            editText3.setText(jsonObject.getString("card_num"));
                            editText4.setText(jsonObject.getString("pin"));
                            editText5.setText(jsonObject.getString("cvv"));
                            editText6.setText(jsonObject.getString("bank"));
                            editText7.setText(jsonObject.getString("f_name"));
                            editText8.setText(jsonObject.getString("l_name"));
                            editText9.setText(jsonObject
                                    .getString("start_date"));
                            editText10.setText(jsonObject
                                    .getString("expiry_date"));
                            editText11.setText(jsonObject
                                    .getString("call_if_loast"));
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                    } else if (extension.equals(MAESTRO_FILE_EXTENSION)) {
                        dialog.setContentView(R.layout.docs_master_card_layout);
                        RelativeLayout containerGeneral = (RelativeLayout) dialog.findViewById(R.id.containergeneral);
                        editText1 = (EditText) dialog
                                .findViewById(R.id.editText1);
                        editText2 = (EditText) dialog
                                .findViewById(R.id.editText2);
                        editText3 = (EditText) dialog
                                .findViewById(R.id.editText3);
                        editText4 = (EditText) dialog
                                .findViewById(R.id.editText4);
                        editText5 = (EditText) dialog
                                .findViewById(R.id.editText5);
                        editText6 = (EditText) dialog
                                .findViewById(R.id.editText6);
                        editText7 = (EditText) dialog
                                .findViewById(R.id.editText7);
                        editText8 = (EditText) dialog
                                .findViewById(R.id.editText8);
                        editText9 = (EditText) dialog
                                .findViewById(R.id.editText9);
                        editText10 = (EditText) dialog
                                .findViewById(R.id.editText10);
                        editText11 = (EditText) dialog
                                .findViewById(R.id.editText11);

                        back2 = (Button) dialog.findViewById(R.id.back_button);
                        back2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onBackPressed();
                            }
                        });
                        dialog.findViewById(R.id.button1).setVisibility(
                                View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(getData);
                            editText1.setText(jsonObject.getString("name"));
                            editText2.setText(jsonObject.getString("iban"));
                            editText3.setText(jsonObject.getString("card_num"));
                            editText4.setText(jsonObject.getString("pin"));
                            editText5.setText(jsonObject.getString("cvv"));
                            editText6.setText(jsonObject.getString("bank"));
                            editText7.setText(jsonObject.getString("f_name"));
                            editText8.setText(jsonObject.getString("l_name"));
                            editText9.setText(jsonObject
                                    .getString("start_date"));
                            editText10.setText(jsonObject
                                    .getString("expiry_date"));
                            editText11.setText(jsonObject
                                    .getString("call_if_loast"));
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                    } else if (extension.equals(BANKACC_FILE_EXTENSION)) {
                        dialog.setContentView(R.layout.docs_bank_acc_layout);
                        RelativeLayout containerGeneral = (RelativeLayout) dialog.findViewById(R.id.containergeneral);
                        editText1 = (EditText) dialog
                                .findViewById(R.id.editText1);
                        editText2 = (EditText) dialog
                                .findViewById(R.id.editText2);
                        editText3 = (EditText) dialog
                                .findViewById(R.id.editText3);
                        editText4 = (EditText) dialog
                                .findViewById(R.id.editText4);
                        editText5 = (EditText) dialog
                                .findViewById(R.id.editText5);
                        editText6 = (EditText) dialog
                                .findViewById(R.id.editText6);
                        editText7 = (EditText) dialog
                                .findViewById(R.id.editText7);
                        editText8 = (EditText) dialog
                                .findViewById(R.id.editText8);

                        back2 = (Button) dialog.findViewById(R.id.back_button);
                        back2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onBackPressed();
                            }
                        });
                        dialog.findViewById(R.id.button1).setVisibility(
                                View.GONE);

                        try {
                            JSONObject jsonObject = new JSONObject(getData);
                            editText1.setText(jsonObject.getString("name"));
                            editText2.setText(jsonObject.getString("bank"));
                            editText3.setText(jsonObject.getString("acc_num"));
                            editText4.setText(jsonObject.getString("iban"));
                            editText5.setText(jsonObject.getString("branch"));
                            editText6.setText(jsonObject.getString("swift"));
                            editText7.setText(jsonObject.getString("phone"));
                            editText8.setText(jsonObject.getString("note"));
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                    } else if (extension.equals(HOMEBANK_FILE_EXTENSION)) {
                        dialog.setContentView(R.layout.docs_home_bank_layout);
                        RelativeLayout containerGeneral = (RelativeLayout) dialog.findViewById(R.id.containergeneral);
                        editText1 = (EditText) dialog
                                .findViewById(R.id.editText1);
                        editText2 = (EditText) dialog
                                .findViewById(R.id.editText2);
                        editText3 = (EditText) dialog
                                .findViewById(R.id.editText3);
                        editText4 = (EditText) dialog
                                .findViewById(R.id.editText4);
                        editText5 = (EditText) dialog
                                .findViewById(R.id.editText5);
                        back2 = (Button) dialog.findViewById(R.id.back_button);
                        back2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onBackPressed();
                            }
                        });

                        dialog.findViewById(R.id.button1).setVisibility(
                                View.GONE);

                        try {
                            JSONObject jsonObject = new JSONObject(getData);
                            editText1.setText(jsonObject.getString("name"));
                            editText2.setText(jsonObject.getString("bank"));
                            editText3.setText(jsonObject.getString("user_code"));
                            editText4.setText(jsonObject.getString("pin"));
                            editText5.setText(jsonObject
                                    .getString("call_if_lost"));
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                    } else if (extension.equals(MEMCARD_FILE_EXTENSION)) {
                        dialog.setContentView(R.layout.docs_membership_card_layout);
                        RelativeLayout containerGeneral = (RelativeLayout) dialog.findViewById(R.id.containergeneral);
                        editText1 = (EditText) dialog
                                .findViewById(R.id.editText1);
                        editText2 = (EditText) dialog
                                .findViewById(R.id.editText2);
                        editText3 = (EditText) dialog
                                .findViewById(R.id.editText3);
                        editText4 = (EditText) dialog
                                .findViewById(R.id.editText4);
                        editText5 = (EditText) dialog
                                .findViewById(R.id.editText5);
                        back2 = (Button) dialog.findViewById(R.id.back_button);
                        back2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onBackPressed();
                            }
                        });
                        dialog.findViewById(R.id.button1).setVisibility(
                                View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(getData);
                            editText1.setText(jsonObject.getString("name"));
                            editText2.setText(jsonObject.getString("company"));
                            editText3.setText(jsonObject.getString("number"));
                            editText4.setText(jsonObject.getString("expires"));
                            editText5.setText(jsonObject.getString("phone"));
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                    } else if (extension.equals(EMAIL_FILE_EXTENSION)) {
                        dialog.setContentView(R.layout.docs_email_acc_layout);
                        RelativeLayout containerGeneral = (RelativeLayout) dialog.findViewById(R.id.containergeneral);

                        editText1 = (EditText) dialog
                                .findViewById(R.id.editText1);
                        editText2 = (EditText) dialog
                                .findViewById(R.id.editText2);
                        editText3 = (EditText) dialog
                                .findViewById(R.id.editText3);
                        editText4 = (EditText) dialog
                                .findViewById(R.id.editText4);
                        editText5 = (EditText) dialog
                                .findViewById(R.id.editText5);
                        editText6 = (EditText) dialog
                                .findViewById(R.id.editText6);

                        back2 = (Button) dialog.findViewById(R.id.back_button);
                        back2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onBackPressed();
                            }
                        });
                        dialog.findViewById(R.id.button1).setVisibility(
                                View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(getData);
                            editText1.setText(jsonObject.getString("name"));
                            editText2.setText(jsonObject.getString("login"));
                            editText3.setText(jsonObject.getString("passwd"));
                            editText4.setText(jsonObject.getString("pop3"));
                            editText5.setText(jsonObject.getString("imap"));
                            editText6.setText(jsonObject.getString("smtp"));
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                    } else if (extension.equals(CONTACT_FILE_EXTENSION)) {
                        dialog.setContentView(R.layout.docs_contact_layout);
                        RelativeLayout containerGeneral = (RelativeLayout) dialog.findViewById(R.id.containergeneral);
                        editText1 = (EditText) dialog
                                .findViewById(R.id.editText1);
                        editText2 = (EditText) dialog
                                .findViewById(R.id.editText2);
                        editText3 = (EditText) dialog
                                .findViewById(R.id.editText3);
                        editText4 = (EditText) dialog
                                .findViewById(R.id.editText4);
                        editText5 = (EditText) dialog
                                .findViewById(R.id.editText5);
                        editText6 = (EditText) dialog
                                .findViewById(R.id.editText6);
                        editText7 = (EditText) dialog
                                .findViewById(R.id.editText7);
                        editText8 = (EditText) dialog
                                .findViewById(R.id.editText8);
                        editText9 = (EditText) dialog
                                .findViewById(R.id.editText9);
                        editText10 = (EditText) dialog
                                .findViewById(R.id.editText10);
                        editText11 = (EditText) dialog
                                .findViewById(R.id.editText11);
                        back2 = (Button) dialog.findViewById(R.id.back_button);
                        back2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onBackPressed();
                            }
                        });
                        dialog.findViewById(R.id.button1).setVisibility(
                                View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(getData);
                            editText1.setText(jsonObject.getString("name"));
                            editText2.setText(jsonObject
                                    .getString("work_phone"));
                            editText3.setText(jsonObject.getString("yahoo"));
                            editText4.setText(jsonObject.getString("skype"));
                            editText5.setText(jsonObject.getString("email"));
                            editText6.setText(jsonObject.getString("company"));
                            editText7.setText(jsonObject.getString("address"));
                            editText8.setText(jsonObject.getString("city"));
                            editText9.setText(jsonObject.getString("state"));
                            editText10.setText(jsonObject.getString("country"));
                            editText11.setText(jsonObject.getString("zipcode"));
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                    } else if (extension.equals(WEBSITE_FILE_EXTENSION)) {
                        dialog.setContentView(R.layout.docs_website_layout);
                        RelativeLayout containerGeneral = (RelativeLayout) dialog.findViewById(R.id.containergeneral);
                        editText1 = (EditText) dialog
                                .findViewById(R.id.editText1);
                        editText2 = (EditText) dialog
                                .findViewById(R.id.editText2);
                        editText3 = (EditText) dialog
                                .findViewById(R.id.editText3);
                        editText4 = (EditText) dialog
                                .findViewById(R.id.editText4);
                        back2 = (Button) dialog.findViewById(R.id.back_button);
                        back2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onBackPressed();
                            }
                        });
                        dialog.findViewById(R.id.button1).setVisibility(
                                View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(getData);
                            editText1.setText(jsonObject.getString("name"));
                            editText2.setText(jsonObject.getString("url"));
                            editText3.setText(jsonObject.getString("user_name"));
                            editText4.setText(jsonObject
                                    .getString("regtd_email"));
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                    } else {
                        byte[] decryptData = new SimppleFileUtil(FolderDefaultActivity.this)
                                .decryptAndGet(pathDocument, fileName);
                        if (decryptData == null) {
                            Toast.makeText(getApplicationContext(), "is not possible to decipher the file",
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }
                        //Try to open as regular document.....
                        File decryptFile = new File(pathDocument + "/dec-" + fileName);

                        try {
                            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(decryptFile));
                            try {
                                bos.write(decryptData);
                                bos.flush();
                                bos.close();
                            } catch (IOException e) {

                            }

                        } catch (FileNotFoundException f) {

                        }


                        try {
                            FileOpen.openFile(FolderDefaultActivity.this, decryptFile);
                        } catch (IOException e) {

                        }
                        return;
                    }

                    dialog.show();
                }
            });
            return view;
        }


    }

}
