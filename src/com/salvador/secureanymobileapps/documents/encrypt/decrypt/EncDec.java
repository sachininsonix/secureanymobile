package com.salvador.secureanymobileapps.documents.encrypt.decrypt;

import java.security.AlgorithmParameters;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class EncDec {
	static byte[] encodedBytes = null;
	 static byte[] decodedBytes = null;
	 static byte[] iv=null;
	public static char[] password;
	public static byte[] salt;
	String string="sigmatech";
	String string2="1234567890";
	public EncDec()
	{
		 password=string.toCharArray();
		 salt=string2.getBytes();
	}
	public static byte[] encrypt(byte[] dataToEncrypt)
	{
		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			KeySpec spec = new PBEKeySpec(password, salt, 65536, 256);
			SecretKey tmp = factory.generateSecret(spec);
			SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");
			/* Encrypt the message. */
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secret);
			AlgorithmParameters params = cipher.getParameters();
			iv = params.getParameterSpec(IvParameterSpec.class).getIV();
			encodedBytes = cipher.doFinal(dataToEncrypt);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return encodedBytes;
	}
	
	public static byte[] decrypt(byte[] data)
	{
		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			KeySpec spec = new PBEKeySpec(password, salt, 65536, 256);
			SecretKey tmp = factory.generateSecret(spec);
			SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(iv));
			decodedBytes =cipher.doFinal(data);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return decodedBytes;
	}
}
