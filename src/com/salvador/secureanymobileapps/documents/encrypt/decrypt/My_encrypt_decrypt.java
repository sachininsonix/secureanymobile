package com.salvador.secureanymobileapps.documents.encrypt.decrypt;

import android.annotation.SuppressLint;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class My_encrypt_decrypt {
	SecretKeySpec sks = null;
	byte[] encodedBytes = null;
	byte[] decodedBytes = null;
	byte[] keyBytes = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,
			0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11,
			0x12, 0x13, 0x14, 0x15, 0x16, 0x17 };
	SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
	@SuppressLint("NewApi")
	public byte[] encrypt(byte[] dataToEncrypt) {
		System.out.println("Origional Data" + dataToEncrypt);
		try {
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			encodedBytes = cipher.doFinal(dataToEncrypt);
		} catch (Exception e) {
		}
		return encodedBytes;
	}

	public byte[] decrypt(byte[] data) {
		try {
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, key);
			decodedBytes = cipher.doFinal(data);
			// System.out.println("Origional Data"+new String(decodedBytes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return decodedBytes;
	}
}
