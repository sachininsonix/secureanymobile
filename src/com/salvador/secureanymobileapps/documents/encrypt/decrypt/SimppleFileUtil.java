package com.salvador.secureanymobileapps.documents.encrypt.decrypt;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.salvador.secureanymobileapps.Settings.UserPreferencesKey;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

@SuppressLint("NewApi")
public class SimppleFileUtil {
    private Context context;

    private List<String> dirList;
    private List<String> fileList;

    private Encryptor objEncryptor;

    InputStream inStream = null;

    OutputStream outStream = null;

    private int BASE_ALGORITHM_CODE = 0;

    public SimppleFileUtil(Context ctx) {
        context = ctx;
        SharedPreferences settings = context.getSharedPreferences(
                UserPreferencesKey.keyPrefName, 0);
        this.BASE_ALGORITHM_CODE = settings
                .getInt(UserPreferencesKey.keyAlgoritmoCode, 0);
        settings = null;
        try {
            this.objEncryptor = new Encryptor("passwordTest", BASE_ALGORITHM_CODE);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }
    // getting all dir list from a pth
    public List<String> getDirectoryList(String path) {
        try {
            File trashDir = new File(path);
            if (trashDir.exists()) {
                dirList = new ArrayList<String>();
                File list[] = trashDir.listFiles();
                if (list.length != 0) {
                    for (int i = 0; i < list.length; i++) {
                        if (list[i].isDirectory()) {
                            dirList.add(list[i].getName());
                        }
                    }
                }

            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return dirList;
    }
    // --------------------------------------------------------------------------------------------------------
    // getting all file list for a path
    public List<String> getFileList(String path) {
        try {
            File trashDir = new File(path);
            if (trashDir.exists()) {
                fileList = new ArrayList<String>();
                File list[] = trashDir.listFiles();
                if (list.length != 0) {
                    for (int i = 0; i < list.length; i++) {
                        if (!list[i].isDirectory()) {
                            fileList.add(list[i].getName());
                        }
                    }
                }

            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        System.out.println("name===" + fileList);
        return fileList;
    }
    public void deleteFile(String inputPath, String inputFile) {
        try {
            // delete the original file
            new File(inputPath + inputFile).delete();


        }

        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
    }

    // -------------------------------------------------------------------------------------------------------
    // move data to trash
    public void moveToTrash(String inputPath, String outputPath,
                            String inputFile) {
        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File (outputPath);
            if (!dir.exists())
            {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath + inputFile);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }


            // write the output file
            in.close();
            out.close();
//            out.flush();
//            out.close();
//            out = null;
//            in.close();
//            in = null;
            // delete the original file
            new File(inputPath + inputFile).delete();


        }

        catch (FileNotFoundException fnfe1) {
            fnfe1.printStackTrace();
            Log.e("tag", fnfe1.getMessage());
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("tag", e.getMessage());
        }

//        File fileInTrash = new File(destinationPath);
//        if (!fileInTrash.exists()) {
//            fileInTrash.mkdirs();
//        }
//        if (fileInTrash.exists()) {
//            try {
//                File destination = new File(fileInTrash.getPath(), "/"
//                        + fileName);
//                destination.createNewFile();
//                File source = new File(sourcePAth + "/" + fileName);
//                inStream = new FileInputStream(source);
//                outStream = new FileOutputStream(destination);
//                byte[] buffer = new byte[1024];
//
//                int length;
//                // copy the file content in bytes
//                while ((length = inStream.read(buffer)) > 0) {
//
//                    outStream.write(buffer, 0, length);
//
//                }
//
//                inStream.close();
//                outStream.close();
//
//                // delete the original file
//                source.delete();
//
//            } catch (Exception e) {
//                // TODO: handle exception
//                e.printStackTrace();
//            }
//        }
    }

    // *****************************************************************************************************
    // storing data to own space and encrypting and delete source file
    public void encrptyAndStoreData(String sourcePath, String destinationPath,
                                    String fileName) {
        try {
            File fileSource = new File(sourcePath);
            int size = (int) fileSource.length();
            byte[] bytes = new byte[size];
            BufferedInputStream buf = new BufferedInputStream(
                    new FileInputStream(fileSource));
            buf.read(bytes, 0, bytes.length);
            buf.close();
            File fileDestination = new File(destinationPath, fileName);
            if (!fileDestination.exists())
                fileDestination.createNewFile();
            System.out.println("path of created file="
                    + fileDestination.getPath());
            byte[] encData = null;
            try {
                encData = this.objEncryptor.encrypt(bytes, false);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (fileDestination.exists()) {
                FileOutputStream fileOutputStream = new FileOutputStream(
                        fileDestination);
                fileOutputStream.write(encData);
                fileOutputStream.close();
                String canonicalPath;
                try {
                    canonicalPath = fileSource.getCanonicalPath();
                } catch (IOException e) {
                    canonicalPath = fileSource.getAbsolutePath();
                }
                ContentResolver contentResolver = context.getContentResolver();
                final Uri uri = MediaStore.Files.getContentUri("external");
                final int result = contentResolver.delete(uri,
                        MediaStore.Files.FileColumns.DATA + "=?",
                        new String[]{canonicalPath});
                if (result == 0) {
                    final String absolutePath = fileSource.getAbsolutePath();
                    if (!absolutePath.equals(canonicalPath)) {
                        contentResolver.delete(uri,
                                MediaStore.Files.FileColumns.DATA + "=?",
                                new String[]{absolutePath});
                    }
                }
                fileSource.delete();
                context.sendBroadcast(new Intent(
                        Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri
                        .fromFile(new File(fileSource.getPath()))
                ));
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    // ----------------------------------------------------------------------------------------------------
    // decrypt and restore data to public directory
    public void decryptAndRestoreData(String destinationPath,
                                      String sourcePath, String fileName) {
        System.out.println("=====" + fileName);
        try {
            File fileSource = new File(sourcePath + "/" + fileName);
            int size = (int) fileSource.length();
            byte[] bytes = new byte[size];
            BufferedInputStream buf = new BufferedInputStream(
                    new FileInputStream(fileSource));
            buf.read(bytes, 0, bytes.length);
            buf.close();
            File file = new File(destinationPath, fileName);
            if (!file.exists())
                file.createNewFile();
            System.out.println("path of created file=" + file.getPath());
            byte[] encData = null;
            try {
                encData = this.objEncryptor.decrypt(bytes, false);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (file.exists()) {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                fileOutputStream.write(encData);
                fileOutputStream.close();
                fileSource.delete();
                context.sendBroadcast(new Intent(
                        Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri
                        .fromFile(new File(fileSource.getPath()))
                ));
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    // *****************************************************************
    // getting encoded data
    public byte[] getEncodedData(String path) throws Exception {
        File file = new File(path);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(
                    new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        byte[] decodeByte = this.objEncryptor.decrypt(bytes, false);

        return decodeByte;
    }
    // encrypt and save file
    public void encryptAndSave(byte[] data, String path, String fileName) {
        try {
            File file = new File(path, fileName);
            if (!file.exists()) {
                file.createNewFile();
                SharedPreferences settings = context.getSharedPreferences(
                        UserPreferencesKey.keyPrefName, 0);
                byte[] encData = null;
                encData = this.objEncryptor.encrypt(data, false);
                if (file.exists()) {
                    FileOutputStream fileOutputStream = new FileOutputStream(
                            file);
                    fileOutputStream.write(encData);
                    fileOutputStream.close();
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    // decrypt and retrieve data
    public byte[] decryptAndGet(String path, String fileName) {
        byte[] decData = null;
        try {
            File file = new File(path, fileName);
            if (file.exists()) {
                int num = 0;
                int sizeNameFile = file.getName().length();
                String finallChar = file.getName().charAt(sizeNameFile - 1) + "";
                try {
                    num = Integer.parseInt(finallChar);
                } catch (NumberFormatException e) {
                    num = 0;
                }
                this.objEncryptor.setDecryptAlgorithmComment(num);
                int size = (int) file.length();
                byte[] bytes = new byte[size];
                BufferedInputStream buf = new BufferedInputStream(
                        new FileInputStream(file));
                buf.read(bytes, 0, bytes.length);
                buf.close();
                decData = this.objEncryptor.decrypt(bytes, false);
            } else {
                System.out.println("file not exists");
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return decData;

    }
}
