package com.salvador.secureanymobileapps.documents;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Prefrences_Manager implements StrongBoxConstants {
    private Context context;
    private SharedPreferences strongBoxPref;
    private Editor editor;

    public Prefrences_Manager(Context ctx) {
        context = ctx;
        strongBoxPref = context.getSharedPreferences(PREF_NAME, PREF_MODE);
        editor = strongBoxPref.edit();
    }

    // various method to get value and set value to stronbox pre

    // validate first launch
    public boolean getFirstLaunch() {
        return strongBoxPref.getBoolean(IS_FIRST_LAUNCH, true);
    }

    public void setFirstLaunch(boolean  value) {
        editor.putBoolean(IS_FIRST_LAUNCH, value);
        editor.commit();
    }

    public boolean getFirstLaunchSms() {
        return strongBoxPref.getBoolean(IS_FIRST_LAUNCH_SMS, true);
    }

    public void setFirstLaunchSms(boolean  value) {
        editor.putBoolean(IS_FIRST_LAUNCH_SMS, value);
        editor.commit();
    }

    public boolean getFirstLaunchVoip() {
        return strongBoxPref.getBoolean(IS_FIRST_LAUNCH_VOIP, true);
    }

    public void setFirstLaunchVoip(boolean  value) {
        editor.putBoolean(IS_FIRST_LAUNCH_VOIP, value);
        editor.commit();
    }

    public boolean getFirstLaunchChat() {
        return strongBoxPref.getBoolean(IS_FIRST_LAUNCH__CHAT, true);
    }

    public void setFirstLaunchChat(boolean  value) {
        editor.putBoolean(IS_FIRST_LAUNCH__CHAT, value);
        editor.commit();
    }

    public boolean getFirstLaunchEmail() {
        return strongBoxPref.getBoolean(IS_FIRST_LAUNCH__EMAIL, true);
    }

    public void setFirstLaunchEmail(boolean  value) {
        editor.putBoolean(IS_FIRST_LAUNCH__EMAIL, value);
        editor.commit();
    }

    // passwords
    public String getPassword() {
        return strongBoxPref.getString(PASSWORD, null);
    }

    public void setPassword(String key) {
        editor.putString(PASSWORD, key);
        editor.commit();
    }

    public String getPasswordSms() {
        return strongBoxPref.getString(PASSWORD_SMS, null);
    }

    public void setPasswordSms(String key) {
        editor.putString(PASSWORD_SMS, key);
        editor.commit();
    }

    public String getPasswordVoip() {
        return strongBoxPref.getString(PASSWORD_VOIP, null);
    }

    public void setPasswordVoip(String key) {
        editor.putString(PASSWORD_VOIP, key);
        editor.commit();
    }

    public String getPasswordEmail() {
        return strongBoxPref.getString(PASSWORD_EMAIL, null);
    }

    public void setPasswordEmail(String key) {
        editor.putString(PASSWORD_EMAIL, key);
        editor.commit();
    }

    public String getPasswordChat() {
        return strongBoxPref.getString(PASSWORD_CHAT, null);
    }

    public void setPasswordChat(String key) {
        editor.putString(PASSWORD_CHAT, key);
        editor.commit();
    }

    // is locked
    public boolean getIsLocked() {
        return strongBoxPref.getBoolean(ISLOCKED, true);
    }

    public void setUnLocked() {
        editor.putBoolean(ISLOCKED, false);
        editor.commit();
    }

    public void setLocked() {
        editor.putBoolean(ISLOCKED, true);
        editor.commit();
    }

    public boolean getIsLockedSms() {
        return strongBoxPref.getBoolean(ISLOCKEDSMS, true);
    }

    public void setUnLockedSms() {
        editor.putBoolean(ISLOCKEDSMS, false);
        editor.commit();
    }

    public void setLockedSms() {
        editor.putBoolean(ISLOCKEDSMS, true);
        editor.commit();
    }

    public boolean getIsLockedVoip() {
        return strongBoxPref.getBoolean(ISLOCKEDVOIP, true);
    }

    public void setUnLockedVoip() {
        editor.putBoolean(ISLOCKEDVOIP, false);
        editor.commit();
    }

    public void setLockedVoip() {
        editor.putBoolean(ISLOCKEDVOIP, true);
        editor.commit();
    }

    public boolean getIsLockedChat() {
        return strongBoxPref.getBoolean(ISLOCKEDCHAT, true);
    }

    public void setUnLockedChat() {
        editor.putBoolean(ISLOCKEDCHAT, false);
        editor.commit();
    }

    public void setLockedChat() {
        editor.putBoolean(ISLOCKEDCHAT, true);
        editor.commit();
    }

    public boolean getIsLockedEmail() {
        return strongBoxPref.getBoolean(ISLOCKEDEMAIL, true);
    }

    public void setUnLockedEmail() {
        editor.putBoolean(ISLOCKEDEMAIL, false);
        editor.commit();
    }

    public void setLockedEmail() {
        editor.putBoolean(ISLOCKEDEMAIL, true);
        editor.commit();
    }

    public void setLockTime(String time) {
        editor.putString(LOCKTIME, time);
        editor.commit();
    }

    public String getLockTime() {
        return strongBoxPref.getString(LOCKTIME, null);
    }

    public void timerChecked(boolean bool) {
        editor.putBoolean(TIMER_CHECKED, bool);
        editor.commit();
    }
    public void Allclear() {
        editor.clear();
        editor.commit();
    }

    public boolean getTimerChecked() {
        return strongBoxPref.getBoolean(TIMER_CHECKED, false);
    }
}
