package com.salvador.secureanymobileapps.documents;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.salvador.secureanymobileapps.R;

public class MyHelp extends Activity {
	private Prefrences_Manager  prefrences_Manager;
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.docs_help);
	prefrences_Manager=new Prefrences_Manager(MyHelp.this);
}
@Override
public void onUserInteraction() {
	// TODO Auto-generated method stub
	if(prefrences_Manager.getTimerChecked())
	{
		SimmpleApp.resetDisconnectTimer();
	}
	else {
		SimmpleApp.stopDisconnectTimer();
	}
}
@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	super.onBackPressed();
	startActivity(new Intent(MyHelp.this,DocsMainActivity.class));
	finish();
}
}
