package com.salvador.secureanymobileapps.documents;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.documents.encrypt.decrypt.SimppleFileUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("NewApi")
public class MyMembership extends Activity implements StrongBoxConstants {
	private String pathMembership = "",trashPath = "";
	private List<String> fileList;
	private ListView listView;
	private Prefrences_Manager prefrences_Manager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.docs_mymembership);
		listView = (ListView) findViewById(R.id.listViewFinance);
		listView.setFastScrollEnabled(true);
		pathMembership = SimmpleApp.simmpleBoxPath + "/Membership";
		fileList = new ArrayList<String>();
		prefrences_Manager=new Prefrences_Manager(MyMembership.this);
		trashPath = SimmpleApp.simmpleBoxPath + "/Trash";
		System.out.println("files list=" + fileList);
		listView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
			final TextView textView=(TextView) arg1.findViewById(R.id.dirName);
			System.out.println("file name="+textView.getText().toString());
			final Dialog dialog=new Dialog(MyMembership.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.docs_custom);
			dialog.findViewById(R.id.deleteTextView).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					new SimppleFileUtil(MyMembership.this).moveToTrash(pathMembership, trashPath + "/Membership",
							textView.getText().toString());
					Toast.makeText(getApplicationContext(), "File moved to trash",
							Toast.LENGTH_SHORT).show();
					getAllFiles(pathMembership);
					dialog.dismiss();
					
				}
			});
			dialog.show();
				return true;
			}
		});
	}
	@Override
	public void onUserInteraction() {
		// TODO Auto-generated method stub
		if(prefrences_Manager.getTimerChecked())
		{
			SimmpleApp.resetDisconnectTimer();
		}
		else {
			SimmpleApp.stopDisconnectTimer();
		}
	}
	private void getAllFiles(String paath) {
		fileList.clear();
		fileList = new SimppleFileUtil(MyMembership.this).getFileList(paath);
		listView.setAdapter(new MyAdapter());
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getAllFiles(pathMembership);
	}
	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		startActivity(new Intent(MyMembership.this, MainView.class));
		finish();
	}
	public void goHome(View view) {
		startActivity(new Intent(MyMembership.this, DocsMainActivity.class));
		finish();
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addFile(View view) {

		final Dialog dialog = new Dialog(MyMembership.this);
		dialog.setTitle("Select a type");
		dialog.setContentView(R.layout.docs_dialog_layout2);
		ListView listView = (ListView) dialog.findViewById(R.id.listViewDialog);
		listView.setAdapter(new ArrayAdapter(MyMembership.this,
				android.R.layout.simple_list_item_1, getResources()
						.getStringArray(R.array.options)));
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				TextView textView = (TextView) arg1
						.findViewById(android.R.id.text1);
				String type = textView.getText().toString();
				if (type.equals("Password")) {
					startActivity(new Intent(MyMembership.this, PasswordDoc.class)
							.putExtra("pathToSave", pathMembership));
					dialog.dismiss();
				} else if (type.equals("Master Card")) {
					Intent intent = new Intent(MyMembership.this,
							MasterCardDoc.class);
					intent.putExtra("pathToSave", pathMembership);
					intent.putExtra("from", "Master_card");
					startActivity(intent);
					dialog.dismiss();
				} else if (type.equals("VISA")) {
					Intent intent = new Intent(MyMembership.this,
							MasterCardDoc.class);
					intent.putExtra("pathToSave", pathMembership);
					intent.putExtra("from", "Visa");
					startActivity(intent);
					dialog.dismiss();
				} else if (type.equals("Maestro")) {
					Intent intent = new Intent(MyMembership.this,
							MasterCardDoc.class);
					intent.putExtra("pathToSave", pathMembership);
					intent.putExtra("from", "Maestro");
					startActivity(intent);
					dialog.dismiss();
				} else if (type.equals("Bank Account")) {
					startActivity(new Intent(MyMembership.this, BankAccDoc.class)
							.putExtra("pathToSave", pathMembership));
					dialog.dismiss();
				} else if (type.equals("Home Bank")) {
					startActivity(new Intent(MyMembership.this, HomeBankDoc.class)
							.putExtra("pathToSave", pathMembership));
					dialog.dismiss();
				} else if (type.equals("Membership Card")) {
					startActivity(new Intent(MyMembership.this,
							MembershipDoc.class).putExtra("pathToSave",
							pathMembership));
					dialog.dismiss();
				} else if (type.equals("Email Account")) {
					startActivity(new Intent(MyMembership.this, EmailDoc.class)
							.putExtra("pathToSave", pathMembership));
					dialog.dismiss();
				} else if (type.equals("Contact")) {
					startActivity(new Intent(MyMembership.this, ContactDoc.class)
							.putExtra("pathToSave", pathMembership));
					dialog.dismiss();
				} else if (type.equals("Website")) {
					startActivity(new Intent(MyMembership.this, WebsiteDoc.class)
							.putExtra("pathToSave", pathMembership));
					dialog.dismiss();
				} else if (type.equals("Other")) {
					Toast.makeText(MyMembership.this, "Still to implement!", Toast.LENGTH_SHORT).show();
				}
			}
		});
		dialog.show();

	}

	private class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return fileList.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int arg0, View convertView, ViewGroup arg2) {
			View view = convertView;
			final Holder holder;
			if (view == null) {
				view = LayoutInflater.from(MyMembership.this).inflate(
						R.layout.docs_custom_trashitem, null);
				holder = new Holder();
				holder.dirName = (TextView) view.findViewById(R.id.dirName);
				view.setTag(holder);
			} else {
				holder = (Holder) view.getTag();
			}
			String filename = fileList.get(arg0);

			holder.dirName.setText(fileList.get(arg0));
			holder.dirName.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					String fileName = holder.dirName.getText().toString();
					EditText editText1, editText2, editText3, editText4, editText5, editText6, editText7, editText8, editText9, editText10, editText11;
					String getData = new String(new SimppleFileUtil(MyMembership.this)
							.decryptAndGet(pathMembership, fileName));
					Dialog dialog = new Dialog(MyMembership.this,
							android.R.style.Theme_Black_NoTitleBar_Fullscreen);
					String extension = fileName.substring(fileName
							.lastIndexOf("."));
					if (extension.equals(PASSWORD_FILE_EXTENSION)) {
						dialog.setContentView(R.layout.docs_password_layout);
						editText1 = (EditText) dialog
								.findViewById(R.id.editText1);
						editText2 = (EditText) dialog
								.findViewById(R.id.editText2);
						editText3 = (EditText) dialog
								.findViewById(R.id.editText3);
						editText4 = (EditText) dialog
								.findViewById(R.id.editText4);
						editText5 = (EditText) dialog
								.findViewById(R.id.editText5);
						dialog.findViewById(R.id.button1).setVisibility(
								View.GONE);
						try {
							JSONObject jsonObject = new JSONObject(getData);
							editText1.setText(jsonObject.getString("name"));
							editText2.setText(jsonObject.getString("username"));
							editText3.setText(jsonObject.getString("password"));
							editText4.setText(jsonObject.getString("access"));
							editText5.setText(jsonObject.getString("other"));
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					} else if (extension.equals(MASTERCARD_FILE_EXTENSION)) {
						dialog.setContentView(R.layout.docs_master_card_layout);
						editText1 = (EditText) dialog
								.findViewById(R.id.editText1);
						editText2 = (EditText) dialog
								.findViewById(R.id.editText2);
						editText3 = (EditText) dialog
								.findViewById(R.id.editText3);
						editText4 = (EditText) dialog
								.findViewById(R.id.editText4);
						editText5 = (EditText) dialog
								.findViewById(R.id.editText5);
						editText6 = (EditText) dialog
								.findViewById(R.id.editText6);
						editText7 = (EditText) dialog
								.findViewById(R.id.editText7);
						editText8 = (EditText) dialog
								.findViewById(R.id.editText8);
						editText9 = (EditText) dialog
								.findViewById(R.id.editText9);
						editText10 = (EditText) dialog
								.findViewById(R.id.editText10);
						editText11 = (EditText) dialog
								.findViewById(R.id.editText11);
						dialog.findViewById(R.id.button1).setVisibility(
								View.GONE);
						try {
							JSONObject jsonObject = new JSONObject(getData);
							editText1.setText(jsonObject.getString("name"));
							editText2.setText(jsonObject.getString("iban"));
							editText3.setText(jsonObject.getString("card_num"));
							editText4.setText(jsonObject.getString("pin"));
							editText5.setText(jsonObject.getString("cvv"));
							editText6.setText(jsonObject.getString("bank"));
							editText7.setText(jsonObject.getString("f_name"));
							editText8.setText(jsonObject.getString("l_name"));
							editText9.setText(jsonObject
									.getString("start_date"));
							editText10.setText(jsonObject
									.getString("expiry_date"));
							editText11.setText(jsonObject
									.getString("call_if_loast"));
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					} else if (extension.equals(VISA_FILE_EXTENSION)) {
						dialog.setContentView(R.layout.docs_master_card_layout);
						editText1 = (EditText) dialog
								.findViewById(R.id.editText1);
						editText2 = (EditText) dialog
								.findViewById(R.id.editText2);
						editText3 = (EditText) dialog
								.findViewById(R.id.editText3);
						editText4 = (EditText) dialog
								.findViewById(R.id.editText4);
						editText5 = (EditText) dialog
								.findViewById(R.id.editText5);
						editText6 = (EditText) dialog
								.findViewById(R.id.editText6);
						editText7 = (EditText) dialog
								.findViewById(R.id.editText7);
						editText8 = (EditText) dialog
								.findViewById(R.id.editText8);
						editText9 = (EditText) dialog
								.findViewById(R.id.editText9);
						editText10 = (EditText) dialog
								.findViewById(R.id.editText10);
						editText11 = (EditText) dialog
								.findViewById(R.id.editText11);
						dialog.findViewById(R.id.button1).setVisibility(
								View.GONE);
						try {
							JSONObject jsonObject = new JSONObject(getData);
							editText1.setText(jsonObject.getString("name"));
							editText2.setText(jsonObject.getString("iban"));
							editText3.setText(jsonObject.getString("card_num"));
							editText4.setText(jsonObject.getString("pin"));
							editText5.setText(jsonObject.getString("cvv"));
							editText6.setText(jsonObject.getString("bank"));
							editText7.setText(jsonObject.getString("f_name"));
							editText8.setText(jsonObject.getString("l_name"));
							editText9.setText(jsonObject
									.getString("start_date"));
							editText10.setText(jsonObject
									.getString("expiry_date"));
							editText11.setText(jsonObject
									.getString("call_if_loast"));
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					} else if (extension.equals(MAESTRO_FILE_EXTENSION)) {
						dialog.setContentView(R.layout.docs_master_card_layout);
						editText1 = (EditText) dialog
								.findViewById(R.id.editText1);
						editText2 = (EditText) dialog
								.findViewById(R.id.editText2);
						editText3 = (EditText) dialog
								.findViewById(R.id.editText3);
						editText4 = (EditText) dialog
								.findViewById(R.id.editText4);
						editText5 = (EditText) dialog
								.findViewById(R.id.editText5);
						editText6 = (EditText) dialog
								.findViewById(R.id.editText6);
						editText7 = (EditText) dialog
								.findViewById(R.id.editText7);
						editText8 = (EditText) dialog
								.findViewById(R.id.editText8);
						editText9 = (EditText) dialog
								.findViewById(R.id.editText9);
						editText10 = (EditText) dialog
								.findViewById(R.id.editText10);
						editText11 = (EditText) dialog
								.findViewById(R.id.editText11);
						dialog.findViewById(R.id.button1).setVisibility(
								View.GONE);
						try {
							JSONObject jsonObject = new JSONObject(getData);
							editText1.setText(jsonObject.getString("name"));
							editText2.setText(jsonObject.getString("iban"));
							editText3.setText(jsonObject.getString("card_num"));
							editText4.setText(jsonObject.getString("pin"));
							editText5.setText(jsonObject.getString("cvv"));
							editText6.setText(jsonObject.getString("bank"));
							editText7.setText(jsonObject.getString("f_name"));
							editText8.setText(jsonObject.getString("l_name"));
							editText9.setText(jsonObject
									.getString("start_date"));
							editText10.setText(jsonObject
									.getString("expiry_date"));
							editText11.setText(jsonObject
									.getString("call_if_loast"));
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					} else if (extension.equals(BANKACC_FILE_EXTENSION)) {
						dialog.setContentView(R.layout.docs_bank_acc_layout);
						editText1 = (EditText) dialog
								.findViewById(R.id.editText1);
						editText2 = (EditText) dialog
								.findViewById(R.id.editText2);
						editText3 = (EditText) dialog
								.findViewById(R.id.editText3);
						editText4 = (EditText) dialog
								.findViewById(R.id.editText4);
						editText5 = (EditText) dialog
								.findViewById(R.id.editText5);
						editText6 = (EditText) dialog
								.findViewById(R.id.editText6);
						editText7 = (EditText) dialog
								.findViewById(R.id.editText7);
						editText8 = (EditText) dialog
								.findViewById(R.id.editText8);
						dialog.findViewById(R.id.button1).setVisibility(
								View.GONE);
						try {
							JSONObject jsonObject = new JSONObject(getData);
							editText1.setText(jsonObject.getString("name"));
							editText2.setText(jsonObject.getString("bank"));
							editText3.setText(jsonObject.getString("acc_num"));
							editText4.setText(jsonObject.getString("iban"));
							editText5.setText(jsonObject.getString("branch"));
							editText6.setText(jsonObject.getString("swift"));
							editText7.setText(jsonObject.getString("phone"));
							editText8.setText(jsonObject.getString("note"));
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					} else if (extension.equals(HOMEBANK_FILE_EXTENSION)) {
						dialog.setContentView(R.layout.docs_home_bank_layout);
						editText1 = (EditText) dialog
								.findViewById(R.id.editText1);
						editText2 = (EditText) dialog
								.findViewById(R.id.editText2);
						editText3 = (EditText) dialog
								.findViewById(R.id.editText3);
						editText4 = (EditText) dialog
								.findViewById(R.id.editText4);
						editText5 = (EditText) dialog
								.findViewById(R.id.editText5);
						dialog.findViewById(R.id.button1).setVisibility(
								View.GONE);
						try {
							JSONObject jsonObject = new JSONObject(getData);
							editText1.setText(jsonObject.getString("name"));
							editText2.setText(jsonObject.getString("bank"));
							editText3.setText(jsonObject.getString("user_code"));
							editText4.setText(jsonObject.getString("pin"));
							editText5.setText(jsonObject
									.getString("call_if_lost"));
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					} else if (extension.equals(MEMCARD_FILE_EXTENSION)) {
						dialog.setContentView(R.layout.docs_membership_card_layout);
						editText1 = (EditText) dialog
								.findViewById(R.id.editText1);
						editText2 = (EditText) dialog
								.findViewById(R.id.editText2);
						editText3 = (EditText) dialog
								.findViewById(R.id.editText3);
						editText4 = (EditText) dialog
								.findViewById(R.id.editText4);
						editText5 = (EditText) dialog
								.findViewById(R.id.editText5);
						dialog.findViewById(R.id.button1).setVisibility(
								View.GONE);
						try {
							JSONObject jsonObject = new JSONObject(getData);
							editText1.setText(jsonObject.getString("name"));
							editText2.setText(jsonObject.getString("company"));
							editText3.setText(jsonObject.getString("number"));
							editText4.setText(jsonObject.getString("expires"));
							editText5.setText(jsonObject.getString("phone"));
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					} else if (extension.equals(EMAIL_FILE_EXTENSION)) {
						dialog.setContentView(R.layout.docs_email_acc_layout);
						editText1 = (EditText) dialog
								.findViewById(R.id.editText1);
						editText2 = (EditText) dialog
								.findViewById(R.id.editText2);
						editText3 = (EditText) dialog
								.findViewById(R.id.editText3);
						editText4 = (EditText) dialog
								.findViewById(R.id.editText4);
						editText5 = (EditText) dialog
								.findViewById(R.id.editText5);
						dialog.findViewById(R.id.button1).setVisibility(
								View.GONE);
						try {
							JSONObject jsonObject = new JSONObject(getData);
							editText1.setText(jsonObject.getString("name"));
							editText2.setText(jsonObject.getString("login"));
							editText3.setText(jsonObject.getString("passwd"));
							editText4.setText(jsonObject.getString("pop3"));
							editText5.setText(jsonObject.getString("imap"));
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					} else if (extension.equals(CONTACT_FILE_EXTENSION)) {
						dialog.setContentView(R.layout.docs_contact_layout);
						editText1 = (EditText) dialog
								.findViewById(R.id.editText1);
						editText2 = (EditText) dialog
								.findViewById(R.id.editText2);
						editText3 = (EditText) dialog
								.findViewById(R.id.editText3);
						editText4 = (EditText) dialog
								.findViewById(R.id.editText4);
						editText5 = (EditText) dialog
								.findViewById(R.id.editText5);
						editText6 = (EditText) dialog
								.findViewById(R.id.editText6);
						editText7 = (EditText) dialog
								.findViewById(R.id.editText7);
						editText8 = (EditText) dialog
								.findViewById(R.id.editText8);
						editText9 = (EditText) dialog
								.findViewById(R.id.editText9);
						editText10 = (EditText) dialog
								.findViewById(R.id.editText10);
						editText11 = (EditText) dialog
								.findViewById(R.id.editText11);
						dialog.findViewById(R.id.button1).setVisibility(
								View.GONE);
						try {
							JSONObject jsonObject = new JSONObject(getData);
							editText1.setText(jsonObject.getString("name"));
							editText2.setText(jsonObject
									.getString("work_phone"));
							editText3.setText(jsonObject.getString("yahoo"));
							editText4.setText(jsonObject.getString("skype"));
							editText5.setText(jsonObject.getString("email"));
							editText6.setText(jsonObject.getString("company"));
							editText7.setText(jsonObject.getString("address"));
							editText8.setText(jsonObject.getString("city"));
							editText9.setText(jsonObject.getString("state"));
							editText10.setText(jsonObject.getString("country"));
							editText11.setText(jsonObject.getString("zipcode"));
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					} else if (extension.equals(WEBSITE_FILE_EXTENSION)) {
						dialog.setContentView(R.layout.docs_website_layout);
						editText1 = (EditText) dialog
								.findViewById(R.id.editText1);
						editText2 = (EditText) dialog
								.findViewById(R.id.editText2);
						editText3 = (EditText) dialog
								.findViewById(R.id.editText3);
						editText4 = (EditText) dialog
								.findViewById(R.id.editText4);
						dialog.findViewById(R.id.button1).setVisibility(
								View.GONE);
						try {
							JSONObject jsonObject = new JSONObject(getData);
							editText1.setText(jsonObject.getString("name"));
							editText2.setText(jsonObject.getString("url"));
							editText3.setText(jsonObject.getString("user_name"));
							editText4.setText(jsonObject
									.getString("regtd_email"));
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					}
					dialog.show();
				}
			});
			return view;
		}

	}

	private static class Holder {
		TextView dirName;
	}
}
