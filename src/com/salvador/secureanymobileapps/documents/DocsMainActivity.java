package com.salvador.secureanymobileapps.documents;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.ChatHistory;
import com.salvador.secureanymobileapps.chat.LoginActivity;
import com.salvador.secureanymobileapps.k9.activity.Accounts;
import com.salvador.secureanymobileapps.k9.activity.setup.AccountSetupBasics;
import com.salvador.secureanymobileapps.sms.MsgList;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainMenu;
import com.salvador.secureanymobileapps.voip.VoipActivity;

import java.io.File;

public class DocsMainActivity extends BaseActivity implements OnClickListener {
    //	private byte[] my=null;
    private Prefrences_Manager prefrences_Manager;
    private SharedPreferences isTextNull;
    private Button back;
    private Button lock;
    private Button unLock;
    int count=10;
   public static boolean isFirstLaunch = false;
    public static  boolean isLocked = false;
    private String isSms;
    private String isDocuments;
    private String isVoip;
    private String isChat;
    private String isEmail;
    private TextView title;
    private int activeFunctionsView;
    String acc="";
    private ApplicationClass appd;


    @Override
    public void onCreate(Bundle savedInstanceState) {
//
        super.onCreate(savedInstanceState);
        setContentView(R.layout.docs_activity_main);
//
        prefrences_Manager = new Prefrences_Manager(DocsMainActivity.this);
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        this.appd = (ApplicationClass) getApplicationContext();
        try {

            ApplicationClass.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                    .setLabel("DocsMain Activity")
                    .build());
            acc=getIntent().getExtras().getString("account");
//            if(acc.equals("null")){
//                acc="2";
//            }
            Log.d("DocsActivity","aaaaa"+acc);
//            acc = getIntent().getExtras().getString("account");
        }catch (Exception e){

        }

        ApplicationClass.context = DocsMainActivity.this;
        createAppDirectory();
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
        this.unLock = (Button) findViewById(R.id.button3);
        this.unLock.setOnClickListener(this);
        this.lock = (Button) findViewById(R.id.button5);
        this.lock.setOnClickListener(this);
        title = (TextView) findViewById(R.id.TitleView);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("Validate");
            if (value.equals("sms")) {
                title.setText("SMS");
                isSms = value;
                isDocuments = null;
                isVoip = null;
                isChat = null;
                isEmail = null;
            } else if (value.equals("documents")) {
                title.setText("Documents");
                isSms = null;
                isDocuments = value;
                isVoip = null;
                isChat = null;
                isEmail = null;
            } else if (value.equals("chat")) {
                title.setText("Chat");
                isSms = null;
                isDocuments = null;
                isVoip = null;
                isChat = value;
                isEmail = null;
            } else if (value.equals("voip")) {
                title.setText("Voip");
                isSms = null;
                isDocuments = null;
                isVoip = value;
                isChat = null;
                isEmail = null;
            } else if (value.equals("email")) {
                title.setText("Email");
                isSms = null;
                isDocuments = null;
                isVoip = null;
                isChat = null;
                isEmail = value;
            }
        }
    }
//
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent in = new Intent(DocsMainActivity.this, MainMenu.class);
        in.putExtra("account",acc);
        startActivity(in);
        overridePendingTransition(0, 0);
        finish();
    }

    @Override
    public void onResume() {

        super.onResume();


        if(isFinishing()) {
            Intent intent = new Intent(DocsMainActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }
    @Override
    public void onClick(View view) {
        String tag = view.getTag() + "";

        if (tag.equals("back")) {
            onBackPressed();
        } else if (tag.equals("unlockBox")) {
            unlockBox(view);
        }
        else if (tag.equals("lockBox")) {
            lockBox(view);
        }
    }


    private void createAppDirectory() {
        // TODO Auto-generated method stub
        String state = Environment.getExternalStorageState();
        Log.d("Media State", state);
        try {
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                File mydir = new File(Environment.getExternalStorageDirectory() + "/Android/obb/com.salvador.secureanymobileapps/");
                SimmpleApp.simmpleBoxPath = mydir.getPath();
                if (!mydir.exists()) {
                    mydir.mkdirs();
                    File documentFile = new File(mydir.getPath() + "/Documents");
                    if (!documentFile.exists()) {
                        documentFile.mkdirs();
                    }
                    File passwordsFile = new File(mydir.getPath() + "/Passwords");
                    if (!passwordsFile.exists()) {
                        passwordsFile.mkdirs();
                    }
                    File financialFile = new File(mydir.getPath() + "/Financial");
                    if (!financialFile.exists()) {
                        financialFile.mkdirs();
                    }
                    File galleryFile = new File(mydir.getPath() + "/Gallery");
                    if (!galleryFile.exists()) {
                        galleryFile.mkdirs();
                    }
                    File MembershipFile = new File(mydir.getPath() + "/Membership");
                    if (!MembershipFile.exists()) {
                        MembershipFile.mkdirs();
                    }
                    File otherFile = new File(mydir.getPath() + "/Other");
                    if (!otherFile.exists()) {
                        otherFile.mkdirs();
                    }
                    File trashFile = new File(mydir.getPath() + "/Trash");
                    if (!trashFile.exists()) {
                        trashFile.mkdirs();
                    }

                }

            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public void onUserInteraction() {
        // TODO Auto-generated method stub
        if (prefrences_Manager.getTimerChecked()) {
            SimmpleApp.resetDisconnectTimer();
        } else {
            SimmpleApp.stopDisconnectTimer();
        }
    }


    public void unlockBox(View view) {
        System.out.println("is first launch=" + prefrences_Manager.getFirstLaunch());
        this.activeFunctionsView = 0;

        if (isSms != null && isSms.equals("sms")) {
            isFirstLaunch = prefrences_Manager.getFirstLaunchSms();
            isLocked = isTextNull.getBoolean("lock",false);
            this.activeFunctionsView = 1;
        } else if (isDocuments != null && isDocuments.equals("documents")) {
            isFirstLaunch = prefrences_Manager.getFirstLaunch();
            isLocked = isTextNull.getBoolean("lock", false);
            this.activeFunctionsView = 2;
        } else if (isVoip != null && isVoip.equals("voip")) {
            isFirstLaunch = prefrences_Manager.getFirstLaunchVoip();
            isLocked = isTextNull.getBoolean("lock", false);
            this.activeFunctionsView = 3;
        } else if (isChat != null && isChat.equals("chat")) {
            isFirstLaunch = prefrences_Manager.getFirstLaunchChat();
            isLocked = isTextNull.getBoolean("lock", false);
            this.activeFunctionsView = 4;
        } else if (isEmail != null && isEmail.equals("email")) {
            isFirstLaunch = prefrences_Manager.getFirstLaunchEmail();
            isLocked = isTextNull.getBoolean("lock",false);
            this.activeFunctionsView = 5;
        }

        if (isFirstLaunch) {

            final Dialog dialog = new Dialog(DocsMainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialog.setContentView(R.layout.docs_dialog_layout);
            RelativeLayout containerPopupUnlock = (RelativeLayout) dialog.findViewById(R.id.containerPopupUnlock);
            Button button = (Button) dialog.findViewById(R.id.button_create_passwd);
            final ImageView strengthMeasure = (ImageView) dialog.findViewById(R.id.PD_strengthView);
            final TextView pet = (TextView) dialog.findViewById((R.id.PD_strengthText));
            final EditText editText = (EditText) dialog.findViewById(R.id.editText1);
            TextView text = (TextView) dialog.findViewById(R.id.tv11);
            final EditText editText1 = (EditText) dialog.findViewById(R.id.editText2);
            TextView text2 = (TextView) dialog.findViewById(R.id.tv12);
            //this.applyThemesFirtsLaunch(containerPopupUnlock, pet, text, text2, editText, editText1);
            editText.addTextChangedListener((new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    strengthMeasure.setImageResource(PasswordAttributes.getSMImageID(PasswordAttributes.checkPasswordStrengthWeight(s.toString())));
                    pet.setText(PasswordAttributes.getCommentID(PasswordAttributes.checkPasswordStrengthWeight(s.toString())));
                }
            }));
            button.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (editText.getText().toString().length() != 0) {

                        String pass1 = editText.getText().toString().trim();
                        String pass2 = editText1.getText().toString().trim();
                        if(pass1.length()<=3){
                            Toast.makeText(DocsMainActivity.this,"Password should be 4 characters",Toast.LENGTH_SHORT).show();
                        }
                        else if (pass1.equals(pass2)) {

                            if (activeFunctionsView == 1) {
                                prefrences_Manager.setPasswordSms(pass1);
                                prefrences_Manager.setFirstLaunchSms(false);
                            } else if (activeFunctionsView == 2) {
                                prefrences_Manager.setPassword(pass1);
                                prefrences_Manager.setFirstLaunch(false);
                            } else if (activeFunctionsView == 3) {
                                prefrences_Manager.setPasswordVoip(pass1);
                                prefrences_Manager.setFirstLaunchVoip(false);
                            } else if (activeFunctionsView == 4) {
                                prefrences_Manager.setPasswordChat(pass1);
                                prefrences_Manager.setFirstLaunchChat(false);
                            } else if (activeFunctionsView == 5) {
                                prefrences_Manager.setPasswordEmail(pass1);
                                prefrences_Manager.setFirstLaunchEmail(false);
                            }

                            dialog.dismiss();
                        } else {
                            Toast toast = Toast.makeText(DocsMainActivity.this.getApplicationContext(), "Password does not match ", Toast.LENGTH_SHORT);
                            toast.show();
                        }

                    }
                }
            });
            dialog.show();
        }

        Log.i(TAG, "unlockBox: "+isLocked);
        if (isFirstLaunch == false && isLocked == true) {
            final Dialog dialog = new Dialog(DocsMainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialog.setContentView(R.layout.docs_dialog_layout1);
            //ThemeSetter.OverrideFonts(this, findViewById(android.R.id.content));
            RelativeLayout containerPopupUnlock = (RelativeLayout) dialog.findViewById(R.id.containerPopupUnlock);
            TextView text = (TextView) dialog.findViewById(R.id.tv12);
            Button button = (Button) dialog.findViewById(R.id.button_create_passwd);
            final EditText editText = (EditText) dialog.findViewById(R.id.editText1);
            //this.applyThemerUnlock(containerPopupUnlock, text, button, editText);
            button.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (editText.getText().toString().length() != 0) {
                        String string = editText.getText().toString().trim();
                        String string2 = null;
                        if (activeFunctionsView == 1) {
                            string2 = prefrences_Manager.getPasswordSms();
                        } else if (activeFunctionsView == 2) {
                            string2 = prefrences_Manager.getPassword();
                        } else if (activeFunctionsView == 3) {
                            string2 = prefrences_Manager.getPasswordVoip();
                        } else if (activeFunctionsView == 4) {
                            string2 = prefrences_Manager.getPasswordChat();
                        }  else if (activeFunctionsView == 5) {
                            string2 = prefrences_Manager.getPasswordEmail();
                        }
                        if (!string.equals(string2)) {
                            editText.setError("Wrong Password");
                        } else {

                            if (activeFunctionsView == 1) {
                                prefrences_Manager.setUnLockedSms();
                            } else if (activeFunctionsView == 2) {
                                prefrences_Manager.setUnLocked();
                            } else if (activeFunctionsView == 3) {
                                prefrences_Manager.setUnLockedVoip();
                            } else if (activeFunctionsView == 4) {
                                prefrences_Manager.setUnLockedChat();
                            }  else if (activeFunctionsView == 5) {
                                prefrences_Manager.setUnLockedEmail();
                            }
                            Toast.makeText(DocsMainActivity.this, "Box Unlocked", Toast.LENGTH_SHORT).show();
                            if (activeFunctionsView == 1) {
                                startActivity(new Intent(DocsMainActivity.this, MsgList.class));
                            } else if (activeFunctionsView == 2) {
                                startActivity(new Intent(DocsMainActivity.this, MainView.class));
                            } else if (activeFunctionsView == 3) {
                                Intent intent = new Intent(DocsMainActivity.this, VoipActivity.class);
                                intent.putExtra("call", "no");
                                startActivity(intent);
                            } else if (activeFunctionsView == 4) {
                                startActivity(new Intent(DocsMainActivity.this, ChatHistory.class));
                            }  else if (activeFunctionsView == 5) {
                                try {
                                    if (acc.equals("1")||acc.equals("")) {
                                        Intent intent = new Intent(DocsMainActivity.this, AccountSetupBasics.class);
                                        startActivity(intent);
//                                        finish();
                                    } else {
                                        Intent intent = new Intent(DocsMainActivity.this, Accounts.class);
                                        startActivity(intent);
//                                        finish();
                                    }
                                }catch (Exception e){
                                    Log.d("Docsmain","getMessage"+e.getMessage());
                                    e.printStackTrace();
                                }


                            }
                            dialog.dismiss();
                            finish();
                        }
                    }
                }
            });
            dialog.show();
        }
        if (isFirstLaunch == false && isLocked == false) {
               if (activeFunctionsView == 1) {
                if(!MainMenu.isSmss()){

                    if(!MainMenu.isSmsPwd()){
                        //MainMenu.setSmsPwd(true);
                        dialogView();
                    }
                    else
                    {
                        changeView();
                    }
                }
                else{
                    dialogView();
                }
            }

            if (activeFunctionsView == 4) {
                if(!MainMenu.isChats()){

                    if(!MainMenu.isChatPwd()){
                        //MainMenu.setChatPwd(true);
                        dialogView();
                    }
                    else
                    {
                        changeView();
                    }
                }
                else{
                    dialogView();
                }
            }
            if (activeFunctionsView == 3) {
                if(!MainMenu.isVoips()){

                    if(!MainMenu.isVoipPwd()){
                        //MainMenu.setChatPwd(true);
                        dialogView();
                    }
                    else
                    {
                        changeView();
                    }
                }
                else{
                    dialogView();
                }
            }
            if (activeFunctionsView == 2) {
                if(!MainMenu.isDocs()){

                    if(!MainMenu.isDocPwd()){
                        //MainMenu.setChatPwd(true);
                        dialogView();
                    }
                    else
                    {
                        changeView();
                    }
                }
                else{
                    dialogView();
                }
            }
            if (activeFunctionsView == 5) {
                if(!MainMenu.isEmails()){

                    if(!MainMenu.isEmailPwd()){
                        //MainMenu.setChatPwd(true);
                        dialogView();
                    }
                    else
                    {
                        changeView();
                    }
                }
                else{
                    dialogView();
                }
            }
        }
    }
    private void dialogView(){
        final Dialog dialog = new Dialog(DocsMainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.docs_dialog_layout1);
        //ThemeSetter.OverrideFonts(this, findViewById(android.R.id.content));
        RelativeLayout containerPopupUnlock = (RelativeLayout) dialog.findViewById(R.id.containerPopupUnlock);
        TextView text = (TextView) dialog.findViewById(R.id.tv12);
        Button button = (Button) dialog.findViewById(R.id.button_create_passwd);
        final EditText editText = (EditText) dialog.findViewById(R.id.editText1);
        //this.applyThemerUnlock(containerPopupUnlock, text, button, editText);
        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (editText.getText().toString().length() != 0) {
                    String string = editText.getText().toString().trim();
                    String string2 = null;
                    if (activeFunctionsView == 1) {
                        string2 = prefrences_Manager.getPasswordSms();
                    } else if (activeFunctionsView == 2) {
                        string2 = prefrences_Manager.getPassword();
                    } else if (activeFunctionsView == 3) {
                        string2 = prefrences_Manager.getPasswordVoip();
                    } else if (activeFunctionsView == 4) {
                        string2 = prefrences_Manager.getPasswordChat();
                    }  else if (activeFunctionsView == 5) {
                        string2 = prefrences_Manager.getPasswordEmail();
                    }
                    if (!string.equals(string2)) {
                        editText.setError("Wrong Password");
                    } else {

                        if (activeFunctionsView == 1) {
                            prefrences_Manager.setUnLockedSms();
                        } else if (activeFunctionsView == 2) {
                            prefrences_Manager.setUnLocked();
                        } else if (activeFunctionsView == 3) {
                            prefrences_Manager.setUnLockedVoip();
                        } else if (activeFunctionsView == 4) {
                            prefrences_Manager.setUnLockedChat();
                        }  else if (activeFunctionsView == 5) {
                            prefrences_Manager.setUnLockedEmail();
                        }
                        Toast.makeText(DocsMainActivity.this, "Box Unlocked", Toast.LENGTH_SHORT).show();
                        if (activeFunctionsView == 1) {
                            startActivity(new Intent(DocsMainActivity.this, MsgList.class));

                            MainMenu.setSmss(true);
                            MainMenu.setChats(false);
                            MainMenu.setEmails(false);
                            MainMenu.setDocs(false);
                            MainMenu.setVoips(false);
                        } else if (activeFunctionsView == 2) {
                            startActivity(new Intent(DocsMainActivity.this, MainView.class));
                            MainMenu.setSmss(false);
                            MainMenu.setChats(false);
                            MainMenu.setEmails(false);
                            MainMenu.setDocs(true);
                            MainMenu.setVoips(false);
                        } else if (activeFunctionsView == 3) {
                            Intent intent = new Intent(DocsMainActivity.this, VoipActivity.class);
                            intent.putExtra("call", "no");
                            startActivity(intent);
                            MainMenu.setSmss(false);
                            MainMenu.setChats(false);
                            MainMenu.setEmails(false);
                            MainMenu.setDocs(false);
                            MainMenu.setVoips(true);

                        } else if (activeFunctionsView == 4) {
                            startActivity(new Intent(DocsMainActivity.this, ChatHistory.class));
                            MainMenu.setSmss(false);
                            MainMenu.setChats(true);
                            MainMenu.setEmails(false);
                            MainMenu.setDocs(false);
                            MainMenu.setVoips(false);
                        }  else if (activeFunctionsView == 5) {
                            try {
                                if (acc.equals("1")) {
                                    startActivity(new Intent(DocsMainActivity.this, AccountSetupBasics.class));
                                    MainMenu.setSmss(false);
                                    MainMenu.setChats(false);
                                    MainMenu.setEmails(true);
                                    MainMenu.setDocs(false);
                                    MainMenu.setVoips(false);

                                } else {
                                    startActivity(new Intent(DocsMainActivity.this, Accounts.class));
                                    MainMenu.setSmss(false);
                                    MainMenu.setChats(false);
                                    MainMenu.setEmails(true);
                                    MainMenu.setDocs(false);
                                    MainMenu.setVoips(false);
                                }
                            }catch (Exception e){
                                Log.d("Docsmain","getMessage"+e.getMessage());
                                e.printStackTrace();
                            }

                        }
                        dialog.dismiss();
                        finish();
                    }
                }
            }
        });
        dialog.show();
    }
    private  void  changeView(){
        if (activeFunctionsView == 1) {
            startActivity(new Intent(DocsMainActivity.this, MsgList.class));
        } else if (activeFunctionsView == 2) {
            startActivity(new Intent(DocsMainActivity.this, MainView.class));
        } else if (activeFunctionsView == 3) {
            Intent intent = new Intent(DocsMainActivity.this, VoipActivity.class);
            intent.putExtra("call", "no");
            startActivity(intent);
        } else if (activeFunctionsView == 4) {
            startActivity(new Intent(DocsMainActivity.this, ChatHistory.class));
        }
        else if (activeFunctionsView == 5) {
            try {
                if (acc.equals("1")) {
                    Intent intent = new Intent(DocsMainActivity.this, AccountSetupBasics.class);
                    startActivity(intent);
                finish();
                } else {
                    Intent intent = new Intent(DocsMainActivity.this, Accounts.class);
                    startActivity(intent);
                finish();
                }
            }catch (Exception e){
                Log.d("Docsmain","getMessage"+e.getMessage());
          e.printStackTrace();
            }
//            startActivity(new Intent(DocsMainActivity.this, ChatHistory.class));
        }
        finish();
    }

    public void lockBox(View view) {

        if (isSms != null && isSms.equals("sms")) {
            prefrences_Manager.setLockedSms();
        } else if (isDocuments != null && isDocuments.equals("documents")) {
            prefrences_Manager.setLocked();
        } else if (isVoip != null && isVoip.equals("voip")) {
            prefrences_Manager.setLockedVoip();
        } else if (isChat != null && isChat.equals("chat")) {
            prefrences_Manager.setLockedChat();
        } else if (isEmail != null && isEmail.equals("email")) {
            prefrences_Manager.setLockedEmail();
        }

        Toast.makeText(DocsMainActivity.this, "Box Locked", Toast.LENGTH_SHORT).show();
    }



    public void boxSetting(View view) {
        startActivity(new Intent(DocsMainActivity.this, MySetting.class));
        finish();
    }

    public void boxHelp(View view) {
        startActivity(new Intent(DocsMainActivity.this, MyHelp.class));
        finish();
    }


}
