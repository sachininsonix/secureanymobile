package com.salvador.secureanymobileapps.documents;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import static com.salvador.secureanymobileapps.tokenlogin.ApplicationClass.context;

@SuppressLint("HandlerLeak")
public class SimmpleApp extends Application implements StrongBoxConstants {
    public static String simmpleBoxPath = "";
    static long DISCONNECT_TIMEOUT;


    private static Handler disconnectHandler = new Handler() {
        public void handleMessage(Message msg) {
        }
    };


    @Override
    public void onCreate() {
        super.onCreate();

    }




    private static Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            // Perform any required operation on disconnect
            new Prefrences_Manager(context).setLocked();
            Intent intent = new Intent(context, DocsMainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
        }
    };

    public static void resetDisconnectTimer() {
        String string = new Prefrences_Manager(context).getLockTime();
        if (string != null) {
            DISCONNECT_TIMEOUT = Long.parseLong(string) * 60 * 1000;
        }
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, DISCONNECT_TIMEOUT);
    }

    public static void stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
    }
}
