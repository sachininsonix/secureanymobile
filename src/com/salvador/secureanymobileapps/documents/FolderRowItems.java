package com.salvador.secureanymobileapps.documents;

/**
 * Created by station12 on 9/10/14.
 */
public class FolderRowItems {

    private int imageFolder;
    private String nameFolder;


    public FolderRowItems(int imageFolderTmp, String namefolder) {
        this.imageFolder = imageFolderTmp;

        this.nameFolder = namefolder;
    }

    public void setiImageFolder(int typerImage) {
        this.imageFolder = typerImage;
    }


    public int getiImageFolder() {
        return imageFolder;
    }


    public void setiNameFolder(String nameFolder) {
        this.nameFolder = nameFolder;
    }


    public String getNameFolder() {
        return nameFolder;
    }

}
