package com.salvador.secureanymobileapps.documents;


import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.salvador.secureanymobileapps.R;

import java.util.List;

/**
 * Created by station12 on 9/10/14.
 */
public class adapterListFolder extends ArrayAdapter<FolderRowItems> {

    private Activity context;
    private List<FolderRowItems> dataFolder;
    private SharedPreferences isTextNull;

    public adapterListFolder(Activity context, List<FolderRowItems> list) {
        super(context, R.layout.folder_row_items, list);
        this.context = context;
        this.dataFolder = list;
    }

    public void adapterListFolderRender(Activity context, List<FolderRowItems> list) {
        this.context = context;
        this.dataFolder = list;
    }

    static class ViewHolder {
        protected TextView nameFolder = null;
        protected ImageView imgFolder = null;

    }

    @Override
    public int getCount() {
        return dataFolder.size();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            convertView = inflator.inflate(R.layout.folder_row_items, null);
            viewHolder = new ViewHolder();

            viewHolder.nameFolder = (TextView) convertView
                    .findViewById(R.id.nameFolder);

            viewHolder.imgFolder = (ImageView) convertView
                    .findViewById(R.id.imgfolder);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Resources resources = context.getResources();

        isTextNull = context.getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        FolderRowItems folderrow = this.dataFolder.get(position);
        viewHolder.nameFolder.setText(folderrow.getNameFolder());
        int typeIcon = Integer.parseInt(folderrow.getiImageFolder() + "");
        if (!isTextNull.getString("theme", "").equals("")) {
            if (isTextNull.getString("theme", "").equals("Other Theme")) {
                if (typeIcon == 1) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.fin_light);
                } else if (typeIcon == 2) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.group_light);
                } else if (typeIcon == 3) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.other_light);
                } else if (typeIcon == 4) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.galry_light);
                } else if (typeIcon == 5) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.document_light);
                } else if (typeIcon == 6) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.delete_light);
                } else if (typeIcon == 7) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.paswd_light);
                } else if (typeIcon == 0) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.fold_add_light);
                }

            } else {
//                viewHolder.nameFolder.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                if (typeIcon == 1) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.fin_blue);
                } else if (typeIcon == 2) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.group_blue);
                } else if (typeIcon == 3) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.other_blue);
                } else if (typeIcon == 4) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.galry_blue);
                } else if (typeIcon == 5) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.document_blue);
                } else if (typeIcon == 6) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.delete_blue);
                } else if (typeIcon == 7) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.paswd_blue);
                } else if (typeIcon == 0) {
                    viewHolder.imgFolder.setBackgroundResource(R.drawable.fold_add_blue);
                }
            }
        } else {
//            viewHolder.nameFolder.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
            if (typeIcon == 1) {
                viewHolder.imgFolder.setBackgroundResource(R.drawable.fin_blue);
            } else if (typeIcon == 2) {
                viewHolder.imgFolder.setBackgroundResource(R.drawable.group_blue);
            } else if (typeIcon == 3) {
                viewHolder.imgFolder.setBackgroundResource(R.drawable.other_blue);
            } else if (typeIcon == 4) {
                viewHolder.imgFolder.setBackgroundResource(R.drawable.galry_blue);
            } else if (typeIcon == 5) {
                viewHolder.imgFolder.setBackgroundResource(R.drawable.document_blue);
            } else if (typeIcon == 6) {
                viewHolder.imgFolder.setBackgroundResource(R.drawable.delete_blue);
            } else if (typeIcon == 7) {
                viewHolder.imgFolder.setBackgroundResource(R.drawable.paswd_blue);
            }  else if (typeIcon == 0) {
                viewHolder.imgFolder.setBackgroundResource(R.drawable.fold_add_blue);
            }
        }

        return convertView;
    }


}
