package com.salvador.secureanymobileapps.documents;

public interface StrongBoxConstants {
// pref constants
	public String PREF_NAME="strong_box";
	public int PREF_MODE=0;

	public String IS_FIRST_LAUNCH="is_first_launch";
	public String IS_FIRST_LAUNCH__FOLDERPASSWORD="is_first_launch_folderpassword";
    public String IS_FIRST_LAUNCH_SMS="is_first_launch_sms";
    public String IS_FIRST_LAUNCH_VOIP="is_first_launch_voip";
    public String IS_FIRST_LAUNCH__CHAT="is_first_launch_chat";
    public String IS_FIRST_LAUNCH__EMAIL="is_first_launch_email";

	public String PASSWORD="enter_key";
    public String PASSWORD_SMS="enter_key_sms";
    public String PASSWORD_VOIP="enter_key_voip";
    public String PASSWORD_CHAT="enter_key_chat";
    public String PASSWORD_EMAIL="enter_key_email";
    public String PASSWORD_FOLDERPASSWORD="enter_key_folderpassword";

	public String ISLOCKED="is_locked";
    public String ISLOCKEDSMS="is_locked_sms";
    public String ISLOCKEDVOIP="is_locked_voip";
    public String ISLOCKEDCHAT="is_locked_chat";
    public String ISLOCKEDEMAIL="is_locked_email";
    public String ISLOCKEDFOLDERPASSWORD="is_locked_folderpassword";

	public String LOCKTIME="locking_time";
	public String TIMER_CHECKED="is_timer_checked";
//	end
	public String PASSWORD_FILE_EXTENSION=".pswd";
	public String MASTERCARD_FILE_EXTENSION=".bankcard";
	public String VISA_FILE_EXTENSION=".visa";
	public String MAESTRO_FILE_EXTENSION=".maestro";
	public String BANKACC_FILE_EXTENSION=".ba";
	public String HOMEBANK_FILE_EXTENSION=".hb";
	public String MEMCARD_FILE_EXTENSION=".mshc";
	public String EMAIL_FILE_EXTENSION=".email";
	public String CONTACT_FILE_EXTENSION=".cnt";
	public String WEBSITE_FILE_EXTENSION=".web";
	
}
