package com.salvador.secureanymobileapps.documents;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.Settings.UserPreferencesKey;
import com.salvador.secureanymobileapps.documents.encrypt.decrypt.SimppleFileUtil;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;

import org.json.JSONObject;

public class PasswordDoc extends BaseActivity implements StrongBoxConstants, View.OnClickListener {
    private EditText editText1, editText2, editText3, editText4, editText5;
    private String name, userName, password, access, other;
    private String pathTosave;
    private Prefrences_Manager prefrences_Manager;

    private SharedPreferences isTextNull;
    private Button back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.docs_password_layout);
        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        editText4 = (EditText) findViewById(R.id.editText4);
        editText5 = (EditText) findViewById(R.id.editText5);
        pathTosave = getIntent().getExtras().getString("pathToSave") + "/";
        prefrences_Manager = new Prefrences_Manager(PasswordDoc.this);
        this.back = (Button)findViewById(R.id.back_button);
        this.back.setOnClickListener(this);

    }
    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        finish();
    }
    @Override
    public void onClick(View view) {
        String tag = view.getTag() + "";
        if (tag.equals("back")) {
            this.onBackPressed();
        }
    }
    @Override
    public void onUserInteraction() {
        // TODO Auto-generated method stub
        if (prefrences_Manager.getTimerChecked()) {
            SimmpleApp.resetDisconnectTimer();
        } else {
            SimmpleApp.stopDisconnectTimer();
        }
    }

    public void save(View view) {
        SharedPreferences settings = this.getSharedPreferences(
                UserPreferencesKey.keyPrefName, 0);
        name = editText1.getText().toString().trim();
        userName = editText2.getText().toString().trim();
        password = editText3.getText().toString().trim();
        access = editText4.getText().toString().trim();
        other = editText5.getText().toString().trim();
        if (name.length() == 0) {
            editText1.setError("can't be  empty");
        }
        if (userName.length() == 0) {
            editText2.setError("can't be  empty");
        }
        if (password.length() == 0) {
            editText3.setError("can't be  empty");
        }
        if (access.length() == 0) {
            editText4.setError("can't be  empty");
        }
        if (name.length() != 0 && userName.length() != 0 && password.length() != 0 && access.length() != 0) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", name);
                jsonObject.put("username", userName);
                jsonObject.put("password", password);
                jsonObject.put("access", access);
                jsonObject.put("other", other);
                String data = jsonObject.toString();
                new SimppleFileUtil(PasswordDoc.this).encryptAndSave(data.getBytes(),
                        pathTosave, name.concat(PASSWORD_FILE_EXTENSION)+"_"+settings.getInt(UserPreferencesKey.keyAlgoritmoCode,0));
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            finish();
        }
    }
}
