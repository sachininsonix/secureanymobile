package com.salvador.secureanymobileapps.documents;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.salvador.secureanymobileapps.R;

import java.util.Arrays;

public class MySetting extends Activity {
	private Spinner spinner;
	private CheckBox checkBox;
	private LinearLayout layout;
	private RelativeLayout hiddenLayout;
	private String[] arr={"1","2","5","10","15"};
	String timeToLock;
	private TextView textView;
	private Prefrences_Manager prefrences_Manager;
@SuppressWarnings({ "unchecked", "rawtypes" })
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.docs_setting);
	prefrences_Manager=new Prefrences_Manager(MySetting.this);
	spinner=(Spinner) findViewById(R.id.spinner1);
	textView=(TextView) findViewById(R.id.textView2);
	textView.setText(SimmpleApp.simmpleBoxPath);
	checkBox=(CheckBox) findViewById(R.id.checkBox1);
	layout=(LinearLayout) findViewById(R.id.edit_types);
	hiddenLayout=(RelativeLayout) findViewById(R.id.hidden_layout);
	if(prefrences_Manager.getTimerChecked())
	{
	checkBox.setChecked(true);
	hiddenLayout.setVisibility(View.VISIBLE);
	}
	System.out.println(Arrays.asList(arr).indexOf(prefrences_Manager.getLockTime()));
	checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		
		@Override
		public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
			// TODO Auto-generated method stub
			if(checkBox.isChecked())
			{
				hiddenLayout.setVisibility(View.VISIBLE);
				prefrences_Manager.timerChecked(true);
			}
			else {
				hiddenLayout.setVisibility(View.GONE);
				prefrences_Manager.timerChecked(false);
			}
		}

	});
	spinner.setAdapter(new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, arr));
	spinner.setSelection(Arrays.asList(arr).indexOf(prefrences_Manager.getLockTime()));
	spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			timeToLock=arr[arg2];
			System.out.println("item selected="+timeToLock);
			savetoPref();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	layout.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			startActivity(new Intent(MySetting.this,Edit_files_fields.class));
		}
	});
	
}
@Override
public void onUserInteraction() {
	// TODO Auto-generated method stub
	if(prefrences_Manager.getTimerChecked())
	{
		SimmpleApp.resetDisconnectTimer();
	}
	else {
		SimmpleApp.stopDisconnectTimer();
	}
}
public  void savetoPref() {
	// TODO Auto-generated method stub
	prefrences_Manager.setLockTime(timeToLock);
}
@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	super.onBackPressed();
	startActivity(new Intent(MySetting.this,DocsMainActivity.class));
	finish();
}
}
