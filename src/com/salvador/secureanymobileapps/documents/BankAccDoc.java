package com.salvador.secureanymobileapps.documents;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.Settings.UserPreferencesKey;
import com.salvador.secureanymobileapps.documents.encrypt.decrypt.SimppleFileUtil;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;

import org.json.JSONObject;

public class BankAccDoc extends BaseActivity implements StrongBoxConstants, View.OnClickListener {
    private EditText editText1, editText2, editText3, editText4, editText5,
            editText6, editText7, editText8;
    private String string1, string2, string3, string4, string5, string6,
            string7, string8;
    private String pathTosave;
    private Prefrences_Manager prefrences_Manager;

    private SharedPreferences isTextNull;
    private Button back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.docs_bank_acc_layout);

        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        editText4 = (EditText) findViewById(R.id.editText4);
        editText5 = (EditText) findViewById(R.id.editText5);
        editText6 = (EditText) findViewById(R.id.editText6);
        editText7 = (EditText) findViewById(R.id.editText7);
        editText8 = (EditText) findViewById(R.id.editText8);
        pathTosave = getIntent().getExtras().getString("pathToSave") + "/";
        prefrences_Manager = new Prefrences_Manager(BankAccDoc.this);
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
    }
    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        String tag = view.getTag() + "";
        if (tag.equals("back")) {
            this.onBackPressed();
        }
    }

    @Override
    public void onUserInteraction() {
        // TODO Auto-generated method stub
        if (prefrences_Manager.getTimerChecked()) {
            SimmpleApp.resetDisconnectTimer();
        } else {
            SimmpleApp.stopDisconnectTimer();
        }
    }

    public void save(View view) {
        SharedPreferences settings = this.getSharedPreferences(
                UserPreferencesKey.keyPrefName, 0);
        string1 = editText1.getText().toString().trim();
        string2 = editText2.getText().toString().trim();
        string3 = editText3.getText().toString().trim();
        string4 = editText4.getText().toString().trim();
        string5 = editText5.getText().toString().trim();
        string6 = editText6.getText().toString().trim();
        string7 = editText7.getText().toString().trim();
        string8 = editText8.getText().toString().trim();
        if (string1.length() == 0) {
            editText1.setError("can't be  empty");
        }
        if (string2.length() == 0) {
            editText2.setError("can't be  empty");
        }
        if (string3.length() == 0) {
            editText3.setError("can't be  empty");
        }
        if (string4.length() == 0) {
            editText4.setError("can't be  empty");
        }
        if (string5.length() == 0) {
            editText5.setError("can't be  empty");
        }
        if (string6.length() == 0) {
            editText6.setError("can't be  empty");
        }
        if (string7.length() == 0) {
            editText7.setError("can't be  empty");
        }
        if (string8.length() == 0) {
            editText8.setError("can't be  empty");
        }
        if (string1.length() != 0 && string2.length() != 0
                && string3.length() != 0 && string4.length() != 0
                && string5.length() != 0 && string6.length() != 0
                && string7.length() != 0) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", string1);
                jsonObject.put("bank", string2);
                jsonObject.put("acc_num", string3);
                jsonObject.put("iban", string4);
                jsonObject.put("branch", string5);
                jsonObject.put("swift", string6);
                jsonObject.put("phone", string7);
                jsonObject.put("note", string8);
                System.out.println("json output=" + jsonObject.toString());
                String data = jsonObject.toString();
                new SimppleFileUtil(BankAccDoc.this).encryptAndSave(data.getBytes(),
                        pathTosave, string1.concat(BANKACC_FILE_EXTENSION) + "_" + settings.getInt(UserPreferencesKey.keyAlgoritmoCode, 0));
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            finish();
        }
    }
}
