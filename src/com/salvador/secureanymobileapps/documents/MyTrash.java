package com.salvador.secureanymobileapps.documents;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.documents.encrypt.decrypt.SimppleFileUtil;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class MyTrash extends BaseActivity implements OnClickListener {
    private ListView myTrashList, myfileListView;
    private String path, dirName;
    private List<String> dirList, fileList;
    private RelativeLayout dirView, fileView;
    private boolean isFileView = false;
    private String sourcePath;
    private Prefrences_Manager prefrences_Manager;
    private SharedPreferences isTextNull;
    private Button back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.docs_mytrash);
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
        myTrashList = (ListView) findViewById(R.id.listViewTrash);
        myfileListView = (ListView) findViewById(R.id.listViewFile);
        dirView = (RelativeLayout) findViewById(R.id.dirView);
        fileView = (RelativeLayout) findViewById(R.id.fileView);
        path = SimmpleApp.simmpleBoxPath + "/Trash";
        prefrences_Manager = new Prefrences_Manager(MyTrash.this);
        dirList = new ArrayList<String>();
        fileList = new ArrayList<String>();
        try {
            dirList = new SimppleFileUtil(MyTrash.this).getDirectoryList(path);
            if (dirList.size() != 0) {
                myTrashList.setAdapter(new MyAdapter());
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
//
    @Override
    public void onResume() {
        super.onResume();
//
    }

    @Override
    public void onUserInteraction() {
        // TODO Auto-generated method stub
        if (prefrences_Manager.getTimerChecked()) {
            SimmpleApp.resetDisconnectTimer();
        } else {
            SimmpleApp.stopDisconnectTimer();
        }
    }

    public void emptyTrash(View view) {
        try {
            File file = new File(path);
            if (file.exists()) {
                File file2[] = file.listFiles();
                for (int i = 0; i < file2.length; i++) {
                    if (file2[i].isDirectory()) {
                        File file3[] = file2[i].listFiles();
                        for (int j = 0; j < file3.length; j++) {
                            file3[j].delete();
                        }
                    }
                    file2[i].delete();
                }
            }
            startActivity(new Intent(MyTrash.this, MainView.class));
            finish();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        if (isFileView) {
            dirView.setVisibility(View.VISIBLE);
            fileView.setVisibility(View.GONE);
            isFileView = false;
        } else {
            startActivity(new Intent(MyTrash.this, MainView.class));
            finish();
        }
    }

    @Override
    public void onClick(View view) {

        if (isFileView) {
            dirView.setVisibility(View.VISIBLE);
            fileView.setVisibility(View.GONE);
            isFileView = false;
        } else {
            onBackPressed();
        }


    }

    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return dirList.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return arg0;
        }

        @Override
        public View getView(int arg0, View convertView, ViewGroup arg2) {
            View view = convertView;
            final Holder holder;
            if (view == null) {
                view = LayoutInflater.from(MyTrash.this).inflate(
                        R.layout.docs_custom_trashitem, null);
                holder = new Holder();
                holder.dirName = (TextView) view.findViewById(R.id.dirName);
                holder.icon = (ImageView) view.findViewById(R.id.iconLabel);
                view.setTag(holder);
            } else {
                holder = (Holder) view.getTag();
            }

            Resources resources = getResources();
            if (!isTextNull.getString("theme", "").equals("")) {
                if (isTextNull.getString("theme", "").equals("Black/Grey")) {
                    holder.dirName.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                    holder.icon.setBackgroundResource(R.drawable.fold_add_light);
                }else{
                    holder.dirName.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                    holder.icon.setBackgroundResource(R.drawable.fold_add_dark);
                }
            }else{
                holder.dirName.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                holder.icon.setBackgroundResource(R.drawable.fold_add_light);
            }


            holder.dirName.setText(dirList.get(arg0));
            holder.dirName.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    dirName = holder.dirName.getText().toString();
                    getFiles(dirName);
                    dirView.setVisibility(View.GONE);
                }
            });
            return view;
        }

    }

    public void getFiles(String dirName) {

        // TODO Auto-generated method stub
        try {
            fileList.clear();
            File file = new File(path + "/" + dirName);
            sourcePath = file.getPath();
            fileList = new SimppleFileUtil(MyTrash.this)
                    .getFileList(sourcePath);
            System.out.println("file path=" + sourcePath);
            if (fileList.size() != 0) {
                myfileListView.setAdapter(new FileAdapter());
            } else {
                myfileListView.setAdapter(new FileAdapter());
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        isFileView = true;
        fileView.setVisibility(View.VISIBLE);
        dirView.setVisibility(View.GONE);

    }

    private static class Holder {
        TextView dirName;

        ImageView icon;
    }

    private class FileAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return fileList.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            // TODO Auto-generated method stub
            View view = arg1;
            final String fileName;
            if (view == null) {
                view = LayoutInflater.from(MyTrash.this).inflate(
                        R.layout.docs_custom_file_view, null);

            }
            TextView textView = (TextView) view.findViewById(R.id.fileName);
            Button button = (Button) view
                    .findViewById(R.id.buttonRestoreTogallery);
            fileName = fileList.get(arg0);
            textView.setText(fileName);
            Resources resources = getResources();
            if (!isTextNull.getString("theme", "").equals("")) {
                if (isTextNull.getString("theme", "").equals("Black/Grey")) {
                    textView.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                  button.setBackgroundResource(R.drawable.restore);
                }else{
                    textView.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                    button.setBackgroundResource(R.drawable.restore);
                }
            }else{
                textView.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                button.setBackgroundResource(R.drawable.restore);
            }


            button.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub
                    InputStream inStream = null;
                    OutputStream outStream = null;
                    String destinationPath = "";
                    if (dirName.equals("Documents")) {
                        destinationPath = SimmpleApp.simmpleBoxPath
                                + "/Documents";
                    } else if (dirName.equals("Passwords")) {
                        destinationPath = SimmpleApp.simmpleBoxPath
                                + "/Passwords";
                    }  else if (dirName.equals("Financial")) {
                        destinationPath = SimmpleApp.simmpleBoxPath
                                + "/Financial";
                    } else if (dirName.equals("Gallery")) {
                        destinationPath = SimmpleApp.simmpleBoxPath
                                + "/Gallery";
                    } else if (dirName.equals("Membership")) {
                        destinationPath = SimmpleApp.simmpleBoxPath
                                + "/Membership";
                    } else if (dirName.equals("Other")) {
                        destinationPath = SimmpleApp.simmpleBoxPath + "/Other";
                    }
                    File sourceFileName = new File(sourcePath + "/" + fileName);
                    if (sourceFileName.exists()) {
                        try {
                            File destination = new File(destinationPath, "/"
                                    + fileName);
                            destination.createNewFile();
                            // File source=new File(path+"/"+sourceFileName);
                            inStream = new FileInputStream(sourceFileName);
                            outStream = new FileOutputStream(destination);
                            byte[] buffer = new byte[1024];

                            int length;
                            // copy the file content in bytes
                            while ((length = inStream.read(buffer)) > 0) {

                                outStream.write(buffer, 0, length);

                            }

                            inStream.close();
                            outStream.close();

                            // delete the original file
                            sourceFileName.delete();

                            Toast.makeText(getApplicationContext(),
                                    "File moved to " + dirName,
                                    Toast.LENGTH_SHORT).show();
                            fileList.clear();
                            getFiles(dirName);
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }
                    }
                }
            });
            return view;
        }

    }
}
