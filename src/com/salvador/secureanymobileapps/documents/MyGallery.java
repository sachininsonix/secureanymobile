package com.salvador.secureanymobileapps.documents;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.documents.encrypt.decrypt.SimppleFileUtil;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MyGallery extends BaseActivity implements StrongBoxConstants {
	private ViewPager viewPager;
	private String imgpath;
	private String path = "", trashPath = "", pathh = "";
	private String fileName = "";
	private String sourceFileName = "";
	private List<String> myList;
	private Prefrences_Manager prefrences_Manager;
	private Button buttonRestore, buttonDelete, addPhoto,home;
	private SharedPreferences isTextNull;
	private Button back;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.docs_mygallery);
		viewPager = (ViewPager) findViewById(R.id.myGallery_pager);
		path = SimmpleApp.simmpleBoxPath + "/Gallery";
		prefrences_Manager=new Prefrences_Manager(MyGallery.this);
		trashPath = SimmpleApp.simmpleBoxPath + "/Trash";
		addPhoto = (Button) findViewById(R.id.button1);
		buttonRestore = (Button) findViewById(R.id.button2);
		buttonDelete = (Button) findViewById(R.id.button3);
		this.home = (Button) findViewById(R.id.button_home);
		home.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				goHome(v);
			}
		});
		buttonDelete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new SimppleFileUtil(MyGallery.this).moveToTrash(path, trashPath + "/Gallery",
						sourceFileName);
				Toast.makeText(getApplicationContext(), "File moved to trash",
						Toast.LENGTH_SHORT).show();
				getAllFiles();
			}
		});
		buttonRestore.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				File publicDirectoryPath = Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
				new SimppleFileUtil(MyGallery.this).decryptAndRestoreData(
						publicDirectoryPath.getPath(), path, sourceFileName);
				Toast.makeText(getApplicationContext(),
						"File restored to public directorys",
						Toast.LENGTH_SHORT).show();
				File restored = new File(publicDirectoryPath, sourceFileName);
				MediaScannerConnection.scanFile(MyGallery.this,
						new String[]{restored.toString()}, null,
						new MediaScannerConnection.OnScanCompletedListener() {
							public void onScanCompleted(String path, Uri uri) {
								Log.i("ExternalStorage", "Scanned " + path + ":");
								Log.i("ExternalStorage", "-> uri=" + uri);
							}
						});
				getAllFiles();
			}
		});
		myList = new ArrayList<String>();
		getAllFiles();
		try {
			sourceFileName = myList.get(viewPager.getCurrentItem());
		} catch (IndexOutOfBoundsException e) {

		}
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				sourceFileName = myList.get(arg0);
				pathh = path + "/" + sourceFileName;
				System.out.println("path=="+pathh);
			}
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});
		this.back = (Button) findViewById(R.id.back_button);
		this.back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				onBackPressed();
			}
		});
	}
	public void goHome(View view) {
		startActivity(new Intent(MyGallery.this, DocsMainActivity.class));
		finish();
	}
	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		startActivity(new Intent(MyGallery.this, MainView.class));
		finish();
	}
	@Override
	public void onUserInteraction() {
		// TODO Auto-generated method stub
		if(prefrences_Manager.getTimerChecked())
		{
			SimmpleApp.resetDisconnectTimer();
		}
		else {
			SimmpleApp.stopDisconnectTimer();
		}
	}


	private void getAllFiles() {
		// TODO Auto-generated method stub
		myList.clear();
		myList = new SimppleFileUtil(MyGallery.this).getFileList(path);
		if (myList.size() != 0) {
			buttonDelete.setVisibility(View.VISIBLE);
			buttonRestore.setVisibility(View.VISIBLE);
			System.out.println("file list" + myList + "size=" + myList.size());
			viewPager.setAdapter(new MyPagerAdapter());
		} else {
			buttonDelete.setVisibility(View.INVISIBLE);
			buttonRestore.setVisibility(View.INVISIBLE);
			viewPager.setAdapter(new MyPagerAdapter());
		}
	}

	public void openGallery(View view) {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, 100);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			if (requestCode == 100) {
				Uri selectedImageUri = data.getData();
				System.out.println("-----Data----" + selectedImageUri);
				imgpath = getPath(MyGallery.this, selectedImageUri);
				System.out.println("-----Data----" + imgpath);
				String arr[] = imgpath.split("/");
				fileName = arr[arr.length - 1];
				System.out.println("name of file:" + fileName + "path=" + path);
				if (imgpath != null) {
					encryptAndStoreImage(imgpath);

				}
			}

		}

	}

	private void encryptAndStoreImage(String string) {
		// TODO Auto-generated method stub

		try {
			ProgressDialog dialog = new ProgressDialog(MyGallery.this);
			dialog.setCancelable(false);
			dialog.setMessage("Encrypting data ...");
			dialog.show();
			new SimppleFileUtil(MyGallery.this).encrptyAndStoreData(string, path, fileName);
			dialog.dismiss();
			getAllFiles();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String getPath(Context context, Uri selectedImageUri) {
		if (selectedImageUri == null) {
			return null;
		}
		String[] projection = { MediaStore.Images.Media.DATA };
		@SuppressWarnings("deprecation")
		Cursor cursor = context.getContentResolver().query(selectedImageUri, projection, null, null,
				null);
		if (cursor != null) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();

			String retVal = cursor.getString(column_index);
			cursor.close();
			return retVal;
		}
		cursor.close();
		return selectedImageUri.getPath();
	}

	private class MyPagerAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return myList.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == (LinearLayout) arg1;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			// TODO Auto-generated method stub
			View view = LayoutInflater.from(MyGallery.this).inflate(
					R.layout.docs_custom_pager_iew, null);
			ImageView image = (ImageView) view
					.findViewById(R.id.imageViewgallery);
			sourceFileName = myList.get(position);
			pathh = path + "/" + sourceFileName;
			try {

				byte[] decodeByte = new SimppleFileUtil(MyGallery.this).getEncodedData(pathh);
				image.setImageBitmap(BitmapFactory.decodeByteArray(decodeByte,
						0, decodeByte.length));
				((ViewPager) container).addView(view);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return view;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((LinearLayout) object);

		}

	}
}