package com.salvador.secureanymobileapps.documents;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.Settings.UserPreferencesKey;
import com.salvador.secureanymobileapps.documents.encrypt.decrypt.SimppleFileUtil;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;

import org.json.JSONObject;

public class MasterCardDoc extends BaseActivity implements StrongBoxConstants, View.OnClickListener {
	private EditText editText1, editText2, editText3, editText4, editText5,
			editText6, editText7, editText8, editText9, editText10, editText11;
	private String string1, string2, string3, string4, string5, string6,
			string7, string8, string9, string10, string11;
	private String pathTosave, from;
	private Prefrences_Manager prefrences_Manager;
    private SharedPreferences isTextNull;
    private Button back;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.docs_master_card_layout);
		editText1 = (EditText) findViewById(R.id.editText1);
		editText2 = (EditText) findViewById(R.id.editText2);
		editText3 = (EditText) findViewById(R.id.editText3);
		editText4 = (EditText) findViewById(R.id.editText4);
		editText5 = (EditText) findViewById(R.id.editText5);
		editText6 = (EditText) findViewById(R.id.editText6);
		editText7 = (EditText) findViewById(R.id.editText7);
		editText8 = (EditText) findViewById(R.id.editText8);
		editText9 = (EditText) findViewById(R.id.editText9);
		editText10 = (EditText) findViewById(R.id.editText10);
		editText11 = (EditText) findViewById(R.id.editText11);
		pathTosave = getIntent().getExtras().getString("pathToSave") + "/";
		from = getIntent().getExtras().getString("from");
		prefrences_Manager=new Prefrences_Manager(MasterCardDoc.this);

        this.back = (Button)findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
	}
    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        String tag = view.getTag() + "";
        if (tag.equals("back")) {
            this.onBackPressed();
        }
    }
	@Override
	public void onUserInteraction() {
		// TODO Auto-generated method stub
		if(prefrences_Manager.getTimerChecked())
		{
			SimmpleApp.resetDisconnectTimer();
		}
		else {
			SimmpleApp.stopDisconnectTimer();
		}
	}

	public void save(View view) {
        SharedPreferences settings = this.getSharedPreferences(
                UserPreferencesKey.keyPrefName, 0);
		string1 = editText1.getText().toString().trim();
		string2 = editText2.getText().toString().trim();
		string3 = editText3.getText().toString().trim();
		string4 = editText4.getText().toString().trim();
		string5 = editText5.getText().toString().trim();
		string6 = editText6.getText().toString().trim();
		string7 = editText7.getText().toString().trim();
		string8 = editText8.getText().toString().trim();
		string9 = editText9.getText().toString().trim();
		string10 = editText10.getText().toString().trim();
		string11 = editText11.getText().toString().trim();
		if (string1.length() == 0) {
			editText1.setError("can't be  empty");
		}
		if (string2.length() == 0) {
			editText2.setError("can't be  empty");
		}
		if (string3.length() == 0) {
			editText3.setError("can't be  empty");
		}
		if (string4.length() == 0) {
			editText4.setError("can't be  empty");
		}
		if (string5.length() == 0) {
			editText5.setError("can't be  empty");
		}
		if (string6.length() == 0) {
			editText6.setError("can't be  empty");
		}
		if (string7.length() == 0) {
			editText7.setError("can't be  empty");
		}
		if (string8.length() == 0) {
			editText8.setError("can't be  empty");
		}
		if (string9.length() == 0) {
			editText9.setError("can't be  empty");
		}
		if (string10.length() == 0) {
			editText10.setError("can't be  empty");
		}
		if (string11.length() == 0) {
			editText11.setError("can't be  empty");
		}
		if (string1.length() != 0 && string2.length() != 0
				&& string3.length() != 0 && string4.length() != 0
				&& string5.length() != 0 && string6.length() != 0
				&& string7.length() != 0 && string10.length() != 0
				&& string9.length() != 0 && string4.length() != 0
				&& string11.length() != 0) {
			try {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("name", string1);
				jsonObject.put("iban", string2);
				jsonObject.put("card_num", string3);
				jsonObject.put("pin", string4);
				jsonObject.put("cvv", string5);
				jsonObject.put("bank", string6);
				jsonObject.put("f_name", string7);
				jsonObject.put("l_name", string8);
				jsonObject.put("start_date", string9);
				jsonObject.put("expiry_date", string10);
				jsonObject.put("call_if_loast", string11);
				System.out.println("json output=" + jsonObject.toString());
				String data = jsonObject.toString();
				if (from.equals("Maestro")) {
					new SimppleFileUtil(MasterCardDoc.this).encryptAndSave(data.getBytes(),
							pathTosave, string1.concat(MAESTRO_FILE_EXTENSION)+"_"+settings.getInt(UserPreferencesKey.keyAlgoritmoCode,0));
				} else if (from.equals("Master_card")) {
					new SimppleFileUtil(MasterCardDoc.this).encryptAndSave(data.getBytes(),
							pathTosave, string1.concat(MASTERCARD_FILE_EXTENSION)+"_"+settings.getInt(UserPreferencesKey.keyAlgoritmoCode,0));
				} else if (from.equals("Visa")) {
					new SimppleFileUtil(MasterCardDoc.this).encryptAndSave(data.getBytes(),
							pathTosave, string1.concat(VISA_FILE_EXTENSION)+"_"+settings.getInt(UserPreferencesKey.keyAlgoritmoCode,0));
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			finish();}
	}
}
