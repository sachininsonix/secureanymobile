package com.salvador.secureanymobileapps.documents;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.Settings.UserPreferencesKey;
import com.salvador.secureanymobileapps.chat.LoginActivity;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainMenu;

import java.io.File;
import java.util.ArrayList;

import static android.view.View.OnClickListener;

public class MainView extends BaseActivity implements OnClickListener, OnItemClickListener {


    private ListView listfolders;
    private FolderRowItems item;
    private adapterListFolder adapter;
    private ArrayList<FolderRowItems> rowItem;
    private SharedPreferences isTextNull;
    private Button back;
    private Button addFolder;

    private boolean isAutoLock = false;
    private Prefrences_Manager prefrences_Manager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

//
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.docs_mainview);
//
        ApplicationClass.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("MainView Activity")
                .build());
        prefrences_Manager = new Prefrences_Manager(MainView.this);
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
        this.addFolder = (Button) findViewById(R.id.btnAddFolder);
        this.addFolder.setOnClickListener(this);
        this.listfolders = (ListView) findViewById(R.id.listviewfolderItems);
        this.listfolders.setOnItemClickListener(this);
        this.renderFolders();
    }
//
    @Override
    public void onResume() {
        super.onResume();
        if(isFinishing()) {
            Intent intent = new Intent(MainView.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        SharedPreferences settings = this.getSharedPreferences(UserPreferencesKey.keyNoPass, 0);
        boolean isPassGeneral = settings.getBoolean(UserPreferencesKey.keyNOPassword, false);

        settings = this.getSharedPreferences(UserPreferencesKey.keyautoLock, 0);
        boolean isLock = settings.getBoolean(UserPreferencesKey.keyisAutoLock, false);

        if(!isPassGeneral){
            if (isLock == true && this.isAutoLock == true) {
                this.prefrences_Manager.setLocked();
                Intent intent = new Intent(MainView.this, DocsMainActivity.class);
                intent.putExtra("Validate", "documents");
                startActivity(intent);
                finish();
            }
        }


    }




    @Override
    public void onPause() {
        super.onPause();
        this.isAutoLock = true;

        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
    }


    @Override
    public void onUserInteraction() {
        // TODO Auto-generated method stub
        if (new Prefrences_Manager(MainView.this).getTimerChecked()) {
            SimmpleApp.resetDisconnectTimer();
        } else {
            SimmpleApp.stopDisconnectTimer();
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();

        SharedPreferences settings = this.getSharedPreferences(UserPreferencesKey.keyNoPass, 0);
        boolean isPassGeneral = settings.getBoolean(UserPreferencesKey.keyNOPassword, false);

        settings = this.getSharedPreferences(UserPreferencesKey.keyautoLock, 0);
        boolean isLock = settings.getBoolean(UserPreferencesKey.keyisAutoLock, false);
        if (!isPassGeneral) {
            if (isLock == true) {
                this.prefrences_Manager.setLocked();
                Intent intent = new Intent(MainView.this, DocsMainActivity.class);
                intent.putExtra("Validate", "documents");
                startActivity(intent);
                finish();
            }
            else {
                Intent intent = new Intent(MainView.this, MainMenu.class);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();
            }
        } else {
            Intent intent = new Intent(MainView.this, MainMenu.class);
            startActivity(intent);
            finish();
        }
    }

    public void runFinance(View view) {
        startActivity(new Intent(MainView.this, MyFinance.class));
        finish();
    }

    public void runMembership(View view) {
        startActivity(new Intent(MainView.this, MyMembership.class));
        finish();
    }

    public void runOther(View view) {
        startActivity(new Intent(MainView.this, MyOther.class));
        finish();
    }

    public void runGallery(View view) {
        startActivity(new Intent(MainView.this, MyGallery.class));
        finish();
    }

    public void runDocument(View view) {
        startActivity(new Intent(MainView.this, MyDocument.class));
        finish();
    }

    public void runTrash(View view) {
        startActivity(new Intent(MainView.this, MyTrash.class));
        finish();
    }

    @Override
    public void onClick(View view) {
        String tag = view.getTag() + "";
        if (tag.equals("back")) {
            this.onBackPressed();
        } else if (tag.equals("Addfolder")) {
            this.PopupAddFolder();
        }

    }

    private void PopupAddFolder() {

        final Dialog popup = new Dialog(this);
        Resources resources = getResources();
        popup.setContentView(R.layout.add_folder_popup);
        popup.setTitle("Add Folder");
        RelativeLayout containerPopupAddFolder = (RelativeLayout) popup.findViewById(R.id.containerPopupAddFolder);
        Button add = (Button) popup.findViewById(R.id.createFolder);
        final EditText txtNameFolder = (EditText) popup.findViewById(R.id.txtNameFolder);
        TextView labelNameFolder = (TextView) popup.findViewById(R.id.labelNameFolder);




        add.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                String nameFolder = txtNameFolder.getText() + "";
                if (nameFolder.trim().length() > 0) {
                    createDirectory(nameFolder);
                }
                nameFolder = null;
                popup.dismiss();

            }
        });
        popup.show();
    }

    private void createDirectory(String nameFolder) {
        String state = Environment.getExternalStorageState();
        try {
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                File mydir = new File(Environment.getExternalStorageDirectory() + "/Android/obb/com.salvador.secureanymobileapps/");
                SimmpleApp.simmpleBoxPath = mydir.getPath();
                if (mydir.exists()) {
                    File documentFile = new File(mydir.getPath() + "/" + nameFolder);
                    if (!documentFile.exists()) {
                        documentFile.mkdirs();
                        renderFolders();
                    }
                }

            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    private void renderFolders() {
        String state = Environment.getExternalStorageState();
        try {
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                File mydir = new File(Environment.getExternalStorageDirectory() + "/Android/obb/com.salvador.secureanymobileapps/");
                SimmpleApp.simmpleBoxPath = mydir.getPath();
                int typeImage = 0;
                if (mydir.exists()) {

                    this.rowItem = new ArrayList<FolderRowItems>();
                    File[] files = mydir.listFiles();
                    for (File inFile : files) {
                        if (inFile.isDirectory()) {
                            // is directory
                            String nameFolder = inFile.getName() + "";
                            if (nameFolder.equals("Documents")) {
                                typeImage = 5;
                            } else if (nameFolder.equals("Passwords")) {
                                typeImage = 7;
                            } else if (nameFolder.equals("Financial")) {
                                typeImage = 1;
                            } else if (nameFolder.equals("Gallery")) {
                                typeImage = 4;
                            } else if (nameFolder.equals("Membership")) {
                                typeImage = 2;
                            } else if (nameFolder.equals("Other")) {
                                typeImage = 3;
                            } else if (nameFolder.equals("Trash")) {
                                typeImage = 6;
                            } else {
                                typeImage = 0;
                            }
                            this.item = new FolderRowItems(typeImage, inFile.getName() + "");
                            this.rowItem.add(this.item);
                        }
                    }
                    if (adapter == null) {
                        adapter = new adapterListFolder(this, this.rowItem);

                    } else {
                        adapter.adapterListFolderRender(this, this.rowItem);
                    }
                    listfolders.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    this.rowItem = null;
                }

            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            RelativeLayout container = (RelativeLayout) view;
            TextView namefolder = (TextView) container.getChildAt(1);
            Intent intent = new Intent(MainView.this, FolderDefaultActivity.class);
            Bundle args = new Bundle();
            args.putString("name", namefolder.getText() + "");
            intent.putExtras(args);
            startActivity(intent);
        finish();
//        }

    }
}
