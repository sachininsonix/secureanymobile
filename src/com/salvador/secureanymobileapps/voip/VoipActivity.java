package com.salvador.secureanymobileapps.voip;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.net.sip.SipAudioCall;
import android.net.sip.SipException;
import android.net.sip.SipProfile;
import android.net.sip.SipSession;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.Settings.UserPreferencesKey;
import com.salvador.secureanymobileapps.documents.DocsMainActivity;
import com.salvador.secureanymobileapps.documents.Prefrences_Manager;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainMenu;


/**
 * Created by station12 on 9/5/14.
 */
public class VoipActivity extends BaseActivity implements View.OnClickListener {
    private SharedPreferences isTextNull;
    private Prefrences_Manager prefrences_Manager;
    private Button back;
    private TextView viewPhoneCall;
//    private ImageView cancel;
//    public  ImageView accept;
    public RelativeLayout containerCancelCall;

    private Uri uriContact;
    private boolean isAutoLock = false;
    public SipAudioCall call;

    private String sipAddress;
    private ApplicationClass app;
    private String USERNAME;
    private String DOMAIN;
    private String PASSWORD ;
    private String TAG = "Security Any Mobile";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voip);
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
        this.viewPhoneCall = (TextView) findViewById(R.id.viewPhoneCall);
        this.containerCancelCall = (RelativeLayout) findViewById(R.id.containerCancelCall);
        ((ImageButton) findViewById(R.id.btnCallContact)).setOnClickListener(this);

        // initialize components
        prefrences_Manager = new Prefrences_Manager(VoipActivity.this);
        this.app = (ApplicationClass) getApplicationContext();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.SipDemo.INCOMING_CALL");
        this.app.setVoipActive(true);

        this.sipAddress = this.app.SIPADRESS;
        this.USERNAME = this.app.USERNAME;
        this.DOMAIN = this.app.DOMAIN;
        this.PASSWORD = this.app.PASSWORD;

        if(this.app.isCall()){
            this.receiveCall(this.app.getDataCall());
        }else{
            closeCall();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences settings = this.getSharedPreferences(UserPreferencesKey.keyNoPass, 0);
        boolean isPassGeneral = settings.getBoolean(UserPreferencesKey.keyNOPassword, false);
        settings = this.getSharedPreferences(UserPreferencesKey.keyautoLock, 0);
        boolean isLock = settings.getBoolean(UserPreferencesKey.keyisAutoLock, false);
        if (!isPassGeneral) {
            if (isLock == true && this.isAutoLock == true) {
                this.prefrences_Manager.setLockedVoip();
                Intent intent = new Intent(VoipActivity.this, DocsMainActivity.class);
                intent.putExtra("Validate", "voip");
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        this.isAutoLock = true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        SharedPreferences settings = this.getSharedPreferences(UserPreferencesKey.keyNoPass, 0);
        boolean isPassGeneral = settings.getBoolean(UserPreferencesKey.keyNOPassword, false);

        settings = this.getSharedPreferences(UserPreferencesKey.keyautoLock, 0);
        boolean isLock = settings.getBoolean(UserPreferencesKey.keyisAutoLock, false);
        if (!isPassGeneral) {
            if (isLock == true) {
                this.prefrences_Manager.setLockedVoip();
                Intent intent = new Intent(VoipActivity.this, DocsMainActivity.class);
                intent.putExtra("Validate", "voip");
                startActivity(intent);
                finish();
            }
            else {
                Intent intent = new Intent(VoipActivity.this, MainMenu.class);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();
            }
        } else {
            Intent intent = new Intent(VoipActivity.this, MainMenu.class);
            startActivity(intent);
            overridePendingTransition(0, 0);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (1001):
                if (data != null) {
                    uriContact = data.getData();
                } else {
                    Toast.makeText(getApplicationContext(), "Not data Validate",
                            Toast.LENGTH_LONG).show();
                    break;
                }
                // cursor one for get name and id contact
                String _ID = ContactsContract.Contacts._ID;
                String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
                Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
                String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
                String contact_id = null;
                String phoneNumber = null;
                // this method get name and phone name
                Cursor cursor = getContentResolver().query(uriContact, null, null,
                        null, null);
                if (cursor.getCount() > 0) {
                    while (cursor.moveToNext()) {
                        contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                        int hasPhoneNumber = Integer
                                .parseInt(cursor.getString(cursor
                                        .getColumnIndex(HAS_PHONE_NUMBER)));
                        // get number Phone
                        if (hasPhoneNumber > 0) {
                            Cursor phoneCursor = getContentResolver().query(
                                    PhoneCONTENT_URI, null,
                                    Phone_CONTACT_ID + " = ?",
                                    new String[]{contact_id}, null);
                            while (phoneCursor.moveToNext()) {
                                phoneNumber = phoneCursor.getString(phoneCursor
                                        .getColumnIndex(NUMBER));
                            }
                            phoneCursor.close();
                        }
                    }
                }
                if (phoneNumber != null && phoneNumber.trim().length() > 0) {
                    this.viewPhoneCall.setText("Call: " + phoneNumber);
                    initiateCall();
                } else {
                    Toast.makeText(getApplicationContext(), "Not Number Validate",
                            Toast.LENGTH_LONG).show();
                }
        }
    }


    public void initiateCall() {
        try {

            this.containerCancelCall.setVisibility(View.VISIBLE);
//            this.accept.setVisibility(View.INVISIBLE);
            updateStatus(sipAddress);

            SipAudioCall.Listener listener = new SipAudioCall.Listener() {

                @Override
                public void onCallEstablished(SipAudioCall call) {
                    call.startAudio();
                    call.setSpeakerMode(true);
                    updateStatus("onCallEstablished");
                    super.onCallEstablished(call);
                }

                @Override
                public void onCallEnded(SipAudioCall call) {
                    updateStatus("Call End");
                    super.onCallEnded(call);
                }


                @Override
                public void onRinging(SipAudioCall call, SipProfile caller) {
                    Log.e(TAG, "onRinging caller=" + caller.getUriString()
                            + "; call=" + call.getState() + "; caller="
                            + caller.getUriString());

                    super.onRinging(call, caller);
                }

                @Override
                public void onRingingBack(SipAudioCall call) {
                    Log.e(TAG, "onRingingBack: call=" + call.getState());
                    super.onRingingBack(call);
                }

                @Override
                public void onCallBusy(SipAudioCall call) {
                    Log.e(TAG, "onCallBusy: call=" + call.getState());
                    super.onCallBusy(call);
                }

                @Override
                public void onCallHeld(SipAudioCall call) {
                    Log.e(TAG, "onCallHeld: call=" + call.getState());
                    super.onCallHeld(call);
                }

                @Override
                public void onError(SipAudioCall call, final int errorCode,
                                    final String errorMessage) {

                    Log.e(TAG, "###onError errorCode=" + errorCode
                            + "; errorMessage=" + errorMessage);
                    super.onError(call, errorCode, errorMessage);
                }

                @Override
                public void onChanged(final SipAudioCall call) {
                    Log.e(TAG, "onChanged: call=" + call.getState());
                    super.onChanged(call);
                    switch (call.getState()) {
                        case SipSession.State.OUTGOING_CALL:
                            // making an outgoing call

                            Log.e(TAG, "OUTGOING_CALL= " + SipSession.State.OUTGOING_CALL);
                            break;
                        case SipSession.State.OUTGOING_CALL_RING_BACK:
                            Log.e(TAG, "OUTGOING_CALL_RING_BACK= " + SipSession.State.OUTGOING_CALL_RING_BACK);
                            break;
                        case SipSession.State.IN_CALL:
                            // call established, it works fine.
                            Log.e(TAG, "IN_CALL= " + SipSession.State.IN_CALL);
                            call.startAudio();
                            call.setSpeakerMode(false);
                            if (call.isMuted()) {
                                call.toggleMute();
                            }
                            break;
                        case SipSession.State.READY_TO_CALL:
                            Log.e(TAG, "READY_TO_CALL= " + SipSession.State.READY_TO_CALL);
                            closeCall();
                            break;
                    }
                }
            };

            call = app.mSipManager.makeAudioCall(app.mSipProfile.getUriString(), sipAddress + "@" + DOMAIN,
                    listener, 30);

            updateStatus(call + "");

        } catch (Exception e) {
            Log.e(TAG, "" + e);
            if ( app.mSipProfile != null) {
                try {
                    app.mSipManager.close(app.mSipProfile.getUriString());
                } catch (Exception ee) {
                    Log.i(TAG, "" + ee);
                }
            }
            if (call != null) {
                call.close();
            }
        }
    }

    public void updateStatus(String tag) {
        Log.i(TAG, tag);
    }

    public void receiveCall(Intent intent) {
        containerCancelCall.setVisibility(View.VISIBLE);
//        accept.setVisibility(View.VISIBLE);
        SipAudioCall.Listener listener = new SipAudioCall.Listener() {
            @Override
            public void onRinging(SipAudioCall call, SipProfile caller) {
                Log.e(TAG, "onRinging caller=" + caller.getUriString()
                        + "; call=" + call.getState() + "; caller="
                        + caller.getUriString());

                try {
                    call.answerCall(30);
                } catch (Exception e) {
                    Log.e(TAG, e + "");
                }
                super.onRinging(call, caller);
            }

            @Override
            public void onCallEstablished(SipAudioCall call) {
                Log.e("Security Any  Mobile", "onCallEstablished2");
                super.onCallEstablished(call);
            }


            @Override
            public void onCallEnded(SipAudioCall call) {
//                  closeCall();
                //accept.setVisibility(View.GONE);


                Log.e("Security Any  Mobile", "onCallEnded2");
                Log.e("Security Any  Mobile", "onCallEnded2 " + call.getState());
                Log.e("Security Any  Mobile", "onCallEnded2");
                super.onCallEnded(call);

//                accept.setVisibility(View.GONE);
            }


            @Override
            public void onChanged(final SipAudioCall call) {
                Log.e(TAG, "onChanged: call=" + call.getState());
                super.onChanged(call);
                switch (call.getState()) {
                    case SipSession.State.OUTGOING_CALL:
                        // making an outgoing call

                        Log.e(TAG, "OUTGOING_CALL= " + SipSession.State.OUTGOING_CALL);
                        break;
                    case SipSession.State.OUTGOING_CALL_RING_BACK:
                        Log.e(TAG, "OUTGOING_CALL_RING_BACK= " + SipSession.State.OUTGOING_CALL_RING_BACK);
                        break;
                    case SipSession.State.IN_CALL:
                        // call established, it works fine.
                        Log.e(TAG, "IN_CALL= " + SipSession.State.IN_CALL);
//                            call.startAudio();
//                            call.setSpeakerMode(false);
//                            if (call.isMuted()) {
//                                call.toggleMute();
//                            }
                        break;
                    case SipSession.State.READY_TO_CALL:
                        Log.e(TAG, "READY_TO_CALL= " + SipSession.State.READY_TO_CALL);
                        closeCall();
                        break;
                }
            }

        };
        try {
            call =  app.mSipManager.takeAudioCall(app.getDataCall(), listener);
            viewPhoneCall.setText("Call: " +call.getPeerProfile().getUserName());
            updateStatus(call + "");
        } catch (SipException e) {
            Log.e(TAG, e + "");
        }
    }

    public void closeCall(){
        if (call != null) {
            try {
                updateStatus("end call0");
                app.setCall(false);
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        containerCancelCall.setVisibility(View.GONE);
                    }
                });
                updateStatus("end call1");
                call.endCall();
            } catch (SipException e) {
                Log.e(TAG, e + "");
            }
        }
    }

    private void acceptCall() {
        app.setCall(false);
//        accept.setVisibility(View.INVISIBLE);
        try {
            updateStatus(call + "");
            call.answerCall(30);
        } catch (SipException e) {
            Log.e(TAG, e + "");
        }
        call.startAudio();
        call.setSpeakerMode(true);
        if (call.isMuted()) {
            call.toggleMute();
        }


    }

    @Override
    public void onClick(View view) {
        String tag = view.getTag() + "";
        if (tag.equals("back")) {
            this.onBackPressed();
        }else if (tag.equals("cancel")) {
            this.closeCall();
        } else if (tag.equals("acept")) {
            this.acceptCall();
        } else if (tag.equals("call")) {
            uriContact = ContactsContract.Contacts.CONTENT_URI;
            Intent intent = new Intent(Intent.ACTION_PICK, uriContact);
            startActivityForResult(intent, 1001);
        }
    }

}