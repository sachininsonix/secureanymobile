package com.salvador.secureanymobileapps;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.salvador.secureanymobileapps.chat.AcceptDecline;
import com.salvador.secureanymobileapps.chat.ChatActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainMenu;


public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {


	private static final String TAG ="GcmBroadcastReceiver" ;
	private Context context;
	@Override
	public void onReceive(Context context, Intent intent) {
		String msg = "";
		try {
			this.context = context;
			Bundle extras = intent.getExtras();
			Log.e(TAG, " onReceive== " + extras.get("message"));
			ComponentName comp = new ComponentName(context.getPackageName(),
					GCMNotificationIntentService.class.getName());
			startWakefulService(context, (intent.setComponent(comp)));
			setResultCode(Activity.RESULT_OK);
//		GCMUtils.sendNotification(context,msg);

//		Log.e(TAG, " msg== " + msg);

			if (extras.get("message").equals("null")) {

			} else {
				sendNotification(" "
						+ extras.get("message"), "" + extras.get("uid"), "" + extras.get("chat_id"), "" + extras.get("name"), "" + extras.get("nick_name")
						, "" + extras.get("msg_id"), "" + extras.get("key"), "" + extras.get("type"), "" + extras.get("from_id"), "" + extras.get("phone")
						, "" + extras.get("default"), "" + extras.get("request_id"), "" + extras.get("firstname"), "" + extras.get("lastname"), "" + extras.get("phone_user"));

			}
		}catch (Throwable e){
e.printStackTrace();
		}
	}

	private void sendNotification(String msg,String uid,String chatid,String name,String nickname,String msg_id,String key,String secret_key,String from_id,String phone,String defaultpar,String request_id,String firstname,String lastname,String phone_user) {

		PendingIntent resultPendingIntent;
		Intent resultIntent;

//
		if(secret_key.equals("secret_key")){

			resultIntent = new Intent(context, AcceptDecline.class);
			resultIntent.putExtra("key",key);
			resultIntent.putExtra("phone",phone);
			resultIntent.putExtra("from_id",from_id);
			resultIntent.putExtra("default",defaultpar);
			resultPendingIntent = PendingIntent.getActivity(context, 0,
					resultIntent, PendingIntent.FLAG_ONE_SHOT);
//
			NotificationCompat.Builder mNotifyBuilder;
			NotificationManager mNotificationManager;

			mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

			mNotifyBuilder = new NotificationCompat.Builder(context)
					.setSmallIcon(R.drawable.logo_secure).setContentText("You have succesfully receive a key").setContentTitle("Securemobileapp");
			// Set pending intent
			mNotifyBuilder.setContentIntent(resultPendingIntent);
			int defaults = 0;
			defaults = defaults | Notification.DEFAULT_LIGHTS;
			defaults = defaults | Notification.DEFAULT_VIBRATE;
			defaults = defaults | Notification.DEFAULT_SOUND;

			mNotifyBuilder.setDefaults(defaults);
			// Set the content for Notification
			// mNotifyBuilder.setContentText(msg);
			// Set autocancel
			mNotifyBuilder.setAutoCancel(true);
			// Post a notification
			mNotificationManager.notify(124, mNotifyBuilder.build());

		}else if(secret_key.equals("key_response")){
			resultIntent = new Intent(context, MainMenu.class);
			resultPendingIntent = PendingIntent.getActivity(context, 0,
					resultIntent, PendingIntent.FLAG_ONE_SHOT);

			NotificationCompat.Builder mNotifyBuilder;
			NotificationManager mNotificationManager;

			mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

			mNotifyBuilder = new NotificationCompat.Builder(context)
					.setSmallIcon(R.drawable.logo_secure).setContentText(msg).setContentTitle("Securemobileapp");
			// Set pending intent
			mNotifyBuilder.setContentIntent(resultPendingIntent);
			int defaults = 0;
			defaults = defaults | Notification.DEFAULT_LIGHTS;
			defaults = defaults | Notification.DEFAULT_VIBRATE;
			defaults = defaults | Notification.DEFAULT_SOUND;

			mNotifyBuilder.setDefaults(defaults);
			// Set the content for Notification
			// mNotifyBuilder.setContentText(msg);
			// Set autocancel
			mNotifyBuilder.setAutoCancel(true);
			// Post a notification
			mNotificationManager.notify(122, mNotifyBuilder.build());

		}
		else if(secret_key.equals("invitation")){
			resultIntent = new Intent(context, AcceptDecline.class);
			resultIntent.putExtra("message",msg);
			resultIntent.putExtra("secret_key",secret_key);
			resultIntent.putExtra("phone",phone);
			resultIntent.putExtra("phone_user",phone_user);
			resultIntent.putExtra("from_id",from_id);
			resultIntent.putExtra("default","inviteAccept");
			resultIntent.putExtra("request_id",request_id);
			resultPendingIntent = PendingIntent.getActivity(context, 0,
					resultIntent, PendingIntent.FLAG_ONE_SHOT);

			NotificationCompat.Builder mNotifyBuilder;
			NotificationManager mNotificationManager;

			mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

			mNotifyBuilder = new NotificationCompat.Builder(context)
					.setSmallIcon(R.drawable.logo_secure).setContentText(msg).setContentTitle("Securemobileapp");
			// Set pending intent
			mNotifyBuilder.setContentIntent(resultPendingIntent);
			int defaults = 0;
			defaults = defaults | Notification.DEFAULT_LIGHTS;
			defaults = defaults | Notification.DEFAULT_VIBRATE;
			defaults = defaults | Notification.DEFAULT_SOUND;

			mNotifyBuilder.setDefaults(defaults);
			// Set the content for Notification
			// mNotifyBuilder.setContentText(msg);
			// Set autocancel
			mNotifyBuilder.setAutoCancel(true);
			// Post a notification
			mNotificationManager.notify(999, mNotifyBuilder.build());

		}
		else {

			resultIntent = new Intent(context, ChatActivity.class);
			resultIntent.putExtra("msg", msg);
			resultIntent.putExtra("chat_id", chatid);
			resultIntent.putExtra("id", uid);
			resultIntent.putExtra("nickname", nickname);
			resultIntent.putExtra("username", name);
			resultIntent.putExtra("msg_id", msg_id);
			resultIntent.putExtra("firstname", firstname);
			resultIntent.putExtra("lastname", lastname);
			resultIntent.putExtra("phone_number",phone);
			resultPendingIntent = PendingIntent.getActivity(context, 0,
					resultIntent, PendingIntent.FLAG_ONE_SHOT);
//
			NotificationCompat.Builder mNotifyBuilder;
			NotificationManager mNotificationManager;

			mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

			mNotifyBuilder = new NotificationCompat.Builder(context)
					.setSmallIcon(R.drawable.logo_secure).setContentText(msg).setContentTitle("Securemobileapp");
			// Set pending intent
			mNotifyBuilder.setContentIntent(resultPendingIntent);

			// Set Vibrate, Sound and Light
			int defaults = 0;
			defaults = defaults | Notification.DEFAULT_LIGHTS;
			defaults = defaults | Notification.DEFAULT_VIBRATE;
			defaults = defaults | Notification.DEFAULT_SOUND;

			mNotifyBuilder.setDefaults(defaults);
			// Set the content for Notification
			// mNotifyBuilder.setContentText(msg);
			// Set autocancel
			mNotifyBuilder.setAutoCancel(true);
			// Post a notification
			mNotificationManager.notify(121, mNotifyBuilder.build());
		}
	}
}
