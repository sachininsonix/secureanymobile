package com.salvador.secureanymobileapps.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.salvador.secureanymobileapps.tokenlogin.Globals;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

/**
 * Created by nigel on 12/02/15.
 */
public class bluetoothManager {

    public static String TAG = "secureanymobileapps";

    public static boolean bluetoothInit() {
        Globals.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (Globals.mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            return false;

        } else {
            return true;
        }
    }

    public static boolean tokenConnect(String address) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> bondedSet =   bluetoothAdapter.getBondedDevices();
        int count = 0;

        if(bondedSet.size() > 0) {
            for(BluetoothDevice device : bondedSet) {
                if (device.getAddress().equals(address)) {
                    BluetoothSocket tmp = null;
                    BluetoothDevice mDevice = device;
                    // Get a BluetoothSocket to connect with the given BluetoothDevice
                    try {
                        // MY_UUID is the app's UUID string, also used by the server code
                        tmp = mDevice.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                    } catch (IOException e) {
                        return false;
                    }

                    Globals.mmSocket = tmp;
                    // Cancel discovery because it will slow down the connection
                    Globals.mBluetoothAdapter.cancelDiscovery();

                    try {
                        // Connect the device through the socket. This will block
                        // until it succeeds or throws an exception
                        Globals.mmSocket.connect();
                    } catch (IOException connectExceptionSearchProgressDialog) {
                        //Failed to Connect
                        return false;
                    } catch (NullPointerException e) {
                        //Failed to Connect
                        return false;
                    }

                    //WE ARE CONNECTED!
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean disconnectToken() {
        try {
            // DisConnect the device through the socket. This will block
            // until it succeeds or throws an exception
            Globals.mmSocket.close();
        } catch (IOException connectException) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }
}
