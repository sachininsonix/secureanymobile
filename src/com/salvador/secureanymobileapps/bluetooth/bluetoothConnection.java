package com.salvador.secureanymobileapps.bluetooth;

import android.bluetooth.BluetoothSocket;

import com.salvador.secureanymobileapps.tokenlogin.Globals;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by nigel on 12/02/15.
 */
public class bluetoothConnection extends Thread {

    //private final BluetoothSocket mmSocket;
    private final InputStream mmInStream;
    private final OutputStream mmOutStream;

    public bluetoothConnection(BluetoothSocket socket) {
        Globals.mmSocket = socket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        // Get the input and output streams, using temp objects because
        // member streams are final
        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {
        }

        mmInStream = tmpIn;
        mmOutStream = tmpOut;
    }

    public void run() {
        byte[] buffer = new byte[1024]; // buffer store for the stream
        int bytes;                        // bytes returned from read()

        // Keep listening to the InputStream until an exception occurs
        while (true) {

            try {
                // Read from the InputStream
                bytes = mmInStream.read(buffer);
                String str1 = new String(buffer);
            } catch (IOException e) {
                break;
            }
        }
    }

    /* Call this from the main activity to send data to the remote device */
    public void write(byte[] bytes) {
        try {
            mmOutStream.write(bytes);
        } catch (IOException e) {
        }
    }

    /* Call this from the main activity to shutdown the connection */
    public static void cancel() {
        try {
            Globals.mmSocket.close();
        } catch (IOException e) {
        }
    }

    public static void sendTokenPacket (byte command, String payload) {

        //Create a suitable packet for sending to the Token...
        byte[] payloadBytes = new byte[100];
        int payloadLen = payload.length();
        byte[] strLoad = payload.getBytes();
        byte checkSum=0;

        payloadBytes[0] = opCode.START_BYTE;
        payloadBytes[1] = command;
        payloadBytes[2] = (byte) (payloadLen & 0xFF);

        for(int i=0; i<payloadLen; i++) {
            payloadBytes[i+3] = strLoad[i];
            //Calculate Checksum...
            checkSum -= strLoad[i];
        }

        payloadBytes[payloadLen+3] = checkSum;
        payloadBytes[payloadLen+4] = opCode.STOP_BYTE;

        Globals.tokenConnection.write(payloadBytes);
    }
}
