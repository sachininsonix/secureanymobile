package com.salvador.secureanymobileapps.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.salvador.secureanymobileapps.R;

/**
 * Created by nigel on 27/05/15.
 */
public class ThemeSetter {



    public static int GetTheme(Context ctx) {

        SharedPreferences isTextNull = ctx.getSharedPreferences(
                "LoginPreferences", 0);

        if (!isTextNull.getString("theme", "").equals("")) {
            if (isTextNull.getString("theme", "").equals("Other Theme")){
                return R.style.Theme_SAM_Black;
            } else if (isTextNull.getString("theme", "").equals("SAM Theme 1")) {
                return R.style.Theme_SAM_White;
            } else if (isTextNull.getString("theme", "").equals("iOS")) {
                return R.style.Theme_SAM_iOS;
            } else if (isTextNull.getString("theme", "").equals("Windows")) {
                return R.style.Theme_SAM_Win;
            } else {
                return R.style.Theme_SAM_White;
            }
        }else {
            return R.style.Theme_SAM_White;
        }
    }

    public static void OverrideFonts(final Context context, final View v) {

        SharedPreferences isTextNull = context.getSharedPreferences(
                "LoginPreferences", 0);

        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    OverrideFonts(context, child);
                }
            } else if (v instanceof TextView) {

                if (!isTextNull.getString("theme", "").equals("")) {
                    if (isTextNull.getString("theme", "").equals("iOS")) {
                        ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/helvetica-medium.ttf"));
                    } else if (isTextNull.getString("theme", "").equals("Windows")) {
                        ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/segoe-ui.ttf"));
                    } else {
                        ((TextView) v).setTypeface(Typeface.DEFAULT_BOLD);
                    }
                }else {
                    ((TextView) v).setTypeface(Typeface.DEFAULT_BOLD);
                }
            }
        } catch (Exception e) {
        }
    }
}
