package com.salvador.secureanymobileapps.chat;


public interface IAppConfigurationConstant 
{
//	xmpp server details
//	final String HOST ="213.120.167.18"; //"213.162.246.196";....
	String HOSTNAME="@213.120.167.18";

//	final int PORT = 5222;
//	xmpp user prefs fields
	final String XMPP_USER_NAME="test1";
	final String XMPP_PASSWORD = "charlie7002";
	final String XMPP_PREF = "xmppLoginDetails";
	final int XMPP_MODE = 0;
	final String IS_SIGNED_IN="is_signed_in";
//	network error msg
    final String networkErrMsg="Please check internet connectivity";
    final String dataNotFoundErrMsg="Please check internet connectivity";
    final String emptyEmail = "Email Address is Empty";
	final String emptyPassword = "Password is Empty";
	

	final String hostnameStr="213.120.167.18";
	
	
//	chit chat database constants
	public static final int VERSION = 1;
	public static final String DBNAME="chitchat.db";
    public final String TABLE_USERS_LIST="userslist";
    public final String TABLE_CHAT_HISTORY = "chathistory";
    public final String chat_histroymsg = "No chat history available";
    
    
//    chat history table fields
    public final String U_ID="unique_id";
    public final String MAIL_ID="user_mail_id";
// joined groups database
    public String JOIED_GROUP_TABLE="joined_groups";
    
    public String GROUP_HISTORY_TABLE="group_history";
    public String SERVICE_NAME="@conference.217.70.39.237";
    public String PATTERN="#!@15#!@";

//    XMPP Error Codes
    public String conflict409="conflict(409)";
    public String loginError="SASL authentication failed using mechanism DIGEST-MD5";
//    xmpp error messages
    public String conflict409errormsg="this id is already registered please try another";
    
//    
    public int TYPE_WIFI = 1;
    public int TYPE_MOBILE = 2;
    public int TYPE_NOT_CONNECTED = 0;


}
