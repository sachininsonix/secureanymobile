package com.salvador.secureanymobileapps.chat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;
import com.salvador.secureanymobileapps.util.ThemeSetter;

/**
 * Created by station12 on 9/22/14.
 */
public class ContactUs extends BaseActivity implements View.OnClickListener {

    private SharedPreferences isTextNull;
    private Button back;
    private EditText txtMessage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        setTheme(ThemeSetter.GetTheme(this));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ThemeSetter.OverrideFonts(this, findViewById(android.R.id.content));
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
        ((Button) findViewById(R.id.btnsendMessage)).setOnClickListener(this);
        this.txtMessage = (EditText) findViewById(R.id.txtMessage);
    }
    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        String tag = view.getTag() + "";
        if (tag.equals("back")) {
            this.onBackPressed();
        } else if (tag.equals("sendMessage")) {
            String message = this.txtMessage.getText().toString();
            if (message.trim().length() > 0) {
                Intent intentEmail = new Intent(Intent.ACTION_SEND);
                intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{"help@secureanymobileapps.com"});
                intentEmail.putExtra(Intent.EXTRA_SUBJECT, "Contact Us");
                intentEmail.putExtra(Intent.EXTRA_TEXT, message);
                intentEmail.setType("message/rfc822");
                startActivity(Intent.createChooser(intentEmail, "Choose an email provider:"));
                Toast.makeText(ContactUs.this, "Email Send", Toast.LENGTH_SHORT).show();
                this.txtMessage.setText("");
            } else {
                Toast.makeText(ContactUs.this, "Error: Not Email Send", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
