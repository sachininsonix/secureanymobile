package com.salvador.secureanymobileapps.chat;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.sms.util.Encryption;
import com.salvador.secureanymobileapps.tokenlogin.Chat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FirebaseListAdapter<T> extends BaseAdapter {
	private Query mRef;
	private Class<Chat> mModelClass;
	 private int mLayout,thirdLayoutChat,fourthLayoutChat,secondLayout;
	 private LayoutInflater mInflater;
	    private List<Chat> mModels;
    ImageView single;
	    String userName,chat_id,msg_id,key,first,url,phone_number;
	    SharedPreferences pref;
    boolean decr=false;
    boolean decr1;
	    ArrayList<String> keysFireBase;
	    SharedPreferences.Editor editor;
	    private Map<String, Chat> mModelKeys;
	    private ChildEventListener mListener;
	    Activity activity;
	    Chat chat;
	public FirebaseListAdapter(Query mRef, Class<Chat> mModelClass, int mLayout, int secondLayout, Activity activity, String chat_id,String key) {
		// TODO Auto-generated constructor stub
		this.mRef=mRef;
		this.mModelClass=mModelClass;
		this.mLayout=mLayout;
		this.secondLayout=secondLayout;
		this.thirdLayoutChat=thirdLayoutChat;
		this.fourthLayoutChat=fourthLayoutChat;
		this.activity=activity;
		this.chat_id = chat_id;
		keysFireBase=new ArrayList<>();
        mInflater = activity.getLayoutInflater();
        mModels = new ArrayList<Chat>();
        pref = activity.getSharedPreferences(
                "LoginPreferences", 0);
        editor=pref.edit();
        userName=pref.getString("uid", " ");
        mModelKeys = new HashMap<String, Chat>();
        mListener = this.mRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
            	Chat model = dataSnapshot.getValue(FirebaseListAdapter.this.mModelClass);
                mModelKeys.put(dataSnapshot.getKey(), model);
                keysFireBase.add(dataSnapshot.getKey());
                // Insert into the correct location, based on previousChildName
                if (previousChildName == null) {
                    mModels.add(0, model);
                } else {
                	Chat previousModel = mModelKeys.get(previousChildName);
                    int previousIndex = mModels.indexOf(previousModel);
                    int nextIndex = previousIndex + 1;
                    if (nextIndex == mModels.size()) {
                        mModels.add(model);
                    } else {
                        mModels.add(nextIndex, model);
                    }
                }
                notifyDataSetChanged();
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                // One of the mModels changed. Replace it in our list and name mapping
                String modelName = dataSnapshot.getKey();
                Chat oldModel = mModelKeys.get(modelName);
                Chat newModel = dataSnapshot.getValue(FirebaseListAdapter.this.mModelClass);
                int index = mModels.indexOf(oldModel);
                mModels.set(index, newModel);
                mModelKeys.put(modelName, newModel);
                notifyDataSetChanged();
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                // A model was removed from the list. Remove it from our list and the name mapping
                String modelName = dataSnapshot.getKey();
                Chat oldModel = mModelKeys.get(modelName);
                mModels.remove(oldModel);
                mModelKeys.remove(modelName);
                notifyDataSetChanged();
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                // A model changed position in the list. Update our list accordingly
                String modelName = dataSnapshot.getKey();
                Chat oldModel = mModelKeys.get(modelName);
                Chat newModel = dataSnapshot.getValue(FirebaseListAdapter.this.mModelClass);
                int index = mModels.indexOf(oldModel);
                mModels.remove(index);
                if (previousChildName == null) {
                    mModels.add(0, newModel);
                } else {
                	Chat previousModel = mModelKeys.get(previousChildName);
                    int previousIndex = mModels.indexOf(previousModel);
                    int nextIndex = previousIndex + 1;
                    if (nextIndex == mModels.size()) {
                        mModels.add(newModel);
                    } else {
                        mModels.add(nextIndex, newModel);
                    }
                }
                notifyDataSetChanged();
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });


	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mModels.size();
	}

    @Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mModels.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		chat=mModels.get(position);

//        final ViewHolder holder;
//
//        if(chat.getChatID().equals(userName)) {
//            if (chat.getType().equalsIgnoreCase("message")) {
//
//                if (convertView == null) {
//
//                    convertView = mInflater.inflate(secondLayout,null);
//                    holder = new ViewHolder();
//                    holder.singleMessage = (TextView) convertView.findViewById(R.id.singleMessage);
//                    holder.time = (TextView) convertView.findViewById(R.id.textView_timeChat);
////                    holder.single = (ImageView) convertView.findViewById(R.id.single);
//                    holder.btn_decr = (Button) convertView.findViewById(R.id.btn_decr);
//                    convertView.setTag(holder);
//            }
//
//            else {
//                holder = (ViewHolder) convertView.getTag();
//            }
////                final TextView singleMessage=(TextView)convertView.findViewById(R.id.singleMessage);
//
////            final TextView singleMessage = (TextView) convertView.findViewById(R.id.singleMessage);
//
////            holder.btn_decr.setTag(position);
////
//            key=chat.getFirst();
//
//                holder.btn_decr.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
////                        int id = (Integer) view.getTag();
////                        chat = mModels.get(id);
//                        holder.btn_decr.setVisibility(View.GONE);
//                        if (decript_sms(chat.getMessage()) != null) {
//                            chat.setDecr(true);
//                            holder.btn_decr.setVisibility(View.GONE);
//
//                            holder.singleMessage.setText(decript_sms(chat.getMessage()));
//
//
//                        } else {
//                            holder.singleMessage.setText(chat.getMessage());
//                        }
////
//                    }
//                });
//                if(chat.getDecr()){
//                    holder.singleMessage.setText(decript_sms(chat.getMessage()));
//                    holder.btn_decr.setVisibility(View.GONE);
//                }else{
//                    holder.singleMessage.setText(chat.getMessage());
//                }
////            TextView time = (TextView) convertView.findViewById(R.id.textView_timeChat);
////            TextView datetv = (TextView) convertView.findViewById(R.id.datetv);
//
//            String string = chat.getTime();
////                Log.d("",""+chat.g)
//            String[] parts = string.split(",");
//            String part1 = parts[0]; // 004
//            String part2 = parts[1]; // 034556
//            holder.time.setText(part1);
//            }
//        }else {
//                if (chat.getType().equalsIgnoreCase("message")) {
//
//                    if (convertView == null) {
//
//                        convertView = mInflater.inflate(mLayout, null);
//                        holder = new ViewHolder();
//
//                        holder.singleMessage = (TextView) convertView.findViewById(R.id.singleMessage1);
//                        holder.time = (TextView) convertView.findViewById(R.id.textView_timeChatOthers);
////                        holder.single = (ImageView) convertView.findViewById(R.id.single);
//                        holder.btn_decr = (Button) convertView.findViewById(R.id.btn_decr1);
//                        convertView.setTag(holder);
//                    }
//
//                    else {
//                        holder = (ViewHolder) convertView.getTag();
//                    }
////                    final TextView singleMessage=(TextView)convertView.findViewById(R.id.singleMessage1);
////                   final TextView singleMessage = (TextView) convertView.findViewById(R.id.singleMessage1);
////
////                    final Button btn_decr=(Button)convertView.findViewById(R.id.btn_decr);
////                    holder.btn_decr.setTag(position);
//                    key=chat.getFirst();
//
//                    holder.btn_decr.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
////                            int id=(Integer)view.getTag();
////                            chat=mModels.get(id);
//                            holder.btn_decr.setVisibility(View.GONE);
//                            if (decript_sms(chat.getMessage()) != null) {
//                                chat.setDecr(true);
//
//                                holder.btn_decr.setVisibility(View.GONE);
//                                holder.singleMessage.setText(decript_sms(chat.getMessage()));
//                                notifyDataSetChanged();
//                            } else {
//                                holder.singleMessage.setText(chat.getMessage());
//                            }
//                        }
//                    });
//                    if(chat.getDecr()){
//                        holder.singleMessage.setText(decript_sms(chat.getMessage()));
//                        holder.btn_decr.setVisibility(View.GONE);
//                }else{
//                        holder.singleMessage.setText(chat.getMessage());
//                }
////                    TextView time = (TextView) convertView.findViewById(R.id.textView_timeChatOthers);
//                   String string = chat.getTime();
//                    String[] parts = string.split(",");
//                    String part1 = parts[0]; // 004
//                    String part2 = parts[1]; // 034556
//                    holder.time.setText(part1);
//                }
//            }
//        final ViewHolder holder;
		if(chat.getChatID().equals(userName)) {


            if (chat.getType().equalsIgnoreCase("message")) {
//                if (convertView == null) {
////
//                    convertView = mInflater.inflate(secondLayout,null);
//                    holder = new ViewHolder();
//                    holder.singleMessage = (TextView) convertView.findViewById(R.id.singleMessage);
//                    holder.time = (TextView) convertView.findViewById(R.id.textView_timeChat);
////                    holder.single = (ImageView) convertView.findViewById(R.id.single);
//                    holder.btn_decr = (Button) convertView.findViewById(R.id.btn_decr);
//                    convertView.setTag(holder);
//            }
//
//            else {
//                holder = (ViewHolder) convertView.getTag();
//            }
                convertView = mInflater.inflate(secondLayout, null);
              final TextView singleMessage = (TextView) convertView.findViewById(R.id.singleMessage);
               final Button btn_decr=(Button)convertView.findViewById(R.id.btn_decr);
                btn_decr.setTag(position);
//
                key=chat.getFirst();
//

//             chat.setDecr(true);

//                btn_decr.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        int id = (Integer) view.getTag();
//                        chat = mModels.get(id);
//
//                        if (decript_sms(chat.getMessage()) != null) {
//                            chat.setDecr(true);
//                            btn_decr.setVisibility(View.GONE);
//
//                            singleMessage.setText(decript_sms(chat.getMessage()));
//
//
//                        } else {
//                            singleMessage.setText(chat.getMessage());
//                        }
//                    }
//                });
                if(decript_sms(chat.getMessage())!=null){
                    singleMessage.setText(decript_sms(chat.getMessage()));
                }else {
                    singleMessage.setText(chat.getMessage());
                }

//                if(chat.getDecr()){
//                    singleMessage.setText(decript_sms(chat.getMessage()));
////                    btn_decr.setVisibility(View.GONE);
//                }else{
//                    singleMessage.setText(chat.getMessage());
//                }

                 TextView time = (TextView) convertView.findViewById(R.id.textView_timeChat);
//                TextView datetv = (TextView) convertView.findViewById(R.id.datetv);
//                single=(ImageView)convertView.findViewById(R.id.single);
                String getdate_time = chat.getTime();
////                Log.d("",""+chat.g)
//                String[] parts = string.split(",");
//                String part1 = parts[0]; // 004
//                String part2 = parts[1]; // 034556
                time.setText(getdate_time);
            }
        }else {
                if (chat.getType().equalsIgnoreCase("message")) {
//
                    convertView = mInflater.inflate(mLayout, null);
                   final TextView singleMessage = (TextView) convertView.findViewById(R.id.singleMessage1);
//
//                    final Button btn_decr=(Button)convertView.findViewById(R.id.btn_decr1);
//                    btn_decr.setTag(position);
                    key=chat.getFirst();

//                    chat.setDecr(true);

//                    btn_decr.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            int id = (Integer) view.getTag();
//                            chat = mModels.get(id);
//
//                            if (decript_sms(chat.getMessage()) != null) {
//                                chat.setDecr(true);
//
//                                btn_decr.setVisibility(View.GONE);
//                                singleMessage.setText(decript_sms(chat.getMessage()));
//
//                            } else {
//                                singleMessage.setText(chat.getMessage());
//                            }
//                        }
//                    });
                    if(decript_sms(chat.getMessage())!=null){
                        singleMessage.setText(decript_sms(chat.getMessage()));
                    }else {
                        singleMessage.setText(chat.getMessage());
                    }
//                    if(chat.getDecr()){
//                        singleMessage.setText(decript_sms(chat.getMessage()));
//                        btn_decr.setVisibility(View.GONE);
//                    }else{
//                        singleMessage.setText(chat.getMessage());
//                    }

                    TextView time = (TextView) convertView.findViewById(R.id.textView_timeChatOthers);
                    String getdate_time = chat.getTime();
//                   String string = chat.getTime();
//                    String[] parts = string.split(",");
//                    String part1 = parts[0]; // 004
//                    String part2 = parts[1]; // 034556
                    time.setText(getdate_time);
                }
            }

		return convertView;
	}

	public void cleanup() {
		// TODO Auto-generated method stub
		 mRef.removeEventListener(mListener);
	        mModels.clear();
	        mModelKeys.clear();
	}
	public static Bitmap decodeSampledBitmapFromByte(byte[] decodedByte,
            int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return  BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length, options);
    }
	public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;
    if (height > reqHeight || width > reqWidth) {
        final int halfHeight = height / 2;
        final int halfWidth = width / 2;
        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
            inSampleSize *= 2;
        }
    }
    return inSampleSize;
}
    private String decript_sms(String smsEncript){
        String encrypted = smsEncript.toString();
        Encryption encryption = new Encryption();
        String decrypted = encryption.decrypt(key, encrypted);
        return decrypted;
    }
    static class ViewHolder {
        public TextView singleMessage;
//        public TextView time;

        public Button btn_decr;
    }


}
