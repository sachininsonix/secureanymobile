package com.salvador.secureanymobileapps.chat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;

public class Settings extends BaseActivity implements OnClickListener {
    private LinearLayout help, profile, chatsetting;
    private SharedPreferences isTextNull;
    private Button back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        help = (LinearLayout) findViewById(R.id.setting_help);
        profile = (LinearLayout) findViewById(R.id.setting_profile);
        chatsetting = (LinearLayout) findViewById(R.id.setting_chat);
        help.setOnClickListener(this);
        profile.setOnClickListener(this);
        chatsetting.setOnClickListener(this);
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
    }
    @Override
    public void onResume() {
        super.onResume();
        Resources resources = getResources();
        RelativeLayout containerSettingsChat = (RelativeLayout) findViewById(R.id.containerSettingsChat);
        TextView title = (TextView) findViewById(R.id.titlegeneraldocs);
        TextView label1 = (TextView) findViewById(R.id.textView1);
        TextView label2 = (TextView) findViewById(R.id.textView14);
        TextView label3 = (TextView) findViewById(R.id.textView12);
        View separatorTitle = (View) findViewById(R.id.lineDivider2);

        ImageView icon1 =(ImageView) findViewById(R.id.imageView10);
        ImageView icon2 =(ImageView) findViewById(R.id.imageView13);
        ImageView icon3 =(ImageView) findViewById(R.id.imageView11);

        isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        if (!isTextNull.getString("theme", "").equals("")) {
            if (isTextNull.getString("theme", "").equals("Black/Grey")) {
                containerSettingsChat.setBackgroundColor(resources.getColor(R.color.backgroundTheme1));
                title.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                label1.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                label2.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                label3.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                separatorTitle.setBackgroundColor(resources.getColor(R.color.colorSeparatorTheme1));
                this.back.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.arrow_light, 0);
                icon1.setBackgroundResource(R.drawable.help_light);
                icon2.setBackgroundResource(R.drawable.profile_light);
                icon3.setBackgroundResource(R.drawable.chat_settings_light);

            } else {

                containerSettingsChat.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
                title.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                label1.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                label2.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                label3.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                separatorTitle.setBackgroundColor(resources.getColor(R.color.colorSeparatorTheme2));
                this.back.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.arrow_dark, 0);
                icon1.setBackgroundResource(R.drawable.help_dark);
                icon2.setBackgroundResource(R.drawable.profile_dark);
                icon3.setBackgroundResource(R.drawable.chat_settings_dark);
            }
        } else {
            containerSettingsChat.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
            title.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
            label1.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
            label2.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
            label3.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
            separatorTitle.setBackgroundColor(resources.getColor(R.color.colorSeparatorTheme2));
            this.back.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                    R.drawable.arrow_dark, 0);
            icon1.setBackgroundResource(R.drawable.help_dark);
            icon2.setBackgroundResource(R.drawable.profile_dark);
            icon3.setBackgroundResource(R.drawable.chat_settings_dark);

        }

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.setting_help:
                startActivity(new Intent(Settings.this, Help.class
                ));
                break;
            case R.id.setting_profile:
                Intent intent = new Intent(Settings.this, UserProfile.class);
                intent.putExtra("titleProfile", "Chat-User Profile");
                startActivity(intent);
                break;
            case R.id.setting_chat:
//                startActivity(new Intent(Settings.this, Chat_settings.class
//                ));
                break;

            case R.id.back_button:
                this.onBackPressed();
                break;

        }
    }
}
