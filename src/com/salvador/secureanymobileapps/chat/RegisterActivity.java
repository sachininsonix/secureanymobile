package com.salvador.secureanymobileapps.chat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;

import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by insonix on 17/12/15.
 */
public class RegisterActivity extends Activity implements View.OnClickListener, Iconstant {
    EditText txtFirstName, _newUser, _newPass, _confirmPass, txtEmail, txtPhoneNumber, txtlastName;
    Spinner txtCountry;
    Button _createUser, back;
    ProgressDialog progressDialog;
    SharedPreferences preferences, sharedprefernces;
    SharedPreferences.Editor editor;
    private String regid = null;
    private Context context = null;
    protected String SENDER_ID = "155375531524";
    private GoogleCloudMessaging gcm = null;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    String url, txtCountry_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_create_account);
//        MainActivity.tracker().send(new HitBuilders.EventBuilder("ui", "open")
//                .setLabel("Register Activity")
//                .build());
        progressDialog = new ProgressDialog(RegisterActivity.this);
        sharedprefernces = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        preferences = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        editor = preferences.edit();
        txtFirstName = (EditText) findViewById(R.id.txtFirstName);
        txtlastName = (EditText) findViewById(R.id.txtlastName);
        _newUser = (EditText) findViewById(R.id.editNewUsername);
        _newPass = (EditText) findViewById(R.id.editNewPasswd);
        _confirmPass = (EditText) findViewById(R.id.editconfirmNewPasswd);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPhoneNumber = (EditText) findViewById(R.id.txtPhoneNumber);
        txtCountry = (Spinner) findViewById(R.id.txtCountry);
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
        this._createUser = (Button) findViewById(R.id.user_create_button);
        this._createUser.setOnClickListener(this);
//        txtFirstName.setInputType(InputType.TYPE_TEXT_VARIATION_NORMAL);

//        alphaValidation();
//        alphaValidation1();
        String[] countries = getResources().getStringArray(R.array.countries_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.county_list, R.id.countries, countries);

        adapter.setDropDownViewResource(R.layout.spinner_text);

        txtCountry.setAdapter(adapter);
        txtCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                ((TextView)parent.getChildAt(0)).setTextColor(Color.parseColor("#FFFFFF"));
                txtCountry_text = txtCountry.getSelectedItem().toString();
                Log.d("textcountry", "textcountry" + txtCountry_text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (checkPlayServices()) {
            registerInBackground();
        } else {
        }
        _newUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _newUser.requestFocus();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.user_create_button:

                final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (isNetworkAvailable() == true) {
                    if (txtFirstName.getText().toString().trim().length() > 0) {
                        if (txtlastName.getText().toString().trim().length() > 0) {
                            if (txtEmail.getText().toString().trim().length() > 0) {
                                if (txtEmail.getText().toString().trim().matches(emailPattern)) {
                                    if (_newUser.getText().toString().trim().length() > 0) {
                                        if (_newUser.getText().toString().trim().length() >= 5) {
                                            if (_newPass.getText().toString().trim().length() > 0) {
                                                if (_newPass.getText().toString().trim().length() > 5) {
                                                    if (_confirmPass.getText().toString().trim().length() > 0) {
                                                        if (_confirmPass.getText().toString().equals(_newPass.getText().toString())) {
                                                            if (txtPhoneNumber.getText().toString().length() > 0) {
                                                                if (txtPhoneNumber.getText().toString().length() >= 10) {
                                                                    if (txtCountry_text.length() > 0) {
                                                                        Register();
                                                                    } else {
                                                                        Toast.makeText(RegisterActivity.this, "Country name is required!", Toast.LENGTH_SHORT).show();
//                                                    new ShowMsg().createDialog(RegisterActivity.this, "Please enter your country");
                                                                    }
                                                                } else {
                                                                    txtPhoneNumber.setError("Mobile number atleast 10 digits!");
                                                                    txtPhoneNumber.requestFocus();
//                                                new ShowMsg().createDialog(RegisterActivity.this, "Please enter your mobile number");
                                                                }
                                                            } else {
                                                                txtPhoneNumber.setError("Mobile number is required!");
                                                                txtPhoneNumber.requestFocus();
//                                                new ShowMsg().createDialog(RegisterActivity.this, "Please enter your mobile number");
                                                            }
                                                        } else {
                                                            _confirmPass.setError("Password does not match!");
                                                            _confirmPass.requestFocus();
//                                            new ShowMsg().createDialog(RegisterActivity.this, "password doesnot match with confirm password");
                                                        }

                                                    } else {
                                                        _confirmPass.setError("Confirm Password is required!");
                                                        _confirmPass.requestFocus();
//                                new ShowMsg().createDialog(RegisterActivity.this, "Please enter your password");
                                                    }
                                                } else {
                                                    _newPass.setError("Password must be atleast 6 character!");
                                                    _newPass.requestFocus();
//                                new ShowMsg().createDialog(RegisterActivity.this, "Please enter your password");
                                                }
                                            } else {
                                                _newPass.setError("Password is required!");
                                                _newPass.requestFocus();
//                                new ShowMsg().createDialog(RegisterActivity.this, "Please enter your password");
                                            }
                                        } else {
                                            _newUser.setError("Username must be atleast 5 character!");
                                            _newUser.requestFocus();
//                            new ShowMsg().createDialog(RegisterActivity.this, "Please enter your username");
                                        }
                                    } else {
                                        _newUser.setError("User name is required!");
                                        _newUser.requestFocus();
//                            new ShowMsg().createDialog(RegisterActivity.this, "Please enter your username");
                                    }
                                } else {
                                    txtEmail.setError("Invalid EmailID!");
                                    txtEmail.requestFocus();
//                        new ShowMsg().createDialog(RegisterActivity.this, "Please enter your email");
                                }
                            } else {
                                txtEmail.setError("EmailId is required!");
                                txtEmail.requestFocus();
//                        new ShowMsg().createDialog(RegisterActivity.this, "Please enter your email");
                            }
                        } else {
                            txtlastName.setError("Last name is required!");
                            txtlastName.requestFocus();
//                        new ShowMsg().createDialog(RegisterActivity.this, "Please enter your last name");
                        }
                    } else {
                        txtFirstName.setError("First name is required!");
                        txtFirstName.requestFocus();
//                    new ShowMsg().createDialog(RegisterActivity.this, "Please enter your first name");
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                    builder.setMessage("Not able to connect to SAM. Please check your network connection and try again.")
                            .setTitle("Error")
                            .setCancelable(false)
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                    //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.back_button:
                Intent intent = new Intent(RegisterActivity.this, ChatMainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void Register() {
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url = BaseUrl + "register.php?name=" + txtFirstName.getText().toString().trim() + "&lastname=" + txtlastName.getText().toString().trim() + "&email=" + txtEmail.getText().toString().trim() + "&mobile=" + txtPhoneNumber.getText().toString().trim() + "&password=" + _newPass.getText().toString().trim() + "&username=" + _newUser.getText().toString().trim() + "&country=" + txtCountry_text + "&device_id=" + regid;
        Log.d("url1111:", "url" + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try {
                    String Status = jsonObject.getString("Status");
                    String message = jsonObject.getString("Message");
                    if (Status.equals("true")) {
//                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
//                        startActivity(intent);
//                        finish();
                        new ShowMsg().createDialogSignUpcon(RegisterActivity.this, message);
                    } else {
//                        Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                        new ShowMsg().createDialog(RegisterActivity.this, message);
                    }
                } catch (Exception e) {
                    Log.d("eeeee:", "eeeee" + e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
                progressDialog.dismiss();
                Toast.makeText(RegisterActivity.this, "Internet is slow!", Toast.LENGTH_SHORT).show();
            }


        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }

    private boolean checkPlayServices() {
        int resultcode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (resultcode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultcode)) {
                GooglePlayServicesUtil.getErrorDialog(resultcode, RegisterActivity.this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {

            }
            return false;
        }
        return true;
    }

    public void registerInBackground() {
        new AsyncTask<Void, Void, String>() {

            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging
                                .getInstance(getApplicationContext());
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Registration ID :" + regid;
                    Log.i("Registration id", regid);

                    editor.putString("reg_id", regid);
                    editor.commit();


                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (!TextUtils.isEmpty(regid)) {
                } else {
                }
            }
        }.execute(null, null, null);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

//
}
