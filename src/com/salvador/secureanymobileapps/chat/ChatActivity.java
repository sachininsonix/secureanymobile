package com.salvador.secureanymobileapps.chat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.analytics.HitBuilders;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.documents.SimmpleApp;
import com.salvador.secureanymobileapps.sms.util.Encryption;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.tokenlogin.Chat;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;
import com.salvador.secureanymobileapps.util.ColorGenerator;
import com.salvador.secureanymobileapps.util.TextDrawable;

import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.Calendar;

/**
 * Created by insonix on 17/11/15.
 */
public class ChatActivity extends Activity implements Iconstant, View.OnClickListener {
    EditText edit_chat;
    ListView chat_list;
    //    Button send;
    private SharedPreferences isTextNull;
    ImageView image;
    String chat_id, Name, url1, key;
    String sendtext, url, id, nickname, msg_id, url2, read, key1, phone_number, encrypted;
    Calendar cal;
    Button back_button, btn_decr;
    boolean decr = false;
    TextView name, time, nickname_user, datetv;
    ProgressDialog progressDialog;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    //    static Dialog imageDialog;
    private static final String FIREBASE_URL = "https://secureanymobileapps.firebaseio.com/";
    FirebaseListAdapter<Chat> firebaseListAdapter;
    private String mUsername, photo;
    private Firebase mFirebaseRef;
    private ValueEventListener mConnectedListener;
    String firstname, msg, lastname, imageBase64String, first = "";
    ImageView back, single, send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);
        ApplicationClass.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("Chat Activity")
                .build());
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        name = (TextView) findViewById(R.id.username);
        nickname_user = (TextView) findViewById(R.id.nickname);
        image = (ImageView) findViewById(R.id.image);
        datetv = (TextView) findViewById(R.id.datetv);
//        time=(TextView)findViewById(R.id.time);
//        price=(TextView)findViewById(R.id.price);
        edit_chat = (EditText) findViewById(R.id.chatting_et_msg);
        chat_list = (ListView) findViewById(R.id.chatting_lv_msg);
        send = (ImageView) findViewById(R.id.chatting_btn_send);
        progressDialog = new ProgressDialog(ChatActivity.this);
//        preferences =getApplicationContext().getSharedPreferences(
//                "LoginPreferences", 0);
        setupUsername();
        Firebase.setAndroidContext(this);
        setTitle("Chatting as " + mUsername);
        try {
            chat_id = getIntent().getExtras().getString("chat_id");
            id = getIntent().getExtras().getString("id");
            Log.d("id is", "id id" + id);
            msg = getIntent().getExtras().getString("msg");
            nickname = getIntent().getExtras().getString("nickname");
            Name = getIntent().getExtras().getString("username");
            firstname = getIntent().getExtras().getString("firstname");
            lastname = getIntent().getExtras().getString("lastname");
            msg_id = getIntent().getExtras().getString("msg_id");
            phone_number = getIntent().getExtras().getString("phone_number");

        } catch (Exception e) {

        }
        name.setText(firstname + " " + lastname);
        String s1 = String.valueOf(nickname);
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(nickname);
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(35)  // width in px
                .height(35) // height in px
                .endConfig()
                .buildRound(s1, color);
        image.setImageDrawable(drawable);
        try {
            mFirebaseRef = new Firebase(FIREBASE_URL).child("chat").child(chat_id);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        back_button = (Button) findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChatActivity.this, ChatHistory.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
//                finish();
            }
        });
//
        increaseInstanceLimit();
        Getkey_first();
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (key.equals("")) {

                    Getkey();
                } else {
                    sendMessage();
                    Getkey();
                }


            }
        });

        firebaseListAdapter = new FirebaseListAdapter<Chat>(mFirebaseRef.limit(50), Chat.class, R.layout.chat_item, R.layout.chat_item_other, ChatActivity.this, chat_id, key);
//
        chat_list.setAdapter(firebaseListAdapter);

        firebaseListAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                chat_list.setSelection(firebaseListAdapter.getCount() - 1);
            }
        });

        mConnectedListener = mFirebaseRef.getRoot().child(".info/connected")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        boolean connected = (Boolean) dataSnapshot.getValue();
                        if (connected) {
                        } else {
                        }
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {
                        // No-op
                    }
                });
    }

    /**
     * Strict mode policy violation i.e. Instances=2 limit=1 error
     */


    private void increaseInstanceLimit() {

        Method m = null;
        try {
            m = StrictMode.class.getMethod("incrementExpectedActivityCount", Class.class);
            m.invoke(null, ChatActivity.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

    }

    private void setupUsername() {

        mUsername = isTextNull.getString("uid", "");

    }

    @Override
    public void onPause() {
        super.onPause();
       /* if (!this.isFinishing()) {
            //Insert code for HOME  key Event
            MainActivity.getInstance().clearPreferences();
        }*/
    }

    private void sendMessage() {

        sendtext = edit_chat.getText().toString();
        Encryption encryption = new Encryption();
        encrypted = encryption.encrypt(key.getBytes(), sendtext.getBytes());
        if (!sendtext.equals("")) {
            // Create our 'model', a Chat object

            Chat chat = new Chat(encrypted, mUsername,  getTime(), "message", key, false);
            // Create a new, auto-generated child of that chat location, and save our chat data there
            mFirebaseRef.push().setValue(chat);
            Sendchat();
//            new ChatTask().execute(sendtext);
            edit_chat.setText("");
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        mFirebaseRef.getRoot().child(".info/connected")
                .removeEventListener(mConnectedListener);
        firebaseListAdapter.cleanup();
    }

    public void Sendchat() {

//
        url = BaseUrl + "chat.php?sender_id=" + isTextNull.getString("uid", "") + "&receiver_id=" + id + "&message=" + URLEncoder.encode(encrypted);
        Log.d("wwwww", "wwwww" + url);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
//
                    String status = jsonObject.getString("status");

                    first = jsonObject.getString("first");


                } catch (Exception e) {
                }
//                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

//                progressDialog.dismiss();
            }


        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

    //
    private String getTime() {
        String hour_st;
        String minute_st;
        String am_pm_st;
        String month_st = null;
        String date_st;
        cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minutes = cal.get(Calendar.MINUTE);
        int month = cal.get(Calendar.MONTH);
        int date = cal.get(Calendar.DATE);
        int am_pm = cal.get(Calendar.AM_PM);
        int year = cal.get(Calendar.YEAR);
        if (am_pm == 0) {
            am_pm_st = "AM";
        } else {
            am_pm_st = "PM";
        }
        if (hour < 10) {
            hour_st = "0" + hour;
        } else {
            hour_st = "" + hour;
        }
        if (minutes < 10) {

            minute_st = "0" + minutes;
        } else {
            minute_st = "" + minutes;
        }
        /*if (month < 10) {

			month_st = "0" + month;
		} else {
			month_st = "" + minutes;
		}*/
        if (date < 10) {
            date_st = "0" + date;
        } else {
            date_st = "" + date;
        }
        switch (month) {
            case 0:
                month_st = "Jan";
                break;
            case 1:
                month_st = "Feb";
                break;
            case 2:
                month_st = "March";
                break;
            case 3:
                month_st = "April";
                break;

            case 4:
                month_st = "May";
                break;

            case 5:
                month_st = "June";
                break;

            case 6:
                month_st = "July";
                break;

            case 7:
                month_st = "Aug";
                break;

            case 8:
                month_st = "Sept";
                break;
            case 9:
                month_st = "Oct";
                break;
            case 10:
                month_st = "Nov";
                break;
            case 11:
                month_st = "Dec";
                break;
        }
        String time = hour_st + ":" + minute_st + ", " + date + " " + month_st +
                " " + year;
        return time;

    }

    public void Getkey() {

        url2 = BaseUrl + "get_keybyid.php?from_id=" + isTextNull.getString("uid", "") + "&phone=" + phone_number + "&user_number=" + isTextNull.getString("phone_number", "");
        Log.d("url2:", "url2" + url2);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());

                String message = edit_chat.getText().toString();
                Log.d("messss", "messs" + message);
                try {
                    String Status = jsonObject.getString("status");
                    if (Status.equals("true")) {
                        String from_id = jsonObject.getString("from_id");
                        int device_id = jsonObject.getInt("device_id");
                        key = jsonObject.getString("key");
                        if (device_id == 0) {
                            new ShowMsg().createDialog(ChatActivity.this, "You can't currently connect.User is not logged in this time!");
                        } else {
//                            sendMessage();
                        }
                        String phone = jsonObject.getString("phone");
                    } else if (Status.equals("false")) {
                        new ShowMsg().createDialogSignUp(ChatActivity.this, "Before start chat you need to first share a key, GoTo-> Settings->Profile-> click on Share icon to share key.");

                    } else {
                        Toast.makeText(ChatActivity.this, "Number not registered, Please enter registered number", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }


        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(ChatActivity.this, ChatHistory.class);
        startActivity(in);
        overridePendingTransition(0, 0);
//        finish();
    }

    public void Getkey_first() {
        url1 = BaseUrl + "get_keybyid.php?from_id=" + isTextNull.getString("uid", "") + "&phone=" + phone_number + "&user_number=" + isTextNull.getString("phone_number", "");
        Log.d("url1:", "url1" + url1);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try {
                    String Status = jsonObject.getString("status");
                    if (Status.equals("true")) {
//                        JSONObject response=jsonObject.getJSONObject("response");
                        String from_id = jsonObject.getString("from_id");
                        key = jsonObject.getString("key");
                        String phone = jsonObject.getString("phone");
                    } else if (Status.equals("false")) {
                    }
                } catch (Exception e) {
                }
//                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }


        });
//        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
        Volley.newRequestQueue(this).add(jsonObjectRequest);

    }
}


