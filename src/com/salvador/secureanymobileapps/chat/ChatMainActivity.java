package com.salvador.secureanymobileapps.chat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.util.ThemeSetter;

import java.io.IOException;


/**
 * Created by KPC on 20/08/2014.
 */
public class ChatMainActivity extends Activity implements IAppConfigurationConstant {
  //    XMPPConnection connection;
    Button _signIN;
    Button _CreateAccount;
    Button back;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private String regid = null;
    private Context context= null;
    protected String SENDER_ID="155375531524";
    private GoogleCloudMessaging gcm=null;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private SharedPreferences isTextNull;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        setTheme(ThemeSetter.GetTheme(this));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_main);
//        progressDialog=new ProgressDialog(RegisterActivity.this);
        preferences= getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        editor=preferences.edit();
        _signIN = (Button) findViewById(R.id.chat_sign_in_button);
        _signIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable() == false) {
                    Intent intent = new Intent(ChatMainActivity.this, TryAgainNetwork.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    Intent intent = new Intent(ChatMainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        back = (Button) findViewById(R.id.back_button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        _CreateAccount = (Button) findViewById(R.id.chat_create_id_button);
        _CreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable() == false) {
                    Intent intent = new Intent(ChatMainActivity.this, TryAgainNetwork.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    Intent intent = new Intent(ChatMainActivity.this, RegisterActivity.class);
                    startActivity(intent);
                }
            }
        });
        if(checkPlayServices()){
//            registerInBackground();
        }else{
        }
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    @Override
    public void onResume() {
        super.onResume();
        Resources resources = getResources();
        TextView title = (TextView) findViewById(R.id.TitleView);
        View separatorTitle = (View) findViewById(R.id.lineDivider2);
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        super.onResume();
        checkPlayServices();
    }
    private boolean checkPlayServices() {
        int resultcode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (resultcode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultcode)) {
                GooglePlayServicesUtil.getErrorDialog(resultcode, ChatMainActivity.this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
            }
            return false;
        }
        return true;
    }
    public void  registerInBackground(){
        new AsyncTask<Void,Void,String>(){

            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging
                                .getInstance(getApplicationContext());
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Registration ID :" + regid;
                    editor.putString("reg_id", regid);
                    editor.commit();


                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (!TextUtils.isEmpty(regid)) {
                } else {
                }
            }
        }.execute(null, null, null);
    }
}