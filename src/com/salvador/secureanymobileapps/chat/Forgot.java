package com.salvador.secureanymobileapps.chat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.documents.SimmpleApp;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;

import org.json.JSONObject;

/**
 * Created by insonix on 21/12/15.
 */
public class Forgot extends Activity implements Iconstant {
    EditText editconfirmNewPasswd,editNewPasswd,editemail,editkey;
    Button save,save_email,save_key;
    ProgressDialog progressDialog;
    String url,edit,pass,url1,token;
    LinearLayout lin_for,lin_key,lin_emai;
    ImageView back;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editprofile);
        ApplicationClass.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("Forgot Activity")
                .build());
        sharedPreferences=getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editconfirmNewPasswd=(EditText)findViewById(R.id.editconfirmNewPasswd);
        editNewPasswd=(EditText)findViewById(R.id.editNewPasswd);
        editemail=(EditText)findViewById(R.id.editemail);
        editkey=(EditText)findViewById(R.id.editkey);
        lin_for=(LinearLayout)findViewById(R.id.lin_for);
        lin_emai=(LinearLayout)findViewById(R.id.lin_emai);
        lin_key=(LinearLayout)findViewById(R.id.lin_key);
        save=(Button)findViewById(R.id.save);
        save_key=(Button)findViewById(R.id.save_key);
        save_email=(Button)findViewById(R.id.save_email);
        progressDialog=new ProgressDialog(Forgot.this);
        back = (ImageView) findViewById(R.id.back_button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Forgot.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        save_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editemail.getText().toString().length()>0) {
                    Forgotpassemail();
                }else{
                    editemail.setError("Please enter email");
                }
            }
        });
        save_key.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(token.equalsIgnoreCase(editkey.getText().toString())){
                    lin_for.setVisibility(View.VISIBLE);
                    lin_emai.setVisibility(View.GONE);
                    lin_key.setVisibility(View.GONE);
                }else{
                    new ShowMsg().createDialog(Forgot.this,"You have entered incorrect code");
                }
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pass=editNewPasswd.getText().toString();
                 if(pass.length()>0){
                        if(editconfirmNewPasswd.getText().toString().equals(pass)){
                            Forgotpass();
                        }else{
                            new ShowMsg().createDialog(Forgot.this,"New Password doesn't match with confirm password");
                        }
                    }else{
                        new ShowMsg().createDialog(Forgot.this,"Enter your password");
                    }
                }


        });
    }
    public void Forgotpass(){
        hideSoftKeyboard();
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url=BaseUrl+"change_passtoken.php?email="+editemail.getText().toString().trim()+"&token="+token+"&password="+editNewPasswd.getText().toString();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    String Status = jsonObject.getString("status");
                    Log.d("sts","sts"+Status);
                    if(Status.equals("true")) {
                        Intent intent = new Intent(Forgot.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                        Toast.makeText(Forgot.this, "Password successfully updated", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(Forgot.this, "Token expired", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                 }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(Forgot.this, "Internet is slow!",Toast.LENGTH_SHORT).show();
            }

        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    public void Forgotpassemail(){
        hideSoftKeyboard();
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url1=BaseUrl+"generate_key.php?email="+editemail.getText().toString().trim();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    String Status = jsonObject.getString("status");
                    String msg = jsonObject.getString("message");
                    if(Status.equals("true")) {

                        lin_emai.setVisibility(View.GONE);
                        lin_key.setVisibility(View.VISIBLE);
                         token = jsonObject.getString("token");
                        new ShowMsg().createDialog(Forgot.this, "Your verification code has been sent to your email");
                    }else{
                        Toast.makeText(Forgot.this, msg, Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                  }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Forgot.this, LoginActivity.class);

        startActivity(intent);
        finish();
    }
}
