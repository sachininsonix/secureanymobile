package com.salvador.secureanymobileapps.chat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.sms.SmsSendActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainMenu;

import org.json.JSONObject;

/**
 * Created by insonix on 30/3/16.
 */
public class AcceptDecline extends Activity implements Iconstant {
    Button accept,reject;
    private SharedPreferences isTextNull;
    String url,url2,res,key,phone,phone_user,key2,from_id,defaultpar,message,secret_key,request_id;
    ProgressDialog progressDialog;
    TextView mesge;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accept_decline);
//        MainActivity.tracker().send(new HitBuilders.EventBuilder("ui", "open")
//                .setLabel("Accept_Decline Activity")
//                .build());
        progressDialog=new ProgressDialog(AcceptDecline.this);
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        accept=(Button)findViewById(R.id.accept);
        reject=(Button)findViewById(R.id.reject);
        mesge=(TextView)findViewById(R.id.msg);
        try{
            phone=getIntent().getExtras().getString("phone");
            phone_user=getIntent().getExtras().getString("phone_user");
            key= isTextNull.getString("phone_number","")+phone_user+"gmai@insonix@220";
            secret_key=getIntent().getExtras().getString("secret_key");
            from_id=getIntent().getExtras().getString("from_id");
            message=getIntent().getExtras().getString("message");
            defaultpar=getIntent().getExtras().getString("default");
            request_id=getIntent().getExtras().getString("request_id");
           if(secret_key.equals("invitation")){
               Log.d("message","message"+message);
                mesge.setText(message);
            }
            key2=key.substring(0,16);
        }catch (Exception e){

        }
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                res="0";
                AcceptNoti();
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                res="1";
                AcceptNoti();
            }
        });
    }
    public void AcceptNoti(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        url=BaseUrl+"get_key_response.php?from_id="+from_id+"&key="+key2+"&phone="+phone+"&response="+res+"&request_id="+request_id;
     Log.d("url1", "url1" + url);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    String Status = jsonObject.getString("status");
                    if(Status.equals("true")) {
                        if(defaultpar.equals("chatActivity")){
                            Intent intent = new Intent(AcceptDecline.this, ChatHistory.class);
                            startActivity(intent);
                            finish();
                        }else if(defaultpar.equals("inviteAccept")) {
//
                            Intent intent = new Intent(AcceptDecline.this, MainMenu.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }else {
                            Intent intent = new Intent(AcceptDecline.this, SmsSendActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }else{

                    }
                }catch (Exception e){
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }
        });
        try {
            MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
        }catch (Exception e){

        }
    }
    @Override
    public void onResume() {

        super.onResume();


        if(isFinishing()) {
            Intent intent = new Intent(AcceptDecline.this, ChatMainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }
}
