package com.salvador.secureanymobileapps.chat;

public class UserInfo {
private String name;
private String pin;
private String status;
private int msg_no;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public int getMsg_no() {
	return msg_no;
}
public void setMsg_no(int msg_no) {
	this.msg_no = msg_no;
}
public String getPin() {
	return pin;
}
public void setPin(String pin) {
	this.pin = pin;
}
}
