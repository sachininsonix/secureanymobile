package com.salvador.secureanymobileapps.chat;

/**
 * Created by karundhawan on 07-04-2016.
 */
public class Getter_Setter2 {

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    String date;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    String body;
}
