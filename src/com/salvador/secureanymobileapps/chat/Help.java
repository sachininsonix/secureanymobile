package com.salvador.secureanymobileapps.chat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;

public class Help extends BaseActivity implements View.OnClickListener {


    private SharedPreferences isTextNull;
    private Button back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);
     this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
    }
    @Override
    public void onResume() {
        super.onResume();
        Resources resources = getResources();
        RelativeLayout containerChatHelp = (RelativeLayout) findViewById(R.id.containerChatHelp);
        TextView title = (TextView) findViewById(R.id.titlegeneraldocs);
        TextView label1 = (TextView) findViewById(R.id.textView1);
        TextView label2 = (TextView) findViewById(R.id.textView14);
        View separatorTitle = (View) findViewById(R.id.lineDivider2);
        isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        if (!isTextNull.getString("theme", "").equals("")) {
            if (isTextNull.getString("theme", "").equals("Black/Grey")) {
                containerChatHelp.setBackgroundColor(resources.getColor(R.color.backgroundTheme1));
                title.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                label1.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                label2.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme1));
                separatorTitle.setBackgroundColor(resources.getColor(R.color.colorSeparatorTheme1));
                this.back.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.arrow_light, 0);

            } else {

                containerChatHelp.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
                title.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                label1.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                label2.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
                separatorTitle.setBackgroundColor(resources.getColor(R.color.colorSeparatorTheme2));
                this.back.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.arrow_dark, 0);
            }
        } else {
            containerChatHelp.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
            title.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
            label1.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
            label2.setTextColor(resources.getColor(R.color.colorlabelDefaultsTheme2));
            separatorTitle.setBackgroundColor(resources.getColor(R.color.colorSeparatorTheme2));
            this.back.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                    R.drawable.arrow_dark, 0);
        }
    }

    public void showContactus(View v) {
        startActivity(new Intent(Help.this, ContactUs.class
        ));
    }

    public void showAbout(View view) {
//        startActivity(new Intent(Help.this, About.class
//        ));
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View arg0) {
        this.onBackPressed();
    }
}
