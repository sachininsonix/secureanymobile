package com.salvador.secureanymobileapps.chat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.documents.SimmpleApp;
import com.salvador.secureanymobileapps.k9.activity.MessageCompose;
import com.salvador.secureanymobileapps.sms.SmsSendActivity;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.tokenlogin.ContactMenu;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;
import com.salvador.secureanymobileapps.tokenlogin.SendInvite;
import com.salvador.secureanymobileapps.util.ColorGenerator;
import com.salvador.secureanymobileapps.util.TextDrawable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by insonix on 1/2/16.
 */
public class ChatUserlist extends Activity implements Iconstant {
    ListView userlist;
    private Button back;
    String url,smssen,contactname;
    SharedPreferences preferences,sharedpreferences;
    ProgressDialog progressDialog;
    ArrayList<HashMap<String,String >> addme;
    int randomAndroidColor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chatuserlist);
        try {
            ApplicationClass.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                    .setLabel("ChatUser_List Activity")
                    .build());
        }catch (Exception e){

        }
        preferences = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        progressDialog=new ProgressDialog(ChatUserlist.this);
        userlist=(ListView)findViewById(R.id.userlist);
        back = (Button) findViewById(R.id.back_button);
        addme=new ArrayList<>();
        try{
            smssen=getIntent().getExtras().getString("smssen");
            Log.d("smssen","smssen"+smssen);
        }catch (Exception e){

        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (smssen.equals("smsact")) {
                    Intent intent = new Intent(ChatUserlist.this, SmsSendActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0,0);
//                    finish();
                }else if(smssen.equals("msgcompose")){
                    Intent intent = new Intent(ChatUserlist.this, MessageCompose.class);
                    startActivity(intent);
                    overridePendingTransition(0,0);
//                    finish();
                }else if(smssen.equals("contact_menu")){
                    Intent intent = new Intent(ChatUserlist.this, ContactMenu.class);
                    startActivity(intent);
                    overridePendingTransition(0,0);
//                    finish();
                }
                else if(smssen.equals("sendInvite")){
                    Intent intent = new Intent(ChatUserlist.this, ContactMenu.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
//                    finish();
                }
                else if(smssen.equals("sendlink")){
                    Intent intent = new Intent(ChatUserlist.this, SendInvite.class);
                    startActivity(intent);
                    overridePendingTransition(0,0);
//                    finish();
                }
                else {
                    Intent intent=new Intent(ChatUserlist.this, ChatHistory.class);
                    startActivity(intent);
                    overridePendingTransition(0,0);
//                    finish();
                }
            }
        });
        if (isNetworkAvailable() == false) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ChatUserlist.this);
            builder.setMessage("Not able to connect to SAM. Please check your network connection and try again.")
                    .setTitle("Error")
                    .setCancelable(false)
                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //  Action for 'NO' Button
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
        }
        else {
            try {
                UserList();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public void UserList(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url=BaseUrl+"list.php?id="+preferences.getString("uid","");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try{
                    Log.d("json","json"+jsonObject.toString());
                    String status=jsonObject.getString("status");
                    if(status.equalsIgnoreCase("OK")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("users");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject City = jsonArray.getJSONObject(i);
                            String lastname = City.getString("lastname");
                            String username = City.getString("username");
                            String firstname = City.getString("firstname");
                            String email = City.getString("email");
                            String id = City.getString("id");
                            String chat_id = City.getString("chat_id");
                            String mobile = City.getString("mobile");
                            String nickname=City.getString("nickname");
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("nickname", nickname);
                            hashMap.put("username", username);
                            hashMap.put("id", id);
                            hashMap.put("chat_id", chat_id);
                            hashMap.put("mobile", mobile);
                            hashMap.put("lastname", lastname);
                            hashMap.put("firstname", firstname);
                            hashMap.put("email", email);
                            addme.add(hashMap);
                        }
                        userlist.setAdapter(new ChathisAdapter(ChatUserlist.this, addme));
                    }else{
                        new com.salvador.secureanymobileapps.chat.ShowMsg().createDialogSignUp(ChatUserlist.this, "Before start chat you need to first share a key, GoTo-> Settings->Profile-> click on Share icon to share key.");
//                        new ShowMsg().createDialog(ChatUserlist.this,"No Contact List");
                    }

                }catch(Exception e){
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }


        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class ChathisAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> addme;
        public ChathisAdapter(Activity activity,ArrayList<HashMap<String,String>> addme){
            this.activity=activity;
            this.addme=addme;
        }
        @Override
        public int getCount() {
            return addme.size();
        }
        @Override
        public Object getItem(int position) {
            return null;
        }
        @Override
        public long getItemId(int position) {
            return 0;
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.userlist_item,null);
            TextView name=(TextView)convertView.findViewById(R.id.name);
            TextView nickname=(TextView)convertView.findViewById(R.id.nickname);
            ImageView image=(ImageView)convertView.findViewById(R.id.image);
            TextView phone=(TextView)convertView.findViewById(R.id.phonenum);
            String s1= String.valueOf(addme.get(position).get("nickname"));
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getColor(addme.get(position).get("nickname"));
//            int color1 = generator.getRandomColor();
            TextDrawable drawable = TextDrawable.builder()
                    .beginConfig()
//                    eight in px
                    .endConfig()
                    .buildRound(s1,color );
            image.setImageDrawable(drawable);
            name.setText(addme.get(position).get("firstname")+" "+addme.get(position).get("lastname"));
            //

            phone.setText(addme.get(position).get("mobile"));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (smssen.equals("smsact")) {
                        Intent intent = new Intent(activity, SmsSendActivity.class);
                        intent.putExtra("smssen","msglist");
                        intent.putExtra("phone_number", addme.get(position).get("mobile"));
                        intent.putExtra("addresscon", addme.get(position).get("firstname")+" "+addme.get(position).get("lastname"));
                        activity.startActivity(intent);
                        activity.finish();
                    }
                    else if (smssen.equals("sendInvite")) {
                        Intent intent = new Intent(activity, SmsSendActivity.class);
                        intent.putExtra("phone_number", addme.get(position).get("mobile"));
                        intent.putExtra("addresscon", addme.get(position).get("firstname")+" "+addme.get(position).get("lastname"));
                        activity.startActivity(intent);
//                        activity.finish();
                    }else if(smssen.equals("msgcompose")) {
                        Intent intent = new Intent(activity, MessageCompose.class);
                        intent.putExtra("email", addme.get(position).get("email"));
                        startActivity(intent);
//                        finish();
//                        intent.putExtra("id", addme.get(position).get("id"));
//                        intent.putExtra("chat_id", addme.get(position).get("chat_id"));
                    }
                    else {
                        Intent intent = new Intent(activity, ChatActivity.class);
                        intent.putExtra("nickname", addme.get(position).get("nickname"));
                        intent.putExtra("id", addme.get(position).get("id"));
                        intent.putExtra("chat_id", addme.get(position).get("chat_id"));
                        intent.putExtra("username", addme.get(position).get("username"));
                        intent.putExtra("phone_number", addme.get(position).get("mobile"));
                        intent.putExtra("firstname", addme.get(position).get("firstname"));
                        intent.putExtra("lastname", addme.get(position).get("lastname"));
                        activity.startActivity(intent);
//                        activity.finish();
                    }
                }
            });
//

            return convertView;
        }
    }
    @Override
    public void onResume() {

        super.onResume();


        if(isFinishing()) {
            Intent intent = new Intent(ChatUserlist.this, ChatMainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }
    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        if (smssen.equals("smsact")) {
            Intent intent = new Intent(ChatUserlist.this, SmsSendActivity.class);
            intent.putExtra("phone_number","");
            startActivity(intent);
            overridePendingTransition(0,0);
//            finish();
        }else if(smssen.equals("msgcompose")){
            Intent intent = new Intent(ChatUserlist.this, MessageCompose.class);
            startActivity(intent);
            overridePendingTransition(0,0);
//            finish();
        }else if(smssen.equals("contact_menu")){
            Intent intent = new Intent(ChatUserlist.this, ContactMenu.class);
            startActivity(intent);
            overridePendingTransition(0, 0);
//            finish();
        }else if(smssen.equals("sendInvite")){
            Intent intent = new Intent(ChatUserlist.this, ContactMenu.class);
            startActivity(intent);
            overridePendingTransition(0, 0);
//            finish();
        }else if(smssen.equals("sendlink")){
            Intent intent = new Intent(ChatUserlist.this, SendInvite.class);
            startActivity(intent);
            overridePendingTransition(0,0);
//            finish();
        }
        else {
            Intent intent=new Intent(ChatUserlist.this, ChatHistory.class);
            startActivity(intent);
            overridePendingTransition(0,0);
//            finish();
        }
    }
}
