package com.salvador.secureanymobileapps.chat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.documents.SimmpleApp;
import com.salvador.secureanymobileapps.tokenlogin.ApplicationClass;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainMenu;
import com.salvador.secureanymobileapps.util.ColorGenerator;
import com.salvador.secureanymobileapps.util.TextDrawable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by insonix on 2/2/16.
 */
public class ChatHistory extends Activity implements View.OnClickListener, Iconstant {
    Button back, button_add;
    String url, id, url2;
    ChathisAdapter chathisAdapter;
    SharedPreferences preferences;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor, sharededitor;
    ProgressDialog progressDialog;
    ArrayList<HashMap<String, String>> addme;
    ListView userlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_history);
        ApplicationClass.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("ChatHistory Activity")
                .build());
        preferences = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        editor = preferences.edit();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        sharededitor = sharedpreferences.edit();
        progressDialog = new ProgressDialog(ChatHistory.this);
        userlist = (ListView) findViewById(R.id.userlist);
        back = (Button) findViewById(R.id.back_button);
        button_add = (Button) findViewById(R.id.button_add);
        button_add.setOnClickListener(this);
        addme = new ArrayList<>();
        if (isNetworkAvailable() == false) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ChatHistory.this);
            builder.setMessage("Not able to connect to SAM. Please check your network connection and try again.")
                    .setTitle("Error")
                    .setCancelable(false)
                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //  Action for 'NO' Button
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
        } else {
            UserList();
            userlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    Intent intent = new Intent(ChatHistory.this, ChatActivity.class);
                    intent.putExtra("nickname", addme.get(position).get("nickname"));
                    intent.putExtra("id", addme.get(position).get("r_id"));
                    intent.putExtra("chat_id", addme.get(position).get("chat_id"));
                    intent.putExtra("username", addme.get(position).get("username"));
                    intent.putExtra("msg_id", addme.get(position).get("msg_id"));
                    intent.putExtra("firstname", addme.get(position).get("firstname"));
                    intent.putExtra("lastname", addme.get(position).get("lastname"));
                    intent.putExtra("phone_number", addme.get(position).get("phone_number"));
                    startActivity(intent);
                    finish();
                }
            });
            userlist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {

                    id = addme.get(position).get("msg_id");
                    AlertDialog.Builder alert = new AlertDialog.Builder(ChatHistory.this);
                    alert.setTitle("Are you sure to delete this chat?");
                    alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            addme.remove(position);
                            chathisAdapter.notifyDataSetChanged();
                            Deletechathis();
                        }
                    });
                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            // Canceled.
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                    return true;
                }
            });
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChatHistory.this, MainMenu.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        });


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void UserList() {
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url = BaseUrl + "get_all_chat.php?uid=" + preferences.getString("uid", "");
        Log.d("chathistoryurl", "chathistoryurl" + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    Log.d("chathistoryurl", "chathistoryurl" + url);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("OK")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("users_chat");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject City = jsonArray.getJSONObject(i);
                            String chat_id = City.getString("chat_id");
                            String firstname = City.getString("firstname");
                            String lastname = City.getString("lastname");
                            String r_id = City.getString("r_id");
                            String username = City.getString("username");
                            String phone_number = City.getString("phone_number");
                            String nickname = City.getString("nickname");
                            String message = City.getString("message");
                            String msg_id = City.getString("msg_id");
                            String status1 = City.getString("status");
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("nickname", nickname);
                            hashMap.put("message", message);
                            hashMap.put("username", username);
                            hashMap.put("msg_id", msg_id);
                            hashMap.put("status", status1);
                            hashMap.put("phone_number", phone_number);
                            hashMap.put("r_id", r_id);
                            hashMap.put("chat_id", chat_id);
                            hashMap.put("lastname", lastname);
                            hashMap.put("firstname", firstname);
                            addme.add(hashMap);
                        }
                        chathisAdapter = new ChathisAdapter(ChatHistory.this, addme);
                        userlist.setAdapter(chathisAdapter);
                    } else {
                        new ShowMsg().createDialog(ChatHistory.this, "No Chat History");
                    }
                } catch (Exception e) {
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }
        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

    @Override
    public void onClick(View view) {
        String tag = view.getTag() + "";
        if (tag.equals("userlist")) {
            Intent intent = new Intent(ChatHistory.this, ChatUserlist.class);
            intent.putExtra("smssen", "chatact");
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onResume() {

        super.onResume();
       /* if(isFinishing()) {
            Intent intent = new Intent(ChatHistory.this, ChatMainActivity.class);
            startActivity(intent);
            finish();
        }*/

    }

    @Override
    public void onPause() {
        super.onPause();
        if (!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }

    class ChathisAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String, String>> addme;

        public ChathisAdapter(Activity activity, ArrayList<HashMap<String, String>> addme) {
            this.activity = activity;
            this.addme = addme;

        }

        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.chathistory_item, null);
            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView nickname = (TextView) convertView.findViewById(R.id.nickname);
            ImageView image = (ImageView) convertView.findViewById(R.id.image);
            ImageView single = (ImageView) convertView.findViewById(R.id.single);
            TextView phone = (TextView) convertView.findViewById(R.id.message);
            name.setText(addme.get(position).get("firstname") + " " + addme.get(position).get("lastname"));
            phone.setText(addme.get(position).get("message"));
            String s1 = String.valueOf(addme.get(position).get("nickname"));
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getColor(addme.get(position).get("nickname"));
            TextDrawable drawable = TextDrawable.builder()
                    .beginConfig()
                    // height in px
                    .endConfig()
                    .buildRound(s1, color);
            image.setImageDrawable(drawable);
            if (addme.get(position).get("status").equals("1")) {
                single.setBackgroundResource(R.drawable.doublee);
            } else {
                single.setBackgroundResource(R.drawable.single);
            }
            return convertView;
        }
    }

    public void Deletechathis() {
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url2 = BaseUrl + "delete_chat.php?msg_id=" + id;
        Log.d("url2", "url2" + url2);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("true")) {
                        Toast.makeText(ChatHistory.this, "Successfully deleted", Toast.LENGTH_SHORT).show();
                    } else {
                        new ShowMsg().createDialog(ChatHistory.this, "Not deleted");
                    }
                } catch (Exception e) {
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }


        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(ChatHistory.this, MainMenu.class);
        startActivity(in);
        overridePendingTransition(0, 0);
        finish();
    }
}

