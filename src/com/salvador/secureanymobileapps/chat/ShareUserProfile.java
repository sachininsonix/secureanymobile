package com.salvador.secureanymobileapps.chat;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.Settings.SettingsActivity;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.tokenlogin.ContactRequests;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;

import org.json.JSONObject;

import java.net.URLEncoder;

public class ShareUserProfile extends Activity implements Iconstant {

    EditText et_address,textmessage;
    String address, subject, message, file_path;
    Button bt_send, bt_attach,back;
    TextView tv_attach,et_subject;
    String url1,url,fname,lname,nikname,email,phone,country;

    private SharedPreferences isTextNull,sharedPreferences;
    SharedPreferences.Editor editor;
    ProgressDialog progressDialog;

    private static final int PICK_IMAGE = 100;
    Uri URI = null;
    int columnindex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_userprofile);
        back = (Button) findViewById(R.id.back_button);
        progressDialog=new ProgressDialog(ShareUserProfile.this);
        sharedPreferences=getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
//        url1 ="http://52.35.22.61/web/send_invite.php?id=8&email=Sachin.insonix@gmail.com";
        url =BaseUrl+"send_invite.php?";
        et_address=(EditText)findViewById(R.id.to);
        et_subject=(TextView)findViewById(R.id.subject);
        textmessage=(EditText)findViewById(R.id.message_content);
        bt_send=(Button)findViewById(R.id.btn_send);
try {
    fname = getIntent().getExtras().getString("fname");
    lname = getIntent().getExtras().getString("lname");
    phone = getIntent().getExtras().getString("phone");
    nikname = getIntent().getExtras().getString("nikname");
    email = getIntent().getExtras().getString("email");
    country = getIntent().getExtras().getString("country");
    textmessage.setText(" Name: "+fname+" "+lname+"\n Email: "+email+"\n Phone: "+phone+"\n Country: "+country);
}catch (Exception e){

}


        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        bt_send.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hideSoftKeyboard();
                // TODO Auto-generated method stub

                final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                address=et_address.getText().toString().trim();
                message= textmessage.getText().toString().trim();

                if(address.length() == 0){
                    et_address.setError("Email Id is required!");
                    et_address.requestFocus();

                }
                else if(!address.matches(emailPattern)){
                    et_address.setError("Invaild Email!");
                    et_address.requestFocus();
                }
                else if(message.length() == 0){
                    textmessage.setError("Message is required!");
                    textmessage.requestFocus();
                }
                else {
                    EmailSend();
                }
            }
        });

    }
    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void EmailSend(){
        url1=url+"id="+isTextNull.getString("uid","")+"&email="+ URLEncoder.encode(et_address.getText().toString().trim())+"&address="+ URLEncoder.encode(textmessage.getText().toString().trim())+"&phone="+phone;
       progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.d("url","url"+url1);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
               try {
                    String Status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    Log.d("msg","Msg"+message);
                    Log.d("Status","Status"+Status);
                    if(Status.equals("true")) {
                        Toast.makeText(ShareUserProfile.this, message, Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(ShareUserProfile.this,SettingsActivity.class);
                        startActivity(intent);
                        finish();
                    }
                   if(Status.equals("invite")) {
                       if(message.equals("Already accepted")) {
                           AlertDialog.Builder builder = new AlertDialog.Builder(ShareUserProfile.this);

                           builder.setTitle("SAM");
                           builder.setMessage("" + et_address.getText().toString().trim() + " already in your contacts! Do you want to check?");

                           builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                               public void onClick(DialogInterface dialog, int which) {
                                   // Do nothing but close the dialog
                                   Intent intent = new Intent(ShareUserProfile.this, ChatUserlist.class);
                                   intent.putExtra("smssen","");
                                   startActivity(intent);
                                   overridePendingTransition(0, 0);
                                   finish();
                               }
                           });
                           builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   // Do nothing//
                                   dialog.dismiss();
                               }
                           });

                           AlertDialog alert = builder.create();
                           alert.show();
                       }else
                       if(message.equals("Invite already sent")) {
                           AlertDialog.Builder builder = new AlertDialog.Builder(ShareUserProfile.this);

                           builder.setTitle("SAM");
                           builder.setMessage("Invitation from " + et_address.getText().toString().trim() + " already exists. Do you want to check?");
                           builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   // Do nothing
//
                                   dialog.dismiss();
                               }
                           });

                           AlertDialog alert = builder.create();
                           alert.show();
                       }
                       else{
                           AlertDialog.Builder builder = new AlertDialog.Builder(ShareUserProfile.this);

                           builder.setTitle("SAM");
                           builder.setMessage("Invite Already Sent to " + et_address.getText().toString().trim() + " . Please wait for " + et_address.getText().toString().trim() + " response.");

                           builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                               public void onClick(DialogInterface dialog, int which) {
                                   // Do nothing but close the dialog
                                   Intent intent = new Intent(ShareUserProfile.this, ContactRequests.class);
                                   startActivity(intent);
                                   overridePendingTransition(0,0);
                                   finish();
                               }

                           });

                           builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   // Do nothing
//
                                   dialog.dismiss();
                               }
                           });

                           AlertDialog alert = builder.create();
                           alert.show();
                       }
                   }
                    else{
                        Toast.makeText(ShareUserProfile.this, message, Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    @Override
    public void onResume() {

        super.onResume();


        if(isFinishing()) {
            Intent intent = new Intent(ShareUserProfile.this, ChatMainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent=new Intent(ShareUserProfile.this, UserProfile.class);

        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
