package com.salvador.secureanymobileapps.chat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.k9.Account;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainMenu;

import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by insonix on 17/12/15.
 */
public class LoginActivity extends Activity implements Iconstant {
    EditText _username,_passwd;
    Button back,_login;
    ProgressDialog progressDialog;
    //    SharedPreferences preferences;
    String url,_user,_pass;
    Account mAccount;
    CheckBox check_tick;
    private SharedPreferences isTextNull;
    SharedPreferences.Editor editor;
    boolean savecredential;
    private String regid = null;
    private Context context= null;
    protected String SENDER_ID="155375531524";
    private GoogleCloudMessaging gcm=null;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    TextView forgot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_login);
//
        progressDialog=new ProgressDialog(LoginActivity.this);
//
        _username = (EditText) findViewById(R.id.editUsername);
        _passwd = (EditText) findViewById(R.id.editPasswd);
        _login = (Button) findViewById(R.id.chat_login_button);
        check_tick=(CheckBox)findViewById(R.id.check_tick);
        back = (Button) findViewById(R.id.back_button);
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        editor=isTextNull.edit();
        forgot=(TextView)findViewById(R.id.forgot);
        savecredential=isTextNull.getBoolean("savelogin",false);
        try {
            if (!isTextNull.getString("username", "").equals("")) {
                _username.setText(isTextNull.getString("username",""));
//               _username.setEnabled(false);
            } else {
                _username.setEnabled(true);
//            DeleteAccount();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        if(savecredential==true){
            _username.setText(isTextNull.getString("username",""));
            check_tick.setChecked(true);
        }
        _login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _user=_username.getText().toString().trim();
                _pass= _passwd.getText().toString().trim();

                if(check_tick.isChecked()){
                    editor.putBoolean("savelogin",true);
                    editor.putString("username",_user);
                    editor.commit();
                }else{
                    editor.clear();
                    editor.commit();
                }
                if(_user.length() == 0){
                    _username. setError("can't be Empty");
                    _username.requestFocus();
                    return;
                }
                if(_pass.length() == 0){
                    _passwd. setError("can't be Empty");
                    _passwd.requestFocus();
                    return;
                }
                Register();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ChatMainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, Forgot.class);

                startActivity(intent);
                finish();
            }
        });
        if(checkPlayServices()){
            registerInBackground();
        }else{

        }

    }
    public void Register(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url=BaseUrl+"login.php?username="+_user+"&password="+_pass+"&device_id="+regid;
        Log.d("", "url" + url);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    String Status = jsonObject.getString("Status");
                    String message = jsonObject.getString("message");
                    if(Status.equals("true")) {
                        JSONObject response=jsonObject.getJSONObject("response");
                        String user_id = response.getString("user_id");
                        String username = response.getString("username");
                        String email = response.getString("email");
                        String phone_number = response.getString("phone_number");
                        editor.putString("uid",user_id);
                        editor.putString("username",username);
                        editor.putString("email", email);
//                        editor.putString("edit_user",_username.getText().toString());
                        editor.putString("phone_number", phone_number);
//                        editor.putString("account", "");

                        editor.commit();
//    DeleteAccount();
                        Intent intent = new Intent(LoginActivity.this, MainMenu.class);

                        startActivity(intent);
                        finish();
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    }else{
//                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        new ShowMsg().createDialog(LoginActivity.this, message);
                    }
                }catch (Exception e){
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(LoginActivity.this,"Slow internet connection",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }


        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    @Override protected void onResume()
    {
        super.onResume();
        checkPlayServices();
    }
    private boolean checkPlayServices() {
        int resultcode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (resultcode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultcode)) {
                GooglePlayServicesUtil.getErrorDialog(resultcode, LoginActivity.this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {

            }
            return false;
        }
        return true;
    }
    public void  registerInBackground(){
        new AsyncTask<Void,Void,String>(){

            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging
                                .getInstance(getApplicationContext());
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Registration ID :" + regid;
                    editor.putString("reg_id", regid);
                    editor.commit();
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (!TextUtils.isEmpty(regid)) {
                } else {
                }
            }
        }.execute(null, null, null);
    }
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(LoginActivity.this, ChatMainActivity.class);
        startActivity(intent);
        finish();
    }

}