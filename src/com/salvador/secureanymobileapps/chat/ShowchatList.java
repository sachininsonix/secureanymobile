package com.salvador.secureanymobileapps.chat;

import android.app.TabActivity;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.Window;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.salvador.secureanymobileapps.R;

@SuppressWarnings("deprecation")
public class ShowchatList extends TabActivity implements IAppConfigurationConstant
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chatlist);
	TabHost tabHost =getTabHost();
        // Tab for Photos
        TabSpec friends = tabHost.newTabSpec("Friends");
        // setting Title and Icon for the Tab
        friends.setIndicator("FRIENDS", getResources().getDrawable(R.drawable.emoticon));
        // Tab for Songs
        TabSpec groupsList = tabHost.newTabSpec("Groups");
        groupsList.setIndicator("GROUPS", getResources().getDrawable(R.drawable.emoticon));
         
        // Adding all TabSpec to TabHost
        tabHost.addTab(friends); // Adding photos tab
        tabHost.addTab(groupsList); // Adding songs tab
	}
	@Override
	public void onAttachedToWindow() {
		// TODO Auto-generated method stub
		super.onAttachedToWindow();
		System.out.println("----------on attach to window-------------");
		 Window window = getWindow();
		    window.setFormat(PixelFormat.RGBA_8888);
	}
}
