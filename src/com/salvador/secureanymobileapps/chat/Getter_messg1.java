package com.salvador.secureanymobileapps.chat;

/**
 * Created by karundhawan on 07-04-2016.
 */
public class Getter_messg1 {
    String body;
    String address;
    String numadress;
    String date;

    public String getNumadress() {
        return numadress;
    }

    public void setNumadress(String numadress) {
        this.numadress = numadress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
