package com.salvador.secureanymobileapps.chat;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.Settings.SettingsActivity;
import com.salvador.secureanymobileapps.Settings.UserPreferencesKey;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;
import com.salvador.secureanymobileapps.util.ImageFilePath;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class UserProfile extends BaseActivity implements View.OnClickListener,Iconstant {
    private ImageView imageView,share_button;
    private EditText etlname;
    private EditText etfname;
    private EditText etemail;
    private EditText etnickname;
    private Spinner editText_country;
    private EditText editText_PhoneNumber;
    private TextView title;
    ProgressDialog progressDialog;
    private String fn, ln, nik, em, phone, country,url,url2;
    private byte[] byteArray;
//    private XMPPConnection connection;
    private SharedPreferences isTextNull;
    private Button back,update;
    ArrayAdapter adapter;
    private String key;
    private SharedPreferences settings,sharedPreferences;
    private String AvatarPath;

    SharedPreferences.Editor editor;
    @SuppressWarnings("static-access")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.profile_layout);
        progressDialog=new ProgressDialog(UserProfile.this);
        sharedPreferences=getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        editor=isTextNull.edit();
        imageView = (ImageView) findViewById(R.id.imageView_user);
        share_button = (ImageView) findViewById(R.id.share_button);
        etemail = (EditText) findViewById(R.id.editText_emailid);
        etfname = (EditText) findViewById(R.id.editText_firstname);
        etlname = (EditText) findViewById(R.id.editText_lastname);
        etnickname = (EditText) findViewById(R.id.editText_nickname);
        editText_country = (Spinner) findViewById(R.id.editText_country);
        editText_PhoneNumber = (EditText) findViewById(R.id.editText_PhoneNumber);
        title = (TextView) findViewById(R.id.titlegeneraldocs);
        String[] countries = getResources().getStringArray(R.array.countries_array);
        adapter = new ArrayAdapter<>(this, R.layout.country_list, R.id.countries, countries);
        adapter.setDropDownViewResource(R.layout.spinner_text);
        editText_country.setAdapter(adapter);
        editText_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                ((TextView)parent.getChildAt(0)).setTextColor(Color.parseColor("#FFFFFF"));
                country = editText_country.getSelectedItem().toString();
                Log.d("textcountry", "textcountry" + country);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("titleProfile");
            Log.d("value is", "value is" + value);
            title.setText(value);
        }
//        String[] countries = getResources().getStringArray(R.array.countries_array);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.county_list,R.id.countries,countries);
//        editText_country.setAdapter(adapter);
        this.back = (Button) findViewById(R.id.back_button);
        this.update = (Button) findViewById(R.id.button_update_profile);
        this.back.setOnClickListener(this);
        // complete field
        this.settings = this
                .getSharedPreferences(UserPreferencesKey.keyUser, 0);
        this.key = settings.getString(UserPreferencesKey.keyUserId, "");
        this.settings = this.getSharedPreferences(
                UserPreferencesKey.keyCreateAcount + "-" + key, 0);
        share_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fn = etfname.getText().toString().trim();
                ln = etlname.getText().toString().trim();
                nik = etnickname.getText().toString().trim();
                em = etemail.getText().toString().trim();
                phone =editText_PhoneNumber.getText().toString().trim();
                country =editText_country.getSelectedItem().toString().trim();
                if(fn.length() == 0){
                    etfname. setError("can't be Empty");
                    return;
                }
                if(ln.length() ==0) {
                    etlname.setError("can't be Empty");
                    return;
                }
                if(nik.length() == 0 ){
                    etnickname. setError("can't be Empty");
                    return;
                }
                if(em.length() == 0){
                    etemail. setError("can't be Empty");
                    return;
                }

                if(phone.length() ==0 ){
                    editText_PhoneNumber. setError("can't be Empty");
                    return;
                }
                if(phone.length()<10){
                    editText_PhoneNumber.setError("Mobile number atleast 10 digits!");
                    editText_PhoneNumber.requestFocus();
                    return;
                }
                Intent intent =new Intent(UserProfile.this,ShareUserProfile.class);
                intent.putExtra("fname", fn);
                intent.putExtra("lname", ln);
                intent.putExtra("nikname", nik);
                intent.putExtra("email", em);
                intent.putExtra("phone", phone);
                intent.putExtra("country",country);
                startActivity(intent);
                finish();
            }
        });
        if (isNetworkAvailable() == false) {
            AlertDialog.Builder builder = new AlertDialog.Builder(UserProfile.this);
            builder.setMessage("Not able to connect to SAM. Please check your network connection and try again.")
                    .setTitle("Error")
                    .setCancelable(false)
                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //  Action for 'NO' Button
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
        }
        else {
            ProfileTask();
        }
update.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        fn = etfname.getText().toString().trim();
        ln = etlname.getText().toString().trim();
        nik = etnickname.getText().toString().trim();
        em = etemail.getText().toString().trim();
        phone =editText_PhoneNumber.getText().toString().trim();
        country =editText_country.getSelectedItem().toString().trim();

        if(fn.length() == 0){
            etfname. setError("can't be Empty");
            return;
        }
        if(ln.length() ==0) {
            etlname.setError("can't be Empty");
            return;
        }
        if(nik.length() == 0 ){
            etnickname. setError("can't be Empty");
            return;
        }

        if(em.length() == 0){
            etemail. setError("can't be Empty");
            return;
        }

        if(phone.length() ==0 ){
            editText_PhoneNumber.setError("Mobile number is required!");
            editText_PhoneNumber.requestFocus();
            return;
        }
        if(phone.length()<10){
            editText_PhoneNumber.setError("Mobile number atleast 10 digits!");
            editText_PhoneNumber.requestFocus();
            return;
        }
        UpdateProfileTask();
    }
});
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    @Override
    public void onResume() {

        super.onResume();


        if(isFinishing()) {
            Intent intent = new Intent(UserProfile.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
       Intent intent=new Intent(UserProfile.this, SettingsActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
    @Override
    public void onClick(View arg0) {
        this.onBackPressed();
    }

    @Override
    public void onAttachedToWindow() {
        // TODO Auto-generated method stub
        super.onAttachedToWindow();
        System.out.println("----------on attach to window-------------");
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }
    public void selectImage(View v) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 100) {
                Uri selectedImageUri = data.getData();

                AvatarPath = ImageFilePath.getPath(this, selectedImageUri);
                ContentResolver cr = getBaseContext().getContentResolver();
                InputStream inputStream;
                try {
                    inputStream = cr.openInputStream(selectedImageUri);
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    imageView.setImageBitmap(bitmap);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byteArray = baos.toByteArray();
                    System.out.println("--------bytes arrary============"
                            + byteArray);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void ProfileTask(){
        url=BaseUrl+"view_profile.php?id="+isTextNull.getString("uid","");
         progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject jsonObject) {
                try{
                     String Status=jsonObject.getString("status");
                    if(Status.equalsIgnoreCase("true")){
                        String firstname=jsonObject.getString("firstname");
                        String lastname=jsonObject.getString("lastname");
                        String country=jsonObject.getString("country");
                        String username=jsonObject.getString("username");
                        String email=jsonObject.getString("email");
                        String mobile=jsonObject.getString("mobile");
                        etfname.setText(firstname);
                        etlname.setText(lastname);
                        etnickname.setText(username);
                        editText_PhoneNumber.setText(mobile);
                        editText_country.setSelection(adapter.getPosition(country));
                        Log.i(TAG, "onResponse: Country selected"+adapter.getPosition(country));
                        etemail.setText(email);
                        editor.putString("email",email);
                        editor.putString("phone_number",mobile);
                        editor.putString("username",username);
                        editor.commit();
                    }
                }catch(Exception e){
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }

        });

        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    public void UpdateProfileTask(){
        url2=BaseUrl+"edit_profile.php?id="+isTextNull.getString("uid","")+"&name="+etfname.getText().toString().trim()+"&lastname="+etlname.getText().toString().trim()+"&mobile="+editText_PhoneNumber.getText().toString().trim()+"&country="+country;
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url2, null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject jsonObject) {
                try{
                     String Status=jsonObject.getString("status");
                    if(Status.equalsIgnoreCase("true")){
                        Toast.makeText(UserProfile.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                        editor.putString("email",etemail.getText().toString());
                        editor.putString("phone_number",editText_PhoneNumber.getText().toString());
                        editor.putString("username",etnickname.getText().toString());
                        editor.commit();
                    }
                }catch(Exception e){
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(UserProfile.this,"Internet is slow",Toast.LENGTH_SHORT).show();
            }

        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    private class Update extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                    if (nik.equals(key)) {
                        settings = getSharedPreferences(
                                UserPreferencesKey.keyCreateAcount + "-" + key,
                                0);
                    } else {
                        settings = getSharedPreferences(
                                UserPreferencesKey.keyUser, 0);
                        settings.edit()
                                .putString(UserPreferencesKey.keyUserId, nik)
                                .commit();
                        settings = getSharedPreferences(
                                UserPreferencesKey.keyCreateAcount + "-" + nik,
                                0);
                    }
                    settings.edit()
                            .putString(UserPreferencesKey.keyFirstName, fn)
                            .commit();
                    settings.edit()
                            .putString(UserPreferencesKey.keyLastName, ln)
                            .commit();
                    settings.edit()
                            .putString(UserPreferencesKey.keyUserName, nik)
                            .commit();
                    settings.edit().putString(UserPreferencesKey.keyEmail, em)
                            .commit();
                    settings.edit()
                            .putString(UserPreferencesKey.keyPhoneNumber, phone)
                            .commit();
                    settings.edit()
                            .putString(UserPreferencesKey.keyCountry, country)
                            .commit();
                    settings.edit()
                            .putString(UserPreferencesKey.keyAvatar, AvatarPath.toString())
                            .commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    AvatarPath = settings.getString(UserPreferencesKey.keyAvatar, "");
                    Toast.makeText(getApplicationContext(), "Profile Updated: ",
                            Toast.LENGTH_LONG).show();
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
        }

    }
}
