package com.salvador.secureanymobileapps.tokenlogin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.ChatUserlist;
import com.salvador.secureanymobileapps.chat.LoginActivity;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.util.BadgeView;

import org.json.JSONObject;

/**
 * Created by insonix on 4/8/16.
 */
public class ContactMenu extends Activity implements View.OnClickListener,Iconstant {
    private ImageButton con_btn,addbtn,exchange_btn,phn_btn;
    LinearLayout con_btnlay,addbtn_lay,exchange_btnlay,phn_btnlay;
    Button back_button;
    ProgressDialog progressDialog;
    String url;
    BadgeView badge;
    private SharedPreferences isTextNull,sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_menu);
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        back_button=(Button)findViewById(R.id.back_button);
        con_btn = (ImageButton) findViewById(R.id.con_btn);
        con_btn.setOnClickListener(this);
        addbtn = (ImageButton) findViewById(R.id.addbtn);
        addbtn.setOnClickListener(this);
        exchange_btn = (ImageButton) findViewById(R.id.exchange_btn);
        exchange_btn.setOnClickListener(this);
        phn_btn = (ImageButton) findViewById(R.id.phn_btn);
        phn_btn.setOnClickListener(this);
//        con_btnlay=(LinearLayout)findViewById(R.id.con_btnlay);
//        con_btnlay.setOnClickListener(this);
//        addbtn_lay=(LinearLayout)findViewById(R.id.addbtn_lay);
//        addbtn_lay.setOnClickListener(this);
//        exchange_btnlay=(LinearLayout)findViewById(R.id.exchange_btnlay);
//        exchange_btnlay.setOnClickListener(this);
//        phn_btnlay=(LinearLayout)findViewById(R.id.phn_btnlay);
//        phn_btnlay.setOnClickListener(this);
        badge = new BadgeView(this, con_btn);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        request_count();
    }

    @Override
    public void onClick(View view) {
 switch (view.getId()){
     case R.id.con_btn:
         Intent con_re=new Intent(ContactMenu.this,ContactRequests.class);
         startActivity(con_re);
         finish();
         break;
     case R.id.addbtn:
         Intent send=new Intent(ContactMenu.this,SendInvite.class);
         startActivity(send);
         finish();
         break;
     case R.id.exchange_btn:
         break;
     case R.id.phn_btn:
         Intent phn_btn=new Intent(ContactMenu.this,ChatUserlist.class);
         phn_btn.putExtra("smssen","contact_menu");
         startActivity(phn_btn);
         finish();
         break;
 }
    }
    public void request_count(){
        url=BaseUrl+"get_invites_count.php?id="+isTextNull.getString("uid","")+"&read_count=1";
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        Log.d("url", "url" + url);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    String Status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
                    if(Status.equals("true")) {
                        String count = jsonObject.getString("count");
                        if(count.equals("")){

                        }else {
                            badge.setText(count);
                            badge.show();
                        }
//                        Intent intent=new Intent(SendInvite.this,SettingsActivity.class);
//                        startActivity(intent);
//                        finish();
                    }else{
//                        Toast.makeText(ContactMenu.this, message, Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                }
//                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                progressDialog.dismiss();
            }
        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    @Override
    public void onResume() {

        super.onResume();


        if(isFinishing()) {
            Intent intent = new Intent(ContactMenu.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent=new Intent(ContactMenu.this,ToolsActivity.class);
        startActivity(intent);
        finish();

    }
}
