package com.salvador.secureanymobileapps.tokenlogin;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.salvador.secureanymobileapps.R;

public class TokenInformation extends Activity implements View.OnClickListener {

    private ScrollView containerViewTokenInfo;

    private TextView TitleTokenInfo;
    private TextView TitleStatus;
    private TextView TitleBattLevel;
    private TextView TitleProximity,Proximitysense;

    private Button back_button;

    private SharedPreferences isTextNull;

    SensorManager mySensorManager;
    Sensor myProximitySensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_token_information);
//        FlurryAgent.init(TokenInformation.this,"9TG2VY5QR74RKTF6XVZX");
        this.containerViewTokenInfo = (ScrollView) findViewById(R.id.containerViewTokenInfo);

        this.TitleTokenInfo = (TextView) findViewById(R.id.TitleTokenInfo);
        this.TitleStatus = (TextView) findViewById(R.id.TitleStatus);
        this.TitleBattLevel = (TextView) findViewById(R.id.TitleBattLevel);
        this.Proximitysense = (TextView) findViewById(R.id.Proximitysense);
        this.TitleProximity = (TextView) findViewById(R.id.TitleProximity);

        this.back_button = (Button) findViewById(R.id.back_button);
        this.back_button.setOnClickListener(this);

        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);

        mySensorManager = (SensorManager)getSystemService(
                Context.SENSOR_SERVICE);
        myProximitySensor = mySensorManager.getDefaultSensor(
                Sensor.TYPE_PROXIMITY);
//        if (myProximitySensor == null){
//            Proximitysense.setText("No Proximity Sensor!");
//        }else{
////            Proximitysense.setText(myProximitySensor.getName());
////
////            mySensorManager.registerListener(proximitySensorEventListener,
////                    myProximitySensor,
////                    SensorManager.SENSOR_DELAY_NORMAL);
//        }
//        getBatteryPercentage();
        Resources resources = getResources();

//
    }
    SensorEventListener proximitySensorEventListener
            = new SensorEventListener(){
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub
        }
        @Override
        public void onSensorChanged(SensorEvent event) {
            // TODO Auto-generated method stub
            if(event.sensor.getType()== Sensor.TYPE_PROXIMITY)
            {
                TitleProximity.setText(String.valueOf(event.values[0]));
            }
        }
    };
    private void getBatteryPercentage() {
        BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                context.unregisterReceiver(this);
                int currentLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                int level = -1;
                if (currentLevel >= 0 && scale > 0) {
                    level = (currentLevel * 100) / scale;
                }
                TitleBattLevel.setText("Battery Level: " + level + "%");
            }
        };
        IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(batteryLevelReceiver, batteryLevelFilter);
    }


    @Override
    public void onClick(View view) {
        String tag = "" + view.getTag();
        if (tag.equals("back")) {
            this.onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent in = new Intent(TokenInformation.this, MainMenu.class);
        startActivity(in);
        overridePendingTransition(0,0);
        finish();
    }

}
