package com.salvador.secureanymobileapps.tokenlogin;


import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.sip.SipAudioCall;
import android.os.Bundle;

import com.salvador.secureanymobileapps.Settings.IncomingCallReceiver;
import com.salvador.secureanymobileapps.voip.VoipActivity;

/**
 * Created by station12 on 10/21/14.
 */
public class BaseActivity extends Activity {
    public ApplicationClass app;
    public static String TAG = "Security Any  Mobile";
    public IncomingCallReceiver callReceiver;
    public SipAudioCall call;
    private boolean isActiveActivity = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.app = (ApplicationClass) getApplicationContext();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.SipDemo.INCOMING_CALL");
        callReceiver = new IncomingCallReceiver();
        this.registerReceiver(callReceiver, filter);
        app.setVoipActive(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        isActiveActivity = true;
        this.app.setVoipActive(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        isActiveActivity = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(callReceiver);
        callReceiver = null;
    }


    public void receiveCall(Intent intent) {
        if (isActiveActivity) {
            app.setCall(false);
            app.setDataCall(null);
            if (!app.isVoipActive()) {
                app.setCall(true);
                app.setDataCall(intent);
                intent = new Intent(this, VoipActivity.class);
                this.startActivity(intent);
            }
        }
    }

    SipAudioCall.Listener listener = new SipAudioCall.Listener() {

        @Override
        public void onCallEstablished(SipAudioCall call) {
            call.startAudio();
            call.setSpeakerMode(true);
            call.toggleMute();
        }

        @Override
        public void onCallEnded(SipAudioCall call) {
            // Do something.
        }
    };
}

