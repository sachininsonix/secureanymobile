package com.salvador.secureanymobileapps.tokenlogin;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.ContactsContract;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.sms.SmsSendActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class ContactList extends Activity {
ListView list_item;
    ArrayList list_phnnumber;
    ArrayList<HashMap<String,String>> addme;
    HashMap map;
    private Button back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_demo);
        list_phnnumber=new ArrayList();
       back = (Button) findViewById(R.id.back_button);
       back.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               onBackPressed();
           }
       });
        addme=new ArrayList();

        list_item=(ListView)findViewById(R.id.list_item);
        getcontactlist();
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(ContactList.this, SendLink.class);

        startActivity(intent);
        finish();
    }

    void getcontactlist() {
        //      progressDialog.show();
        //   progressDialog.setMessage("Loading");
        ContentResolver cr =getContentResolver();
        Cursor cur = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null,
                ContactsContract.Contacts.HAS_PHONE_NUMBER + " = 1",
                null,
                "UPPER(" + ContactsContract.Contacts.DISPLAY_NAME + ") ASC");


        if (cur.getCount() > 0) {
//            String phn_number = null;
            while (cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {

                        String phn_number = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER)).trim();
                        map = new HashMap();
                        String output = phn_number.replaceAll("\\s+","");

                        if (list_phnnumber.contains(output)) {

                        } else {
                            list_phnnumber.add(output);
                            map.put(output, name);
                            addme.add(map);
                        }
                    }
                    pCur.close();
                }
            }
        }
        else{
            Toast.makeText(ContactList.this,"No Contacts Found!",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(ContactList.this, SendLink.class);
            intent.putExtra("phone_number", "");
            intent.putExtra("addresscon", "");
            startActivity(intent);
            finish();
        }
        phn_adap adpter=new phn_adap(list_phnnumber,addme);
        list_item.setAdapter(adpter);
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog()
                .penaltyDeath().build());
    }


        class phn_adap extends BaseAdapter{
            ArrayList list_phnnumber;
            ArrayList<HashMap<String, String>> addme;
            public phn_adap(ArrayList list_phnnumber, ArrayList<HashMap<String, String>> addme) {
                this.list_phnnumber=list_phnnumber;
                this.addme=addme;
            }

            @Override
    public int getCount() {
        return addme.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater=LayoutInflater.from(ContactList.this);
        convertView=layoutInflater.inflate(R.layout.contact_list_row,null);
        TextView phn=(TextView)convertView.findViewById(R.id.phn);
        TextView name=(TextView)convertView.findViewById(R.id.name);
phn.setText(""+list_phnnumber.get(position));
name.setText(""+addme.get(position).get(list_phnnumber.get(position)));
convertView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
//        Toast.makeText(ContactList.this,"phn="+list_phnnumber.get(position)+"name="+addme.get(position).get(list_phnnumber.get(position)),Toast.LENGTH_LONG).show();
        Intent intent = new Intent(ContactList.this, SendLink.class);
        intent.putExtra("phone_number", String.valueOf(list_phnnumber.get(position)));
        intent.putExtra("addresscon", addme.get(position).get(list_phnnumber.get(position)));
        startActivity(intent);
        finish();
    }
});
        return convertView;
    }
}
}
