package com.salvador.secureanymobileapps.tokenlogin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.Settings.PassGenActivity;
import com.salvador.secureanymobileapps.chat.ChatMainActivity;
import com.salvador.secureanymobileapps.chat.LoginActivity;
import com.salvador.secureanymobileapps.documents.SimmpleApp;

/**
 * Created by insonix on 29/12/15.
 */
public class ToolsActivity extends Activity {
    Button back;
    TextView labelGeneratePassword,calender,contactrequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tools_activty);
        try {
            ApplicationClass.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                    .setLabel("Tools Activity")
                    .build());
        }catch (Exception e){
            e.printStackTrace();
        }
        this.back = (Button) findViewById(R.id.back_button);
        this.calender = (TextView) findViewById(R.id.calender);
        this.contactrequest = (TextView) findViewById(R.id.contactrequest);
        this.labelGeneratePassword = (TextView) findViewById(R.id.labelGeneratePassword);
       back.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent=new Intent(ToolsActivity.this,MainMenu.class);
               startActivity(intent);
               finish();
           }
       });
        labelGeneratePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ToolsActivity.this, PassGenActivity.class);
                startActivity(myIntent);
                finish();
            }
        });
        calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ToolsActivity.this, CalenderActivity.class);
                startActivity(myIntent);
                finish();
            }
        });
        contactrequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ToolsActivity.this, ContactMenu.class);
                startActivity(myIntent);
                finish();
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(ToolsActivity.this, MainMenu.class);
        startActivity(in);
        overridePendingTransition(0,0);
        finish();
    }
    @Override
    public void onResume() {

        super.onResume();


        if(isFinishing()) {
            Intent intent = new Intent(ToolsActivity.this, ChatMainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }
}
