package com.salvador.secureanymobileapps.tokenlogin;

/**
 * @author greg
 * @since 6/21/13
 */
public class Chat {
    private String message;
    private String chatID;
    private String time;
    private String image;
    private String type;
    private String first;
    private boolean decr;
    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    public Chat() {
    }

    public Chat(String message, String chatID, String time, String type,String first,boolean decr) {
        this.message = message;
        this.chatID = chatID;
        this.time = time;
        this.type = type;
        this.first = first;
        this.decr=decr;
    }
    public String getFirst() {
        return first;
    }
    public String getTime() {
        return time;
    }
    public String getImage() {
        return image;
    }
    public String getMessage() {
        return message;
    }
    public String getChatID() {
        return chatID;
    }
    public String getType() {
		return type;
	}
    public boolean getDecr() {
        return decr;
    }
    public void setDecr(boolean decr) {
        this.decr = decr;
    }


}
