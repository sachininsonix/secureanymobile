package com.salvador.secureanymobileapps.tokenlogin;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothSocket;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.widget.TextView;

import com.salvador.secureanymobileapps.bluetooth.bluetoothConnection;

public class Globals {
	
	public static BluetoothAdapter mBluetoothAdapter;
	static TextView tvStat;
	public static BluetoothSocket mmSocket;

    public static bluetoothConnection tokenConnection = null;
    public static boolean tokenActiveConnection = false;
	static TelephonyManager telephonyManager;
	static String device_imei;
    static boolean _DEBUG;

    //chat
    static boolean ChatConnectionActive;

    //For Documents received via Share Menu
    public static String sSharedDocument = "";
    public static Uri sSharedDocumentURI = null;
    static boolean bSharedDocIsReady = false;

    static boolean bTokenEnabled = false;

    public static String serverVersionCode = "";
    public static String serverVersionName = "";
}
