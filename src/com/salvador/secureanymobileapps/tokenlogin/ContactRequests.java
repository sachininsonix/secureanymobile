package com.salvador.secureanymobileapps.tokenlogin;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.LoginActivity;
import com.salvador.secureanymobileapps.constant.Iconstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by insonix on 5/5/16.
 */
public class ContactRequests extends Activity implements Iconstant {
    ListView requestslist;
    Button back;
    ProgressDialog progressDialog;
    String url1,res,url,fromID,requestId,EmailID,userName,Firstname,LastName,phone,key,key2;

    ArrayList<HashMap<String,String >> addme;
    SharedPreferences preferences,sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contactrequests);
        back=(Button)findViewById(R.id.back_button);
        addme=new ArrayList<>();
        requestslist=(ListView)findViewById(R.id.requestslist);
        progressDialog=new ProgressDialog(ContactRequests.this);
        sharedPreferences=getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
        preferences = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        NotificationManager notifManager= (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.cancel(999);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ContactRequests.this, ContactMenu.class);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();
            }
        });
        if (isNetworkAvailable() == false) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ContactRequests.this);
            builder.setMessage("Not able to connect to SAM. Please check your network connection and try again.")
                    .setTitle("Error")
                    .setCancelable(false)
                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //  Action for 'NO' Button
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
        }
        else {
            SellList();
        }
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public void SellList(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url1=BaseUrl+"get_invites.php?id="+preferences.getString("uid","");
        Log.d("url1", "url1" + url1);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try {
                    String status=jsonObject.getString("status");
                    if(status.equalsIgnoreCase("true")){
                        JSONArray jsonArray=jsonObject.getJSONArray("invites");
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject invite=jsonArray.getJSONObject(i);
                            String request_id=invite.getString("request_id");
                            String username=invite.getString("username");
                            String email=invite.getString("email");
                            String from_id=invite.getString("from_id");
                            String phone=invite.getString("phone");
                            String firstname=invite.getString("firstname");
                            String lastname=invite.getString("lastname");
                            String request_status=invite.getString("request_status");
                            HashMap<String,String> hashMap=new HashMap<>();
                            hashMap.put("req_id", request_id);
                            hashMap.put("userName", username);
                            hashMap.put("email", email);
                            hashMap.put("from_id", from_id);
                            hashMap.put("phone", phone);
                            hashMap.put("firstname", firstname);
                            hashMap.put("lastname", lastname);
                            hashMap.put("request_status", request_status);
                            addme.add(hashMap);
                        }
                        requestslist.setAdapter(new RequestListAdapter(ContactRequests.this, addme));
                    }else{
                        //Put up the Yes/No message box
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ContactRequests.this);
                        alertDialog.setTitle("              Sam");
                        alertDialog.setMessage("Oops! no invites found!");
//                        alertDialog.setIcon(R.drawable.add_icon);
                        alertDialog.show();
//                        new ShowMsg().createDialog(SellingList.this, "Oops! no seller in your area Be the first?");
                    }
                }catch(Exception e){

                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
                progressDialog.dismiss();
            }


        });
//        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }


    class RequestListAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<HashMap<String,String>> addme;
        public RequestListAdapter(Activity activity,ArrayList<HashMap<String,String>> addme){
            this.activity=activity;
            this.addme=addme;
        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.contactrequest_listitem,null);
            TextView name=(TextView)convertView.findViewById(R.id.name);
            ImageView acceptimage=(ImageView)convertView.findViewById(R.id.acceptImage);
            ImageView rejectimage=(ImageView)convertView.findViewById(R.id.rejectImage);
            key= preferences.getString("phone_number","")+phone+"gmai@insonix@220";
            fromID=addme.get(position).get("from_id");
            requestId=addme.get(position).get("req_id");
            EmailID=addme.get(position).get("email");
            phone=addme.get(position).get("phone");
            userName=addme.get(position).get("username");
            Firstname=addme.get(position).get("Firstname");
            LastName=addme.get(position).get("LastName");
            key2=key.substring(0,16);

            try {
                Log.d("request", "request" + addme.get(position).get("request_status"));
                Log.d("request", "requestId" + addme.get(position).get("req_id"));
                if (addme.get(position).get("request_status").equals("1")) {
                    acceptimage.setVisibility(View.GONE);
                    rejectimage.setVisibility(View.GONE);
                } else {

                }
            }catch (Exception e){
                e.printStackTrace();
            }
            name.setText(addme.get(position).get("firstname")+" "+addme.get(position).get("lastname"));

            rejectimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    res="0";
                    AcceptNoti();
                    addme.remove(position);
                    notifyDataSetChanged();
                }
            });
            acceptimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    res = "1";
                    AcceptNoti();

                }
            });
            return convertView;
        }
    }
    public void AcceptNoti(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url=BaseUrl+"get_key_response.php?from_id="+fromID+"&key="+key2+"&phone="+phone+"&response="+res+"&request_id="+requestId;
        Log.d("url1111:", "url" + url);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try {
                    String Status = jsonObject.getString("status");
                    String message = jsonObject.getString("msg");
                    if(Status.equals("true")) {
                        Toast.makeText(ContactRequests.this, message, Toast.LENGTH_SHORT).show();
                      Intent intent=new Intent(ContactRequests.this,ContactRequests.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(ContactRequests.this, message, Toast.LENGTH_SHORT).show();

                    }
                }catch (Exception e){
                    Log.d("eeeee:", "eeeee" + e.getMessage());
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
                progressDialog.dismiss();
            }


        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent=new Intent(ContactRequests.this, ContactMenu.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
    @Override
    public void onResume() {

        super.onResume();


        if(isFinishing()) {
            Intent intent = new Intent(ContactRequests.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }
}
