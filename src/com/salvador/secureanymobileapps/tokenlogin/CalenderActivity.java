package com.salvador.secureanymobileapps.tokenlogin;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.Settings.PassGenActivity;

/**
 * Created by insonix on 20/1/16.
 */
public class CalenderActivity extends Activity {
    Button back;
    ComponentName cn;
    TextView labelGeneratePassword,calender;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tools_activty);
        this.back = (Button) findViewById(R.id.back_button);
        this.calender = (TextView) findViewById(R.id.calender);
        this.labelGeneratePassword = (TextView) findViewById(R.id.labelGeneratePassword);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(CalenderActivity.this,MainMenu.class);
                startActivity(intent);
                finish();
            }
        });
        labelGeneratePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(CalenderActivity.this, PassGenActivity.class);
                startActivity(myIntent);
                finish();
            }
        });
        calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(CalenderActivity.this, CalenderActivity.class);
                startActivity(myIntent);
                finish();
            }
        });
        Intent myIntent ;
        long startMillis = System.currentTimeMillis();
        Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
        builder.appendPath("time");
        ContentUris.appendId(builder, startMillis);
        myIntent = new Intent(Intent.ACTION_VIEW).setData(builder.build());
        startActivity(myIntent);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(CalenderActivity.this,MainMenu.class);
        startActivity(intent);
        finish();
    }
}
