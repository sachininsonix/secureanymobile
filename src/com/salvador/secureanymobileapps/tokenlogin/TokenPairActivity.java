package com.salvador.secureanymobileapps.tokenlogin;


import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.Settings.UserPreferencesKey;
import com.salvador.secureanymobileapps.bluetooth.bluetoothConnection;
import com.salvador.secureanymobileapps.bluetooth.bluetoothManager;
import com.salvador.secureanymobileapps.bluetooth.opCode;

import java.util.ArrayList;
import java.util.Set;

public class TokenPairActivity extends Activity {

    private ListView pairedTokensList;
    private ListView availTokensList;

    private SharedPreferences settings;

    public static String TAG = "Security Any  Mobile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_token_pair);
//        FlurryAgent.init(TokenPairActivity.this,"9TG2VY5QR74RKTF6XVZX");

        refreshTokenList();

    }
//    public void onStart()
//    {
//        super.onStart();
//        FlurryAgent.onStartSession(TokenPairActivity.this, "9TG2VY5QR74RKTF6XVZX");
//        // your code
//    }
//
//    public void onStop()
//    {
//        super.onStop();
//        FlurryAgent.onEndSession(TokenPairActivity.this);
//        // your code
//    }

    public void refreshTokenList(){

        this.settings = this.getSharedPreferences(UserPreferencesKey.keyPrefTokenAddress, 0);
        String tokenAddress = this.settings.getString(UserPreferencesKey.keyTokenAddress, "");
        // Get ListView objects from xml
        pairedTokensList = (ListView) findViewById(R.id.pairedTokensList);
        availTokensList = (ListView) findViewById(R.id.availTokensList);

        // Defined Array values to show in ListView
        // Defined Array values to show in ListView
        final ArrayList<String> Source = new ArrayList<String>();
        final ArrayList<String> Target = new ArrayList<String>();

        //Check if app has a Hardware Token paired...
        if (!tokenAddress.equals("")) {

            //Search for Paired Bluetooth Devices named .S.A.M. and add them to the list...
            try {
                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                Set<BluetoothDevice> bondedSet = bluetoothAdapter.getBondedDevices();
                int count = 0;
                if (bondedSet.size() > 0) {
                    for (BluetoothDevice device : bondedSet) {
                        if (device.getName().contentEquals(".S.A.M.")) {
                            Log.i(TAG, device.getName() + " " + device.getAddress());
                            Log.i(TAG, "SAM Token Found!");
                            Source.add(device.getAddress());
                        }
                    }
                } else {
                    Log.i(TAG, "No Bonded Devices...");
                }
            } catch (Exception e) {
                Log.e(TAG, "Error");
            }


            final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, Source);
            final ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, Target);

            // Assign adapter to ListView
            pairedTokensList.setAdapter(adapter);
            availTokensList.setAdapter(adapter2);

            // ListView Item Click Listener
            pairedTokensList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // ListView Clicked item index
                    int itemPosition = position;
                    // ListView Clicked item value
                    String itemValue = (String) pairedTokensList.getItemAtPosition(position);
                    // Show Alert
                    Toast.makeText(getApplicationContext(),
                            "Removing Device: " + itemValue, Toast.LENGTH_LONG)
                            .show();
                    Target.add(itemValue);
                    Source.remove(position);
                    adapter.notifyDataSetChanged();
                    adapter2.notifyDataSetChanged();
                    saveTokenAddress("");
                    saveDisableToken();
                    saveDisableProximity();
                    if(Globals.tokenActiveConnection == true) {
                        Globals.tokenConnection=null;
                        bluetoothConnection.cancel();
                        Globals.tokenActiveConnection = false;
                    }
                    refreshTokenList();
                }

            });
        } else {
            //NO PAIRED DEVICES...

            //Search for Paired Bluetooth Devices named .S.A.M. and add them to the list...
            try {
                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                Set<BluetoothDevice> bondedSet = bluetoothAdapter.getBondedDevices();
                int count = 0;
                if (bondedSet.size() > 0) {
                    for (BluetoothDevice device : bondedSet) {
                        if (device.getName().contentEquals(".S.A.M.")) {
                            Log.i(TAG, device.getName() + " " + device.getAddress());
                            Log.i(TAG, "SAM Token Found!");
                            Source.add(device.getAddress());
                        }
                    }
                } else {
                    Log.i(TAG, "No Bonded Devices...");
                }
            } catch (Exception e) {
                Log.e(TAG, "Error");
            }


            final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, Source);
            final ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, Target);

            // Assign adapter to ListView
            availTokensList.setAdapter(adapter);
            pairedTokensList.setAdapter(adapter2);

            // ListView Item Click Listener
            availTokensList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // ListView Clicked item index
                    int itemPosition = position;
                    // ListView Clicked item value
                    String itemValue = (String) availTokensList.getItemAtPosition(position);


                    if(Globals.tokenActiveConnection == false) {

                        if(!bluetoothManager.bluetoothInit()) {
                            Log.i(TAG, "Failed to Init Bluetooth");
                        } else {
                            launchSearchDialog(view, itemValue);

                            // Show Alert
                            Toast.makeText(getApplicationContext(),
                                    "Pairing Device" + itemValue, Toast.LENGTH_LONG)
                                    .show();
                            Source.remove(position);
                            Target.add(itemValue);
                            adapter.notifyDataSetChanged();
                            adapter2.notifyDataSetChanged();
                            saveTokenAddress(itemValue);
                            refreshTokenList();
                        }
                    } else {
                        //get phone IMEI:
                        Globals.telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                        Globals.telephonyManager.getDeviceId();
                        Globals.device_imei = Globals.telephonyManager.getDeviceId();
                        Globals.tokenConnection.sendTokenPacket(opCode.PAIR_WITH_IMEI, Globals.device_imei);
                        Log.i(TAG, "IMEI: " + Globals.device_imei);
                    }
                }
            });
        }

    }
//    public void onStart()
//    {
//        super.onStart();
//        FlurryAgent.onStartSession(this, "9TG2VY5QR74RKTF6XVZX");
//        // your code
//    }
//
//    public void onStop()
//    {
//        super.onStop();
//        FlurryAgent.onEndSession(this);
//        // your code
//    }

    public void launchSearchDialog(View view, String address) {
        final ProgressDialog SearchProgressDialog = ProgressDialog.show(TokenPairActivity.this, "Please wait ...", "Searching for SAM Token ...", true);
        SearchProgressDialog.setCancelable(true);
        final View thisView = (View) view;
        final String add = address;
        //final SharedPreferences prefs;
        //prefs = this.getSharedPreferences(UserPreferencesKey.keyPrefTokenAddress, 0);
        //final String address = prefs.getString(UserPreferencesKey.keyTokenAddress, "");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if(!bluetoothManager.tokenConnect(add)) {
                        Log.i(TAG, "Failed to Connect");
                        SearchProgressDialog.dismiss();
                    } else {
                        Log.i(TAG, "Connected to Device: " + add);
                        Thread.sleep(2000);
                        SearchProgressDialog.dismiss();
                        // Test Connection
                        Globals.tokenConnection = new bluetoothConnection(Globals.mmSocket);
                        Globals.tokenConnection.start();
                        //String hello = "[hello world!]";
                        //byte[] b = hello.getBytes();
                        //Globals.tokenConnection.write(b);

                        //get phone IMEI:
                        Globals.telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                        Globals.telephonyManager.getDeviceId();
                        Globals.device_imei = Globals.telephonyManager.getDeviceId();
                        Globals.tokenConnection.sendTokenPacket(opCode.PAIR_WITH_IMEI, Globals.device_imei);
                        Log.i(TAG, "IMEI: " + Globals.device_imei);

                    }
                } catch (Exception e) {
                    Log.e(TAG, "Error");
                }
            }
        }).start();
    }


    private void saveDisableToken() {
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyEnToken, 0);
        SharedPreferences.Editor editor = this.settings.edit();
        editor.putBoolean(UserPreferencesKey.keyEnableToken, false);
        // Commit the edits!
        editor.commit();
    }

    private void saveDisableProximity() {
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyEnProxy, 0);
        SharedPreferences.Editor editor = this.settings.edit();
        editor.putBoolean(UserPreferencesKey.keyEnableProximity, false);
        // Commit the edits!
        editor.commit();
    }

    public void refreshDialog(View view) {
        final ProgressDialog SearchProgressDialog = ProgressDialog.show(TokenPairActivity.this, "Please wait ...", "Refreshing List of Tokens.", true);
        SearchProgressDialog.setCancelable(true);
        final View thisView = (View) view;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                        Thread.sleep(1500);
                        SearchProgressDialog.dismiss();
                } catch (Exception e) {
                    Log.e(TAG, "Error");
                }
            }
        }).start();
    }

    private void saveTokenAddress(String tokenAddress) {
        this.settings = this.getSharedPreferences(UserPreferencesKey.keyPrefTokenAddress, 0);
        SharedPreferences.Editor editor = this.settings.edit();
        editor.putString(UserPreferencesKey.keyTokenAddress, tokenAddress);
        // Commit the edits!
        editor.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.token_pair_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;

        if (item.getItemId() == R.id.refresh_token_list) {
            refreshTokenList();
        }

        if (intent != null) {
            startActivity(intent);
            intent = null;
        }
        return (super.onOptionsItemSelected(item));
    }

}
