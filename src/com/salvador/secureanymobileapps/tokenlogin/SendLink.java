package com.salvador.secureanymobileapps.tokenlogin;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.ChatMainActivity;
import com.salvador.secureanymobileapps.chat.ChatUserlist;
import com.salvador.secureanymobileapps.chat.LoginActivity;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.documents.SimmpleApp;
import com.salvador.secureanymobileapps.sms.util.Encryption;
import com.salvador.secureanymobileapps.tokenlogin.BaseActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainActivity;

import org.json.JSONObject;

/**
 * Created by KPC on 25/07/2014.
 */
public class SendLink extends BaseActivity implements View.OnClickListener,Iconstant {

    private static final String TAG = SendLink.class.getSimpleName();
    private static final int REQUEST_CODE_PICK_CONTACTS = 1;
    private Uri uriContact;
    private String contactID,ContName;     // contacts unique ID
    private String fn, msg, url, url1, key1, phoneNo,key2,number;
    private Button sendBtn;
    private ImageView ibSelectContact;
    private Button menu;
    private Button back;
    SharedPreferences.Editor editor;
    Dialog imageDialog;
    static int w = 0, h = 0;
    private EditText txtphoneNo;
    private EditText txtMessage;
    private SharedPreferences isTextNull;
    ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_send);
        try {
            ApplicationClass.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                    .setLabel("SmsSend Activity")
                    .build());
        }catch (Exception e){

        }
        // btn  of  View  send  message
        this.sendBtn = (Button) findViewById(R.id.btnSendSMS);
        this.sendBtn.setOnClickListener(this);
        this.ibSelectContact = (ImageView) findViewById(R.id.ibContactsSelect);
        this.ibSelectContact.setOnClickListener(this);
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
        this.menu = (Button) findViewById(R.id.menuSendMessage);
        this.menu.setOnClickListener(this);
        this.registerForContextMenu(this.menu);
        /// textInput  view  send  message
        this.txtphoneNo = (EditText) findViewById(R.id.editTextPhoneNo);
        this.txtMessage = (EditText) findViewById(R.id.editTextSMS);
        Bundle extras = getIntent().getExtras();
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        editor=isTextNull.edit();
        txtMessage.setText(R.string.playstore_url);
        progressDialog = new ProgressDialog(SendLink.this);
        Display display = getWindowManager().getDefaultDisplay();
        w = display.getWidth();
        h = display.getHeight();
        TelephonyManager tMgr = (TelephonyManager)SendLink.this.getSystemService(Context.TELEPHONY_SERVICE);

        number = tMgr.getLine1Number();
        Log.d("number", "number" + number);
        try{
            phoneNo=getIntent().getExtras().getString("phone_number");
            ContName=getIntent().getExtras().getString("addresscon");
//            address1=ContName.replace("+91", "");
            Log.d("ContName", "ContName" + ContName);
            Log.d("phoneNo", "phoneNo" + phoneNo);
            if(ContName==null){
                txtphoneNo.setText(phoneNo);
            }else{
                txtphoneNo.setText(ContName);
            }



        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(SendLink.this, SendInvite.class);

        startActivity(intent);
        finish();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
    @Override
    public void onClick(View view) {

        String tag = view.getTag() + "";
        if (tag.equals("back")) {
            onBackPressed();
        } else if (tag.equals("ibcontacsSelect")) {
            hideSoftKeyboard(SendLink.this);
            Intent intent=new Intent(SendLink.this,ContactList.class);
//            intent.putExtra("smssen","smsact");
            startActivity(intent);
            finish();
        } else if (tag.equals("sendmessages")) {
            fn = txtphoneNo.getText().toString().trim();
            msg = txtMessage.getText().toString().trim();
            if (fn.length() == 0) {
                txtphoneNo.setError("can't be Empty");
                return;
            }
            if (msg.length() == 0) {
                txtphoneNo.setError(null);
                txtMessage.setError("can't be Empty");
                return;
            }
            if (msg.length() > 0) {
                txtMessage.setError(null);
            }
            sendSMSMessage();
//            Getkey();
        } else if (tag.equals("menu")) {
            this.openContextMenu(view);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        int view_id = v.getId();
        menu.setHeaderTitle("Options");
        menu.add(0, view_id, 0, "Inbox");
        menu.add(1, view_id, 0, "Sent Message");
        view_id = 0;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int itemId = item.getGroupId();
        Intent intent;
//        if (itemId == 0) {
//            intent = new Intent(SmsSendActivity.this, MsgList.class);
//            startActivity(intent);
//            finish();
//        } else if (itemId == 1) {
//            intent = new Intent(SmsSendActivity.this, SmsSentActivity.class);
//            startActivity(intent);
//            finish();
//        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case (1001):
                if (data != null) {
                    uriContact = data.getData();
                } else {
                    Toast.makeText(getApplicationContext(), "Not data Validate",
                            Toast.LENGTH_LONG).show();
                    break;
                }
                // cursor  one  for get name  and id contact
                String _ID = ContactsContract.Contacts._ID;
                String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
                String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;


                Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
                String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

//                Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
//                String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
//                String DATA = ContactsContract.CommonDataKinds.Email.DATA;


                String contact_id = null;
                String name = null;
                String phoneNumber = null;


                // this method  get  name and  phone  name
                Cursor cursor = getContentResolver().query(uriContact, null, null, null, null);
                if (cursor.getCount() > 0) {
                    while (cursor.moveToNext()) {
                        contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                        name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                        int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
                        //get  number  Phone
                        if (hasPhoneNumber > 0) {
                            Cursor phoneCursor = getContentResolver().query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);
                            while (phoneCursor.moveToNext()) {
                                phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                            }
                            phoneCursor.close();
                        }
                        // get ID contact
                        if (contact_id.length() > 0) {
                            Cursor cursorID = getContentResolver().query(uriContact, new String[]{ContactsContract.Contacts._ID}, null, null, null);
                            while (cursorID.moveToNext()) {
                                contact_id = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
                            }
                            cursorID.close();
                        }
                    }
                }

                if (phoneNumber != null) {
                    txtphoneNo.setText(phoneNumber);
                }

                Log.d(TAG, "Contact Name Contact : " + name);
                Log.d(TAG, "Contact Phone Number: " + phoneNumber);
                Log.d(TAG, "Contact Id contact: " + contact_id);
        }

    }
    @Override
    public void onResume() {

        super.onResume();


        if(isFinishing()) {
            Intent intent = new Intent(SendLink.this, SendInvite.class);

            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }

//    protected void sendSMSMessage() {
//        Log.i("Send SMS", "");
//        String phone = phoneNo;
//        final String message = txtMessage.getText().toString();
////        Encryption encryption = new Encryption();
////        String encrypted = encryption.encrypt(key2.getBytes(), message.getBytes());
////        Log.d("encrypted", "encrypted" + encrypted);
//        try {
//            SmsManager smsManager = SmsManager.getDefault();
//            if(phoneNo.equals("null")||phoneNo.equals("")) {
//                smsManager.sendTextMessage(txtphoneNo.getText().toString(), null, message, null, null);
//            }else{
//                smsManager.sendTextMessage(phoneNo, null, message, null, null);
//            }
//
////            Toast.makeText(getApplicationContext(), "SMS sent.",        Toast.LENGTH_LONG).show();
//            Intent intent = new Intent(SendLink.this, SendInvite.class);
//
//            startActivity(intent);
//            finish();
//        } catch (Exception e) {
//
//            Log.e("debug  app datos ", e + "");
//            Toast.makeText(getApplicationContext(),
//                    "SMS failed, please try again.",
//                    Toast.LENGTH_LONG).show();
//            e.printStackTrace();
//        }
//        String SENT = "SMS_SENT";
//        String DELIVERED = "SMS_DELIVERED";
//
//        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
//                new Intent(SENT), 0);
//
//        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
//                new Intent(DELIVERED), 0);
//        try {
//        //---when the SMS has been sent---
//        registerReceiver(new BroadcastReceiver(){
//            @Override
//            public void onReceive(Context arg0, Intent arg1) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        Toast.makeText(getBaseContext(), "SMS sent",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                        Toast.makeText(getBaseContext(), "Generic failure",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NO_SERVICE:
//                        Toast.makeText(getBaseContext(), "No service",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NULL_PDU:
//                        Toast.makeText(getBaseContext(), "Null PDU",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_RADIO_OFF:
//                        Toast.makeText(getBaseContext(), "Radio off",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        }, new IntentFilter(SENT));
//
//        //---when the SMS has been delivered---
//        registerReceiver(new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context arg0, Intent arg1) {
//                switch (getResultCode()) {
//                    case Activity.RESULT_OK:
//                        Toast.makeText(getBaseContext(), "SMS delivered",
//                                Toast.LENGTH_SHORT).show();
//                        Intent intent=new Intent(SendLink.this,SendInvite.class);
////
//            startActivity(intent);
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        Toast.makeText(getBaseContext(), "SMS not delivered",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        }, new IntentFilter(DELIVERED));
//
//        SmsManager smsManager = SmsManager.getDefault();
//        smsManager.sendTextMessage(phone, null, message, sentPI, deliveredPI);
//    } catch (Exception e) {
//
//        Log.e("debug  app datos ", e + "");
//        Toast.makeText(getApplicationContext(),
//                "SMS failed, please try again.",
//                Toast.LENGTH_LONG).show();
//        e.printStackTrace();
//    }
////        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//
//
//
//
//    }

    protected void sendSMSMessage() {
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.i("Send SMS", "");
        String phone = phoneNo;
        final String message = txtMessage.getText().toString();    String SENT = "sent";
        String DELIVERED = "delivered";

        Intent sentIntent = new Intent(SENT);
     /*Create Pending Intents*/
        PendingIntent sentPI = PendingIntent.getBroadcast(
                getApplicationContext(), 0, sentIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Intent deliveryIntent = new Intent(DELIVERED);

        PendingIntent deliverPI = PendingIntent.getBroadcast(
                getApplicationContext(), 0, deliveryIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
     /* Register for SMS send action */
        registerReceiver(new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String result = "";
                progressDialog.dismiss();
                switch (getResultCode()) {

                    case Activity.RESULT_OK:
                        result = "SMS sent successfully";
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        result = "SMS sending failed";
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        result = "Radio off";
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        result = "No PDU defined";
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        result = "No service";
                        break;
                }

                Toast.makeText(getApplicationContext(), result,
                        Toast.LENGTH_LONG).show();

            }

        }, new IntentFilter(SENT));
     /* Register for Delivery event */
        registerReceiver(new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(getApplicationContext(), "Delivered",
                        Toast.LENGTH_LONG).show();
            }

        }, new IntentFilter(DELIVERED));

      /*Send SMS*/
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(phone, null, message, sentPI,
                deliverPI);




    }



    }
