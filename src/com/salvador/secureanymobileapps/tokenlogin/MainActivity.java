package com.salvador.secureanymobileapps.tokenlogin;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.Settings.UserPreferencesKey;
import com.salvador.secureanymobileapps.bluetooth.bluetoothConnection;
import com.salvador.secureanymobileapps.bluetooth.bluetoothManager;
import com.salvador.secureanymobileapps.chat.ChatMainActivity;
import com.salvador.secureanymobileapps.chat.LruBitmapCache;
import com.salvador.secureanymobileapps.documents.SimmpleApp;

import java.io.File;

public class MainActivity extends Activity {
    private Button connectButton;
    private TextView tvStatus;
    private boolean connectStatus = false;
    private int buttonStatus = 0;
    private boolean closeProgressDialog = false;

    private SharedPreferences settings;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static MainActivity mInstance;
    private int REQ_IMEI = 0;
    private int REQ_NEW_REGISTRATION = 1;

    private int MESSAGE_STATE = REQ_IMEI;

    private static final int REQUEST_ENABLE_BT = 1;

    private SharedPreferences isTextNull;
    private SharedPreferences hasTokenPaired;

    private ApplicationClass app;

    private String BuildVersionPath = "";
    private String updatePath = "";

    public static String TAG = "secureanymobileapps";


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ApplicationClass.tracker();       //Code migrated to Application class i.e.SimmpleApp ,tracker is initialized in application class

        //*********** Share Menu Receive Function ******************
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String action = intent.getAction();
        mInstance = this;
        // if this is from the share menu
        if (Intent.ACTION_SEND.equals(action)) {
            if (extras.containsKey(Intent.EXTRA_STREAM)) {
                // Get resource path
                Uri uri = (Uri) extras.getParcelable(Intent.EXTRA_STREAM);
                Globals.sSharedDocument = getFileName(uri);
                Globals.bSharedDocIsReady = true;
                Globals.sSharedDocumentURI = uri;
                //Toast.makeText(getApplicationContext(), Globals.sSharedDocument,
                //Toast.LENGTH_LONG).show();


            }
        } else {
            Globals.bSharedDocIsReady = false;
            Globals.sSharedDocument = "";
        }
        //**********************************************************

        BuildVersionPath = "http://www.circatek.co.uk/downloads/samversion.txt";

//        GetVersionFromServer getVersion = new GetVersionFromServer();
//        getVersion.setContext(getApplicationContext());
//        getVersion.execute(BuildVersionPath);


        Globals._DEBUG = false;
        //Set the Layout for the Activity
        setContentView(R.layout.activity_main);
//        FlurryAgent.init(MainActivity.this, "9TG2VY5QR74RKTF6XVZX");

        final View controlsView = findViewById(R.id.fullscreen_content_controls);
        //final View contentView = findViewById(R.id.fullscreen_content);

        controlsView.setVisibility(View.VISIBLE);

        Globals.tvStat = (TextView) findViewById(R.id.textView1);

        this.settings = this.getSharedPreferences(UserPreferencesKey.keyEnToken, 0);
        boolean isToken = this.settings.getBoolean(UserPreferencesKey.keyEnableToken, false);
        Globals.bTokenEnabled = isToken;

        if(Globals.bTokenEnabled == true) {
            connectButton = (Button) findViewById(R.id.connect_button);
            connectButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (buttonStatus == 0) //Not Connected
                    {
                        launchSearchDialog(view);
                    }
                }
            });

            //connectButton.performClick(); //Force the app to start searching straight away...
            if(!bluetoothManager.bluetoothInit()) {
                Toast.makeText(MainActivity.this, "Device does not support Bluetooth!", Toast.LENGTH_LONG).show();
                finish();
            } else {
                if (!Globals.mBluetoothAdapter.isEnabled()) {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                }
            }
        }else{
            //Go Straight to Password Entry...
            Intent myIntent = new Intent(MainActivity.this, ChatMainActivity.class);
            MainActivity.this.startActivity(myIntent);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        RelativeLayout ContainerMainActivity = (RelativeLayout) findViewById(R.id.ContainerMainActivity);
        Resources resources = getResources();
        isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        if (!isTextNull.getString("theme", "").equals("")) {
            if (isTextNull.getString("theme", "").equals("Black/Grey")) {
                ContainerMainActivity.setBackgroundColor(resources.getColor(R.color.backgroundTheme1));
            } else if (isTextNull.getString("theme", "").equals("White/Grey")) {
                ContainerMainActivity.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
            } else if (isTextNull.getString("theme", "").equals("iOS")) {
                ContainerMainActivity.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
            } else if (isTextNull.getString("theme", "").equals("Windows")) {
                ContainerMainActivity.setBackgroundColor(resources.getColor(R.color.backgroundTheme1));
            }
        } else {
            ContainerMainActivity.setBackgroundColor(resources.getColor(R.color.backgroundTheme2));
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public void launchSearchDialog(View view) {
        final ProgressDialog SearchProgressDialog = ProgressDialog.show(MainActivity.this, "Please wait ...", "Searching for SAM Token ...", true);
        SearchProgressDialog.setCancelable(true);
        final View thisView = (View) view;
        final SharedPreferences prefs;
        prefs = this.getSharedPreferences(UserPreferencesKey.keyPrefTokenAddress, 0);
        final String address = prefs.getString(UserPreferencesKey.keyTokenAddress, "");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if(!bluetoothManager.tokenConnect(address)) {
                        Log.i(TAG, "Failed to Connect");
                        SearchProgressDialog.dismiss();
                    } else {
                        Log.i(TAG, "Connected to Device: "+address);
                        Thread.sleep(2000);
                        SearchProgressDialog.dismiss();
                        // Test Connection
                        Globals.tokenConnection = new bluetoothConnection(Globals.mmSocket);
                        Globals.tokenConnection.start();

                        Globals.tokenActiveConnection = true;

                        // Go to Main Menu
                        Intent myIntent = new Intent(MainActivity.this, ChatMainActivity.class);
                        MainActivity.this.startActivity(myIntent);
                    }
                } catch (Exception e) {
                    Log.e(TAG,"Error");
                }
            }
        }).start();
    }

    public void onBackPressed() {
        closeProgressDialog = true;
        Globals.tokenConnection=null;
        bluetoothConnection.cancel();
        finish();
    }

    public static synchronized MainActivity getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

public void clearPreferences() {
    try {
        // clearing app data
        Runtime runtime = Runtime.getRuntime();
        runtime.exec("pm clear com.salvador.secureanymobileapps");
//            Intent intent=new Intent(SettingsActivity.this, ChatMainActivity.class);
////            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
        Log.e("UpdateAPP", "Update error! ");
    } catch (Exception e) {
        e.printStackTrace();
    }
}

}
