package com.salvador.secureanymobileapps.tokenlogin;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.ChatMainActivity;
import com.salvador.secureanymobileapps.chat.ChatUserlist;
import com.salvador.secureanymobileapps.constant.Iconstant;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.List;

/**
 * Created by insonix on 4/8/16.
 */
public class SendInvite extends Activity implements Iconstant {
    Button send_invitebtn,sendlink;
    String url;
    EditText email_user;
    Button back_button;
    ProgressDialog progressDialog;
    private SharedPreferences isTextNull;
    InputMethodManager inputMethodManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_invite);
        inputMethodManager =
                (InputMethodManager) getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        back_button=(Button)findViewById(R.id.back_button);
        send_invitebtn=(Button)findViewById(R.id.send_invitebtn);
        email_user=(EditText)findViewById(R.id.email_user);
        progressDialog=new ProgressDialog(SendInvite.this);
        sendlink=(Button)findViewById(R.id.send_link);
        this.isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        send_invitebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                if(email_user.getText().toString().length()>0){
                    SendRequest();
                }else{
                    Toast.makeText(SendInvite.this, "Please enter Username/Email/Mobile number", Toast.LENGTH_SHORT).show();
                }
            }
        });
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        sendlink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SendInvite.this,SendLink.class);
                intent.putExtra("phone_number", "");
                startActivity(intent);
                finish();
            }
        });
    }
    public void SendRequest(){
        url=BaseUrl+"send_invite_emu.php?id="+isTextNull.getString("uid","")+"&email="+ URLEncoder.encode(email_user.getText().toString().trim())+"&phone="+isTextNull.getString("phone_number","");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.d("url", "url" + url);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {

                    String Status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if(Status.equals("true")) {
                        Toast.makeText(SendInvite.this, message, Toast.LENGTH_SHORT).show();
                        email_user.setText("");
                        Intent intent=new Intent(SendInvite.this,ContactMenu.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                    else if(Status.equals("invite")) {
                        if(message.equals("Already accepted")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(SendInvite.this);

                            builder.setTitle("SAM");
                            builder.setMessage(""+email_user.getText().toString().trim()+ " already in your contacts! Do you want to check?");

                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing but close the dialog
                                    Intent intent = new Intent(SendInvite.this, ChatUserlist.class);
                                    intent.putExtra("smssen","sendInvite");
                                    startActivity(intent);
                                    overridePendingTransition(0,0);
                                    finish();
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing//
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }else if(message.equals("Invite already sent")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(SendInvite.this);

                            builder.setTitle("SAM");
                            builder.setMessage("Invitation from "+email_user.getText().toString().trim()+ " already exists. Do you want to check?");

                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing but close the dialog
                                    Intent intent = new Intent(SendInvite.this, ContactRequests.class);
                                    startActivity(intent);
                                    overridePendingTransition(0,0);
                                    finish();
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing//
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                        else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(SendInvite.this);

                            builder.setTitle("SAM");
                            builder.setMessage("Invite Already Sent to " + email_user.getText().toString().trim() + " . Please wait for " + email_user.getText().toString().trim() + " response.");
                            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing
//
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();



                        }
                    }
                    else{
                      String type = jsonObject.getString("type");
                        if(type.equals("email") && Status.equals("false") ) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(SendInvite.this);
                                builder.setTitle("SAM");
                                builder.setMessage("" + email_user.getText().toString().trim() + " not registered. Do you want to send Email?");

                                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
//
                                        String[] TO = {email_user.getText().toString().trim()};
                                        String[] CC = {""};
                                        String body="<!DOCTYPE html><html><body><p> Hey</p><br/> <b>"+isTextNull.getString("username","")+"</b> sent you invitation to join Secure Any Mobile. <br/> <a href=" + "https://play.google.com/store/apps/details?id=com.salvador.secureanymobileapps" +">Click Here </a>  to get started.</p><br/><br/><br/>Thanks.<br/>Your Friend at <b>Secure Any Mobile</b></body></html>";

                                        final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                                        intent.setType("text/html");
                                        intent.putExtra(Intent.EXTRA_EMAIL, TO);
                                        intent.putExtra(Intent.EXTRA_CC, CC);
                                        intent.putExtra(Intent.EXTRA_SUBJECT, "Secure Any Mobile App");
                                        intent.putExtra(Intent.EXTRA_TEXT,Html.fromHtml(body));
                                        final PackageManager pm = getPackageManager();
                                        final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
                                        ResolveInfo best = null;
                                        for (final ResolveInfo info : matches)
                                            if (info.activityInfo.packageName.endsWith(".gm") ||
                                                    info.activityInfo.name.toLowerCase().contains("gmail")) best = info;
                                        if (best != null)
                                            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
                                        startActivity(intent);


                                    }
                                });
                                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Do nothing//
                                        dialog.dismiss();
                                    }
                                });

                                AlertDialog alert = builder.create();
                                alert.show();
                            }else if(type.equals("number") && Status.equals("false"))  {
                                AlertDialog.Builder builder = new AlertDialog.Builder(SendInvite.this);

                                builder.setTitle("SAM");
                            builder.setMessage("" + email_user.getText().toString().trim() + " not registered. Do you want to send Message?");

                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        // Do nothing but close the dialog
                                        Intent intent = new Intent(SendInvite.this, SendLink.class);
                                        intent.putExtra("phone_number", email_user.getText().toString().trim());
                                        startActivity(intent);
                                        overridePendingTransition(0, 0);
                                        finish();
                                    }
                                });
                                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Do nothing//
                                        dialog.dismiss();
                                    }
                                });

                                AlertDialog alert = builder.create();
                                alert.show();
                         }else if(type.equals("") && Status.equals("false"))  {
                            Toast.makeText(SendInvite.this, message, Toast.LENGTH_SHORT).show();
                        }
                        else {
                        Toast.makeText(SendInvite.this, message, Toast.LENGTH_SHORT).show();
                        }
                        }


                }catch (Exception e){
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }
        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent=new Intent(SendInvite.this,ContactMenu.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }
    @Override
    public void onResume() {

        super.onResume();



        if(isFinishing()) {
            Intent intent = new Intent(SendInvite.this, ChatMainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }
}
