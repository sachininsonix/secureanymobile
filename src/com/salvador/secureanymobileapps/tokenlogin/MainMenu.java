package com.salvador.secureanymobileapps.tokenlogin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.Settings.SettingsActivity;
import com.salvador.secureanymobileapps.Settings.UserPreferencesKey;
import com.salvador.secureanymobileapps.chat.ChatHistory;
import com.salvador.secureanymobileapps.chat.ChatMainActivity;
import com.salvador.secureanymobileapps.chat.IAppConfigurationConstant;
import com.salvador.secureanymobileapps.constant.Iconstant;
import com.salvador.secureanymobileapps.documents.DocsMainActivity;
import com.salvador.secureanymobileapps.documents.MainView;
import com.salvador.secureanymobileapps.documents.Prefrences_Manager;
import com.salvador.secureanymobileapps.documents.SimmpleApp;
import com.salvador.secureanymobileapps.documents.encrypt.decrypt.SimppleFileUtil;
import com.salvador.secureanymobileapps.k9.activity.Accounts;
import com.salvador.secureanymobileapps.sms.MsgList;
import com.salvador.secureanymobileapps.voip.VoipActivity;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

public class MainMenu extends BaseActivity implements IAppConfigurationConstant, Iconstant, View.OnClickListener {
    private SharedPreferences isTextNull;
    private Button back;
    //private Button sms;
    ProgressDialog progressDialog;
    private ImageButton documents;
    private ImageButton voip;
    //private Button chat;
    private ImageButton batteryFull;
    private ImageButton settings;
    private ImageButton emailButton;
    private ImageButton sms;
    private ImageButton chat;
    private ImageButton tools;
    SharedPreferences.Editor editor;
    TextView cur_val;
    private String url, url1;
    private String servername = "2.8.3";
    File file;
    ImageView log_out;
    private Prefrences_Manager prefrences_Manager;
    int downloadedSize = 0;
    int totalSize = 0;
    private LinearLayout smsLayout;
    private LinearLayout docsLayout;
    private LinearLayout voipLayout;
    private LinearLayout chatLayout;
    private LinearLayout emailLayout;
    private LinearLayout settingsLayout;
    private LinearLayout tokenLayout;
    private LinearLayout toolsLayout;
    ProgressBar pb;
    Dialog dialog;


    private static boolean smss = false;
    private static boolean smsPwd = false;

    private static boolean chats = false;
    private static boolean chatPwd = false;

    private static boolean Emails = false;
    private static boolean EmailPwd = false;

    private static boolean Voips = false;
    private static boolean VoipPwd = false;

    private static boolean Docs = false;
    private static boolean DocPwd = false;
    String acc = "";


  /*  public SharedPreferences getIsTextNull() {
        return isTextNull;
    }

    public void setIsTextNull(SharedPreferences isTextNull) {
        this.isTextNull = isTextNull;
    }*/

    public static boolean isChatPwd() {
        return chatPwd;
    }

    public static void setChatPwd(boolean chatPwd) {
        MainMenu.chatPwd = chatPwd;
    }

    public static boolean isChats() {
        return chats;
    }

    public static void setChats(boolean chats) {
        MainMenu.chats = chats;
    }

    public static boolean isSmsPwd() {
        return smsPwd;
    }

    public static void setSmsPwd(boolean smsPwd) {
        MainMenu.smsPwd = smsPwd;
    }

    public static boolean isSmss() {
        return smss;
    }

    public static void setSmss(boolean smss) {
        MainMenu.smss = smss;
    }


    public static boolean isEmails() {
        return Emails;
    }

    public static void setEmails(boolean emails) {
        Emails = emails;
    }

    public static boolean isEmailPwd() {
        return EmailPwd;
    }

    public static void setEmailPwd(boolean emailPwd) {
        EmailPwd = emailPwd;
    }

    public static boolean isVoips() {
        return Voips;
    }

    public static void setVoips(boolean voips) {
        Voips = voips;
    }

    public static boolean isVoipPwd() {
        return VoipPwd;
    }

    public static void setVoipPwd(boolean voipPwd) {
        VoipPwd = voipPwd;
    }

    public static boolean isDocs() {
        return Docs;
    }

    public static void setDocs(boolean docs) {
        Docs = docs;
    }

    public static boolean isDocPwd() {
        return DocPwd;
    }

    public static void setDocPwd(boolean docPwd) {
        DocPwd = docPwd;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

//        setTheme(ThemeSetter.GetTheme(this));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        editor = isTextNull.edit();
        progressDialog = new ProgressDialog(MainMenu.this);
        prefrences_Manager = new Prefrences_Manager(MainMenu.this);
//

        try {
//
            acc = getIntent().getExtras().getString("account");
            Log.d("ddd", "ddd" + acc);
        } catch (Exception e) {

        }
        final View controlsView = findViewById(R.id.fullscreen_content_controls);
        this.back = (Button) findViewById(R.id.back_button);
        this.back.setOnClickListener(this);
        this.sms = (ImageButton) findViewById(R.id.smsButton);
        this.sms.setOnClickListener(this);
        this.documents = (ImageButton) findViewById(R.id.docsButton);
        this.documents.setOnClickListener(this);
        this.voip = (ImageButton) findViewById(R.id.voipButton);
        this.voip.setOnClickListener(this);
        this.chat = (ImageButton) findViewById(R.id.chatButton);
        this.chat.setOnClickListener(this);
        this.batteryFull = (ImageButton) findViewById(R.id.tokenButton);
        this.batteryFull.setOnClickListener(this);
        this.settings = (ImageButton) findViewById(R.id.settingsButton);
        this.settings.setOnClickListener(this);
        this.emailButton = (ImageButton) findViewById(R.id.emailButton);
        this.emailButton.setOnClickListener(this);
        this.tools = (ImageButton) findViewById(R.id.toolsButton);
        this.tools.setOnClickListener(this);
        log_out = (ImageView) findViewById(R.id.log_out);
        log_out.setOnClickListener(this);

        this.smsLayout = (LinearLayout) findViewById(R.id.smsLayout);
        this.docsLayout = (LinearLayout) findViewById(R.id.docsLayout);
        this.voipLayout = (LinearLayout) findViewById(R.id.voipLayout);
        this.chatLayout = (LinearLayout) findViewById(R.id.chatLayout);
        this.emailLayout = (LinearLayout) findViewById(R.id.emailLayout);
        this.settingsLayout = (LinearLayout) findViewById(R.id.settingsLayout);
        this.tokenLayout = (LinearLayout) findViewById(R.id.tokenLayout);
        this.toolsLayout = (LinearLayout) findViewById(R.id.toolsLayout);
        dialog = new Dialog(MainMenu.this);
        ApplicationClass app = (ApplicationClass) getApplicationContext();

        log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable() == false) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainMenu.this);
                    builder.setMessage("Not able to connect to SAM. Please check your network connection and try again.")
                            .setTitle("Error")
                            .setCancelable(false)
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainMenu.this);

                    builder.setTitle("Confirm");
                    builder.setMessage("Are you sure you want to logout?");

                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing but close the dialog

                            Logout();
                        }

                    });

                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
//
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();

                }
            }
        });
//Version();

        app.initializeManager();
        onResume();
    }

//

    @Override
    public void onResume() {

        super.onResume();


        isTextNull = getApplicationContext().getSharedPreferences(
                "LoginPreferences", 0);
        if (isFinishing()) {
            Intent intent = new Intent(MainMenu.this, ChatMainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (!isFinishing()) {
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {

//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    finish();
//                mTextField.setText("done!");
                }

            }.start();
        }
//        myTimer.schedule(myTask, 10000, 1000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();
//            clearPreferences();
            moveTaskToBack(true);
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }
//
//public void DeleteAccount(){
//    Account realAccount = (Account) Accounts.mSelectedContextAccount;
////								Log.d("real_account","real_account"+realAccount);
//    try {
////        realAccount.getLocalStore().delete();
//        MessagingController.getInstance(
//                getApplication()).notifyAccountCancel(
//                MainMenu.this, realAccount);
//        Preferences.getPreferences(MainMenu.this)
//                .deleteAccount(realAccount);
//        ApplicationClass
//                .setServicesEnabled(MainMenu.this);
//    } catch (Exception e) {
//        Log.d("Message","Message"+e.getMessage());
//        // Ignore, this may lead to localStores on
//        // sd-cards that are
//        // currently not inserted to be left
//    }
//}


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void showProgress(String file_path) {

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.myprogressdialog);
        dialog.setTitle("Download Progress");

        TextView text = (TextView) dialog.findViewById(R.id.tv1);
        text.setText("Downloading file from ... " + file_path);
        cur_val = (TextView) dialog.findViewById(R.id.cur_pg_tv);
        cur_val.setText("Starting download...");
        dialog.show();

        pb = (ProgressBar) dialog.findViewById(R.id.progress_bar);
        pb.setProgress(0);
        pb.setProgressDrawable(getResources().getDrawable(R.drawable.green_progress));
    }

    private void clearPreferences() {
        try {
            // clearing app data
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("pm clear com.salvador.secureanymobileapps");
//            Intent intent=new Intent(SettingsActivity.this, ChatMainActivity.class);
////            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
            Log.e("UpdateAPP", "Update error! ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {

        SharedPreferences settings = this.getSharedPreferences(UserPreferencesKey.keyNoPass, 0);
        boolean isPassGeneral = settings.getBoolean(UserPreferencesKey.keyNOPassword, false);

        String tag = view.getTag() + "";

        if (tag.equals("back")) {
            onBackPressed();
        } else if (tag.equals("sms")) {
            if (isPassGeneral) {
                Intent intent = new Intent(MainMenu.this, MsgList.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(MainMenu.this, DocsMainActivity.class);
                intent.putExtra("Validate", "sms");
                intent.putExtra("account", acc);
                startActivity(intent);
                finish();
            }
        } else if (tag.equals("documents")) {

            if (Globals.bSharedDocIsReady == true) {
                AlertDialog.Builder adb = new AlertDialog.Builder(this);
                // Setting Dialog Title
                adb.setTitle("New Shared File");
                // Setting Dialog Message
                adb.setMessage("Are you sure you want to move the file " + Globals.sSharedDocument + " to your encypted documents folder? Doing so will delete the original file from memory.");
                // Setting Icon to Dialog
                adb.setIcon(android.R.drawable.ic_dialog_alert);

                // Setting OK Button
                adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        File mydir = new File(Environment.getExternalStorageDirectory() + "/SimmpleBox/");
                        SimmpleApp.simmpleBoxPath = mydir.getPath();

                        //Encrypt File....
                        Log.d("FILE SHARE: ", "Attempting to Encrypt File: " + Globals.sSharedDocument + " From Path: " + Globals.sSharedDocumentURI);

                        //Detect If File is Image or not:
                        final String[] okFileExtensions = new String[]{"jpg", "png", "gif", "jpeg"};
                        for (String extension : okFileExtensions) {
                            if (Globals.sSharedDocument.toLowerCase().endsWith(extension)) {
                                //This is an Image File;
                                //Encrypt file, store in gallery and delete original....
                                try {
                                    ProgressDialog encDialog = new ProgressDialog(MainMenu.this);
                                    encDialog.setCancelable(false);
                                    encDialog.setMessage("Encrypting data ...");
                                    encDialog.show();
                                    new SimppleFileUtil(MainMenu.this).encrptyAndStoreData(getPath(MainMenu.this, Globals.sSharedDocumentURI), SimmpleApp.simmpleBoxPath + "/Gallery", Globals.sSharedDocument);
                                    encDialog.dismiss();
                                } catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            } else {
                                //Other Type of File...
                                //Encrypt file, store in documents and delete original....
                                try {
                                    ProgressDialog encDialog = new ProgressDialog(MainMenu.this);
                                    encDialog.setCancelable(false);
                                    encDialog.setMessage("Encrypting data ...");
                                    encDialog.show();
                                    String path = Globals.sSharedDocumentURI.toString();
                                    if (path.toLowerCase().startsWith("file://")) {
                                        // Selected file/directory path is below
                                        path = (new File(URI.create(path))).getAbsolutePath();
                                    }
                                    new SimppleFileUtil(MainMenu.this).encrptyAndStoreData(path, SimmpleApp.simmpleBoxPath + "/Documents", Globals.sSharedDocument);
                                    encDialog.dismiss();
                                } catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                        }


                        //Go To documents area...
                        Intent in = new Intent(MainMenu.this, DocsMainActivity.class);
                        in.putExtra("Validate", "documents");
                        startActivity(in);
                    }
                });

                adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //Drop the shared document as Cancel was pressed
                        Globals.bSharedDocIsReady = false;
                        Globals.sSharedDocument = "";

                    }
                });

                // Showing Alert Message
                adb.show();

                //Reset the Documents Icon
                if (!this.isTextNull.getString("theme", "").equals("")) {
                    if (this.isTextNull.getString("theme", "").equals("Other Theme")) {
                        if (Globals.bSharedDocIsReady)
                            this.documents.setBackgroundResource(R.drawable.ic_documents_shared);
                        else
                            this.documents.setBackgroundResource(R.drawable.ic_documents_dark);
                    } else {
                        if (Globals.bSharedDocIsReady)
                            this.documents.setBackgroundResource(R.drawable.ic_documents_shared);
                        else
                            this.documents.setBackgroundResource(R.drawable.ic_documents_dark);
                    }
                } else {
                    if (Globals.bSharedDocIsReady)
                        this.documents.setBackgroundResource(R.drawable.ic_documents_shared);
                    else
                        this.documents.setBackgroundResource(R.drawable.ic_documents_dark);
                }

                //Reset the view and OnClick listeners
                setContentView(R.layout.activity_main_menu);
                final View controlsView = findViewById(R.id.fullscreen_content_controls);
                this.back = (Button) findViewById(R.id.back_button);
                this.back.setOnClickListener(this);
                this.sms = (ImageButton) findViewById(R.id.smsButton);
                this.sms.setOnClickListener(this);
                this.documents = (ImageButton) findViewById(R.id.docsButton);
                this.documents.setOnClickListener(this);
                this.voip = (ImageButton) findViewById(R.id.voipButton);
                this.voip.setOnClickListener(this);
                this.chat = (ImageButton) findViewById(R.id.chatButton);
                this.chat.setOnClickListener(this);
                this.batteryFull = (ImageButton) findViewById(R.id.tokenButton);
                this.batteryFull.setOnClickListener(this);
                this.settings = (ImageButton) findViewById(R.id.settingsButton);
                this.settings.setOnClickListener(this);
                this.emailButton = (ImageButton) findViewById(R.id.emailButton);
                this.emailButton.setOnClickListener(this);
            } else {
                if (isPassGeneral) {
                    Intent intent = new Intent(MainMenu.this, MainView.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(MainMenu.this, DocsMainActivity.class);
                    intent.putExtra("Validate", "documents");
                    intent.putExtra("account", acc);
                    startActivity(intent);
                    finish();
                }
            }

        } else if (tag.equals("voip")) {

            if (isPassGeneral) {
                Intent intent = new Intent(MainMenu.this, VoipActivity.class);
                intent.putExtra("call", "no");
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(MainMenu.this, DocsMainActivity.class);
                intent.putExtra("Validate", "voip");
                intent.putExtra("account", acc);
                startActivity(intent);
                finish();
            }
        } else if (tag.equals("chat")) {
            if (isPassGeneral) {
                Intent intent = new Intent(MainMenu.this, ChatHistory.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(MainMenu.this, DocsMainActivity.class);
                intent.putExtra("Validate", "chat");
                intent.putExtra("account", acc);
                startActivity(intent);
                finish();

            }
        } else if (tag.equals("batterylevelfull")) {
            Intent intent = new Intent(MainMenu.this, TokenInformation.class);
            startActivity(intent);
            finish();
        } else if (tag.equals("settings")) {
            Intent intent = new Intent(MainMenu.this, SettingsActivity.class);
            intent.putExtra("account", acc);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
//            finish();

        } else if (tag.equals("email")) {
            Log.d("isPassGeneral", "isPassGeneral" + isPassGeneral);
            if (isPassGeneral) {
                Intent intent = new Intent(MainMenu.this, Accounts.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(MainMenu.this, DocsMainActivity.class);

                intent.putExtra("Validate", "email");
                intent.putExtra("account", acc);

                startActivity(intent);
                finish();
            }
        } else if (tag.equals("tools")) {
            Intent intent = new Intent(MainMenu.this, ToolsActivity.class);
            intent.putExtra("account", acc);
            startActivity(intent);
            finish();
        }
//        if (intent != null) {
//
//        }

    }

    private String getPath(Context context, Uri selectedImageUri) {
        if (selectedImageUri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        @SuppressWarnings("deprecation")
        Cursor cursor = context.getContentResolver().query(selectedImageUri, projection, null, null,
                null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();

            String retVal = cursor.getString(column_index);
            cursor.close();
            return retVal;
        }
        cursor.close();
        return selectedImageUri.getPath();
    }

    public class UpdateApplication extends AsyncTask<String, Void, Void> {
        public Context context;

        public void setContext(Context contextf) {
            context = contextf;
        }

        @Override
        protected Void doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();

                String PATH = "/mnt/sdcard/Download/";
                file = new File(PATH);
                file.mkdirs();
                File outputFile = new File(file, "update.apk");
                if (outputFile.exists()) {
                    outputFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);

                InputStream is = c.getInputStream();
                totalSize = c.getContentLength();
                runOnUiThread(new Runnable() {
                    public void run() {
                        pb.setMax(totalSize);
                    }
                });


                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                    downloadedSize += len1;
                    // update the progressbar //
                    runOnUiThread(new Runnable() {
                        public void run() {
                            pb.setProgress(downloadedSize);
                            float per = ((float) downloadedSize / totalSize) * 100;
                            cur_val.setText("Downloaded " + downloadedSize + "KB / " + totalSize + "KB (" + (int) per + "%)");
                        }
                    });
                }
                fos.close();
                is.close();
                runOnUiThread(new Runnable() {
                    public void run() {
                        dialog.dismiss(); // if you want close it..
                    }
                });
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File("/mnt/sdcard/Download/update.apk")), "application/vnd.android.package-archive");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.setDataAndType(Uri.fromFile(new File("/mnt/sdcard/Download/update.apk")),"application/vnd.android.package-archive");
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
//            context.startActivity(intent);


            } catch (Exception e) {
                Log.e("UpdateAPP", "Update error! " + e.getMessage());
            }
            return null;
        }


    }

    public void Logout() {
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url1 = BaseUrl + "logout.php?id=" + isTextNull.getString("uid", "");
        Log.d("url1111:", "url" + url1);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try {
                    String Status = jsonObject.getString("status");
//                    DocsMainActivity.isFirstLaunch=true;
//                    AccountManager am = AccountManager.get(SettingsActivity.this);
//                    Account[] accounts = Preferences.getPreferences(SettingsActivity.this).getAccounts();
//                 ApplicationClass.isHideSpecialAccounts();
//                        android.accounts.Account accountToRemove = accounts[0];
//                        am.removeAccount(accountToRemove, null, null);
//TODO not being cleared to retain passwords if created
//                    prefrences_Manager.Allclear();
//                    clearPreferences();

//                    DocsMainActivity.isLocked=false;
//                    prefrences_Manager.
                    Intent intent = new Intent(MainMenu.this, ChatMainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();


//                    if (Accounts.mSelectedContextAccount instanceof Account) {
//                        Account realAccount = (Account) Accounts.mSelectedContextAccount;
//                        try {
//                            realAccount.getLocalStore().delete();
//                        } catch (Exception e) {
//                            Log.d("message","message"+e.getMessage());
//                            e.printStackTrace();
//                            // Ignore, this may lead to localStores on
//                            // sd-cards that are
//                            // currently not inserted to be left
//                        }
//                        MessagingController.getInstance(
//                                getApplication()).notifyAccountCancel(
//                                SettingsActivity.this, realAccount);
//                        Preferences.getPreferences(SettingsActivity.this)
//                                .deleteAccount(realAccount);
//                        ApplicationClass
//                                .setServicesEnabled(SettingsActivity.this);
//                        refresh();
//                    }


                } catch (Exception e) {
                    Log.d("eeeee:", "eeeee" + e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:", "volleyError" + volleyError);
            }


        });
        MainActivity.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }

}
