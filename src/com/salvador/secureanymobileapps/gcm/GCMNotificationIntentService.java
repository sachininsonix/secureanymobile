package com.salvador.secureanymobileapps.gcm;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.salvador.secureanymobileapps.R;
import com.salvador.secureanymobileapps.chat.AcceptDecline;
import com.salvador.secureanymobileapps.chat.ChatActivity;
import com.salvador.secureanymobileapps.tokenlogin.MainMenu;

import java.util.Iterator;
import java.util.Set;

public class GCMNotificationIntentService extends IntentService {
	// Sets an ID for the notification, so it can be updated
	//static final String MSG_KEY = "m";
	public static final int notifyID = 9001;
	private static final String TAG = "GCMNotifi";
	NotificationCompat.Builder builder;
	SharedPreferences preferences ;
	SharedPreferences.Editor editor;
	String getclass,newString;


	public GCMNotificationIntentService() {
		super("GcmIntentService");

	}


	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		//extras.get("message");
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

		String messageType = gcm.getMessageType(intent);
		Log.e(TAG,"messageType"+messageType);

		if (!extras.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				sendNotification("Send error: " + extras.toString(),""+extras.toString(),""+extras.toString(),"" +extras.toString(),
						"" +extras.toString(), "" +extras.toString(),""+extras.toString(),""+extras.toString(),"" +extras.toString(),
						"" +extras.toString(),"" +extras.toString(),"" +extras.toString(),"" +extras.toString(),"" +extras.toString(),"" +extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				sendNotification("Deleted messages on server: "
						+ extras.toString(),""+extras.toString(),""+extras.toString(),"" +extras.toString(),"" +extras.toString(), ""
						+ extras.toString(),""+extras.toString(),""+extras.toString(),"" +extras.toString(),"" +extras.toString()
						,"" +extras.toString(),"" +extras.toString(),"" +extras.toString(),"" +extras.toString(),"" +extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {
				sendNotification(" "
						+ extras.get("message"), "" + extras.get("uid"), "" + extras.get("chat_id"), "" + extras.get("name"), "" + extras.get("nick_name")
						, "" + extras.get("msg_id"), "" + extras.get("key"), "" + extras.get("type"), "" + extras.get("from_id"), "" + extras.get("phone")
						, "" + extras.get("default"), "" + extras.get("request_id"), "" + extras.get("firstname"), "" + extras.get("lastname"), "" + extras.get("phone_user"));
//
				Set<String> sss= extras.keySet();
				Iterator it=sss.iterator();
				while (it.hasNext()) {
				}
			}
		}
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void sendNotification(String msg,String uid,String chatid,String name,String nickname,String msg_id,String key,String secret_key,String from_id,String phone,String defaultpar,String request_id,String firstname,String lastname,String phone_user) {
		preferences =  getApplicationContext().getSharedPreferences(
				"LoginPreferences", 0);
		editor = preferences.edit();
		PendingIntent resultPendingIntent;
		Intent resultIntent;

//
if(secret_key.equals("secret_key")){

	resultIntent = new Intent(this, AcceptDecline.class);
	resultIntent.putExtra("key",key);
	resultIntent.putExtra("phone",phone);
	resultIntent.putExtra("from_id",from_id);
	resultIntent.putExtra("default",defaultpar);
	resultPendingIntent = PendingIntent.getActivity(this, 0,
			resultIntent, PendingIntent.FLAG_ONE_SHOT);
//
	NotificationCompat.Builder mNotifyBuilder;
	NotificationManager mNotificationManager;

	mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

	mNotifyBuilder = new NotificationCompat.Builder(this)
			.setSmallIcon(R.drawable.logo_secure).setContentText("You have succesfully receive a key").setContentTitle("Securemobileapp");
	// Set pending intent
	mNotifyBuilder.setContentIntent(resultPendingIntent);
	int defaults = 0;
	defaults = defaults | Notification.DEFAULT_LIGHTS;
	defaults = defaults | Notification.DEFAULT_VIBRATE;
	defaults = defaults | Notification.DEFAULT_SOUND;

	mNotifyBuilder.setDefaults(defaults);
	// Set the content for Notification
	// mNotifyBuilder.setContentText(msg);
	// Set autocancel
	mNotifyBuilder.setAutoCancel(true);
	// Post a notification
	mNotificationManager.notify(notifyID, mNotifyBuilder.build());

}else if(secret_key.equals("key_response")){
	resultIntent = new Intent(this, MainMenu.class);
	resultPendingIntent = PendingIntent.getActivity(this, 0,
			resultIntent, PendingIntent.FLAG_ONE_SHOT);

	NotificationCompat.Builder mNotifyBuilder;
	NotificationManager mNotificationManager;

	mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

	mNotifyBuilder = new NotificationCompat.Builder(this)
			.setSmallIcon(R.drawable.logo_secure).setContentText(msg).setContentTitle("Securemobileapp");
	// Set pending intent
	mNotifyBuilder.setContentIntent(resultPendingIntent);
	int defaults = 0;
	defaults = defaults | Notification.DEFAULT_LIGHTS;
	defaults = defaults | Notification.DEFAULT_VIBRATE;
	defaults = defaults | Notification.DEFAULT_SOUND;

	mNotifyBuilder.setDefaults(defaults);
	// Set the content for Notification
	// mNotifyBuilder.setContentText(msg);
	// Set autocancel
	mNotifyBuilder.setAutoCancel(true);
	// Post a notification
	mNotificationManager.notify(notifyID, mNotifyBuilder.build());

}
	else if(secret_key.equals("invitation")){
		resultIntent = new Intent(getApplicationContext(), AcceptDecline.class);
	resultIntent.putExtra("message",msg);
	resultIntent.putExtra("secret_key",secret_key);
	resultIntent.putExtra("phone",phone);
	resultIntent.putExtra("phone_user",phone_user);
	resultIntent.putExtra("from_id",from_id);
	resultIntent.putExtra("default","inviteAccept");
	resultIntent.putExtra("request_id",request_id);
		resultPendingIntent = PendingIntent.getActivity(this, 0,
				resultIntent, PendingIntent.FLAG_ONE_SHOT);

		NotificationCompat.Builder mNotifyBuilder;
		NotificationManager mNotificationManager;

		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		mNotifyBuilder = new NotificationCompat.Builder(this)
				.setSmallIcon(R.drawable.logo_secure).setContentText(msg).setContentTitle("Securemobileapp");
		// Set pending intent
		mNotifyBuilder.setContentIntent(resultPendingIntent);
		int defaults = 0;
		defaults = defaults | Notification.DEFAULT_LIGHTS;
		defaults = defaults | Notification.DEFAULT_VIBRATE;
		defaults = defaults | Notification.DEFAULT_SOUND;

		mNotifyBuilder.setDefaults(defaults);
		// Set the content for Notification
		// mNotifyBuilder.setContentText(msg);
		// Set autocancel
		mNotifyBuilder.setAutoCancel(true);
		// Post a notification
		mNotificationManager.notify(notifyID, mNotifyBuilder.build());

	}
else {

	resultIntent = new Intent(this, ChatActivity.class);
	resultIntent.putExtra("msg", msg);
	resultIntent.putExtra("chat_id", chatid);
	resultIntent.putExtra("id", uid);
	resultIntent.putExtra("nickname", nickname);
	resultIntent.putExtra("username", name);
	resultIntent.putExtra("msg_id", msg_id);
	resultIntent.putExtra("firstname", firstname);
	resultIntent.putExtra("lastname", lastname);
	resultIntent.putExtra("phone_number",phone);
	resultPendingIntent = PendingIntent.getActivity(this, 0,
			resultIntent, PendingIntent.FLAG_ONE_SHOT);
//
	NotificationCompat.Builder mNotifyBuilder;
	NotificationManager mNotificationManager;

	mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

	mNotifyBuilder = new NotificationCompat.Builder(this)
			.setSmallIcon(R.drawable.logo_secure).setContentText(msg).setContentTitle("Securemobileapp");
	// Set pending intent
	mNotifyBuilder.setContentIntent(resultPendingIntent);

	// Set Vibrate, Sound and Light
	int defaults = 0;
	defaults = defaults | Notification.DEFAULT_LIGHTS;
	defaults = defaults | Notification.DEFAULT_VIBRATE;
	defaults = defaults | Notification.DEFAULT_SOUND;

	mNotifyBuilder.setDefaults(defaults);
	// Set the content for Notification
	// mNotifyBuilder.setContentText(msg);
	// Set autocancel
	mNotifyBuilder.setAutoCancel(true);
	// Post a notification
	mNotificationManager.notify(notifyID, mNotifyBuilder.build());
}
	}


	private Object getFragmentManager() {
		// TODO Auto-generated method stub
		return null;
	}
}